﻿using InventoryApp.Custom.Controls;
using InventoryApp.UWP.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.UWP;

[assembly: ExportRenderer(typeof(SelectableLabel), typeof(SelectableLabelRenderer))]
namespace InventoryApp.UWP.Renderers
{
    public class SelectableLabelRenderer : LabelRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Label> e)
        {
            base.OnElementChanged(e);

            if (Control != null)
            {
                Control.IsTextSelectionEnabled = true;
            }
        }
    }
}
