﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Devices.Bluetooth;

namespace Zebra.Sdk.Printer.Discovery
{
    public interface DeviceFilter
    {
        bool ShouldAddPrinter(BluetoothDevice device);
    }
}
