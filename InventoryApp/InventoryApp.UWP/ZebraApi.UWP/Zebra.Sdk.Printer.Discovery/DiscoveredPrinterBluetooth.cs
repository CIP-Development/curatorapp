﻿using System;
using System.Collections.Generic;
using Zebra.Sdk.Comm;
using Zebra.Sdk.Comm.Internal;

namespace Zebra.Sdk.Printer.Discovery
{
    /// <summary>
    /// Instance of DiscoveredPrinter that is returned when performing a Bluetooth® discovery.
    /// </summary>
    public class DiscoveredPrinterBluetooth : DiscoveredPrinter
    {
        /// <summary>
        /// The friendly name of the Bluetooth® device.
        /// </summary>
        public readonly string friendlyName;

        /// <summary>
        /// Returns an instance of a DiscoveredPrinterBluetooth with <c>macAddress</c>.
        /// </summary>
        /// <param name="macAddress">MAC address of the printer.</param>
        /// <param name="friendlyName">Friendly name of the printer.</param>
        public DiscoveredPrinterBluetooth(string macAddress, string friendlyName) : base(macAddress)
        {
            if (!(new BluetoothHelper()).IsWindows10())
            {
                throw new ConnectionException("Operating system not supported for Bluetooth discovery");
            }
            this.friendlyName = friendlyName;
            this.discoSettings.Add("MAC_ADDRESS", base.Address);
            this.discoSettings.Add("FRIENDLY_NAME", friendlyName);
        }

        /// <inheritdoc />
        public override Connection GetConnection()
        {
            return new BluetoothConnection(base.Address);
        }
    }
}
