﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Zebra.Sdk.Comm.Internal;
using Zebra.Sdk.Util.Internal;

namespace Zebra.Sdk.Comm
{
    /// <summary>
    /// Establishes a Bluetooth® connection to a printer. (Windows 10 only)
    /// </summary>
    /// <remarks>
    /// In order to connect to a device with Bluetooth®, the device must be discoverable, authentication must be enabled,
    /// and the authentication pin must be set (1-16 alphanumeric characters).<br />
    /// The encryption type and whether or not it is used is determined by the device initiating the connection and not
    /// the device being connected to.
    /// </remarks>
    /// <example><code source="../SdkApi_Test/Test/Zebra/Sdk/Comm/Examples/BluetoothConnectionExample.cs" /></example>
    public class BluetoothConnection : ConnectionA
    {
        private const int MAX_TIMEOUT_FOR_READ = 10000;

        /// <summary>
        /// For internal use of the Zebra Printer API only.
        /// </summary>
        protected string friendlyName = "";

        /// <summary>
        /// For internal use of the Zebra Printer API only.
        /// </summary>
        protected string macAddress;

        internal BluetoothStreamReader streamReader;

        private const int PRE_CLOSE_DELAY = 5000;

        private const int POST_CLOSE_DELAY = 100;

        private static string ConnectionBuilderPrefix
        {
            get
            {
                return "BT";
            }
        }

        /// <summary>
        /// Gets the friendly name of the Bluetooth® connection. The friendly name is obtained from the device when
        /// this connection is opened. If the friendly name changes on the device, it will not be refreshed until the
        /// connection is closed and reopened.
        /// </summary>
        public string FriendlyName
        {
            get
            {
                return this.friendlyName;
            }
        }

        /// <summary>
        /// Gets the MAC address which was passed into the constructor. The MAC address is a hexadecimal string with
        /// separators between the octets (e.g. 00:11:BB:DD:55:FF).
        /// </summary>
        public string MACAddress
        {
            get
            {
                return this.macAddress;
            }
        }

        /// <summary>
        /// Gets the MAC address and the friendly name as the description.
        /// </summary>
        /// <returns>[MAC Address]:[Friendly Name]</returns>
        /// <see cref="P:Zebra.Sdk.Comm.Connection.SimpleConnectionName" /> 
        public override string SimpleConnectionName
        {
            get
            {
                return string.Concat(this.MACAddress, ":", this.FriendlyName);
            }
        }

        /// <summary>
        /// For internal use of the Zebra Printer API only.
        /// </summary>
        protected BluetoothConnection()
        {
        }

        /// <summary>
        /// For internal use of the Zebra Printer API only.
        /// </summary>
        /// <param name="connectionInfo"></param>
        /// <exception cref="T:Zebra.Sdk.Comm.ConnectionException">Printer connection error or unsupported operating system.</exception>
        /// <exception cref="T:Zebra.Sdk.Comm.Internal.NotMyConnectionDataException">For internal use of the Zebra Printer API only.</exception>
        protected BluetoothConnection(ConnectionInfo connectionInfo)
        {
            string connectionInfostring = connectionInfo.GetMyData();
            string macAddressColonsRegex = string.Concat("^\\s*(", BluetoothConnection.ConnectionBuilderPrefix, ":)?([a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2}:[a-fA-F0-9]{2})\\s*$");
            List<string> matches = RegexUtil.GetMatches(macAddressColonsRegex, connectionInfostring);
            if (matches.Count < 3)
            {
                matches = RegexUtil.GetMatches("(.*)mB=(.*?)&(.*)", connectionInfostring);
                if (!matches.Any<string>())
                {
                    string friendlyNameRegex = string.Concat("^\\s*(", BluetoothConnection.ConnectionBuilderPrefix, ":)?([^:]+)\\s*$");
                    matches = RegexUtil.GetMatches(friendlyNameRegex, connectionInfostring);
                    if (!matches.Any<string>())
                    {
                        throw new NotMyConnectionDataException(string.Concat("BT Connection doesn't understand ", connectionInfostring));
                    }
                }
            }
            BluetoothHelper bluetoothHelper = new BluetoothHelper();
            if (!bluetoothHelper.IsWindows10())
            {
                throw new ConnectionException("Operating system not supported for Bluetooth connections");
            }
            string address = bluetoothHelper.FormatMacAddress(matches[2]);
            this.zebraConnector = new BluetoothZebraConnectorImpl(address, ConnectionChannel.PRINTING_CHANNEL);
            this.macAddress = address;
            this.maxTimeoutForRead = 10000;
            this.timeToWaitForMoreData = 500;//this.timeToWaitForMoreData = ConnectionA.DEFAULT_TIME_TO_WAIT_FOR_MORE_DATA;
        }

        /// <summary>
        /// Constructs a new Bluetooth® connection with the given <c>macAddress</c>.
        /// </summary>
        /// <remarks>
        /// The MAC address is a hexadecimal string with or without separators between the octets (e.g. 00:11:BB:DD:55:FF or 0011BBDD55FF). 
        /// This constructor will use the default timeouts for <see cref="M:Zebra.Sdk.Comm.Connection.Read" />. The default timeout is a maximum of 10
        /// seconds for any data to be received. If no more data is available after 500 milliseconds the read operation is assumed to be 
        /// complete.<br />
        /// To specify timeouts other than the defaults, use:<br />
        /// <see cref="M:Zebra.Sdk.Comm.BluetoothConnection.#ctor(System.String,System.Int32,System.Int32)" />
        /// </remarks>
        /// <param name="macAddress">The device's MAC address.</param>
        /// <exception cref="T:Zebra.Sdk.Comm.ConnectionException">Unsupported operating system.</exception>
        public BluetoothConnection(string macAddress) : this(macAddress, 10000, 500)//this(macAddress, 10000, ConnectionA.DEFAULT_TIME_TO_WAIT_FOR_MORE_DATA)
        {
        }

        /// <summary>
        /// Constructs a new Bluetooth® connection with the given<c>macAddress</c> and timeout values.
        /// </summary>
        /// <remarks>
        /// The MAC address is a hexadecimal string with or without separators between the octets (e.g. 00:11:BB:DD:55:FF or 0011BBDD55FF).  
        /// This constructor will use the specified timeouts for <see cref="M:Zebra.Sdk.Comm.Connection.Read" />. The timeout is a maximum of 
        /// <c>maxTimeoutForRead</c> milliseconds for any data to be received. If no more data is available after <c>timeToWaitForMoreData</c> 
        /// milliseconds the read operation is assumed to be complete.
        /// </remarks>
        /// <param name="macAddress">The device's MAC address.</param>
        /// <param name="maxTimeoutForRead">The maximum time, in milliseconds, to wait for any data to be received.</param>
        /// <param name="timeToWaitForMoreData">The maximum time, in milliseconds, to wait in-between reads after the initial read.</param>
        /// <exception cref="T:Zebra.Sdk.Comm.ConnectionException">Unsupported operating system.</exception>
        public BluetoothConnection(string macAddress, int maxTimeoutForRead, int timeToWaitForMoreData) : this(new BluetoothZebraConnectorImpl((new BluetoothHelper()).FormatMacAddress(macAddress), ConnectionChannel.PRINTING_CHANNEL), (new BluetoothHelper()).FormatMacAddress(macAddress), maxTimeoutForRead, timeToWaitForMoreData)
        {
        }

        /// <summary>
        /// Exposed this protected constructor for testing. We can pass in a mock Connector rather than the BT specific one.
        /// The MAC address is a hexadecimal string with no separators between the octets (e.g. 0011BBDD55FF).
        /// </summary>
        /// <param name="zebraConnector"></param>
        /// <param name="macAddress"></param>
        /// <param name="maxTimeoutForRead"></param>
        /// <param name="timeToWaitForMoreData"></param>
        /// <exception cref="T:Zebra.Sdk.Comm.ConnectionException">Unsupported operating system.</exception>
        protected BluetoothConnection(ZebraConnector zebraConnector, string macAddress, int maxTimeoutForRead, int timeToWaitForMoreData)
        {
            if (!(new BluetoothHelper()).IsWindows10())
            {
                throw new ConnectionException("Operating system not supported for Bluetooth connections");
            }
            this.zebraConnector = zebraConnector;
            this.macAddress = macAddress;
            this.maxTimeoutForRead = maxTimeoutForRead;
            this.timeToWaitForMoreData = timeToWaitForMoreData;
        }

        /// <inheritdoc />
        public override int BytesAvailable()
        {
            if (!this.Connected)
            {
                return -1;
            }
            return this.streamReader.BytesAvailable();
        }

        /// <summary>
        /// Closes the Bluetooth® connection.
        /// </summary>
        /// <see cref="M:Zebra.Sdk.Comm.Connection.Close" /> 
        public override void Close()
        {
            if (this.Connected)
            {
                Sleeper.Sleep((long)5000);
            }
            this.friendlyName = "";
            base.Close();
            if (this.streamReader != null)
            {
                this.streamReader.Close();
            }
            Sleeper.Sleep((long)100);
        }

        /// <inheritdoc />
        public override ConnectionReestablisher GetConnectionReestablisher(long thresholdTime)
        {
            return new BluetoothConnectionReestablisher(this, thresholdTime);
        }

        /// <summary>
        /// Opens a Bluetooth® connection as specified in the constructor.
        /// </summary>
        /// <see cref="M:Zebra.Sdk.Comm.ConnectionA.Open" /> 
        public override void Open()
        {
            base.Open();
            this.friendlyName = BluetoothDeviceHelper.GetFriendlyNameFromDevice(this.macAddress);
            this.streamReader = new BluetoothStreamReader(this.inputStream.BaseStream);
        }

        /// <inheritdoc />
        public override byte[] Read(int maxBytesToRead)
        {
            if (!this.Connected)
            {
                return null;
            }
            return this.streamReader.Read(maxBytesToRead);
        }

        /// <inheritdoc />
        public override int ReadChar()
        {
            byte[] data = this.Read(1);
            if (data == null || data.Length == 0)
            {
                return -1;
            }
            return data[0];
        }

        /// <summary>
        /// The friendly name is obtained from the device when this connection is opened. If the friendly name changes on the
        /// device, it will not be refreshed until the connection is closed and reopened.
        /// </summary>
        /// <returns><c>Bluetooth</c>:[MAC Address]:[Friendly Name].</returns>
        /// <see cref="M:Zebra.Sdk.Comm.Connection.ToString" />
        public override string ToString()
        {
            return string.Concat("Bluetooth:", this.MACAddress, ":", this.FriendlyName);
        }
    }
}
