﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;
using Windows.Devices.Radios;
using Windows.Foundation;

namespace Zebra.Sdk.Comm.Internal
{
    public class BluetoothRadioHelper
    {
        private const int BLUETOOTH_MAX_NAME_SIZE = 248;
        public BluetoothRadioHelper()
        {
        }
        [DllImport("irprops.cpl", CharSet = CharSet.None, ExactSpelling = false, SetLastError = true)]
        private static extern IntPtr BluetoothFindFirstRadio(ref BluetoothRadioHelper.BLUETOOTH_FIND_RADIO_PARAM findRadioParam, out IntPtr radioHandle);

        [DllImport("irprops.cpl", CharSet = CharSet.None, ExactSpelling = false, SetLastError = true)]
        private static extern bool BluetoothFindRadioClose(IntPtr radioFindHande);

        [DllImport("irprops.cpl", CharSet = CharSet.None, ExactSpelling = false, SetLastError = true)]
        private static extern uint BluetoothGetRadioInfo(IntPtr radioPtr, ref BluetoothRadioHelper.BLUETOOTH_RADIO_INFO radioInfo);

        [DllImport("irprops.cpl", CharSet = CharSet.None, ExactSpelling = false, SetLastError = true)]
        private static extern bool BluetoothIsConnectable(IntPtr radioPtr);

        [DllImport("irprops.cpl", CharSet = CharSet.None, ExactSpelling = false, SetLastError = true)]
        private static extern bool BluetoothIsDiscoverable(IntPtr radioPtr);

        [DllImport("Kernel32.dll", CharSet = CharSet.None, ExactSpelling = false, SetLastError = true)]
        private static extern bool CloseHandle(IntPtr handle);

        private static DeviceInformationCollection FindAllRadios()
        {
            IAsyncOperation<DeviceInformationCollection> devCol = DeviceInformation.FindAllAsync(Radio.GetDeviceSelector());
            devCol.AsTask<DeviceInformationCollection>().Wait();
            return devCol.GetResults();
        }
        private static DeviceInformation GetBtRadioDeviceInfo(DeviceInformationCollection radioDevInfoCol)
        {
            DeviceInformation btRadioInfo = null;
            IEnumerator<DeviceInformation> devInfo = ((IEnumerable<DeviceInformation>)radioDevInfoCol).GetEnumerator();
            while (devInfo.MoveNext())
            {
                if (!devInfo.Current.Id.ToLower().Contains("bluetooth"))
                {
                    continue;
                }
                btRadioInfo = devInfo.Current;
                break;
            }
            return btRadioInfo;
        }
        public static async Task<DeviceInformation> GetRadioInfo()
        {
            DeviceInformation btRadioDeviceInfo = null;
            var radioAccessStatus = await Radio.RequestAccessAsync();
            if (RadioAccessStatus.Allowed == radioAccessStatus) //if (1 == await Radio.RequestAccessAsync())
            {
                DeviceInformationCollection deviceInformationCollection = BluetoothRadioHelper.FindAllRadios();
                if (deviceInformationCollection != null && ((IReadOnlyCollection<DeviceInformation>)deviceInformationCollection).Count > 0)
                {
                    btRadioDeviceInfo = BluetoothRadioHelper.GetBtRadioDeviceInfo(deviceInformationCollection);
                }
            }
            else
                throw new Exception($"RadioAccessStatus: {radioAccessStatus}");
            return btRadioDeviceInfo;
        }
        public static bool IsConnectable()
        {
            bool isConnectable = false;
            IntPtr firstRadio = IntPtr.Zero;
            IntPtr nextRadio = IntPtr.Zero;
            try
            {
                try
                {
                    BluetoothRadioHelper.BLUETOOTH_FIND_RADIO_PARAM bLUETOOTHFINDRADIOPARAM = new BluetoothRadioHelper.BLUETOOTH_FIND_RADIO_PARAM()
                    {
                        dwSize = (uint)Marshal.SizeOf(typeof(BluetoothRadioHelper.BLUETOOTH_FIND_RADIO_PARAM))
                    };
                    BluetoothRadioHelper.BLUETOOTH_FIND_RADIO_PARAM parameters = bLUETOOTHFINDRADIOPARAM;
                    nextRadio = BluetoothRadioHelper.BluetoothFindFirstRadio(ref parameters, out firstRadio);
                    if (firstRadio != IntPtr.Zero)
                    {
                        BluetoothRadioHelper.BLUETOOTH_RADIO_INFO bLUETOOTHRADIOINFO = new BluetoothRadioHelper.BLUETOOTH_RADIO_INFO()
                        {
                            dwSize = (uint)Marshal.SizeOf(typeof(BluetoothRadioHelper.BLUETOOTH_RADIO_INFO))
                        };
                        BluetoothRadioHelper.BLUETOOTH_RADIO_INFO radioInfo = bLUETOOTHRADIOINFO;
                        if (BluetoothRadioHelper.BluetoothGetRadioInfo(firstRadio, ref radioInfo) == 0)
                        {
                            isConnectable = BluetoothRadioHelper.BluetoothIsConnectable(firstRadio);
                        }
                    }
                }
                catch (Exception exception)
                {
                    isConnectable = true;
                }
            }
            finally
            {
                try
                {
                    if (firstRadio != IntPtr.Zero)
                    {
                        BluetoothRadioHelper.CloseHandle(firstRadio);
                    }
                    if (nextRadio != IntPtr.Zero)
                    {
                        BluetoothRadioHelper.BluetoothFindRadioClose(nextRadio);
                    }
                }
                catch
                {
                }
            }
            return isConnectable;
        }
        public static async Task<bool> IsEnabled()
        {
            bool flag;
            bool flag1 = false;
            if (await Radio.RequestAccessAsync() == RadioAccessStatus.Allowed)//if (await Radio.RequestAccessAsync() == 1)
            {
                IReadOnlyList<Radio> radiosAsync = await Radio.GetRadiosAsync();
                Radio radio1 = radiosAsync.FirstOrDefault<Radio>((Radio radio) => radio.Kind == RadioKind.Bluetooth);
                flag = (radio1 == null ? false : radio1.State == RadioState.On);
                flag1 = flag;
            }
            return flag1;
        }
        private struct BLUETOOTH_FIND_RADIO_PARAM
        {
            internal uint dwSize;
        }

        private struct BLUETOOTH_RADIO_INFO
        {
            internal uint dwSize;

            internal ulong address;

            internal string szName;

            internal uint ulClassOfDevice;

            internal ushort lmpSubversion;

            internal ushort manufacturer;
        }
    }
}
