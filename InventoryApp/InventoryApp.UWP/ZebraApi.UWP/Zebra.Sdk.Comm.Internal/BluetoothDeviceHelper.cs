﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Windows.Devices.Bluetooth;
using Windows.Devices.Bluetooth.Rfcomm;
using Windows.Devices.Enumeration;
using Windows.Foundation;
using Windows.Networking;
using Windows.Networking.Sockets;
using Zebra.Sdk.Util.Internal;

namespace Zebra.Sdk.Comm.Internal
{
    internal class BluetoothDeviceHelper
    {
        internal static uint DEVICE_NOT_READY;

        private const int WAIT_FOR_SERVICES_TIMEOUT = 5000;

        private const int WAIT_FOR_DEVICE_TIMEOUT = 5000;

        static BluetoothDeviceHelper()
        {
            BluetoothDeviceHelper.DEVICE_NOT_READY = 2147020577;//BluetoothDeviceHelper.DEVICE_NOT_READY = -2147020577;
        }

        public BluetoothDeviceHelper()
        {
        }

        /// <exception cref="T:Zebra.Sdk.Comm.ConnectionException"></exception>
        private static async Task CheckRadioStatus()
        {
            if (await BluetoothRadioHelper.GetRadioInfo() == null)
            {
                throw new ConnectionException("No bluetooth radio found");
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="btAddress"></param>
        /// <returns></returns>
        /// <exception cref="T:System.AggregateException"></exception>
        /// <exception cref="T:System.ArgumentException"></exception>
        /// <exception cref="T:System.ObjectDisposedException"></exception>
        public static BluetoothDevice GetBluetoothDevice(ulong btAddress)
        {
            IAsyncOperation<BluetoothDevice> deviceAsync = BluetoothDevice.FromBluetoothAddressAsync(btAddress);
            if (!deviceAsync.AsTask<BluetoothDevice>().Wait(5000))
            {
                return null;
            }
            return deviceAsync.GetResults();
        }

        /// <summary>
        /// </summary>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        /// <exception cref="T:System.ArgumentException"></exception>
        /// <exception cref="T:Zebra.Sdk.Comm.ConnectionException"></exception>
        public static string GetFriendlyNameFromDevice(string macAddress)
        {
            string name;
            using (BluetoothDevice btDev = BluetoothDeviceHelper.GetBluetoothDevice((new BluetoothHelper()).ConvertMacAddressToUlong(macAddress)))
            {
                if (btDev == null)
                {
                    throw new ConnectionException("Device not found.");
                }
                name = btDev.Name;
            }
            return name;
        }

        /// <exception cref="T:System.ArgumentException"></exception>
        /// <exception cref="T:System.Text.RegularExpressions.RegexMatchTimeoutException"></exception>
        public static string GetMacAddressFromDevice(BluetoothDevice device)
        {
            string macAddress = "";
            if (device != null)
            {
                macAddress = Regex.Replace(device.HostName.DisplayName, "[()]", "");
            }
            return macAddress;
        }

        /// <summary> 
        /// </summary>
        /// <param name="macAddress"></param>
        /// <param name="channelId"></param>
        /// <param name="protectionLevel"></param>
        /// <returns></returns>
        /// <exception cref="T:Zebra.Sdk.Comm.ConnectionException"></exception>
        /// <exception cref="T:System.ArgumentException"></exception>
        public static async Task<ZebraBluetoothSocket> OpenConnection(string macAddress, Guid channelId, SocketProtectionLevel protectionLevel)
        {
            //await BluetoothDeviceHelper.CheckRadioStatus();
            //DeviceInformation btRadioDeviceInfo

            BluetoothDevice bluetoothDevice = null;
            ZebraBluetoothSocket zebraBluetoothSocket = null;
            ulong num = (new BluetoothHelper()).ConvertMacAddressToUlong(macAddress);
            try
            {
                try
                {
                    bluetoothDevice = BluetoothDeviceHelper.GetBluetoothDevice(num);
                    DeviceInformationPairing pairing = bluetoothDevice.DeviceInformation.Pairing;
                    //DeviceAccessStatus deviceAccessStatu = await bluetoothDevice.RequestAccessAsync();
                    //if (deviceAccessStatu != DeviceAccessStatus.Allowed)
                    //{
                    //    throw new ConnectionException(string.Concat("Access denied: ", deviceAccessStatu.ToString()));
                    //}
                    RfcommDeviceService rfcommDeviceService = null;
                    int num1 = 0;
                    while (true)
                    {
                        IAsyncOperation<RfcommDeviceServicesResult> rfcommServicesAsync = bluetoothDevice.GetRfcommServicesAsync(BluetoothCacheMode.Uncached);
                        if (rfcommServicesAsync.AsTask<RfcommDeviceServicesResult>().Wait(5000) && rfcommServicesAsync.GetResults() != null)
                        {
                            var services = rfcommServicesAsync.GetResults();
                            foreach (RfcommDeviceService rfcommDeviceService1 in services.Services)//foreach (RfcommDeviceService rfcommDeviceService1 in rfcommServicesAsync.GetResults().get_Services())
                            {
                                if (!rfcommDeviceService1.ServiceId.Uuid.Equals(channelId))
                                {
                                    continue;
                                }
                                rfcommDeviceService = rfcommDeviceService1;
                                break;
                            }
                        }
                        if (rfcommDeviceService != null)
                        {
                            break;
                        }
                        int num2 = num1 + 1;
                        num1 = num2;
                        if (num2 >= 2)
                        {
                            break;
                        }
                        Sleeper.Sleep((long)100);
                    }
                    if (rfcommDeviceService == null)
                    {
                        throw new ConnectionException("No services found");
                    }
                    zebraBluetoothSocket = new ZebraBluetoothSocket(rfcommDeviceService, protectionLevel, bluetoothDevice);
                    zebraBluetoothSocket.Connect();
                    bluetoothDevice = null;
                }
                catch (Exception exception)
                {
                    if (exception.HResult == -2147020577) //if (exception.HResult == BluetoothDeviceHelper.DEVICE_NOT_READY)
                    {
                        throw new ConnectionException("Bluetooth radio is currently disabled");
                    }
                    throw;
                }
            }
            finally
            {
                if (bluetoothDevice != null)
                {
                    bluetoothDevice.Dispose();
                }
            }
            return zebraBluetoothSocket;
        }
    }
}
