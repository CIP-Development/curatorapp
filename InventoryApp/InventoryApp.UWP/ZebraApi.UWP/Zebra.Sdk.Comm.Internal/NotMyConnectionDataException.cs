﻿using System;

namespace Zebra.Sdk.Comm.Internal
{
    /// <summary>
    /// For internal use of the Zebra Printer API only.
    /// </summary>
    internal class NotMyConnectionDataException : Exception
    {
        /// <summary>
        /// For internal use of the Zebra Printer API only.
        /// </summary>
        /// <param name="message">the error message</param>
        public NotMyConnectionDataException(string message) : base(message)
        {
        }
    }
}
