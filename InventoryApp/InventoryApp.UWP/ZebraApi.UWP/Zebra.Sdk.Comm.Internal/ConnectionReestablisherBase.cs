﻿using System;
using Zebra.Sdk.Comm;
using Zebra.Sdk.Printer;
using Zebra.Sdk.Util.Internal;

namespace Zebra.Sdk.Comm.Internal
{
    internal abstract class ConnectionReestablisherBase : ConnectionReestablisher
    {
        protected Connection zebraPrinterConnection;

        protected long thresholdTime;

        protected long startTime;

        internal Action<PrinterReconnectionHandler> reestablishConnection;

        protected ConnectionReestablisherBase(Connection c, long thresholdTime)
        {
            ConnectionReestablisherBase connectionReestablisherBase = this;
            this.reestablishConnection = new Action<PrinterReconnectionHandler>(connectionReestablisherBase.ReestablishConnection);
            this.zebraPrinterConnection = c;
            this.thresholdTime = thresholdTime;
        }

        public abstract void ReestablishConnection(PrinterReconnectionHandler handler);

        /// <summary>
        /// </summary>
        /// <exception cref="T:System.TimeoutException"></exception>
        protected void TimeoutCheck()
        {
            if ((long)Math.Abs(Environment.TickCount) > this.startTime + this.thresholdTime)
            {
                throw new TimeoutException(String.Format("Task timed out waiting for '{0}' to come back online", this.zebraPrinterConnection));
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="connection"></param>
        /// <returns></returns>
        /// <exception cref="T:System.TimeoutException"></exception>
        protected string WaitForPrinterToComeOnlineViaSgdAndGetFwVer(Connection connection)
        {
            string str;
            this.startTime = (long)Math.Abs(Environment.TickCount);
            while (true)
            {
                try
                {
                    connection.Open();
                    string fwVer = SGD.GET("appl.name", connection);
                    if (String.IsNullOrEmpty(fwVer.Trim()))
                    {
                        throw new ConnectionException("Printer is not responding");
                    }
                    str = fwVer;
                    break;
                }
                catch (ConnectionException connectionException1)
                {
                    try
                    {
                        connection.Close();
                    }
                    catch (ConnectionException connectionException)
                    {
                    }
                }
                Sleeper.Sleep((long)2500);
                this.TimeoutCheck();
            }
            return str;
        }
    }
}
