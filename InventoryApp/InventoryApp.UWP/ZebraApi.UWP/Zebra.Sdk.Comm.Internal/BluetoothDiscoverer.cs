﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.Devices.Bluetooth;
using Windows.Devices.Enumeration;
using Windows.Foundation;
using Zebra.Sdk.Comm;
using Zebra.Sdk.Comm.Internal;
using Zebra.Sdk.Util.Internal;

namespace Zebra.Sdk.Printer.Discovery
{
    public class BluetoothDiscoverer
    {
        private DiscoveryHandler discoveryHandler;

        private DeviceFilter deviceFilter;

        private const uint BLUETOOTH_PRINTER_CLASS = 1664;
        private BluetoothDiscoverer(DiscoveryHandler discoveryHandler, DeviceFilter deviceFilter)
        {
            this.discoveryHandler = discoveryHandler;
            this.deviceFilter = deviceFilter;
        }
        private void DoBluetoothDisco(int waitForResponsesTimeout)
        {
            List<string> strs = new List<string>();
            string btDeviceSelector = BluetoothDevice.GetDeviceSelectorFromClassOfDevice(BluetoothClassOfDevice.FromRawValue(1664));
            DeviceWatcher deviceWatcher = DeviceInformation.CreateWatcher(btDeviceSelector);
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            DeviceWatcher deviceWatcher1 = deviceWatcher;
            DeviceWatcher deviceWatcher2 = deviceWatcher1;
            DeviceWatcher deviceWatcher3 = deviceWatcher1;
            //WindowsRuntimeMarshal.AddEventHandler<TypedEventHandler<DeviceWatcher, DeviceInformation>>(
            //    new Func<TypedEventHandler<DeviceWatcher, DeviceInformation>, EventRegistrationToken>(deviceWatcher2.add_Added), new Action<EventRegistrationToken>(deviceWatcher3.remove_Added),
            //    new TypedEventHandler<DeviceWatcher, DeviceInformation>((DeviceWatcher devWatcher, DeviceInformation devInfo) => {
            //    IAsyncOperation<BluetoothDevice> btDevice = BluetoothDevice.FromIdAsync(devInfo.Id);
            //    while (btDevice.Status != AsyncStatus.Error && btDevice.Status != AsyncStatus.Completed)
            //    {
            //        Sleeper.Sleep((long)100);
            //    }
            //    try
            //    {
            //        using (BluetoothDevice device = btDevice.GetResults())
            //        {
            //            string address = BluetoothDeviceHelper.GetMacAddressFromDevice(device);
            //            if (!strs.Contains(address) && this.deviceFilter != null && this.deviceFilter.ShouldAddPrinter(device))
            //            {
            //                this.discoveryHandler.FoundPrinter(new DiscoveredPrinterBluetooth(address, device.Name));
            //                strs.Add(address);
            //            }
            //        }
            //    }
            //    catch (Exception exception)
            //    {
            //        if (exception.HResult == BluetoothDeviceHelper.DEVICE_NOT_READY)
            //        {
            //            cancellationTokenSource.Cancel();
            //            deviceWatcher.Stop();
            //            this.discoveryHandler.DiscoveryError("Bluetooth radio is currently disabled");
            //        }
            //    }
            //}));
            //deviceWatcher1 = deviceWatcher;
            //DeviceWatcher deviceWatcher4 = deviceWatcher1;
            //DeviceWatcher deviceWatcher5 = deviceWatcher1;
            //WindowsRuntimeMarshal.AddEventHandler<TypedEventHandler<DeviceWatcher, object>>(
            //    new Func<TypedEventHandler<DeviceWatcher, object>, EventRegistrationToken> ( deviceWatcher4.add_EnumerationCompleted),
            //    new Action<EventRegistrationToken>(deviceWatcher5.remove_EnumerationCompleted),
            //    new TypedEventHandler<DeviceWatcher, object>(
            //        (DeviceWatcher devWatcher, object objArgs) => {
            //            this.discoveryHandler.DiscoveryFinished();
            //            cancellationTokenSource.Cancel();
            //        })
            //    );
            Task.Delay(waitForResponsesTimeout).ContinueWith((Task t) => deviceWatcher.Stop(), cancellationTokenSource.Token);
            deviceWatcher.Start();
        }
        public static void FindPrinters(DiscoveryHandler discoveryHandler)
        {
            BluetoothDiscoverer.FindPrinters(discoveryHandler, new BluetoothDiscoverer.DeviceFilterImpl(), -1);
        }
        public static void FindPrinters(DiscoveryHandler discoveryHandler, DeviceFilter deviceFilter, int waitForResponsesTimeout)
        {
            if (!(new BluetoothHelper()).IsWindows10())
            {
                throw new ConnectionException("Operating system not supported for Bluetooth discovery");
            }
            BluetoothDiscoverer.GetBluetoothPrinters(discoveryHandler, deviceFilter, waitForResponsesTimeout);
        }
        private static async void GetBluetoothPrinters(DiscoveryHandler discoveryHandler, DeviceFilter deviceFilter, int waitForResponsesTimeout)
        {
            if (await BluetoothRadioHelper.GetRadioInfo() != null)
            {
                (new BluetoothDiscoverer(discoveryHandler, deviceFilter)).DoBluetoothDisco(waitForResponsesTimeout);
            }
            else
            {
                discoveryHandler.DiscoveryError("No bluetooth radio found");
            }
        }

        private class DeviceFilterImpl : DeviceFilter
        {
            public DeviceFilterImpl()
            {
            }

            public bool ShouldAddPrinter(BluetoothDevice device)
            {
                return true;
            }
        }
    }
}
