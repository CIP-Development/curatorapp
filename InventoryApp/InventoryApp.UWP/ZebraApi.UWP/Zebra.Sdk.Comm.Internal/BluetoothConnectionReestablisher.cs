﻿using System;
using Zebra.Sdk.Comm;
using Zebra.Sdk.Printer;

namespace Zebra.Sdk.Comm.Internal
{
    internal class BluetoothConnectionReestablisher : ConnectionReestablisherBase, ConnectionReestablisher
    {
        public BluetoothConnectionReestablisher(Connection c, long thresholdTime) : base(c, thresholdTime)
        {
        }

        /// <inheritdoc />
        public override void ReestablishConnection(PrinterReconnectionHandler handler)
        {
            BluetoothConnection originalBtConnection = (BluetoothConnection)this.zebraPrinterConnection;
            string origMacAddress = originalBtConnection.MACAddress;
            Connection newConnection = new BluetoothConnection(origMacAddress, originalBtConnection.MaxTimeoutForRead, originalBtConnection.TimeToWaitForMoreData);
            string fwVer = base.WaitForPrinterToComeOnlineViaSgdAndGetFwVer(newConnection);
            ZebraPrinterLinkOs printer = ZebraPrinterFactory.CreateLinkOsPrinter(ZebraPrinterFactory.GetInstance(newConnection));
            handler.PrinterOnline(printer, fwVer);
        }
    }
}
