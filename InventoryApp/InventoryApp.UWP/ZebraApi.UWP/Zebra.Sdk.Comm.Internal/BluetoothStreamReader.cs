﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Windows.Storage.Streams;
using Zebra.Sdk.Util.Internal;

namespace Zebra.Sdk.Comm.Internal
{
    public class BluetoothStreamReader : IDisposable
    {
        private const int STREAM_BUFFER_SIZE = 16384;

        internal object readLock = new object();

        private Task readTask;

        private CancellationTokenSource cts = new CancellationTokenSource();

        private readonly Stream inputStream;

        protected MemoryStream dataBufferStream;

        private byte[] temporaryReadBuffer;

        private bool disposedValue;

        public BluetoothStreamReader(Stream inputStream)
        {
            this.inputStream = inputStream;
            this.dataBufferStream = new MemoryStream();
            this.readTask = Task.Run(async () => await this.ReadDataTask(), this.cts.Token);
        }

        internal int BytesAvailable()
        {
            int num;
            lock (this.readLock)
            {
                int bytesAvailable = 0;
                try
                {
                    if (this.temporaryReadBuffer == null)
                    {
                        this.temporaryReadBuffer = this.dataBufferStream.ToArray();
                        this.ResetDataBuffer();
                        if (this.temporaryReadBuffer == null)
                        {
                            bytesAvailable = 0;
                        }
                        else if ((int)this.temporaryReadBuffer.Length != 1 || this.temporaryReadBuffer[0] != 0)
                        {
                            bytesAvailable = (int)this.temporaryReadBuffer.Length;
                        }
                        else
                        {
                            bytesAvailable = 0;
                            this.temporaryReadBuffer = null;
                        }
                    }
                    else
                    {
                        bytesAvailable = (int)this.temporaryReadBuffer.Length;
                        if (bytesAvailable == 0)
                        {
                            this.temporaryReadBuffer = this.dataBufferStream.ToArray();
                            this.ResetDataBuffer();
                            bytesAvailable = (int)this.temporaryReadBuffer.Length;
                        }
                    }
                }
                catch (Exception exception)
                {
                }
                num = bytesAvailable;
            }
            return num;
        }

        internal void Close()
        {
            this.WaitForReadTask();
            if (this.dataBufferStream != null)
            {
                this.dataBufferStream.Dispose();
                this.dataBufferStream = null;
            }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposedValue)
            {
                if (disposing)
                {
                    this.Close();
                }
                this.disposedValue = true;
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
        }

        internal byte[] Read(int maxBytesToRead)
        {
            byte[] tmpBuffer = null;
            this.BytesAvailable();
            if (this.temporaryReadBuffer != null)
            {
                if (maxBytesToRead < 0 || maxBytesToRead >= (int)this.temporaryReadBuffer.Length)
                {
                    tmpBuffer = new byte[(int)this.temporaryReadBuffer.Length];
                    Array.Copy(this.temporaryReadBuffer, 0, tmpBuffer, 0, (int)this.temporaryReadBuffer.Length);
                    this.temporaryReadBuffer = null;
                }
                else
                {
                    tmpBuffer = new byte[maxBytesToRead];
                    Array.Copy(this.temporaryReadBuffer, 0, tmpBuffer, 0, maxBytesToRead);
                    int remainingLength = (int)this.temporaryReadBuffer.Length - maxBytesToRead;
                    byte[] tmpBuf2 = new byte[remainingLength];
                    Array.Copy(this.temporaryReadBuffer, maxBytesToRead, tmpBuf2, 0, remainingLength);
                    this.temporaryReadBuffer = tmpBuf2;
                }
            }
            return tmpBuffer;
        }
        //cvelasquez
        internal async Task ReadDataTask()
        {
            //BluetoothStreamReader.<ReadDataTask>d__10 variable = new BluetoothStreamReader.<ReadDataTask>d__10();
            //variable.<>4__this = this;
            //variable.<>t__builder = AsyncTaskMethodBuilder.Create();
            //variable.<>1__state = -1;
            //variable.<>t__builder.Start<BluetoothStreamReader.<ReadDataTask>d__10>(ref variable);
            //return variable.<>t__builder.Task;
        }

        internal virtual void ResetDataBuffer()
        {
            try
            {
                this.dataBufferStream.SetLength((long)0);
                this.dataBufferStream.Position = (long)0;
            }
            catch
            {
            }
        }

        private void WaitForReadTask()
        {
            if (this.readTask != null)
            {
                if (!this.readTask.Wait(100))
                {
                    try
                    {
                        this.cts.Cancel();
                        while (this.readTask.Status != TaskStatus.Faulted && this.readTask.Status != TaskStatus.Canceled)
                        {
                            Sleeper.Sleep((long)10);
                        }
                    }
                    catch
                    {
                    }
                }
                try
                {
                    this.readTask.Dispose();
                }
                catch (InvalidOperationException invalidOperationException)
                {
                }
                this.readTask = null;
            }
        }
    }
}
