﻿using ImTools;
using InventoryApp.Enums;
using InventoryApp.EventAggregators;
using InventoryApp.Extensions;
using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using InventoryApp.Services;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace InventoryApp.ViewModels
{
    public class InventoriesPageViewModel : ViewModelBaseWithDialog
    {
        private AddInventoryToListEvent _addInventoryToListEvent;
        private bool _cropTraitsColumnsLoaded;
        #region Properties
        private ObservableCollection<InventoryThumbnail> _inventoryCollection;
        public ObservableCollection<InventoryThumbnail> InventoryCollection
        {
            get { return _inventoryCollection; }
            set { SetProperty(ref _inventoryCollection, value); }
        }

        private ObservableCollection<WrappedSelection<InventoryThumbnail>> _inventoryList;
        public ObservableCollection<WrappedSelection<InventoryThumbnail>> InventoryList
        {
            get { return _inventoryList; }
            set { SetProperty(ref _inventoryList, value); }
        }

        private WrappedSelection<InventoryThumbnail> _selectedInventory;
        public WrappedSelection<InventoryThumbnail> SelectedInventory
        {
            get { return _selectedInventory; }
            set { SetProperty(ref _selectedInventory, value); }
        }
        private ObservableCollection<object> _selectedInventoryList;
        public ObservableCollection<object> SelectedInventoryList
        {
            get { return _selectedInventoryList; }
            set { SetProperty(ref _selectedInventoryList, value); }
        }

        private int _accessionCount;
        public int AccessionCount
        {
            get { return _accessionCount; }
            set { SetProperty(ref _accessionCount, value); }
        }

        private bool _isAllSelected;
        public bool IsAllSelected
        {
            get { return _isAllSelected; }
            set
            {
                foreach (var wi in _inventoryList)
                {
                    wi.IsSelected = value;
                }
                if (value)
                    SelectedRowsCount = InventoryList.Count;
                else
                    SelectedRowsCount = 0;
                SetProperty(ref _isAllSelected, value);
            }
        }

        private int _selectedRowsCount;
        public int SelectedRowsCount
        {
            get { return _selectedRowsCount; }
            set { SetProperty(ref _selectedRowsCount, value); }
        }

        private decimal? _totalQuantity;
        public decimal? TotalQuantity
        {
            get { return _totalQuantity; }
            set { SetProperty(ref _totalQuantity, value); }
        }

        private List<CropTrait> _cropTraits = new List<CropTrait>();
        #endregion

        private RestClient _restClient;
        private List<DataviewColumn> _dataviewColumnList;
        private IDataStoreService _dataStoreService;
        #region PropertiesLang
        private string _UxTitle;
        public string UxTitle
        {
            get { return _UxTitle; }
            set { SetProperty(ref _UxTitle, value); }
        }
        private string _header1;
        public string Header1
        {
            get { return _header1; }
            set { SetProperty(ref _header1, value); }
        }
        private string _header2;
        public string Header2
        {
            get { return _header2; }
            set { SetProperty(ref _header2, value); }
        }
        private string _header3;
        public string Header3
        {
            get { return _header3; }
            set { SetProperty(ref _header3, value); }
        }
        private string _header4;
        public string Header4
        {
            get { return _header4; }
            set { SetProperty(ref _header4, value); }
        }
        private string _header5;
        public string Header5
        {
            get { return _header5; }
            set { SetProperty(ref _header5, value); }
        }
        private string _header6;
        public string Header6
        {
            get { return _header6; }
            set { SetProperty(ref _header6, value); }
        }
        private string _header7;
        public string Header7
        {
            get { return _header7; }
            set { SetProperty(ref _header7, value); }
        }
        private string _header8;
        public string Header8
        {
            get { return _header8; }
            set { SetProperty(ref _header8, value); }
        }

        private string _uxToolbarItemNew;
        public string UxToolbarItemNew
        {
            get { return _uxToolbarItemNew; }
            set { SetProperty(ref _uxToolbarItemNew, value); }
        }
        private string _uxToolbarItemScan;
        public string UxToolbarItemScan
        {
            get { return _uxToolbarItemScan; }
            set { SetProperty(ref _uxToolbarItemScan, value); }
        }
        private string _uxToolbarItemSearch;
        public string UxToolbarItemSearch
        {
            get { return _uxToolbarItemSearch; }
            set { SetProperty(ref _uxToolbarItemSearch, value); }
        }
        private string _uxToolbarItemRegisterTransaction;
        public string UxToolbarItemRegisterTransaction
        {
            get { return _uxToolbarItemRegisterTransaction; }
            set { SetProperty(ref _uxToolbarItemRegisterTransaction, value); }
        }
        private string _uxToolbarItemChangeLocation;
        public string UxToolbarItemChangeLocation
        {
            get { return _uxToolbarItemChangeLocation; }
            set { SetProperty(ref _uxToolbarItemChangeLocation, value); }
        }
        private string _uxToolbarItemEdit;
        public string UxToolbarItemEdit
        {
            get { return _uxToolbarItemEdit; }
            set { SetProperty(ref _uxToolbarItemEdit, value); }
        }
        private string _uxToolbarItemPrint;
        public string UxToolbarItemPrint
        {
            get { return _uxToolbarItemPrint; }
            set { SetProperty(ref _uxToolbarItemPrint, value); }
        }
        private string _uxToolbarItemViewInventoryActions;
        public string UxToolbarItemViewInventoryActions
        {
            get { return _uxToolbarItemViewInventoryActions; }
            set { SetProperty(ref _uxToolbarItemViewInventoryActions, value); }
        }
        private string _uxToolbarItemRemove;
        public string UxToolbarItemRemove
        {
            get { return _uxToolbarItemRemove; }
            set { SetProperty(ref _uxToolbarItemRemove, value); }
        }
        private string _uxToolbarItemCreateInventoryViability;
        public string UxToolbarItemCreateInventoryViability
        {
            get { return _uxToolbarItemCreateInventoryViability; }
            set { SetProperty(ref _uxToolbarItemCreateInventoryViability, value); }
        }
        private string _uxToolbarItemRefresh;
        public string UxToolbarItemRefresh
        {
            get { return _uxToolbarItemRefresh; }
            set { SetProperty(ref _uxToolbarItemRefresh, value); }
        }
        private string _UxToolbarItemCreatePrintInventory;
        public string UxToolbarItemCreatePrintInventory
        {
            get { return _UxToolbarItemCreatePrintInventory; }
            set { SetProperty(ref _UxToolbarItemCreatePrintInventory, value); }
        }
        #endregion
        public InventoriesPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService, IEventAggregator eventAggregator, IDataStoreService dataStoreService)
            : base(navigationService, pageDialogService)
        {
            Title = "Inventory list";

            _restClient = new RestClient();
            _dataStoreService = dataStoreService;

            if (_dataStoreService.InventoryList != null)
                _inventoryList = _dataStoreService.InventoryList;
            else
                _inventoryList = new ObservableCollection<WrappedSelection<InventoryThumbnail>>();
            SelectedInventoryList = new ObservableCollection<object>();

            SelectItemCommand = new DelegateCommand<Object>(OnSelectItemCommand);
            ItemTappedCommand = new DelegateCommand<Object>(OnItemTappedCommandExecuted);

            NewCommand = new DelegateCommand(OnNewCommandExecuted);
            ScanCommand = new DelegateCommand(OnScanCommand);
            SearchCommand = new DelegateCommand(OnSearchCommandExecuted);

            RegisterTransactionCommand = new DelegateCommand(OnRegisterTransactionCommandExecuted);
            ChangeLocationCommand = new DelegateCommand(OnChangeLocationCommandExecuted);
            EditCommand = new DelegateCommand(OnEditCommand);
            PrintCommand = new DelegateCommand(OnPrintCommandExecuted);
            ViewInventoryActionsCommand = new DelegateCommand(OnViewInventoryActionsCommand);
            RemoveCommand = new DelegateCommand(OnRemoveCommandExecuted);

            CreateInventoryViabilityCommand = new DelegateCommand(OnCreateInventoryViabilityCommand);
            RefreshCommand = new DelegateCommand(OnRefreshCommand);
            SaveListCommand = new DelegateCommand(OnSaveListCommand);
            CreatePrintInventoryCommand = new DelegateCommand(OnCreatePrintInventoryCommand);

            SelectedRowsCount = InventoryList.Count(i => i.IsSelected == true);
            AccessionCount = InventoryList.Select(i => i.Item.accession_id).Distinct().Count();
            TotalQuantity = _inventoryList.Select(i => i.Item.quantity_on_hand).Sum();

            _addInventoryToListEvent = eventAggregator.GetEvent<AddInventoryToListEvent>();
            _addInventoryToListEvent.Subscribe(OnAddItemToList);

            ShowInfoCommand = new DelegateCommand(OnShowInfoCommand);
            EditCropTraitObservationsCommand = new DelegateCommand(OnEditCropTraitObservationsCommand);
            EvaluateCommand = new DelegateCommand(ExecuteEvaluateCommand).ObservesCanExecute(() => IsNotBusy);

            CheckAllCommand = new DelegateCommand(ExecuteCheckAllCommand).ObservesCanExecute(() => IsNotBusy);
            UncheckAllCommand = new DelegateCommand(ExecuteUncheckAllCommand).ObservesCanExecute(() => IsNotBusy);
        }

        #region EventHandlers
        private async void OnAddItemToList(InventoryThumbnail inventory)
        {
            try
            {
                if (_inventoryList.Any(i => i.Item.inventory_id == inventory.inventory_id))
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        PageDialogService.DisplayAlertAsync(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "InventoriesPage", "MessageAddInventoryResults"),
                            _dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "InventoriesPage", "MessageInventoryExistsInList"), "OK");
                    });
                else
                {
                    CheckCropTraitObservations(inventory);
                    await FillCropTraitObservationsAsync(inventory);

                    _inventoryList.Add(new WrappedSelection<InventoryThumbnail>() { IsSelected = false, Item = inventory });
                    AccessionCount = InventoryList.Select(i => i.Item.AccessionNumber).Distinct().Count();
                    TotalQuantity = _inventoryList.Select(i => i.Item.quantity_on_hand).Sum();
                    _dataStoreService.InventoryList = InventoryList;
                }
            }
            catch (Exception ex)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
                });
            }
        }
        #endregion
        public DelegateCommand SaveListCommand { get; }
        private async void OnSaveListCommand()
        {
            try
            {
                await PageDialogService.DisplayAlertAsync("Message", "Under Construction", "OK");
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public DelegateCommand NewCommand { get; }
        private async void OnNewCommandExecuted()
        {
            await NavigationService.NavigateAsync("InventoryPage");
        }
        public DelegateCommand ScanCommand { get; }
        private async void OnScanCommand()
        {
            try
            {
                if (Settings.ScanByCamera)
                {
                    await NavigationService.NavigateAsync("ScanPage");
                }
                else
                {
                    await NavigationService.NavigateAsync("ScanPageByReaderPage");
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public DelegateCommand<Object> SelectItemCommand { get; }
        private async void OnSelectItemCommand(Object param)
        {
            try
            {
                WrappedSelection<InventoryThumbnail> listItem = (WrappedSelection<InventoryThumbnail>)param;
                if (listItem != null)
                {
                    listItem.IsSelected = !listItem.IsSelected;
                }

                if (listItem.IsSelected)
                    SelectedRowsCount++;
                else
                    SelectedRowsCount--;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public DelegateCommand SearchCommand { get; }
        private async void OnSearchCommandExecuted()
        {
            await NavigationService.NavigateAsync("SearchInventoriesPage");
        }
        public DelegateCommand ViewInventoryActionsCommand { get; }
        private async void OnViewInventoryActionsCommand()
        {
            var navigationParams = new NavigationParameters();
            var selection = GetSelection();
            navigationParams.Add("InventoryThumbnailList", selection);
            await NavigationService.NavigateAsync("InventoryActionsPage", navigationParams, useModalNavigation: false, animated: true);
        }

        public DelegateCommand RegisterTransactionCommand { get; }
        private async void OnRegisterTransactionCommandExecuted()
        {
            var navigationParams = new NavigationParameters();
            var selection = GetSelection();
            navigationParams.Add("InventoryThumbnailList", selection);
            await NavigationService.NavigateAsync("MovementPage", navigationParams);
        }
        
        public DelegateCommand EditCommand { get; }
        private async void OnEditCommand()
        {
            try
            {
                var selection = GetSelection();
                if (selection == null) return;
                if (selection.Count == 1)
                {
                    var navResult = await NavigationService.NavigateAsync("InventoryPage", new NavigationParameters
                    {
                        { "InventoryThumbnail", selection[0] }
                    });
                    if (!navResult.Success)
                        throw navResult.Exception;
                }
                else if(selection.Count > 1)
                {
                    throw new Exception("Debe seleccionar un solo inventario");
                }
                else
                {
                    throw new Exception("No se ha seleccionado un inventario");
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public DelegateCommand ChangeLocationCommand { get; }
        private async void OnChangeLocationCommandExecuted()
        {
            try
            {
                var selection = GetSelection();
                var navigationParams = new NavigationParameters
                {
                    { "InventoryThumbnailList", selection }
                };
                await NavigationService.NavigateAsync("ChangeLocationPage", navigationParams);
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public DelegateCommand<Object> ItemTappedCommand { get; }
        private async void OnItemTappedCommandExecuted(Object param)
        {
            try
            {
                var navigationParams = new NavigationParameters
                {
                    { "InventoryThumbnail", (InventoryThumbnail)param }
                };
                await NavigationService.NavigateAsync("InventoryPage", navigationParams);
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public DelegateCommand PrintCommand { get; }
        private async void OnPrintCommandExecuted()
        {
            try
            {
                var selection = GetSelection();
                var navigationParams = new NavigationParameters
                {
                    { "InventoryThumbnailList", selection }
                };
                await NavigationService.NavigateAsync("PrintingPage", navigationParams, useModalNavigation: false, animated: true);
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public DelegateCommand RemoveCommand { get; }
        private void OnRemoveCommandExecuted()
        {
            foreach (var invitroPropagationItem in SelectedInventoryList.ToList())
            {
                _ = InventoryList.Remove((WrappedSelection<InventoryThumbnail>)invitroPropagationItem);
            }
            SelectedInventoryList = new ObservableCollection<object>();

            SelectedRowsCount = 0;
            AccessionCount = _inventoryList.Select(i => i.Item.AccessionNumber).Distinct().Count();
            TotalQuantity = _inventoryList.Select(i => i.Item.quantity_on_hand).Sum();
            _dataStoreService.InventoryList = InventoryList;
        }
        public DelegateCommand CreateInventoryViabilityCommand { get; }
        private async void OnCreateInventoryViabilityCommand()
        {
            try
            {
                var selection = GetSelection();
                var navigationParams = new NavigationParameters();
                navigationParams.Add("InventoryThumbnailList", selection);

                if (selection == null) return;
                if (selection.Count == 1)
                {
                    navigationParams.Add("InventoryId", selection[0].inventory_id);
                    await NavigationService.NavigateAsync("CreateInventoryViabilityPage", navigationParams);
                }
                else if(selection.Count > 1)
                    throw new Exception("Debe seleccionar un solo inventario");
                else
                    throw new Exception("No se ha seleccionado un inventario");
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public DelegateCommand RefreshCommand { get; }
        private async void OnRefreshCommand()
        {
            try
            {
                var selection = GetSelection();
                if (selection != null && selection.Count > 0)
                {
                    var idList = selection.Select(x => x.inventory_id).ToList();
                    string strIdList = string.Join(",", idList);

                    var inventoryThumbnailList = await _restClient.Search("@inventory.inventory_id in (" + strIdList + ")", "get_mob_inventory_thumbnail", "inventory");

                    foreach (WrappedSelection<InventoryThumbnail> item in _inventoryList)
                    {
                        if(item.IsSelected)
                        {
                            item.Item = inventoryThumbnailList.FirstOrDefault(x => x.inventory_id == item.Item.inventory_id);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public DelegateCommand CreatePrintInventoryCommand { get; }
        private async void OnCreatePrintInventoryCommand()
        {
            try
            {
                var navigationParams = new NavigationParameters();
                var selection = GetSelection();
                
                if (selection != null && selection.Count > 0)
                {
                    navigationParams.Add("InventoryThumbnailList", selection);
                    await NavigationService.NavigateAsync("CreatePrintInventoryPage", navigationParams);
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public DelegateCommand UpdateAttributeBatchCommand { get; }
        private async void OnUpdateAttributeBatchCommandExecuted()
        {
            await NavigationService.NavigateAsync("UpdateAttributePage");
        }

        private List<InventoryThumbnail> GetSelection()
        {
            //return _inventoryList.Where(item => item.IsSelected).Select(wrappedItem => wrappedItem.Item).ToList();
            return SelectedInventoryList.Cast<WrappedSelection<InventoryThumbnail>>().Select(x => x.Item).ToList();
        }
        private void CheckCropTraitsObservations(IEnumerable<WrappedSelection<InventoryThumbnail>> items)
        {
            if (_cropTraits == null || !_cropTraits.Any())
                return;

            foreach (var inventoryItem in items)
            {
                if (inventoryItem.Item.CropTraitObservations == null || !inventoryItem.Item.CropTraitObservations.Any())
                {
                    var dic = new Dictionary<int, CropTraitObservation>();
                    foreach (var cropTrait in _cropTraits)
                    {
                        dic.Add(cropTrait.CropTraitId, new CropTraitObservation
                        {
                            crop_trait_observation_id = -1,
                            crop_trait_id = cropTrait.CropTraitId,
                            DisplayText = string.Empty
                        });
                    }
                    inventoryItem.Item.CropTraitObservations = dic;
                }
            }
        }
        private void CheckCropTraitObservations(InventoryThumbnail item)
        {
            if (_cropTraits == null || !_cropTraits.Any())
                return;

            if (item.CropTraitObservations == null || !item.CropTraitObservations.Any())
            {
                var dic = new Dictionary<int, CropTraitObservation>();
                foreach (var cropTrait in _cropTraits)
                {
                    dic.Add(cropTrait.CropTraitId, new CropTraitObservation
                    {
                        crop_trait_observation_id = -1,
                        crop_trait_id = cropTrait.CropTraitId,
                        DisplayText = string.Empty
                    });
                }
                item.CropTraitObservations = dic;
            }
        }
        private async Task FillCropTraitObservationsAsync(InventoryThumbnail inventoryThumbnail)
        {
            if (_cropTraits == null || !_cropTraits.Any())
            {
                return;
            }
            //Get observations
            var cropTraitsIdsArray = _cropTraits.Select(x => x.CropTraitId).ToArray();
            var observations = await _restClient.GetCropTraitObservationList(inventoryThumbnail.inventory_id.ToString(), string.Join(",", cropTraitsIdsArray), 1);
            if (observations != null && observations.Any())
            {
                foreach (var cropTrait in _cropTraits)
                {
                    var observation = observations.FirstOrDefault(x => x.crop_trait_id == cropTrait.CropTraitId);
                    if (observation != null)
                    {
                        if (cropTrait.IsCoded.Equals('Y'))
                        {
                            observation.DisplayText = observation.code;
                        }
                        else if (CropTraitDataTypeEnum.Numeric.GetString().Equals(cropTrait.DataTypeCode))
                        {
                            observation.DisplayText = observation.numeric_value.ToString();
                        }
                        else
                        {
                            observation.DisplayText = observation.string_value;
                        }
                        inventoryThumbnail.CropTraitObservations[cropTrait.CropTraitId] = observation;
                    }
                }
            }
        }
        public DelegateCommand ShowInfoCommand { get; }
        private async void OnShowInfoCommand()
        {
            try
            {
                if(SelectedInventory != null)
                {
                    var json = Newtonsoft.Json.JsonConvert.SerializeObject(SelectedInventory, Newtonsoft.Json.Formatting.Indented);
                    await PageDialogService.DisplayAlertAsync("Info", json, "OK");
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public DelegateCommand EditCropTraitObservationsCommand { get; }
        private async void OnEditCropTraitObservationsCommand()
        {
            try
            {
                if (!InventoryList.Any())
                {
                    return;
                }
                if (SelectedInventory == null)
                {
                    SelectedInventory = InventoryList.First();
                }
                INavigationParameters parameters = new NavigationParameters
                {
                    { "InventoryThumbnailList", InventoryList.Select(x => x.Item).ToList() },
                    { "CropTraitList", _cropTraits },
                    { "CurrentInventory", SelectedInventory.Item }
                };
                var result = await NavigationService.NavigateAsync("CropTraitObservationsPage", parameters);
                if (result.Exception != null)
                {
                    throw result.Exception;
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public DelegateCommand EvaluateCommand { get; }
        private async void ExecuteEvaluateCommand()
        {
            try
            {
                IsBusy = true;

                var selection = GetSelection();
                var navigationParams = new NavigationParameters()
                {
                    {"InventoryThumbnailList", selection }
                };
                
                var navigationResult = await NavigationService.NavigateAsync("EvaluateInventoriesPage", navigationParams);
                if (!navigationResult.Success)
                    throw navigationResult.Exception;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        
        public DelegateCommand CheckAllCommand { get; }
        private void ExecuteCheckAllCommand()
        {
            IsBusy = true;
            if (InventoryList.Any())
            {
                SelectedInventoryList = new ObservableCollection<object>(InventoryList);
            }
            //if (InventoryList != null)
            //{
            //    foreach (var inventoryItem in InventoryList)
            //    {
            //        if (inventoryItem.IsSelected)
            //            continue;
            //        SelectedInventoryList.Add(inventoryItem);
            //        inventoryItem.IsSelected = true;
            //    }
            //}
            IsBusy = false;
        }
        public DelegateCommand UncheckAllCommand { get; }
        private void ExecuteUncheckAllCommand()
        {
            IsBusy = true;
            SelectedInventoryList = new ObservableCollection<object>();
            //if (InventoryList != null)
            //{
            //    foreach (var inventoryItem in InventoryList)
            //    {
            //        if (!inventoryItem.IsSelected)
            //            continue;
            //        _ = SelectedInventoryList.Remove(inventoryItem);
            //        inventoryItem.IsSelected = false;
            //    }
            //}
            IsBusy = false;
        }

        public override void Destroy()
        {
            _addInventoryToListEvent.Unsubscribe(OnAddItemToList);
        }
        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            try
            {
                IsBusy = true;
                if (!_cropTraitsColumnsLoaded)
                {
                    var groupCropTraitIdsSetting = _dataStoreService.GetSetting(Settings.WorkgroupId, "group_settings", "crop_trait_ids");
                    if(groupCropTraitIdsSetting != null && !string.IsNullOrEmpty(groupCropTraitIdsSetting.gui_filter))
                    {
                        //get_mob_read_crop_trait
                        _cropTraits = await _restClient.GetGroupCropTraitList(groupCropTraitIdsSetting.gui_filter);
                    }
                    
                    if (_inventoryList != null && _inventoryList.Any())
                    {
                        //Fill all crop_trait_observations
                        CheckCropTraitsObservations(InventoryList);

                        if (_cropTraits != null && _cropTraits.Any())
                        {
                            //Get observations
                            var inventoryIdsArray = _inventoryList.Select(x => x.Item.inventory_id).ToArray();
                            var observations = await _restClient.GetCropTraitObservationList(string.Join(",", inventoryIdsArray), groupCropTraitIdsSetting.gui_filter, 1);
                            if(observations != null && observations.Any())
                            {
                                foreach (var inventoryItem in InventoryList)
                                {
                                    foreach (var cropTrait in _cropTraits)
                                    {
                                        var observation = observations.FirstOrDefault(x => x.inventory_id == inventoryItem.Item.inventory_id && x.crop_trait_id == cropTrait.CropTraitId);
                                        if (observation != null)
                                        {
                                            if (cropTrait.IsCoded.Equals('Y'))
                                            {
                                                observation.DisplayText = observation.code;
                                            }
                                            else if (CropTraitDataTypeEnum.Numeric.GetString().Equals(cropTrait.DataTypeCode))
                                            {
                                                observation.DisplayText = observation.numeric_value.ToString();
                                            }
                                            else
                                            {
                                                observation.DisplayText = observation.string_value;
                                            }
                                            inventoryItem.Item.CropTraitObservations[cropTrait.CropTraitId] = observation;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    _cropTraitsColumnsLoaded = true;
                }
                //Load crop traits columns
                if (_cropTraits != null && _cropTraits.Any())
                {
                    Xamarin.Forms.MessagingCenter.Send<InventoriesPageViewModel, IEnumerable<CropTrait>>(this, "EvaluationDataGrid.GenerateColumns", _cropTraits);
                }

                //if (InventoryList == null)
                //{
                //    InventoryList = _dataStoreService.InventoryList;
                //}


                if (_dataviewColumnList == null)
                {
                    _dataviewColumnList = await _restClient.GetDataviewAtributeList(Settings.WorkgroupInventoryThumbnailDataview);
                    if (_dataviewColumnList != null)
                    {
                        if (_dataviewColumnList.Count >= 1) Header1 = _dataviewColumnList[0].title;
                        if (_dataviewColumnList.Count >= 2) Header2 = _dataviewColumnList[1].title;
                        if (_dataviewColumnList.Count >= 3) Header3 = _dataviewColumnList[2].title;
                        if (_dataviewColumnList.Count >= 4) Header4 = _dataviewColumnList[3].title;
                        if (_dataviewColumnList.Count >= 5) Header5 = _dataviewColumnList[4].title;
                        if (_dataviewColumnList.Count >= 6) Header6 = _dataviewColumnList[5].title;
                        if (_dataviewColumnList.Count >= 7) Header7 = _dataviewColumnList[6].title;
                        if (_dataviewColumnList.Count >= 8) Header8 = _dataviewColumnList[7].title;
                    }
                }

                if (parameters.ContainsKey("inventory")) //Update UI
                {
                    InventoryThumbnail tempInventory = (InventoryThumbnail)parameters["inventory"];
                    WrappedSelection<InventoryThumbnail> inventoryThumbnailItem = _inventoryList.FirstOrDefault(item => item.Item.inventory_id == tempInventory.inventory_id);
                    if (inventoryThumbnailItem == null)
                    {
                        InventoryList.Add(new WrappedSelection<InventoryThumbnail> { Item = tempInventory, IsSelected = false });
                    }
                    else
                    {
                        inventoryThumbnailItem.Item = tempInventory;
                        /*
                        int index = InventoryList.IndexOf(inventoryThumbnailItem);
                        InventoryList.Remove(inventoryThumbnailItem);
                        InventoryList.Insert(index, new WrappedSelection<InventoryThumbnail> { Item = tempInventory, IsSelected = false });
                        */
                    }
                }

                if (parameters.ContainsKey("InventoryThumbnail"))
                {
                    InventoryThumbnail tempInventory = (InventoryThumbnail)parameters["InventoryThumbnail"];

                    // This is executed only when an inventory is created so it is not necessary to obtain the observations. They will be filled with empty values
                    CheckCropTraitObservations(tempInventory);

                    WrappedSelection<InventoryThumbnail> inventoryThumbnailItem = _inventoryList.FirstOrDefault(item => item.Item.inventory_id == tempInventory.inventory_id);
                    if (inventoryThumbnailItem == null)
                    {
                        InventoryList.Add(new WrappedSelection<InventoryThumbnail> { Item = tempInventory, IsSelected = false });
                    }
                    else
                    {
                        SelectedInventoryList.Clear();
                        inventoryThumbnailItem.Item = tempInventory;
                        Xamarin.Forms.MessagingCenter.Send<InventoriesPageViewModel>(this, "EvaluationDataGrid.RefreshGrid");
                        SelectedInventoryList.Add(inventoryThumbnailItem);
                    }

                    AccessionCount = InventoryList.Select(i => i.Item.AccessionNumber).Distinct().Count();
                    TotalQuantity = _inventoryList.Select(i => i.Item.quantity_on_hand).Sum();
                    _dataStoreService.InventoryList = InventoryList;
                }

                if (parameters.ContainsKey("InventoryThumbnailList"))
                {
                    List<InventoryThumbnail> inventoryList = (List<InventoryThumbnail>)parameters["InventoryThumbnailList"];

                    foreach (var inventoryThumbnail in inventoryList)
                    {
                        CheckCropTraitObservations(inventoryThumbnail);
                    }

                    if (_cropTraits != null && _cropTraits.Any())
                    {
                        //Get observations
                        var inventoryIdsArray = inventoryList.Select(x => x.inventory_id).ToArray();
                        var cropTraitsIdsArray = _cropTraits.Select(x => x.CropTraitId).ToArray();
                        var observations = await _restClient.GetCropTraitObservationList(string.Join(",", inventoryIdsArray), string.Join(",", cropTraitsIdsArray), 1);
                        if (observations != null && observations.Any())
                        {
                            foreach (var inventoryItem in inventoryList)
                            {
                                foreach (var cropTrait in _cropTraits)
                                {
                                    var observation = observations.FirstOrDefault(x => x.inventory_id == inventoryItem.inventory_id && x.crop_trait_id == cropTrait.CropTraitId);
                                    if (observation != null)
                                    {
                                        if (cropTrait.IsCoded.Equals('Y'))
                                        {
                                            observation.DisplayText = observation.code;
                                        }
                                        else if (CropTraitDataTypeEnum.Numeric.GetString().Equals(cropTrait.DataTypeCode))
                                        {
                                            observation.DisplayText = observation.numeric_value.ToString();
                                        }
                                        else
                                        {
                                            observation.DisplayText = observation.string_value;
                                        }
                                        inventoryItem.CropTraitObservations[cropTrait.CropTraitId] = observation;
                                    }
                                }
                            }
                        }
                    }

                    foreach (var inventoryThumbnail in inventoryList)
                    {
                        var wrappedInventory = InventoryList.FirstOrDefault(x => x.Item.inventory_id == inventoryThumbnail.inventory_id);
                        if (wrappedInventory == null)
                        {
                            WrappedSelection<InventoryThumbnail> temp = new WrappedSelection<InventoryThumbnail>() { Item = inventoryThumbnail, IsSelected = false };
                            InventoryList.Add(temp);
                        }
                        else
                        {
                            wrappedInventory.Item = inventoryThumbnail;
                            /*int index = _inventoryList.IndexOf(wrappedInventory);
                            InventoryList.Remove(wrappedInventory);
                            InventoryList.Insert(index, new WrappedSelection<InventoryThumbnail> { Item = inventoryThumbnail, IsSelected = wrappedInventory.IsSelected });*/
                        }
                    }

                    AccessionCount = InventoryList.Select(i => i.Item.AccessionNumber).Distinct().Count();
                    TotalQuantity = _inventoryList.Select(i => i.Item.quantity_on_hand).Sum();
                    _dataStoreService.InventoryList = InventoryList;
                }

                if (parameters.ContainsKey("RefreshDatagrid"))
                {
                    if (_cropTraitsColumnsLoaded)
                    {
                        Xamarin.Forms.MessagingCenter.Send<InventoriesPageViewModel>(this, "EvaluationDataGrid.RefreshGrid");
                    }
                }

                Utils.RefreshLang(this, _dataStoreService, "InventoriesPage");
                IsBusy = false;
            }
            catch (Exception ex)
            {
                IsBusy = false;
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
    }
}
