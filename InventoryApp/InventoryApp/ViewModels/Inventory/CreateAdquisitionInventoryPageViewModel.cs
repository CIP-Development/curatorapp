﻿using InventoryApp.Extensions;
using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using InventoryApp.Models.Database;
using InventoryApp.Services;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InventoryApp.ViewModels
{
    public class CreateAdquisitionInventoryPageViewModel : ViewModelBaseWithDialog
    {
        private RestClient _restClient;
        private readonly RestService _restService;

        private Inventory _newInventory;
        public Inventory NewInventory
        {
            get { return _newInventory; }
            set { SetProperty(ref _newInventory, value); }
        }
        private List<string> _location1List;
        public List<string> Location1List
        {
            get { return _location1List; }
            set { SetProperty(ref _location1List, value); }
        }
        private IEnumerable<string> _location2List;
        public IEnumerable<string> Location2List
        {
            get { return _location2List; }
            set { SetProperty(ref _location2List, value); }
        }
        private IEnumerable<string> _location3List;
        public IEnumerable<string> Location3List
        {
            get { return _location3List; }
            set { SetProperty(ref _location3List, value); }
        }
        private IEnumerable<ILookup> _formTypeCodeValueList;
        public IEnumerable<ILookup> FormTypeCodeValueList
        {
            get { return _formTypeCodeValueList; }
            set { SetProperty(ref _formTypeCodeValueList, value); }
        }
        private ILookup _selectedFormType;
        public ILookup SelectedFormType
        {
            get { return _selectedFormType; }
            set
            {
                var notified = SetProperty(ref _selectedFormType, value);
                if(notified)
                    NewInventory.form_type_code = value == null ? null : (string)value.ValueMember;
            }
        }
        private IEnumerable<ILookup> _maintPolicyList;
        public IEnumerable<ILookup> MaintPolicyList
        {
            get { return _maintPolicyList; }
            set { SetProperty(ref _maintPolicyList, value); }
        }
        private ILookup _selectedMaintPolicy;
        public ILookup SelectedMaintPolicy
        {
            get { return _selectedMaintPolicy; }
            set
            {
                var notified = SetProperty(ref _selectedMaintPolicy, value);
                if (notified)
                    NewInventory.inventory_maint_policy_id = value == null ? -1 : (int)value.ValueMember;
            }
        }
        private IEnumerable<ILookup> _availabilityStatusCodeList;
        public IEnumerable<ILookup> AvailabilityStatusCodeList
        {
            get { return _availabilityStatusCodeList; }
            set { SetProperty(ref _availabilityStatusCodeList, value); }
        }
        private ILookup _selectedAvailabilityStatusCode;
        public ILookup SelectedAvailabilityStatusCode
        {
            get { return _selectedAvailabilityStatusCode; }
            set
            {
                var notified = SetProperty(ref _selectedAvailabilityStatusCode, value);
                if (notified)
                    NewInventory.availability_status_code = value == null ? null : (string)value.ValueMember;
            }
        }
        private IEnumerable<ILookup> _pollinationMethodCodeList;
        public IEnumerable<ILookup> PollinationMethodCodeList
        {
            get { return _pollinationMethodCodeList; }
            set { SetProperty(ref _pollinationMethodCodeList, value); }
        }
        private ILookup _selectedPollinationMethodCode;
        public ILookup SelectedPollinationMethodCode
        {
            get { return _selectedPollinationMethodCode; }
            set
            {
                SetProperty(ref _selectedPollinationMethodCode, value);
                NewInventory.pollination_method_code = value == null ? null : (string)value.ValueMember;
            }
        }
        private IEnumerable<ILookup> _pathogenStatusCodeList;
        public IEnumerable<ILookup> PathogenStatusCodeList
        {
            get { return _pathogenStatusCodeList; }
            set { SetProperty(ref _pathogenStatusCodeList, value); }
        }
        private ILookup _selectedPathogenStatusCode;
        public ILookup SelectedPathogenStatusCode
        {
            get { return _selectedPathogenStatusCode; }
            set
            {
                SetProperty(ref _selectedPathogenStatusCode, value);
                NewInventory.pathogen_status_code = value == null ? null : (string)value.ValueMember;
            }
        }
        private IEnumerable<ILookup> _quantityOnHandUnitCodeValueList;
        public IEnumerable<ILookup> QuantityOnHandUnitCodeValueList
        {
            get { return _quantityOnHandUnitCodeValueList; }
            set { SetProperty(ref _quantityOnHandUnitCodeValueList, value); }
        }
        private ILookup _selectedQuantityOnHandUnitCodeValue;
        public ILookup SelectedQuantityOnHandUnitCodeValue
        {
            get { return _selectedQuantityOnHandUnitCodeValue; }
            set
            {
                var notified = SetProperty(ref _selectedQuantityOnHandUnitCodeValue, value);
                if (notified)
                    NewInventory.quantity_on_hand_unit_code = value == null ? null : (string)value.ValueMember;
            }
        }
        private IEnumerable<string> _containerTypeList;
        public IEnumerable<string> ContainerTypeList
        {
            get { return _containerTypeList; }
            set { SetProperty(ref _containerTypeList, value); }
        }
        private string _selectedContainerType;
        public string SelectedContainerType
        {
            get { return _selectedContainerType; }
            set
            {
                var notified = SetProperty(ref _selectedContainerType, value);
                if (notified)
                    NewInventory.storage_location_part4 = value ?? null;
            }
        }
        private IEnumerable<ILookup> _propagationDateCodeList;
        public IEnumerable<ILookup> PropagationDateCodeList
        {
            get { return _propagationDateCodeList; }
            set { SetProperty(ref _propagationDateCodeList, value); }
        }
        private ILookup _selectedPropagationDateCode;
        public ILookup SelectedPropagationDateCode
        {
            get { return _selectedPropagationDateCode; }
            set
            {
                var notified = SetProperty(ref _selectedPropagationDateCode, value);
                if (notified)
                    NewInventory.propagation_date_code = value == null ? null : (string)value.ValueMember;
            }
        }
        private IEnumerable<ILookup> _PollinationVectorCodeList;
        public IEnumerable<ILookup> PollinationVectorCodeList
        {
            get { return _PollinationVectorCodeList; }
            set { SetProperty(ref _PollinationVectorCodeList, value); }
        }
        private ILookup _SelectedPollinationVectorCode;
        public ILookup SelectedPollinationVectorCode
        {
            get { return _SelectedPollinationVectorCode; }
            set
            {
                var modified = SetProperty(ref _SelectedPollinationVectorCode, value);
                if (modified)
                    NewInventory.pollination_vector_code = value == null ? null : (string)value.ValueMember;
            }
        }
        private IEnumerable<ILookup> _regenerationLocalityCodeValueList;
        public IEnumerable<ILookup> RegenerationLocalityCodeValueList
        {
            get { return _regenerationLocalityCodeValueList; }
            set { SetProperty(ref _regenerationLocalityCodeValueList, value); }
        }
        private ILookup _selectedRegenerationLocalityCodeValue;
        public ILookup SelectedRegenerationLocalityCodeValue
        {
            get { return _selectedRegenerationLocalityCodeValue; }
            set { SetProperty(ref _selectedRegenerationLocalityCodeValue, value); }
        }
        private bool _propagationDateIsNull;
        public bool PropagationDateIsNull
        {
            get { return _propagationDateIsNull; }
            set { SetProperty(ref _propagationDateIsNull, value); }
        }
        private DateTime _propagationDate;
        public DateTime PropagationDate
        {
            get { return _propagationDate; }
            set { SetProperty(ref _propagationDate, value); }
        }
        public CreateAdquisitionInventoryPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService,
            RestService restService)
            : base(navigationService, pageDialogService)
        {
            _restClient = new RestClient();
            _restService = restService;

            SaveCommand = new DelegateCommand(ExecuteSaveCommand).ObservesCanExecute(() => IsNotBusy);
            LocationChangedCommand = new DelegateCommand<object>(ExecuteLocationChangedCommand).ObservesCanExecute(() => IsNotBusy);
            SearchAccessionCommand = new DelegateCommand(ExecuteSearchAccessionCommand).ObservesCanExecute(() => IsNotBusy);

            ShowDebugInfoCommand = new DelegateCommand(ExecuteShowDebugInfoCommand);
            SetPropagationDateCommand = new DelegateCommand(ExecuteSetPropagationDateCommand);
            NewInventory = new Inventory()
            {
                accession_id = -1,
                accession_number = string.Empty,
                acc_name_col = string.Empty,
                acc_name_cul = string.Empty,
                availability_end_date = null,
                availability_start_date = null,
                availability_status_code = null,
                availability_status_note = null,
                backup_inventory_id = null,
                distribution_critical_quantity = null,
                distribution_default_form_code = null,
                distribution_default_quantity = null,
                distribution_unit_code = null,
                form_type_code = null,
                hundred_seed_weight = null,
                inventory_number = string.Empty,
                modified_by = null,
                modified_date = null,
                note = string.Empty,
                parent_inventory_id = null,
                pathogen_status_code = null,
                plant_sex_code = null,
                pollination_method_code = null,
                pollination_vector_code = null,
                preservation_method_id = null,
                regeneration_method_id = null,
                regeneration_critical_quantity = null,
                quantity_on_hand = 0,
                inventory_id = -1,
                inventory_number_part1 = string.Empty,
                inventory_number_part2 = null,
                inventory_number_part3 = string.Empty,
                inventory_maint_policy_id = -1,
                is_auto_deducted = "N",
                is_available = "N",
                is_distributable = "N",
                owned_by = Settings.UserCooperatorId,
                created_by = Settings.UserCooperatorId,
                quantity_on_hand_unit_code = "ct",
                propagation_date = null,
                propagation_date_code = "MM/dd/yyyy",
                workgroup_cooperator_id = Settings.WorkgroupCooperatorId,
            };
            PropagationDate = DateTime.Now;
            PropagationDateIsNull = true;
        }
        public DelegateCommand ShowDebugInfoCommand { get; }
        private async void ExecuteShowDebugInfoCommand()
        {
            try
            {
                var json = Newtonsoft.Json.JsonConvert.SerializeObject(NewInventory, Newtonsoft.Json.Formatting.Indented);
                await PageDialogService.DisplayAlertAsync("", json, "OK");
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
        }
        public DelegateCommand<object> LocationChangedCommand { get; }
        private async void ExecuteLocationChangedCommand(object param)
        {
            try
            {
                IsBusy = true;

                if (param == null) return;
                if (!(param is string)) return;
                var entityAttribute = (string)param;
                if (entityAttribute == null) return;

                switch (entityAttribute)
                {
                    case "storage_location_part1":
                        {
                            Location3List = new List<string>();

                            if (!string.IsNullOrEmpty(NewInventory.storage_location_part1))
                                Location2List = await _restClient.GetAllLocation2List(NewInventory.storage_location_part1);
                            else
                                Location2List = new List<string>();
                        }
                        break;
                    case "storage_location_part2":
                        {
                            if (!string.IsNullOrEmpty(NewInventory.storage_location_part2))
                                Location3List = await _restClient.GetAllLocation3List(NewInventory.storage_location_part1, NewInventory.storage_location_part2);
                            else
                                Location3List = new List<string>();
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand SearchAccessionCommand { get; }
        private async void ExecuteSearchAccessionCommand()
        {
            try
            {
                IsBusy = true;

                var navResult = await NavigationService.NavigateAsync("AccessionPickerPage");
                if (!navResult.Success)
                    throw navResult.Exception;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand SaveCommand { get; }
        private async void ExecuteSaveCommand()
        {
            try
            {
                IsBusy = true;

                if (!PropagationDateIsNull)
                    NewInventory.propagation_date = PropagationDate;
                NewInventory.created_date = DateTime.Now;
                NewInventory.owned_date = DateTime.Now;
                NewInventory.workgroup_cooperator_id = Settings.WorkgroupCooperatorId;

                var newInventoryId = await _restClient.CreateInventory(NewInventory);
                if (SelectedRegenerationLocalityCodeValue != null)
                {
                    var newInventoryActionId = await _restService.CreateInventoryActionAsync(new InventoryAction
                    {
                        inventory_action_id = -1,
                        inventory_id = int.Parse(newInventoryId),
                        action_name_code = "REGLOC",
                        note = (string)SelectedRegenerationLocalityCodeValue.ValueMember,
                        started_date = NewInventory.propagation_date,
                        started_date_code = NewInventory.propagation_date_code,
                        owned_by = Settings.WorkgroupCooperatorId,
                    });
                }
                await PageDialogService.DisplayAlertAsync("Resultado de guardar", "Se ha registrado el inventario\n" + newInventoryId, "OK");

                var newInventoryList = await _restClient.Search("@inventory.inventory_id = " + newInventoryId, "get_mob_inventory_thumbnail", "inventory");
                
                _ = await NavigationService.GoBackAsync(new NavigationParameters
                {
                    { "InventoryThumbnail", newInventoryList.First()}
                });
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand SetPropagationDateCommand { get; }
        private void ExecuteSetPropagationDateCommand()
        {
            PropagationDateIsNull = false;
        }
        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            try
            {
                IsBusy = true;

                if (!IsInitialized)
                {
                    Location1List = await _restClient.GetAllLocation1List();
                    FormTypeCodeValueList = await _restClient.GetCodeValueByGroupName("GERMPLASM_FORM");
                    AvailabilityStatusCodeList = await _restClient.GetCodeValueByGroupName("INVENTORY_AVAILABILITY_STATUS");
                    PollinationMethodCodeList = await _restClient.GetCodeValueByGroupName("INVENTORY_POLLINATION_METHOD");
                    PathogenStatusCodeList = await _restClient.GetCodeValueByGroupName("PATHOGEN_STATUS");
                    QuantityOnHandUnitCodeValueList = await _restClient.GetCodeValueByGroupName("UNIT_OF_QUANTITY");
                    ContainerTypeList = await _restService.GetInventoryStorageLocationPart4Distinct();
                    RegenerationLocalityCodeValueList = await _restClient.GetCodeValueByGroupName("REGENERATION_LOCALITY");
                    PropagationDateCodeList = await _restClient.GetCodeValueByGroupName("DATE_FORMAT");
                    PollinationVectorCodeList = await _restClient.GetCodeValueByGroupName("INVENTORY_POLLINATION_VECTOR");

                    SelectedAvailabilityStatusCode = AvailabilityStatusCodeList.FirstOrDefault(x => x.ValueMember.Equals("UNKN"));
                    SelectedPathogenStatusCode = PathogenStatusCodeList.FirstOrDefault(x => x.ValueMember.Equals("Unknown"));
                    SelectedQuantityOnHandUnitCodeValue = QuantityOnHandUnitCodeValueList.FirstOrDefault(x => x.ValueMember.Equals("ct"));
                    SelectedPropagationDateCode = PropagationDateCodeList.FirstOrDefault(x => x.ValueMember.Equals("MM/dd/yyyy"));

                    var tempMaintPolicyList = await _restClient.GetLookupList("inventory_maint_policy_lookup");
                    if (!string.IsNullOrEmpty(Settings.WorkgroupInvMaintPolicies))
                    {
                        var filterList = Settings.WorkgroupInvMaintPolicies.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = tempMaintPolicyList.Count - 1; i >= 0; i--)
                        {
                            if (!filterList.Any(x => x.Equals(tempMaintPolicyList[i].ValueMember.ToString())))
                            {
                                tempMaintPolicyList.RemoveAt(i);
                            }
                        }
                    }
                    MaintPolicyList = tempMaintPolicyList;
                    SelectedMaintPolicy = MaintPolicyList.FirstOrDefault();

                    IsInitialized = true;
                }
                else
                {
                    if (parameters.ContainsKey("SelectedAccession"))
                    {
                        var selectedAccession = parameters.GetValue<ILookup>("SelectedAccession");
                        NewInventory.accession_number = selectedAccession.DisplayMember;
                        NewInventory.accession_id = (int)selectedAccession.ValueMember;
                    }
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
