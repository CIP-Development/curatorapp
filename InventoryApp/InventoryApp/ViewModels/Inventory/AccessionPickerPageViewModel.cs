﻿using InventoryApp.Extensions;
using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Services;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryApp.ViewModels
{
    public class AccessionPickerPageViewModel : ViewModelBaseWithDialog
    {
        private RestClient _restClient;

        private string _searchText;
        public string SearchText
        {
            get { return _searchText; }
            set { SetProperty(ref _searchText, value); }
        }
        private List<ILookup> _accessionList;
        public List<ILookup> AccessionList
        {
            get { return _accessionList; }
            set { SetProperty(ref _accessionList, value); }
        }
        private ILookup _selectedAccession;
        public ILookup SelectedAccession
        {
            get { return _selectedAccession; }
            set { SetProperty(ref _selectedAccession, value); }
        }
        private bool _SearchInExistingInventories;
        public bool SearchInExistingInventories
        {
            get { return _SearchInExistingInventories; }
            set { SetProperty(ref _SearchInExistingInventories, value); }
        }
        public AccessionPickerPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService, pageDialogService)
        {
            _restClient = new RestClient();

            SearchCommand = new DelegateCommand(OnSearchCommandExecuted).ObservesCanExecute(()=> IsNotBusy);
            ItemTappedCommand = new DelegateCommand(ExecuteItemTappedCommand).ObservesCanExecute(() => IsNotBusy);

            SearchInExistingInventories = true;
        }
        public DelegateCommand SearchCommand { get; }
        private async void OnSearchCommandExecuted()
        {
            try
            {
                IsBusy = true;
                if (SearchInExistingInventories && !string.IsNullOrEmpty(Settings.WorkgroupInvMaintPolicies))
                    AccessionList = await _restClient.GetAccessionLookUpListByAccessionNumberChecked(SearchText, Settings.WorkgroupInvMaintPolicies);
                else
                    AccessionList = await _restClient.GetAccessionLookUpListByAccessionNumber(SearchText);
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand ItemTappedCommand { get; }
        private async void ExecuteItemTappedCommand()
        {
            try
            {
                IsBusy = true;

                var navResult = await NavigationService.GoBackAsync(new NavigationParameters
                {
                    { "SelectedAccession", SelectedAccession }
                });
                
                if (!navResult.Success)
                    throw navResult.Exception;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
