﻿using InventoryApp.EventAggregators;
using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using InventoryApp.Services;
using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xamarin.Forms;
using ZXing;

namespace InventoryApp.ViewModels
{
    public class ScanPageViewModel : ViewModelBase
    {
        AddInventoryToListEvent _addInventoryToListEvent;
        private RestClient _restClient;
        IPageDialogService PageDialogService { get; }
        IDeviceService DeviceService { get; }
        IEventAggregator _eventAggregator;
        private IDataStoreService _dataStoreService;

        private bool _isScanning;
        public bool IsScanning
        {
            get { return _isScanning; }
            set { SetProperty(ref _isScanning, value); }
        }

        private Result _result;
        public Result Result
        {
            get { return _result; }
            set { SetProperty(ref _result, value); }
        }

        private string _resultText;
        public string ResultText
        {
            get { return _resultText; }
            set { SetProperty(ref _resultText, value); }
        }

        private bool _isAnalyzing;
        public bool IsAnalyzing
        {
            get { return _isAnalyzing; }
            set { SetProperty(ref _isAnalyzing, value); }
        }

        private InventoryThumbnail _scannedInventory;
        public InventoryThumbnail ScannedInventory
        {
            get { return _scannedInventory; }
            set { SetProperty(ref _scannedInventory, value); }
        }

        private List<Models.BarcodeFormat> _barcodeFormatList;
        public List<Models.BarcodeFormat> BarcodeFormatList
        {
            get { return _barcodeFormatList; }
            set { SetProperty(ref _barcodeFormatList, value); }
        }
        private Models.BarcodeFormat _barcodeFormat;
        public Models.BarcodeFormat BarcodeFormat
        {
            get { return _barcodeFormat; }
            set { SetProperty(ref _barcodeFormat, value); }
        }
        private bool _isInventoryFound;
        public bool IsInventoryFound
        {
            get { return _isInventoryFound; }
            set { SetProperty(ref _isInventoryFound, value); }
        }
        #region Lang
        private string _UxTitle;
        public string UxTitle
        {
            get { return _UxTitle; }
            set { SetProperty(ref _UxTitle, value); }
        }
        private string _UxButtonAddToList;
        public string UxButtonAddToList
        {
            get { return _UxButtonAddToList; }
            set { SetProperty(ref _UxButtonAddToList, value); }
        }
        private string _UxButtonScanAgain;
        public string UxButtonScanAgain
        {
            get { return _UxButtonScanAgain; }
            set { SetProperty(ref _UxButtonScanAgain, value); }
        }
        private string _UxButtonClose;
        public string UxButtonClose
        {
            get { return _UxButtonClose; }
            set { SetProperty(ref _UxButtonClose, value); }
        }
        private string _UxScannerOverlayTitle;
        public string UxScannerOverlayTitle
        {
            get { return _UxScannerOverlayTitle; }
            set { SetProperty(ref _UxScannerOverlayTitle, value); }
        }
        private string _UxLabelBarcodeFormat;
        public string UxLabelBarcodeFormat
        {
            get { return _UxLabelBarcodeFormat; }
            set { SetProperty(ref _UxLabelBarcodeFormat, value); }
        }
        #endregion
        public ScanPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService, IDeviceService deviceService, IEventAggregator eventAggregator, IDataStoreService dataStoreService)
            : base(navigationService)
        {
            PageDialogService = pageDialogService;
            DeviceService = deviceService;
            _eventAggregator = eventAggregator;
            _restClient = new RestClient();
            _dataStoreService = dataStoreService;

            CloseCommand = new DelegateCommand(OnCloseCommandExecuted);
            ScanResultCommand = new DelegateCommand(OnScanResultCommandExecuted);
            AcceptCommand = new DelegateCommand(OnAcceptCommandExecuted);
            ScanAgainCommand = new DelegateCommand(OnScanAgainCommandExecuted);
            BarcodeFormatChangedCommand = new DelegateCommand(OnBarcodeFormatChangedCommand);
            SearchCommand = new DelegateCommand(OnSearchCommand);

            _isScanning = true;
            _isAnalyzing = true;

            ResultText = "";
            IsInventoryFound = false;

            _addInventoryToListEvent = _eventAggregator.GetEvent<AddInventoryToListEvent>();
        }

        public DelegateCommand CloseCommand { get; }
        private async void OnCloseCommandExecuted()
        {
            IsScanning = false;
            await NavigationService.GoBackAsync();
        }
        private async Task SearchInventoryAsync(string scanText)
        {
            if (BarcodeFormat == null)
                throw new Exception(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "ScanPage", "MessageBarcodeFormatIsEmpty"));

            Regex rgx = new Regex(BarcodeFormat.Format, RegexOptions.IgnoreCase);
            MatchCollection matches = rgx.Matches(scanText);

            if (matches.Count > 0)
            {
                GroupCollection groups = matches[0].Groups;
                string searchText = groups["barcode_value"].Value;

                if (!string.IsNullOrEmpty(BarcodeFormat.Query))
                {
                    string query = BarcodeFormat.Query.Replace("{barcode_value}", searchText);
                    var resp = await _restClient.Search(query, "get_mob_inventory_thumbnail", "inventory");
                    if (resp != null && resp.Count > 0)
                    {
                        if (resp.Count == 1)
                        {
                            IsInventoryFound = true;
                            ScannedInventory = resp[0];
                        }
                        else
                        {
                            DeviceService.BeginInvokeOnMainThread(async () =>
                            {
                                var addToList = await PageDialogService.DisplayAlertAsync(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "ScanPage", "MessageScanResults"),
                                string.Format(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "ScanPage", "MessageInventoriesFound"), resp.Count),
                                _dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "ScanPage", "MessageYes"),
                                _dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "ScanPage", "MessageNo"));
                                if (addToList)
                                {
                                    int countExistingInventories = 0;
                                    int countNewInventories = 0;
                                    foreach (var inventory in resp)
                                    {
                                        if (_dataStoreService.InventoryList != null && _dataStoreService.InventoryList.Any(i => i.Item.inventory_id == inventory.inventory_id))
                                            countExistingInventories++;
                                        else
                                        {
                                            _eventAggregator.GetEvent<AddInventoryToListEvent>().Publish(inventory);
                                            countNewInventories++;
                                        }
                                    }
                                    if (countExistingInventories > 0 && countNewInventories == 0)
                                        await PageDialogService.DisplayAlertAsync(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "ScanPage", "MessageAddInventoriesResults"),
                                        _dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "ScanPage", "MessageAllExistingInventories"), "OK");
                                    else if (countExistingInventories == 0 && countNewInventories > 0)
                                        await PageDialogService.DisplayAlertAsync(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "ScanPage", "MessageAddInventoriesResults"),
                                        _dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "ScanPage", "MessageAllNewInventories"), "OK");
                                    else
                                        await PageDialogService.DisplayAlertAsync(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "ScanPage", "MessageAddInventoriesResults"),
                                        string.Format(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "ScanPage", "MessageMixedInventories"), countNewInventories, countExistingInventories), "OK");
                                }
                            });
                        }
                    }
                    else
                        throw new Exception(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "ScanPage", "MessageScanNotFound"));
                }
                else if (!string.IsNullOrEmpty(BarcodeFormat.QueryDataview) && !string.IsNullOrEmpty(BarcodeFormat.QueryDataviewParameters))
                {
                    //string parameters = string.Format(BarcodeFormat.QueryDataviewParameters, searchText);
                    List<int> invList = await _restClient.SearchLookup(BarcodeFormat.QueryDataview, "equals", searchText, Settings.WorkgroupCooperatorId, false);
                    if (invList != null && invList.Count > 0)
                    {
                        var resp = await _restClient.Search("@inventory.inventory_id in (" + string.Join(",", invList) + ")", "get_mob_inventory_thumbnail", "inventory");
                        if (resp.Count == 1)
                        {
                            IsInventoryFound = true;
                            ScannedInventory = resp[0];
                        }
                        else
                        {
                            DeviceService.BeginInvokeOnMainThread(async () => {
                                var addToList = await PageDialogService.DisplayAlertAsync(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "ScanPage", "MessageScanResults"),
                                string.Format(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "ScanPage", "MessageInventoriesFound"), resp.Count),
                                _dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "ScanPage", "MessageYes"),
                                _dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "ScanPage", "MessageNo"));
                                if (addToList)
                                {
                                    int countExistingInventories = 0;
                                    int countNewInventories = 0;
                                    foreach (var inventory in resp)
                                    {
                                        if (_dataStoreService.InventoryList != null && _dataStoreService.InventoryList.Any(i => i.Item.inventory_id == inventory.inventory_id))
                                            countExistingInventories++;
                                        else
                                        {
                                            _eventAggregator.GetEvent<AddInventoryToListEvent>().Publish(inventory);
                                            countNewInventories++;
                                        }
                                    }
                                    if (countExistingInventories > 0 && countNewInventories == 0)
                                        await PageDialogService.DisplayAlertAsync(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "ScanPage", "MessageAddInventoriesResults"),
                                        _dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "ScanPage", "MessageAllExistingInventories"), "OK");
                                    else if (countExistingInventories == 0 && countNewInventories > 0)
                                        await PageDialogService.DisplayAlertAsync(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "ScanPage", "MessageAddInventoriesResults"),
                                        _dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "ScanPage", "MessageAllNewInventories"), "OK");
                                    else
                                        await PageDialogService.DisplayAlertAsync(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "ScanPage", "MessageAddInventoriesResults"),
                                        string.Format(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "ScanPage", "MessageMixedInventories"), countNewInventories, countExistingInventories), "OK");
                                }
                            });
                        }
                    }
                    else
                        throw new Exception(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "ScanPage", "MessageScanNotFound"));
                }
                else
                {
                    throw new Exception(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "ScanPage", "MessageBarcodeFormatIsNotWellDefined"));
                }
            }
            else
            {
                throw new Exception(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "ScanPage", "MessageBarcodePatternNotMatch"));
            }
        }
        public DelegateCommand ScanResultCommand { get; }
        private async void OnScanResultCommandExecuted()
        {
            try
            {
                if (IsAnalyzing)
                {
                    IsBusy = true;
                    IsAnalyzing = false;

                    await SearchInventoryAsync(Result.Text);

                    IsBusy = false;
                }
            }
            catch (Exception ex)
            {
                DeviceService.BeginInvokeOnMainThread(async () => {
                    await PageDialogService.DisplayAlertAsync(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "ScanPage", "MessageScanResultsError"),
                        ex.Message, "OK");
                });

                IsBusy = false;
            }
            finally
            {

            }


            //DeviceService.BeginInvokeOnMainThread(async () => {


            //    //await Task.Delay(50);
            //    //await NavigationService.GoBackAsync(null, null, true);


            //    //if (_isScanning)
            //    //{
            //    //    _isScanning = false;
            //    //    IsAnalyzing = false;

            //    //    /*var response = await PageDialogService.DisplayAlertAsync("Barcode value", ResultText, "OK", "Scan again");
            //    //    if (response)
            //    //    {
            //    //        IsScanning = false;
            //    //        await NavigationService.GoBackAsync(null, null, false);
            //    //    }
            //    //    else
            //    //    {
            //    //        Result = null;
            //    //        ResultText = "";
            //    //    }*/
            //    //}
            //    //IsAnalyzing = true;
            //    //_isScanning = true;
            //});    
        }
        public DelegateCommand SearchCommand { get; }
        private async void OnSearchCommand()
        {
            try
            {
                IsBusy = true;

                IsInventoryFound = false;
                await SearchInventoryAsync(ResultText);

                IsBusy = false;
            }
            catch (Exception ex)
            {
                IsBusy = false;
                DeviceService.BeginInvokeOnMainThread(async () => {
                    await PageDialogService.DisplayAlertAsync(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "ScanPage", "MessageScanResultsError"),
                        ex.Message, "OK");
                });
            }
        }
        public DelegateCommand AcceptCommand { get; }
        private void OnAcceptCommandExecuted()
        {
            if (Settings.ScanByCamera)
            {
                if (!IsAnalyzing && ScannedInventory != null)
                {
                    //_eventAggregator.GetEvent<AddInventoryToListEvent>().Publish(ScannedInventory);
                    _addInventoryToListEvent.Publish(ScannedInventory);
                    OnScanAgainCommandExecuted();
                }
            }
            else
            {
                if(ScannedInventory != null)
                {
                    //_eventAggregator.GetEvent<AddInventoryToListEvent>().Publish(ScannedInventory);
                    _addInventoryToListEvent.Publish(ScannedInventory);
                    OnScanAgainCommandExecuted();
                }
            }
            
        }

        public DelegateCommand ScanAgainCommand { get; }
        private void OnScanAgainCommandExecuted()
        {
            if(Settings.ScanByCamera)
            {
                Result = null;
                ScannedInventory = null;
                IsAnalyzing = true;
            }
            else
            {
                ResultText = string.Empty;
                IsInventoryFound = false;
                ScannedInventory = null;
                Xamarin.Forms.MessagingCenter.Send<ScanPageViewModel>(this, "SearchEntry.Focus");
            }
        }

        public DelegateCommand BarcodeFormatChangedCommand { get; }
        private void OnBarcodeFormatChangedCommand()
        {
            if (BarcodeFormat != null)
                Settings.BarcodeFormatCode = BarcodeFormat.Code;
        }
        public override async void Initialize(INavigationParameters parameters)
        {
            try
            {
                Utils.RefreshLang(this, _dataStoreService, "ScanPage");

                if (BarcodeFormatList == null)
                {
                    BarcodeFormatList = await _restClient.GetBarcodeFormatList();

                    if (BarcodeFormatList != null && BarcodeFormatList.Count > 0)
                    {
                        BarcodeFormat = BarcodeFormatList.Find(x => x.Code.Equals(Settings.BarcodeFormatCode));
                        if (BarcodeFormat == null)
                        {
                            Settings.BarcodeFormatCode = string.Empty;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            try
            {
                if (!Settings.ScanByCamera)
                {
                    Xamarin.Forms.MessagingCenter.Send<ScanPageViewModel>(this, "SearchEntry.Focus");
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
    }
}

/*  var navigationParams = new NavigationParameters();
    navigationParams.Add("ScanResult", _result);
    await NavigationService.GoBackAsync(navigationParams);*/
