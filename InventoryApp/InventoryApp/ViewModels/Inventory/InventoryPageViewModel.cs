﻿using ImTools;
using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using InventoryApp.Models.Database;
using InventoryApp.Services;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Xml;

namespace InventoryApp.ViewModels
{
    public class InventoryPageViewModel : ViewModelBaseWithDialog
    {
        //private DataTable _dtEntity = new System.Data.DataTable();
        //public DataRow drInventory { get; set; }

        IDataStoreService _dataStoreService;
        private IRestService _restService;
        private RestClient _restClient;
        private List<DataviewColumn> _dataviewColumnList;

        private ObservableCollection<EntityAttribute> _inventoryAttributeList;
        public ObservableCollection<EntityAttribute> InventoryAtributeList
        {
            get { return _inventoryAttributeList; }
            set { SetProperty(ref _inventoryAttributeList, value); }
        }

        private InventoryThumbnail _inventory;
        public InventoryThumbnail Inventory
        {
            get { return _inventory; }
            set { SetProperty(ref _inventory, value); }
        }

        private Inventory _newInventory;
        public Inventory NewInventory
        {
            get { return _newInventory; }
            set { SetProperty(ref _newInventory, value); }
        }
        private bool _isNewInventory = true;
        public bool IsNewInventory
        {
            get { return _isNewInventory; }
            set { SetProperty(ref _isNewInventory, value); }
        }
        private bool _createInventoryAction;
        public bool CreateInventoryAction
        {
            get { return _createInventoryAction; }
            set { SetProperty(ref _createInventoryAction, value); }
        }

        private EntityAttribute[] _inventoryAttributeArray = new EntityAttribute[] {
            new EntityAttribute{ Caption = "Inventory Id", Name = "inventory_id", Type = typeof(int), Value=null, ControlType="INTEGER", IsReadOnly = true},
            new EntityAttribute{ Caption = "Accession", Name = "accession_id", Type =  typeof(int), Value=null, ControlType="LOOKUPPICKER", IsRequired = true, ControlSource="accession_lookup"},
            new EntityAttribute{ Caption = "Inventory number part 1", Name = "inventory_number_part1", Type =  typeof(string), Value=null, ControlType="STRING"},
            new EntityAttribute{ Caption = "Inventory number part 2", Name = "inventory_number_part2", Type =  typeof(int), Value=null, ControlType="INTEGER"},
            new EntityAttribute{ Caption = "Inventory number part 3", Name = "inventory_number_part3", Type =  typeof(string), Value=null, ControlType="STRING"},

            new EntityAttribute{ Caption = "Inventory Type", Name = "form_type_code", Type = typeof(string), Value="N", ControlType="DROPDOWN", ControlSource="GERMPLASM_FORM", IsRequired = false },
            new EntityAttribute{ Caption = "Inventory Maintenance Policy", Name = "inventory_maint_policy_id", Type =  typeof(int), Value=null, ControlType="DROPDOWN", ControlSource="inventory_maint_policy_lookup", IsRequired = true},

            new EntityAttribute{ Caption = "Location Section 1", Name = "storage_location_part1", Type =  typeof(string), Value=null, ControlType="STRING"},
            new EntityAttribute{ Caption = "Location Section 2", Name = "storage_location_part2", Type =  typeof(string), Value=null, ControlType="STRING"},
            new EntityAttribute{ Caption = "Location Section 3", Name = "storage_location_part3", Type =  typeof(string), Value=null, ControlType="STRING"},
            new EntityAttribute{ Caption = "Location Section 4", Name = "storage_location_part4", Type =  typeof(string), Value=null, ControlType="STRING"},

            new EntityAttribute{ Caption = "Quantity on hand", Name = "quantity_on_hand", Type =  typeof(decimal), Value=null, ControlType="DECIMAL"},
            new EntityAttribute{ Caption = "Quantity on hand unit code", Name = "quantity_on_hand_unit_code", Type =  typeof(string), Value=null, ControlType="DROPDOWN",ControlSource="UNIT_OF_QUANTITY"},

            new EntityAttribute{ Caption = "Parent Inventory Id", Name = "parent_inventory_id", Type = typeof(int?), Value=null, ControlType="LOOKUPPICKER", ControlSource="inventory_lookup"},

            new EntityAttribute{ Caption = "Pollination method", Name = "pollination_method_code", Type =  typeof(string), Value=null, ControlType="DROPDOWN", ControlSource="INVENTORY_POLLINATION_METHOD" },
            new EntityAttribute{ Caption = "Pollination vector", Name = "pollination_vector_code", Type =  typeof(string), Value=null, ControlType="DROPDOWN", ControlSource="INVENTORY_POLLINATION_VECTOR" },
            new EntityAttribute{ Caption = "Regeneration locality", Name = "regeneration_locality_code", Type = typeof(string), Value = null, ControlType="DROPDOWN", ControlSource="REGENERATION_LOCALITY" },
            new EntityAttribute{ Caption = "Propagation Date", Name = "propagation_date", Type =  typeof(DateTime), Value=null, ControlType="DATETIME" },
            new EntityAttribute{ Caption = "Propagation Date Code", Name = "propagation_date_code", Type =  typeof(string), Value=null, ControlType="DROPDOWN", ControlSource="DATE_FORMAT"},

            new EntityAttribute{ Caption = "Is Default Inventory?", Name = "is_distributable", Type =  typeof(string), Value='N', ControlType="CHECKBOX", IsRequired = false},
            new EntityAttribute{ Caption = "Is Available?", Name = "is_available", Type = typeof(string), Value='N', ControlType="CHECKBOX", IsRequired = false},
            new EntityAttribute{ Caption = "Availability Status", Name = "availability_status_code", Type = typeof(string), Value=null, ControlType="DROPDOWN", ControlSource="INVENTORY_AVAILABILITY_STATUS", IsRequired=true},
            new EntityAttribute{ Caption = "Status Note", Name = "availability_status_note", Type =  typeof(string), Value=null, ControlType="STRING"},
            new EntityAttribute{ Caption = "Web Availability Note", Name = "web_availability_note", Type =  typeof(string), Value=null, ControlType="STRING"},
            
            /*new EntityAttribute{ Caption = "Availability Start Date", Name = "availability_start_date", Type =  typeof(DateTime), Value=null, ControlType="DATETIME" },
            new EntityAttribute{ Caption = "Availability End Date", Name = "availability_end_date", Type =  typeof(DateTime), Value=null, ControlType="DATETIME" },*/

            new EntityAttribute{ Caption = "Is Auto Deducted?", Name = "is_auto_deducted", Type =  typeof(string), Value='N', ControlType="CHECKBOX", IsRequired = false},
            new EntityAttribute{ Caption = "Pathogen Status", Name = "pathogen_status_code", Type =  typeof(string), Value=null, ControlType="DROPDOWN", ControlSource="PATHOGEN_STATUS" },

            new EntityAttribute{ Caption = "Preservation Method", Name = "preservation_method_id", Type = typeof(int?), Value=null, ControlType="LOOKUPPICKER", ControlSource="method_lookup"},
            new EntityAttribute{ Caption = "Regeneration Method", Name = "regeneration_method_id", Type = typeof(int?), Value=null, ControlType="LOOKUPPICKER", ControlSource="method_lookup"},
            
            new EntityAttribute{ Caption = "Note", Name = "note", Type =  typeof(string), Value=string.Empty, ControlType="STRING"},

            
        };

        private string _UxTitle;
        public string UxTitle
        {
            get { return _UxTitle; }
            set { SetProperty(ref _UxTitle, value); }
        }
        private string _UxToolbarItemSave;
        private List<InventoryAction> _inventoryActions = new List<InventoryAction>();

        public string UxToolbarItemSave
        {
            get { return _UxToolbarItemSave; }
            set { SetProperty(ref _UxToolbarItemSave, value); }
        }
        public InventoryPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService, IRestService restService, IDataStoreService dataStoreService)
            : base(navigationService, pageDialogService)
        {
            //_dtEntity.Columns.Add(new System.Data.DataColumn("inventory_id", typeof(string)));
            _restService = restService;
            _restClient = new RestClient();
            _dataStoreService = dataStoreService;

            SaveInventoryCommand = new DelegateCommand(OnSaveInventoryCommandExecuted);

            OpenLookupPickerCommand = new DelegateCommand<Object>(OnOpenLookupPickerCommandExecuted);
            TextPickerChangedCommand = new DelegateCommand<object>(OnTextPickerChangedCommand);

            CreateInventoryAction = false;
        }

        public DelegateCommand SaveInventoryCommand { get; }
        private async void OnSaveInventoryCommandExecuted()
        {
            try
            {
                /*foreach (var att in InventoryAtributeList)
                {
                    if (att.IsRequired)
                    {
                        if (att.Value == null)
                        {
                            throw new Exception(string.Format("{0} is required", att.Caption));
                        }
                        if (att.Value.GetType() == typeof(string) && att.Type != typeof(string) && string.IsNullOrWhiteSpace(att.Value.ToString()))
                        {
                            throw new Exception(string.Format("{0} is required", att.Caption));
                        }
                        if (att.Type == typeof(int) && (int)att.Value <= 0)
                        {
                            throw new Exception(string.Format("{0} is required", att.Caption));
                        }
                    }
                }*/
                foreach (var att in InventoryAtributeList)
                {
                    if (!att.IsReadOnly)
                    {
                        PropertyInfo[] props = typeof(Inventory).GetProperties();
                        var prop = props.FirstOrDefault(x => x.Name.Equals(att.Name));
                        if (prop != null)
                        {
                            if (att.ControlType.Equals("TEXTPICKER") )
                            {
                                string value;
                                if (att.IsPicker)
                                {
                                    value = att.Value == null ? null : (string)att.Value;
                                }
                                else {
                                    value = att.SecondValue; 
                                }
                                prop.SetValue(NewInventory, value);
                            }
                            else if (att.ControlType.Equals("DROPDOWN"))
                            {
                                if (att.CodeValueList != null && att.CodeValue != null)
                                    if (att.ControlSource.EndsWith("_lookup"))
                                    {
                                        prop.SetValue(NewInventory, (int)att.CodeValue.ValueMember);
                                    }
                                    else
                                    {
                                        prop.SetValue(NewInventory, (string)att.CodeValue.ValueMember);
                                    }
                                else
                                    prop.SetValue(NewInventory, null);
                            }
                            else if (att.ControlType.Equals("DATETIME"))
                            {
                                DateTime? value = (DateTime?)(att.IsPicker ? att.Value : null);
                                prop.SetValue(NewInventory, value);
                            }
                            else
                            {
                                //check if Value is null
                                if (att.Value == null)
                                {
                                    prop.SetValue(NewInventory, null);
                                }
                                else
                                {
                                    if (att.Value.GetType() == typeof(string)) //If value is empty and attribute type is different from string, assign null or 0?
                                    {
                                        TypeConverter converter = TypeDescriptor.GetConverter(att.Type);
                                        converter = TypeDescriptor.GetConverter(att.Type);
                                        var newValue = converter.ConvertFrom(att.Value);
                                        prop.SetValue(NewInventory, newValue);
                                    }
                                    else
                                    {
                                        if (att.ControlType.Equals("CHECKBOX") && att.Value.GetType() == typeof(bool))
                                        {
                                            if ((bool)att.Value)
                                                prop.SetValue(NewInventory, "Y");
                                            else
                                                prop.SetValue(NewInventory, "N");
                                        }
                                        else
                                            prop.SetValue(NewInventory, att.Value);
                                    }
                                }
                            }
                        }
                        if (att.Name.Equals("regeneration_locality_code") && att.CodeValue != null)
                        {
                            var regerationLocalityAction = _inventoryActions.FirstOrDefault(x => x.action_name_code.Equals("REGLOC"));
                            if (regerationLocalityAction != null)
                            {
                                regerationLocalityAction.note = att.CodeValue.ValueMember.ToString();
                            }
                            else
                            {
                                _inventoryActions.Add(new InventoryAction
                                {
                                    inventory_action_id = -1,
                                    inventory_id = NewInventory.inventory_id,
                                    action_name_code = "REGLOC",
                                    note = att.CodeValue.ValueMember.ToString()
                                });
                            }
                        }
                    }
                }
                
                string result = null;
                List<InventoryThumbnail> newInventoryList = null;
                if (_isNewInventory)
                {
                    NewInventory.workgroup_cooperator_id = Settings.WorkgroupCooperatorId;
                    result = await _restClient.CreateInventory(NewInventory);
                    await PageDialogService.DisplayAlertAsync( _dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "InventoryPage", "MessageSaveResults"), 
                        string.Format(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "InventoryPage", "MessageSuccessfullyCreated"), result), "OK");
                    newInventoryList = await _restClient.Search("@inventory.inventory_id = " + result, "get_mob_inventory_thumbnail", "inventory");
                }
                else
                {
                    result = await _restClient.UpdateInventory(NewInventory);
                    foreach (var inventoryAction in _inventoryActions)
                    {
                        if (inventoryAction.inventory_action_id > 0)
                            await _restService.UpdateInventoryActionAsync(inventoryAction);
                        else
                            await _restService.CreateInventoryActionAsync(inventoryAction);
                    }
                    

                    await PageDialogService.DisplayAlertAsync(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "InventoryPage", "MessageSaveResults"),
                        string.Format(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "InventoryPage", "MessageSuccessfullySaved"), result), "OK");
                    newInventoryList = await _restClient.Search("@inventory.inventory_id = " + NewInventory.inventory_id, "get_mob_inventory_thumbnail", "inventory");
                }
                InventoryThumbnail newInventory = newInventoryList[0];

                if (CreateInventoryAction)
                {
                    InventoryAction ia = new InventoryAction
                    {
                        inventory_action_id = -1,
                        inventory_id = newInventory.inventory_id,
                        action_name_code = "CREATED",
                        quantity = NewInventory.quantity_on_hand,
                        quantity_unit_code = NewInventory.quantity_on_hand_unit_code,
                        started_date = DateTime.Now,
                        method_id = null,
                        cooperator_id = Settings.UserCooperatorId,
                        note = string.Format("Inventory number:{0}\nForm:{1}\nLocation:{2}", newInventory.inventory_number, NewInventory.form_type_code, newInventory.storage_location)
                    };
                    await _restClient.CreateInventoryAction(ia);
                }

                var navigationParams = new NavigationParameters();
                navigationParams.Add("InventoryThumbnail", newInventory);

                await NavigationService.GoBackAsync(navigationParams);

            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "InventoryPage", "MessageSaveResultsError"), 
                    ex.Message, "OK");
            }
        }
        public DelegateCommand<Object> OpenLookupPickerCommand { get; }
        private async void OnOpenLookupPickerCommandExecuted(Object param)
        {
            try
            {
                var entityAttribute = (EntityAttribute)param;
                var navigationParams = new NavigationParameters
                {
                    { "EntityAttribute", param }
                };

                if (entityAttribute.Name.Equals("accession_id"))
                    await NavigationService.NavigateAsync("AccessionLookupPickerPage", navigationParams, true, true);
                else if (entityAttribute.Name.Equals("parent_inventory_id"))
                {
                    var attAccessionId = InventoryAtributeList.FirstOrDefault(x => x.Name.Equals("accession_id"));
                    if (string.IsNullOrEmpty(attAccessionId.DisplayValue))
                        throw new Exception("Accession number is empty");

                    navigationParams.Add("AccessionNumber", attAccessionId.DisplayValue);
                    navigationParams.Add("AccessionId", (int) attAccessionId.Value);
                    await NavigationService.NavigateAsync("LookupPickerPage", navigationParams, true, true);
                }
                else
                {
                    await NavigationService.NavigateAsync("LookupPickerPage", navigationParams, true, true);
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }

        public DelegateCommand<Object> TextPickerChangedCommand { get; }
        private async void OnTextPickerChangedCommand(Object param)
        {
            try
            {
                if (param == null) return;

                if (!(param is EntityAttribute)) return;

                var entityAttribute = (EntityAttribute)param;
                if (entityAttribute.Value == null) return;

                switch (entityAttribute.Name)
                {
                    case "storage_location_part1":
                        {
                            var attLocation2 = _inventoryAttributeArray.FirstOrDefault(att => att.Name.Equals("storage_location_part2"));
                            var attLocation3 = _inventoryAttributeArray.FirstOrDefault(att => att.Name.Equals("storage_location_part3"));
                            attLocation3.ListValues = new List<string>();

                            if (!string.IsNullOrEmpty(entityAttribute.Value.ToString()))
                                attLocation2.ListValues = await _restClient.GetAllLocation2List(entityAttribute.Value.ToString());
                            else
                            {
                                attLocation2.ListValues = new List<string>();
                            }
                        }
                        break;
                    case "storage_location_part2":
                        {
                            var attLocation1 = _inventoryAttributeArray.FirstOrDefault(att => att.Name.Equals("storage_location_part1"));
                            var attLocation3 = _inventoryAttributeArray.FirstOrDefault(att => att.Name.Equals("storage_location_part3"));
                            if (!string.IsNullOrEmpty(entityAttribute.Value.ToString()))
                                attLocation3.ListValues = await _restClient.GetAllLocation3List(attLocation1.Value.ToString(), entityAttribute.Value.ToString());
                            else
                                attLocation3.ListValues = new List<string>();
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        
        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            try
            {
                Utils.RefreshLang(this, _dataStoreService, "InventoryPage");

                if (_dataviewColumnList == null)
                {
                    _dataviewColumnList = await _restClient.GetDataviewAtributeList(Settings.WorkgroupInventoryDataview);
                    foreach (var dataviewColumn in _dataviewColumnList)
                    {
                        switch (dataviewColumn.field_name)
                        {
                            case "propagation_date":
                                dataviewColumn.title = "Fecha de regeneración";
                                break;
                            case "propagation_date_code":
                                dataviewColumn.title = "Formato de fecha de regeneración";
                                break;
                            case "storage_location_part4":
                                dataviewColumn.title = "Tipo de contenedor";
                                break;
                            case "inventory_id":
                                dataviewColumn.title = "Id de inventario";
                                break;
                            case "form_type_code":
                                dataviewColumn.title = "Forma";
                                break;
                            case "inventory_maint_policy_id":
                                dataviewColumn.title = "Política de mantenimiento";
                                break;
                            case "storage_location_part1":
                                dataviewColumn.title = "Ubicación 1";
                                break;
                            case "storage_location_part2":
                                dataviewColumn.title = "Ubicación 2";
                                break;
                            case "storage_location_part3":
                                dataviewColumn.title = "Ubicación 3";
                                break;
                            case "parent_inventory_id":
                                dataviewColumn.title = "Inventario de procedencia";
                                break;
                            case "web_availability_note":
                                dataviewColumn.title = "Nota web de disponibilidad";
                                break;
                            case "pollination_vector_code":
                                dataviewColumn.title = "Vector de polinización";
                                break;
                            case "preservation_method_id":
                                dataviewColumn.title = "Método de preservación";
                                break;
                            case "regeneration_method_id":
                                dataviewColumn.title = "Método de regeneración";
                                break;
                            //case "regeneration_locality_code":
                            //    dataviewColumn.title = "Localidad de regeneración";
                            //    break;
                            case "quantity_on_hand_unit_code":
                                dataviewColumn.title = "Unidad de cantidad disponible";
                                break;
                            //case "":
                            //    dataviewColumn.title = "";
                            //    break;
                        }
                    }
                }

                InventoryThumbnail tempInventory;

                if (parameters.ContainsKey("InventoryThumbnail"))
                {
                    IsNewInventory = false;
                    tempInventory = (InventoryThumbnail)parameters["InventoryThumbnail"];
                    NewInventory = await _restClient.ReadInventory(tempInventory.inventory_id);

                    PropertyInfo[] props = typeof(Inventory).GetProperties();
                    foreach (PropertyInfo prop in props)
                    {
                        var att = _inventoryAttributeArray.FirstOrDefault(x => x.Name.Equals(prop.Name));
                        if (att != null)
                        {
                            att.Value = prop.GetValue(NewInventory, null);

                            if (att.ControlType.Equals("DROPDOWN"))
                            {
                                if (att.ControlSource.EndsWith("_lookup"))
                                {
                                    att.CodeValueList = await _restClient.GetLookupList(att.ControlSource);
                                }
                                else
                                {
                                    att.CodeValueList = await _restClient.GetCodeValueByGroupName(att.ControlSource);
                                }
                                //Apply gui_filter
                                if (_dataviewColumnList != null)
                                {
                                    var dvField = _dataviewColumnList.FirstOrDefault(f => f.field_name.Equals(att.Name));
                                    if (dvField != null && !string.IsNullOrEmpty(dvField.gui_filter))
                                    {
                                        //var filterList = dvField.gui_filter.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                        var filterList = dvField.gui_filter.Split(new char[] { ',' }, StringSplitOptions.None);

                                        var filteredCodeValueList = new List<ILookup>();
                                        foreach (var filter in filterList)
                                        {
                                            var iLookup = att.CodeValueList.FirstOrDefault(x => x.ValueMember.Equals(filter));
                                            if (iLookup != null)
                                                filteredCodeValueList.Add(iLookup);
                                            if (string.IsNullOrEmpty(filter))
                                                filteredCodeValueList.Add(new CodeValueLookup() { value_member = "", display_member = "" });
                                        }
                                        att.CodeValueList = filteredCodeValueList;

                                        /*if (string.IsNullOrEmpty(filterList[0]))
                                            att.CodeValue = att.CodeValueList.FirstOrDefault();
                                        else
                                            att.CodeValue = null;*/

                                        /*for (int i = att.CodeValueList.Count - 1; i >= 0; i--)
                                        {
                                            if (!filterList.Any(x => x.Equals(att.CodeValueList[i].ValueMember.ToString())))
                                            {
                                                att.CodeValueList.RemoveAt(i);
                                            }
                                        }*/
                                    }
                                }
                                if (att.CodeValueList != null && att.CodeValueList.Count > 0 && att.Value != null)
                                    att.CodeValue = att.CodeValueList.FirstOrDefault(c => c.ValueMember.ToString().Equals(att.Value.ToString()));
                            }
                            else if (att.ControlType.Equals("LOOKUPPICKER") && att.Value != null && (int)att.Value > 0)
                            {
                                var displayName = await _restClient.GetLookupByValueMember(att.ControlSource, (int)att.Value);
                                att.DisplayValue = displayName;
                            }
                            else if (att.ControlType.Equals("DATETIME"))
                            {
                                if(att.Value == null)
                                {
                                    att.Value = DateTime.Now;
                                }
                            }

                            //update atribute title
                            var dataviewColumn = _dataviewColumnList.AsEnumerable().FirstOrDefault(x => x.field_name.Equals(att.Name));
                            if (dataviewColumn != null)
                                att.Caption = dataviewColumn.title;
                        }
                        //else 
                        //{
                        //    if (prop.Name.Equals("regeneration_locality_code"))
                        //    {

                        //    }
                        //}
                    }
                    //Atributes saved as inventory action
                    _inventoryActions = (await _restService.GetInventoryActionsByInventoryIdAndActionCodesAsync(tempInventory.inventory_id, new string[] { "REGLOC" })).ToList();
                    var regenerationLocalityCode = _inventoryAttributeArray.FirstOrDefault(att => att.Name.Equals("regeneration_locality_code"));
                    regenerationLocalityCode.CodeValueList = await _restClient.GetCodeValueByGroupName(regenerationLocalityCode.ControlSource);
                    regenerationLocalityCode.Caption = "Localidad de regeneración";
                    var regerationLocalityAction = _inventoryActions.FirstOrDefault(x => x.action_name_code.Equals("REGLOC"));
                    if (regerationLocalityAction != null)
                    {
                        regenerationLocalityCode.Value = regerationLocalityAction.note;
                        regenerationLocalityCode.CodeValue = regenerationLocalityCode.CodeValueList.FirstOrDefault(x => x.ValueMember.ToString().Equals(regerationLocalityAction.note));
                    }

                    var attLocation1 = _inventoryAttributeArray.FirstOrDefault(att => att.Name.Equals("storage_location_part1"));
                    attLocation1.ControlType = "TEXTPICKER";
                    attLocation1.ListValues = await _restClient.GetAllLocation1List();
                    attLocation1.IsPicker = true;
                    var attLocation2 = _inventoryAttributeArray.FirstOrDefault(att => att.Name.Equals("storage_location_part2"));
                    attLocation2.ControlType = "TEXTPICKER";
                    attLocation2.IsPicker = true;
                    var attLocation3 = _inventoryAttributeArray.FirstOrDefault(att => att.Name.Equals("storage_location_part3"));
                    attLocation3.ControlType = "TEXTPICKER";
                    attLocation3.IsPicker = true;
                    if (attLocation1.Value != null)
                    {
                        attLocation2.ListValues = await _restClient.GetAllLocation2List(attLocation1.Value.ToString());
                        if (attLocation2.Value != null)
                            attLocation3.ListValues = await _restClient.GetAllLocation3List(attLocation1.Value.ToString(), attLocation2.Value.ToString());
                        else
                            attLocation3.ListValues = new List<string>();
                    }
                    else
                    {
                        attLocation2.ListValues = new List<string>();
                        attLocation3.ListValues = new List<string>();
                    }
                    var attInventoryMaintPolicy = _inventoryAttributeArray.FirstOrDefault(att => att.Name.Equals("inventory_maint_policy_id"));
                    attInventoryMaintPolicy.ControlType = "DROPDOWN";
                    attInventoryMaintPolicy.CodeValueList = await _restClient.GetLookupList(attInventoryMaintPolicy.ControlSource);

                    var attLocation4 = _inventoryAttributeArray.FirstOrDefault(att => att.Name.Equals("storage_location_part4"));
                    attLocation4.ControlType = "TEXTPICKER";
                    attLocation4.ListValues = (List<string>)await _restService.GetInventoryStorageLocationPart4Distinct();
                    attLocation4.IsPicker = true;
                    var propagationDate = _inventoryAttributeArray.FirstOrDefault(att => att.Name.Equals("propagation_date"));
                    propagationDate.IsPicker = NewInventory.propagation_date != null;

                    //Apply 
                    if (!string.IsNullOrEmpty(Settings.WorkgroupInvMaintPolicies))
                    {
                        var filterList = Settings.WorkgroupInvMaintPolicies.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = attInventoryMaintPolicy.CodeValueList.Count - 1; i >= 0; i--)
                        {
                            if (!filterList.Any(x => x.Equals(attInventoryMaintPolicy.CodeValueList[i].ValueMember.ToString())))
                            {
                                attInventoryMaintPolicy.CodeValueList.RemoveAt(i);
                            }
                        }
                    }
                    if (attInventoryMaintPolicy.CodeValueList != null && attInventoryMaintPolicy.CodeValueList.Count > 0 && attInventoryMaintPolicy.Value != null)
                        attInventoryMaintPolicy.CodeValue = attInventoryMaintPolicy.CodeValueList.FirstOrDefault(c => c.ValueMember.ToString().Equals(attInventoryMaintPolicy.Value.ToString()));

                    InventoryAtributeList = new ObservableCollection<EntityAttribute>(_inventoryAttributeArray.ToList());
                }

                if (parameters.ContainsKey("inventory"))
                {
                    IsNewInventory = false;
                    Inventory = (InventoryThumbnail)parameters["inventory"];

                    PropertyInfo[] props = typeof(InventoryThumbnail).GetProperties();
                    foreach (PropertyInfo prop in props)
                    {
                        var att = _inventoryAttributeArray.FirstOrDefault(x => x.Name.Equals(prop.Name));
                        if (att != null)
                        {
                            att.Value = prop.GetValue(Inventory, null);
                        }
                    }
                    InventoryAtributeList = new ObservableCollection<EntityAttribute>(_inventoryAttributeArray.ToList());
                }
                //Updete from LookupPicker
                if (parameters.ContainsKey("LookupPicker"))
                {
                    EntityAttribute tempAtt = (EntityAttribute)parameters["LookupPicker"];
                    /*
                    int index = InventoryAtributeList.IndexOf(tempAtt);
                    if (index > -1) 
                    {
                        InventoryAtributeList[index] = tempAtt;
                        //InventoryAtributeList.RemoveAt(index);
                        //InventoryAtributeList.Insert(index, tempAtt);
                    }
                    */
                    /*
                    var att = InventoryAtributeList.FirstOrDefault(a => a.Name.Equals(tempAtt.Name));
                    if (att != null)
                    {
                        att.Value = tempAtt.Value;
                    }
                    */
                }
                
                if (IsNewInventory && NewInventory == null)
                {
                    NewInventory = new Inventory() { inventory_id = -1, quantity_on_hand = 0 };

                    PropertyInfo[] props = typeof(Inventory).GetProperties();
                    foreach (PropertyInfo prop in props)
                    {
                        var att = _inventoryAttributeArray.FirstOrDefault(x => x.Name.Equals(prop.Name));
                        if (att != null)
                        {
                            att.Value = prop.GetValue(NewInventory, null);
                            if (att.ControlType.Equals("DROPDOWN"))
                            {
                                if(att.ControlSource.EndsWith("_lookup"))
                                {
                                    att.CodeValueList = await _restClient.GetLookupList(att.ControlSource);
                                }
                                else
                                {
                                    att.CodeValueList = await _restClient.GetCodeValueByGroupName(att.ControlSource);
                                }
                                //Apply gui_filter
                                if (_dataviewColumnList != null)
                                {
                                    var dvField = _dataviewColumnList.FirstOrDefault(f => f.field_name.Equals(att.Name));
                                    if (dvField != null && !string.IsNullOrEmpty(dvField.gui_filter))
                                    {
                                        var filterList = dvField.gui_filter.Split(new char[] { ',' }, StringSplitOptions.None);
                                        var filteredCodeValueList = new List<ILookup>();
                                        foreach (var filter in filterList)
                                        {
                                            var iLookup = att.CodeValueList.FirstOrDefault(x => x.ValueMember.Equals(filter));
                                            if (iLookup != null)
                                                filteredCodeValueList.Add(iLookup);
                                            if (string.IsNullOrEmpty(filter))
                                                filteredCodeValueList.Add(new CodeValueLookup() { value_member = "", display_member=""});
                                        }
                                        att.CodeValueList = filteredCodeValueList;

                                        if (!string.IsNullOrEmpty(filterList[0]))
                                            att.CodeValue = att.CodeValueList.FirstOrDefault();
                                        else
                                            att.CodeValue = null;

                                        /*var filterList = dvField.gui_filter.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                        for (int i = att.CodeValueList.Count - 1; i >= 0; i--)
                                        {
                                            if (!filterList.Any(x => x.Equals(att.CodeValueList[i].ValueMember.ToString())))
                                            {
                                                att.CodeValueList.RemoveAt(i);
                                            }
                                        }*/
                                    }
                                    else
                                        if (att.CodeValueList != null && att.CodeValueList.Count > 0)
                                            att.CodeValue = att.CodeValueList[0];
                                }
                                /*if (att.CodeValueList != null && att.CodeValueList.Count > 0)
                                    att.CodeValue = att.CodeValueList[0];*/
                            }
                            else if (att.ControlType.Equals("CHECKBOX"))
                            {
                                att.Value = "N";
                            }

                            //update atribute title
                            var dataviewColumn = _dataviewColumnList.AsEnumerable().FirstOrDefault(x => x.field_name.Equals(att.Name));
                            if (dataviewColumn != null)
                                att.Caption = dataviewColumn.title;
                        }
                    }

                    var availability_status_code = _inventoryAttributeArray.FirstOrDefault(att => att.Name.Equals("availability_status_code"));
                    availability_status_code.CodeValue = availability_status_code.CodeValueList.FirstOrDefault(x => x.ValueMember.Equals("UNKN"));
                    var pollination_method_code = _inventoryAttributeArray.FirstOrDefault(att => att.Name.Equals("pollination_method_code"));
                    pollination_method_code.CodeValue = null;
                    var pathogen_status_code = _inventoryAttributeArray.FirstOrDefault(att => att.Name.Equals("pathogen_status_code"));
                    pathogen_status_code.CodeValue = pathogen_status_code.CodeValueList.FirstOrDefault(x => x.ValueMember.Equals("Unknown"));

                    var attLocation1 = _inventoryAttributeArray.FirstOrDefault(att => att.Name.Equals("storage_location_part1"));
                    attLocation1.ControlType = "TEXTPICKER";
                    attLocation1.ListValues = await _restClient.GetAllLocation1List();
                    attLocation1.IsPicker = true;
                    var attLocation2 = _inventoryAttributeArray.FirstOrDefault(att => att.Name.Equals("storage_location_part2"));
                    attLocation2.ControlType = "TEXTPICKER";
                    attLocation2.IsPicker = true;
                    var attLocation3 = _inventoryAttributeArray.FirstOrDefault(att => att.Name.Equals("storage_location_part3"));
                    attLocation3.ControlType = "TEXTPICKER";
                    attLocation3.IsPicker = true;
                    if (attLocation1.Value != null)
                    {
                        attLocation2.ListValues = await _restClient.GetAllLocation2List(attLocation1.Value.ToString());
                        if(attLocation2.Value != null)
                            attLocation3.ListValues = await _restClient.GetAllLocation3List(attLocation1.Value.ToString(), attLocation2.Value.ToString());
                        else
                            attLocation3.ListValues = new List<string>();
                    }
                    else
                    {
                        attLocation2.ListValues = new List<string>();
                        attLocation3.ListValues = new List<string>();
                    }
                    var attInventoryMaintPolicy = _inventoryAttributeArray.FirstOrDefault(att => att.Name.Equals("inventory_maint_policy_id"));
                    attInventoryMaintPolicy.ControlType = "DROPDOWN";
                    attInventoryMaintPolicy.CodeValueList = await _restClient.GetLookupList(attInventoryMaintPolicy.ControlSource);

                    var attLocation4 = _inventoryAttributeArray.FirstOrDefault(att => att.Name.Equals("storage_location_part4"));
                    attLocation4.ControlType = "TEXTPICKER";
                    attLocation4.ListValues = (List<string>)await _restService.GetInventoryStorageLocationPart4Distinct();
                    attLocation4.IsPicker = true;

                    //Apply 
                    if (!string.IsNullOrEmpty(Settings.WorkgroupInvMaintPolicies))
                    {
                        var filterList = Settings.WorkgroupInvMaintPolicies.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = attInventoryMaintPolicy.CodeValueList.Count - 1; i >= 0; i--)
                        {
                            if (!filterList.Any(x => x.Equals(attInventoryMaintPolicy.CodeValueList[i].ValueMember.ToString())))
                            {
                                attInventoryMaintPolicy.CodeValueList.RemoveAt(i);
                            }
                        }
                    }
                    if (attInventoryMaintPolicy.CodeValueList != null && attInventoryMaintPolicy.CodeValueList.Count > 0)
                        attInventoryMaintPolicy.CodeValue = attInventoryMaintPolicy.CodeValueList[0];

                    InventoryAtributeList = new ObservableCollection<EntityAttribute>(_inventoryAttributeArray.ToList());
                }

                if (_isNewInventory) CreateInventoryAction = true;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
    }
}
