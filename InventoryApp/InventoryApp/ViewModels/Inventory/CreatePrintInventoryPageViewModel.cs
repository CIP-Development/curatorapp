﻿using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using InventoryApp.Models.Database;
using InventoryApp.Services;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InventoryApp.ViewModels
{
    public class CreatePrintInventoryPageViewModel : ViewModelBaseWithDialog
    {
        IDialogService _dialogService;
        IDataStoreService _dataStoreService;
        RestClient _restClient;
        private List<InventoryThumbnail> _inventoryList;
        private List<Location> _location;
        private int _discountMethodId;
        
        private int _labelIndex;
        private int _endRecordIndex;
        private List<string> _labelVariables = new List<string>();

        #region Properties
        private DateTime _propagationDate;
        public DateTime PropagationDate
        {
            get { return _propagationDate; }
            set { SetProperty(ref _propagationDate, value); }
        }
        private ILookup _inventoryMaintPolicy;
        public ILookup InventoryMaintPolicy
        {
            get { return _inventoryMaintPolicy; }
            set { SetProperty(ref _inventoryMaintPolicy, value); }
        }
        private List<ILookup> _inventoryMaintPolicyList;
        public List<ILookup> InventoryMaintPolicyList
        {
            get { return _inventoryMaintPolicyList; }
            set { SetProperty(ref _inventoryMaintPolicyList, value); }
        }
        private List<ILookup> _inventoryFormList;
        public List<ILookup> InventoryFormList
        {
            get { return _inventoryFormList; }
            set { SetProperty(ref _inventoryFormList, value); }
        }
        private ILookup _inventoryForm;
        public ILookup InventoryForm
        {
            get { return _inventoryForm; }
            set { SetProperty(ref _inventoryForm, value); }
        }
        private decimal _quantity;
        public decimal Quantity
        {
            get { return _quantity; }
            set { SetProperty(ref _quantity, value); }
        }
        private ILookup _quantityUnit;
        public ILookup QuantityUnit
        {
            get { return _quantityUnit; }
            set { SetProperty(ref _quantityUnit, value); }
        }
        private List<ILookup> _quantityUnitList;
        public List<ILookup> QuantityUnitList
        {
            get { return _quantityUnitList; }
            set { SetProperty(ref _quantityUnitList, value); }
        }
        private bool _isPicker1;
        public bool IsPicker1
        {
            get { return _isPicker1; }
            set { SetProperty(ref _isPicker1, value); }
        }
        private bool _isPicker2;
        public bool IsPicker2
        {
            get { return _isPicker2; }
            set { SetProperty(ref _isPicker2, value); }
        }
        private bool _isPicker3;
        public bool IsPicker3
        {
            get { return _isPicker3; }
            set { SetProperty(ref _isPicker3, value); }
        }
        private bool _isPicker4;
        public bool IsPicker4
        {
            get { return _isPicker4; }
            set { SetProperty(ref _isPicker4, value); }
        }
        private string _location1Text;
        public string Location1Text
        {
            get { return _location1Text; }
            set { SetProperty(ref _location1Text, value); }
        }
        private string _Location2Text;
        public string Location2Text
        {
            get { return _Location2Text; }
            set { SetProperty(ref _Location2Text, value); }
        }
        private string _Location3Text;
        public string Location3Text
        {
            get { return _Location3Text; }
            set { SetProperty(ref _Location3Text, value); }
        }
        private string _Location4Text;
        public string Location4Text
        {
            get { return _Location4Text; }
            set { SetProperty(ref _Location4Text, value); }
        }
        private string _location1;
        public string Location1
        {
            get { return _location1; }
            set { SetProperty(ref _location1, value); }
        }
        private string _location2;
        public string Location2
        {
            get { return _location2; }
            set { SetProperty(ref _location2, value); }
        }
        private string _location3;
        public string Location3
        {
            get { return _location3; }
            set { SetProperty(ref _location3, value); }
        }
        private string _location4;
        public string Location4
        {
            get { return _location4; }
            set { SetProperty(ref _location4, value); }
        }
        private List<string> _listLocation1;
        public List<string> ListLocation1
        {
            get { return _listLocation1; }
            set { SetProperty(ref _listLocation1, value); }
        }
        private List<string> _listLocation2;
        public List<string> ListLocation2
        {
            get { return _listLocation2; }
            set { SetProperty(ref _listLocation2, value); }
        }
        private List<string> _listLocation3;
        public List<string> ListLocation3
        {
            get { return _listLocation3; }
            set { SetProperty(ref _listLocation3, value); }
        }
        private List<string> _listLocation4;
        public List<string> ListLocation4
        {
            get { return _listLocation4; }
            set { SetProperty(ref _listLocation4, value); }
        }
        private string _notes;
        public string Notes
        {
            get { return _notes; }
            set { SetProperty(ref _notes, value); }
        }
        private ILookup _propagatorCooperator;
        public ILookup PropagatorCooperator
        {
            get { return _propagatorCooperator; }
            set { SetProperty(ref _propagatorCooperator, value); }
        }
        private List<ILookup> _propagatorCooperatorList;
        public List<ILookup> PropagatorCooperatorList
        {
            get { return _propagatorCooperatorList; }
            set { SetProperty(ref _propagatorCooperatorList, value); }
        }
        private ILookup _regeneration;
        public ILookup Regeneration
        {
            get { return _regeneration; }
            set { SetProperty(ref _regeneration, value); }
        }
        private List<ILookup> _regenerationList;
        public List<ILookup> RegenerationList
        {
            get { return _regenerationList; }
            set { SetProperty(ref _regenerationList, value); }
        }
        private ILookup _preservation;
        public ILookup Preservation
        {
            get { return _preservation; }
            set { SetProperty(ref _preservation, value); }
        }
        private List<ILookup> _preservationList;
        public List<ILookup> PreservationList
        {
            get { return _preservationList; }
            set { SetProperty(ref _preservationList, value); }
        }
        private bool _clearPreviousInventories;
        public bool ClearPreviousInventories
        {
            get { return _clearPreviousInventories; }
            set { SetProperty(ref _clearPreviousInventories, value); }
        }
        private bool _registerCreateInventoryAction;
        public bool RegisterCreateInventoryAction
        {
            get { return _registerCreateInventoryAction; }
            set { SetProperty(ref _registerCreateInventoryAction, value); }
        }

        private Printer _printer;
        public Printer Printer
        {
            get { return _printer; }
            set { SetProperty(ref _printer, value); }
        }
        private LabelTemplate _labelTemplate;
        public LabelTemplate LabelTemplate
        {
            get { return _labelTemplate; }
            set { SetProperty(ref _labelTemplate, value); }
        }
        private List<Printer> _printerList;
        public List<Printer> PrinterList
        {
            get { return _printerList; }
            set { SetProperty(ref _printerList, value); }
        }
        private List<LabelTemplate> _labelTemplateList;
        public List<LabelTemplate> LabelTemplateList
        {
            get { return _labelTemplateList; }
            set { SetProperty(ref _labelTemplateList, value); }
        }
        private int _labelsPerInventory;
        public int LabelsPerInventory
        {
            get { return _labelsPerInventory; }
            set { SetProperty(ref _labelsPerInventory, value); }
        }
        private bool _registerInventoryPrintingAction;
        public bool RegisterInventoryPrintingAction
        {
            get { return _registerInventoryPrintingAction; }
            set { SetProperty(ref _registerInventoryPrintingAction, value); }
        }

        private bool _registerInventoryDiscountAction;
        public bool RegisterInventoryDiscountAction
        {
            get { return _registerInventoryDiscountAction; }
            set { SetProperty(ref _registerInventoryDiscountAction, value); }
        }
        private decimal _discountQuantity;
        public decimal DiscountQuantity
        {
            get { return _discountQuantity; }
            set { SetProperty(ref _discountQuantity, value); }
        }
        private ILookup _actionJustification;
        public ILookup ActionJustification
        {
            get { return _actionJustification; }
            set { SetProperty(ref _actionJustification, value); }
        }
        private IList<ILookup> _actionJustificationList;
        public IList<ILookup> ActionJustificationList
        {
            get { return _actionJustificationList; }
            set { SetProperty(ref _actionJustificationList, value); }
        }
        #endregion

        #region Lang
        private string _UxTitle;
        public string UxTitle
        {
            get { return _UxTitle; }
            set { SetProperty(ref _UxTitle, value); }
        }
        private string _UxLabelPropagationDate;
        public string UxLabelPropagationDate
        {
            get { return _UxLabelPropagationDate; }
            set { SetProperty(ref _UxLabelPropagationDate, value); }
        }
        private string _UxLabelInventoryMaintPolicy;
        public string UxLabelInventoryMaintPolicy
        {
            get { return _UxLabelInventoryMaintPolicy; }
            set { SetProperty(ref _UxLabelInventoryMaintPolicy, value); }
        }
        private string _UxLabelInventoryFormCode;
        public string UxLabelInventoryFormCode
        {
            get { return _UxLabelInventoryFormCode; }
            set { SetProperty(ref _UxLabelInventoryFormCode, value); }
        }
        private string _UxLabelQuantity;
        public string UxLabelQuantity
        {
            get { return _UxLabelQuantity; }
            set { SetProperty(ref _UxLabelQuantity, value); }
        }
        private string _UxLabelQuantityUnit;
        public string UxLabelQuantityUnit
        {
            get { return _UxLabelQuantityUnit; }
            set { SetProperty(ref _UxLabelQuantityUnit, value); }
        }
        private string _UxLabelRegeneration;
        public string UxLabelRegeneration
        {
            get { return _UxLabelRegeneration; }
            set { SetProperty(ref _UxLabelRegeneration, value); }
        }
        private string _UxLabelPreservation;
        public string UxLabelPreservation
        {
            get { return _UxLabelPreservation; }
            set { SetProperty(ref _UxLabelPreservation, value); }
        }
        private string _UxLabelLocation1;
        public string UxLabelLocation1
        {
            get { return _UxLabelLocation1; }
            set { SetProperty(ref _UxLabelLocation1, value); }
        }
        private string _UxLabelLocation2;
        public string UxLabelLocation2
        {
            get { return _UxLabelLocation2; }
            set { SetProperty(ref _UxLabelLocation2, value); }
        }
        private string _UxLabelLocation3;
        public string UxLabelLocation3
        {
            get { return _UxLabelLocation3; }
            set { SetProperty(ref _UxLabelLocation3, value); }
        }
        private string _UxLabelLocation4;
        public string UxLabelLocation4
        {
            get { return _UxLabelLocation4; }
            set { SetProperty(ref _UxLabelLocation4, value); }
        }
        private string _UxLabelNotes;
        public string UxLabelNotes
        {
            get { return _UxLabelNotes; }
            set { SetProperty(ref _UxLabelNotes, value); }
        }
        private string _UxLabelPropagator;
        public string UxLabelPropagator
        {
            get { return _UxLabelPropagator; }
            set { SetProperty(ref _UxLabelPropagator, value); }
        }
        private string _UxLabelClearPreviousInventories;
        public string UxLabelClearPreviousInventories
        {
            get { return _UxLabelClearPreviousInventories; }
            set { SetProperty(ref _UxLabelClearPreviousInventories, value); }
        }
        private string _UxLabelRegisterCreateInventoryAction;
        public string UxLabelRegisterCreateInventoryAction
        {
            get { return _UxLabelRegisterCreateInventoryAction; }
            set { SetProperty(ref _UxLabelRegisterCreateInventoryAction, value); }
        }
        private string _UxButtonCreateAndPrint;
        public string UxButtonCreateAndPrint
        {
            get { return _UxButtonCreateAndPrint; }
            set { SetProperty(ref _UxButtonCreateAndPrint, value); }
        }

        private string _UxLabelPrinter;
        public string UxLabelPrinter
        {
            get { return _UxLabelPrinter; }
            set { SetProperty(ref _UxLabelPrinter, value); }
        }
        private string _UxLabelLabelDesign;
        public string UxLabelLabelDesign
        {
            get { return _UxLabelLabelDesign; }
            set { SetProperty(ref _UxLabelLabelDesign, value); }
        }
        private string _UxLabelLabelsPerInventory;
        public string UxLabelLabelsPerInventory
        {
            get { return _UxLabelLabelsPerInventory; }
            set { SetProperty(ref _UxLabelLabelsPerInventory, value); }
        }
        private string _UxLabelRegisterInventoryPrintingAction;
        public string UxLabelRegisterInventoryPrintingAction
        {
            get { return _UxLabelRegisterInventoryPrintingAction; }
            set { SetProperty(ref _UxLabelRegisterInventoryPrintingAction, value); }
        }

        private string _UxLabelRegisterInventoryDiscountAction;
        public string UxLabelRegisterInventoryDiscountAction
        {
            get { return _UxLabelRegisterInventoryDiscountAction; }
            set { SetProperty(ref _UxLabelRegisterInventoryDiscountAction, value); }
        }
        private string _UxLabelDiscountQuantity;
        public string UxLabelDiscountQuantity
        {
            get { return _UxLabelDiscountQuantity; }
            set { SetProperty(ref _UxLabelDiscountQuantity, value); }
        }
        private string _UxLabelActionJustification;
        public string UxLabelActionJustification
        {
            get { return _UxLabelActionJustification; }
            set { SetProperty(ref _UxLabelActionJustification, value); }
        }
        #endregion

        public CreatePrintInventoryPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService, IDialogService dialogService, IDataStoreService dataStoreService)
            : base(navigationService, pageDialogService)
        {
            _dialogService = dialogService;
            _dataStoreService = dataStoreService;
            _restClient = new RestClient();
            PropagationDate = DateTime.Now;
            Quantity = 0;

            IsPicker1 = true;
            IsPicker2 = true;
            IsPicker3 = true;
            IsPicker4 = false;

            RegisterInventoryDiscountAction = false;
            RegisterCreateInventoryAction = true;
            LabelsPerInventory = 1;
            
            ListLocation1ChangedCommand = new DelegateCommand(OnListLocation1ChangedCommandExecuted);
            ListLocation2ChangedCommand = new DelegateCommand(OnListLocation2ChangedCommandExecuted);
            CreateAndPrintCommand = new DelegateCommand(OnCreateAndPrintCommand);
        }

        public DelegateCommand ListLocation1ChangedCommand { get; }
        private async void OnListLocation1ChangedCommandExecuted()
        {
            try
            {
                if (Location1 != null)
                {
                    /*ListLocation2 = _location.Where(l => l.storage_location_part1.Equals(Location1) && !string.IsNullOrEmpty(l.storage_location_part2))
                        .Select(l => l.storage_location_part2).Distinct().ToList();*/
                    ListLocation3 = new List<string>();

                    if (!string.IsNullOrEmpty(Location1))
                        ListLocation2 = await _restClient.GetAllLocation2List(Location1);
                    else
                    {
                        ListLocation2 = new List<string>();
                    }
                }
            }
            catch (Exception e)
            {
                await PageDialogService.DisplayAlertAsync("Error", e.Message, "OK");
            }
        }
        public DelegateCommand ListLocation2ChangedCommand { get; }
        private async void OnListLocation2ChangedCommandExecuted()
        {
            try
            {
                if (Location2 != null)
                {
                    /*ListLocation3 = _location.Where(l => l.storage_location_part1.Equals(Location1) && l.storage_location_part2.Equals(Location2) && !string.IsNullOrEmpty(l.storage_location_part3))
                        .Select(l => l.storage_location_part3).Distinct().ToList();*/

                    if (!string.IsNullOrEmpty(Location2))
                        ListLocation3 = await _restClient.GetAllLocation3List(Location1, Location2);
                    else
                        ListLocation3 = new List<string>();
                }
            }
            catch (Exception e)
            {
                await PageDialogService.DisplayAlertAsync("Error", e.Message, "OK");
            }
        }
        public DelegateCommand CreateAndPrintCommand { get; }
        private async void OnCreateAndPrintCommand()
        {
            try
            {
                List<InventoryThumbnail> newInventoryThumbnailList = new List<InventoryThumbnail>();

                if (InventoryForm == null) throw new Exception("InventoryForm");
                if (InventoryMaintPolicy == null) throw new Exception("InventoryMaintPolicy");
                if (PropagationDate == null) throw new Exception("PropagationDate");
                if (Quantity <= 0) throw new Exception("Quantity");
                if (QuantityUnit == null) throw new Exception("QuantityUnit");
                if (Regeneration == null) throw new Exception("Regeneration");
                if (Preservation == null) throw new Exception("Preservation");

                if (Printer == null) throw new Exception("Printer is empty");
                if (LabelTemplate == null) throw new Exception("Label template is empty");
                if (_inventoryList == null || _inventoryList.Count == 0) throw new Exception("Printing inventory list is empty");

                List<string> errorMessageList = new List<string>();

                string param1 = string.Join(",", _inventoryList);
                string param2 = PropagationDate.ToString("MM/dd/yyyy");
                var propagatedInventories = await _restClient.GetInventoryLookupListByParentId(string.Join(",", _inventoryList.Select(x=>x.inventory_id)), PropagationDate.ToString("MM/dd/yyyy"));
               //inventoryList.ForEach(x => x.TaskStatus = propagatedInventories.Any(y => y.DisplayMember.Equals(x.parent_inventory_id)) ? "Created" : "Pending");

                foreach (var item in _inventoryList)
                {
                    if (propagatedInventories.Any(x => x.DisplayMember.Equals(item.inventory_id.ToString())))
                        item.TaskStatus = "Created";
                    else
                        item.TaskStatus = "Pending";

                }

                var existingList = _inventoryList.FindAll(x => x.TaskStatus.Equals("Created"));
                if (existingList.Count > 0)
                {
                    //Get existing propagated inventory from server
                    var existingInventoryList = await _restClient.Search("@inventory.inventory_id in (" + string.Join(",", existingList.Select(x => x.inventory_id).ToList()) + ")", "get_mob_inventory_thumbnail", "inventory");
                    if (existingInventoryList != null)
                    {
                        foreach (InventoryThumbnail inventoryThumbnail in existingInventoryList)
                        {
                            //Add to result list
                            newInventoryThumbnailList.Add(inventoryThumbnail);

                            //Update inventory
                            //await _restClient.UpdateInventory(inventoryThumbnail);
                        }
                    }
                }
                
                //Create inventory
                foreach (InventoryThumbnail inventoryThumbnail in _inventoryList.Where(x=>x.TaskStatus.Equals("Pending")))
                {
                    Inventory NewInventory = null;
                    string resultInventoryId = null;

                    //Create inventory
                    try
                    {
                        NewInventory = new Inventory()
                        {
                            accession_id = inventoryThumbnail.accession_id,
                            form_type_code = InventoryForm.ValueMember.ToString(),
                            inventory_id = -1,
                            inventory_maint_policy_id = (int)InventoryMaintPolicy.ValueMember,
                            inventory_number_part1 = Guid.NewGuid().ToString(),
                            inventory_number_part2 = null,
                            inventory_number_part3 = string.Empty,
                            note = Notes,
                            parent_inventory_id = inventoryThumbnail.inventory_id,
                            propagation_date = PropagationDate,
                            propagation_date_code = "",
                            quantity_on_hand = Quantity,
                            quantity_on_hand_unit_code = QuantityUnit.ValueMember.ToString(),
                            storage_location_part1 = IsPicker1 ? Location1 : Location1Text,
                            storage_location_part2 = IsPicker2 ? Location2 : Location2Text,
                            storage_location_part3 = IsPicker3 ? Location3 : Location3Text,
                            storage_location_part4 = IsPicker4 ? Location4 : Location4Text,
                            regeneration_method_id = (int)Regeneration.ValueMember,
                            preservation_method_id = (int)Preservation.ValueMember,
                            is_distributable = "N",
                            is_available = "N",
                            availability_status_code = ""
                        };

                        resultInventoryId = await _restClient.CreateInventory(NewInventory);
                        NewInventory.inventory_id = int.Parse(resultInventoryId);
                    }
                    catch(Exception creatingInventoryEx)
                    {
                        errorMessageList.Add(creatingInventoryEx.Message);
                        inventoryThumbnail.TaskStatus = "Error";
                        continue;
                    }
                    //Register Propagated Action
                    try
                    {
                        // Add propagated action
                        InventoryAction iaPropagated = new InventoryAction()
                        {
                            inventory_action_id = -1,
                            inventory_id = NewInventory.inventory_id,
                            action_name_code = "PROPAGATED",
                            quantity = NewInventory.quantity_on_hand,
                            quantity_unit_code = NewInventory.quantity_on_hand_unit_code,
                            started_date = PropagationDate,
                            method_id = null,
                            cooperator_id = (int)PropagatorCooperator.ValueMember,
                            note = "LabelTemplate : " + LabelTemplate.TemplateName
                        };
                        await _restClient.CreateInventoryAction(iaPropagated);
                    }
                    catch(Exception registerPropagatedActionEx)
                    {
                        errorMessageList.Add(registerPropagatedActionEx.Message);
                        inventoryThumbnail.TaskStatus = "Error";
                        continue;
                    }

                    //Update inventory number
                    var newInventoryListPre = await _restClient.Search("@inventory.inventory_id = " + resultInventoryId, "get_mob_inventory_thumbnail", "inventory");
                    if (newInventoryListPre != null && newInventoryListPre.Count == 1)
                    {
                        InventoryThumbnail newInventoryThumbnail = newInventoryListPre[0];
                        newInventoryThumbnail.inventory_number_part1 = newInventoryThumbnail.inventory_id.ToString();
                        newInventoryThumbnail.inventory_number_part2 = null;
                        newInventoryThumbnail.inventory_number_part3 = string.Empty;

                        await _restClient.UpdateInventory(newInventoryThumbnail);
                    }
                    else
                    {
                        errorMessageList.Add("Error getting inventory thumbnail");
                        inventoryThumbnail.TaskStatus = "Error";
                        continue;
                    }
                    
                    //Get new inventory from server
                    var newInventoryList = await _restClient.Search("@inventory.inventory_id = " + resultInventoryId, "get_mob_inventory_thumbnail", "inventory");
                    if (newInventoryList != null && newInventoryList.Count == 1)
                    {
                        InventoryThumbnail newInventoryThumbnail = newInventoryList[0];

                        //Add to result list
                        newInventoryThumbnailList.Add(newInventoryThumbnail);

                        //Register discount
                        if (RegisterInventoryDiscountAction)
                        {
                            try
                            {
                                decimal? parentInventoryQuantity = inventoryThumbnail.quantity_on_hand;
                                inventoryThumbnail.quantity_on_hand = inventoryThumbnail.quantity_on_hand - DiscountQuantity;

                                string result = await _restClient.UpdateInventory(inventoryThumbnail);

                                //Register inventory action for discount
                                InventoryAction ia = new InventoryAction
                                {
                                    inventory_action_id = -1,
                                    inventory_id = inventoryThumbnail.inventory_id,
                                    action_name_code = ActionJustification.ValueMember.ToString(),
                                    quantity = DiscountQuantity,
                                    quantity_unit_code = inventoryThumbnail.quantity_on_hand_unit_code,
                                    started_date = PropagationDate,
                                    method_id = _discountMethodId,
                                    cooperator_id = (int)PropagatorCooperator.ValueMember,
                                    note = string.Empty
                                };
                                await _restClient.CreateInventoryAction(ia);

                            }
                            catch (Exception clearParentInventoryEx)
                            {
                                errorMessageList.Add(clearParentInventoryEx.Message);
                                inventoryThumbnail.TaskStatus = "Error";
                                continue;
                            }
                        }
                        //Clear Quantity and Location of parent inventory
                        if (ClearPreviousInventories)
                        {
                            try
                            {
                                decimal? parentInventoryQuantity = inventoryThumbnail.quantity_on_hand;
                                string parentInventoryLocation = inventoryThumbnail.storage_location;

                                inventoryThumbnail.quantity_on_hand = 0;
                                inventoryThumbnail.storage_location_part1 = string.Empty;
                                inventoryThumbnail.storage_location_part2 = string.Empty;
                                inventoryThumbnail.storage_location_part3 = string.Empty;
                                inventoryThumbnail.storage_location_part4 = string.Empty;
                                inventoryThumbnail.storage_location = string.Empty;

                                string result = await _restClient.UpdateInventory(inventoryThumbnail);

                                //Register inventory action for discount
                                InventoryAction ia = new InventoryAction
                                {
                                    inventory_action_id = -1,
                                    inventory_id = inventoryThumbnail.inventory_id,
                                    action_name_code = ActionJustification.ValueMember.ToString(),
                                    quantity = parentInventoryQuantity,
                                    quantity_unit_code = inventoryThumbnail.quantity_on_hand_unit_code,
                                    started_date = PropagationDate,
                                    method_id = _discountMethodId,
                                    cooperator_id = (int)PropagatorCooperator.ValueMember,
                                    note = "Inventory was removed from location: \"" + parentInventoryLocation + "\" to generate new inventory"
                                };
                                await _restClient.CreateInventoryAction(ia);

                            }
                            catch (Exception clearParentInventoryEx)
                            {
                                errorMessageList.Add(clearParentInventoryEx.Message);
                                inventoryThumbnail.TaskStatus = "Error";
                                continue;
                            }
                        }
                        //Register creation action
                        if (RegisterCreateInventoryAction)
                        {
                            try
                            {
                                InventoryAction ia = new InventoryAction
                                {
                                    inventory_action_id = -1,
                                    inventory_id = NewInventory.inventory_id,
                                    action_name_code = "CREATED",
                                    quantity = NewInventory.quantity_on_hand,
                                    quantity_unit_code = NewInventory.quantity_on_hand_unit_code,
                                    started_date = DateTime.Now,
                                    method_id = null,
                                    cooperator_id = Settings.UserCooperatorId,
                                    note = string.Format("Inventory number: {0}\nForm: {1}\nLocation: {2}", newInventoryThumbnail.inventory_number,
                                                            NewInventory.form_type_code, newInventoryThumbnail.storage_location)
                                };
                                await _restClient.CreateInventoryAction(ia);
                            }
                            catch(Exception registerInventoryCreationActionEx)
                            {
                                errorMessageList.Add(registerInventoryCreationActionEx.Message);
                                inventoryThumbnail.TaskStatus = "Error";
                            }
                        }
                    }
                    else
                    {
                        errorMessageList.Add("Error getting inventory thumbnail");
                        inventoryThumbnail.TaskStatus = "Error";
                        continue;
                    }

                    inventoryThumbnail.TaskStatus = "Created";
                }

                if (errorMessageList.Count == 0)
                {
                    await PageDialogService.DisplayAlertAsync(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "InventoryPage", "MessageSaveResults"),
                            "All inventories were created successfully\nNow the labels will be sent to the printer", "OK");

                    //Printing inventory label
                    string errorPrinting = string.Empty;
                    try
                    {
                        Settings.Printer = Printer.PrinterName;
                        var idList = newInventoryThumbnailList.Select(x => x.inventory_id).ToList();
                        var dataview = await _restClient.GetDataview(LabelTemplate.Dataview, ":accessionid;:accessioninvgroupid;:inventorymaintpolicyid;:orderrequestid;:inventoryid=" + string.Join(",", idList));

                        _labelIndex = 0;
                        _endRecordIndex = dataview.Count - 1;

                        string zpl = GenerateZPL(dataview);
                        //PrintingLog = "Printer URI:\n" + Printer.PrinterUri + "\n\n" + zpl;

                        await _restClient.Print(Printer.PrinterUri, Printer.PrinterConnectionType, zpl);
                    }
                    catch (Exception printInventoryEx)
                    {
                        errorPrinting = printInventoryEx.Message;
                    }

                    if (String.IsNullOrEmpty(errorPrinting))
                    {
                        await PageDialogService.DisplayAlertAsync(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "PrintingPage", "MessagePrintResults"),
                            "All labels were sent successfully", "OK");

                        var navigationParams = new NavigationParameters
                        {
                            { "InventoryThumbnailList", newInventoryThumbnailList }
                        };
                        await NavigationService.GoBackAsync(navigationParams);
                        return;
                    }
                    else
                    {
                        await PageDialogService.DisplayAlertAsync(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "PrintingPage", "MessagePrintResults"),
                            errorPrinting, "OK");
                    }
                }
                else
                {
                    string error = @"Some errors ocurred (" + errorMessageList.Count + ")\n\n" + string.Join("\n*****************************\n", errorMessageList);
                    await PageDialogService.DisplayAlertAsync(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "InventoryPage", "MessageSaveResults"), error, "OK");
                }
                
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }

        private string GenerateZPL(Newtonsoft.Json.Linq.JArray dataview)
        {
            string zpl = string.Empty;

            int printerDPI = LabelTemplate.Density;
            float labelWidth = (float)LabelTemplate.Width * printerDPI;
            float labelHeight = (float)LabelTemplate.Height * printerDPI;

            float marginLeft = (float)LabelTemplate.MarginLeft * printerDPI;
            float marginTop = (float)LabelTemplate.MarginTop * printerDPI;
            float horizontalGap = (float)LabelTemplate.HorizontalGap * printerDPI;
            float verticalGap = (float)LabelTemplate.VerticalGap * printerDPI;

            int labelsPerRecord = (int) LabelsPerInventory;

            int columnsPerPage = (int)LabelTemplate.HorizontalCount;
            int rowsPerPage = 0;

            float paperHeight = (float)LabelTemplate.PaperHeight * printerDPI;

            if (labelHeight > paperHeight)
            {
                rowsPerPage = 1;
            }
            else if (2 * labelHeight + verticalGap > paperHeight) // 1 row
            {
                rowsPerPage = 1;
            }
            else //more than 1 row
            {
                rowsPerPage = 1 + (int)((paperHeight - labelHeight) / (labelHeight + verticalGap));
            }

            // Generate zpl
            string templateContent = LabelTemplate.Zpl.Replace("^XA", "").Replace("^XZ", "").Replace("\r\n\r\n", "\r\n").Trim();
            int zplX = 0, zplY = 0;
            int recordIndex = (_labelIndex / labelsPerRecord);

            while (recordIndex <= _endRecordIndex)
            {
                zpl += "^XA\n";
                for (int iRow = 0; iRow < rowsPerPage; iRow++)
                {
                    zplY = (int)(marginTop + iRow * (labelHeight + verticalGap));
                    for (int iCol = 0; iCol < columnsPerPage; iCol++)
                    {
                        zplX = (int)(marginLeft + iCol * (labelWidth + horizontalGap));
                        if (recordIndex <= _endRecordIndex)
                        {
                            zpl += $"^LH{zplX},{zplY}\n";
                            string label = ReplaceVariables(templateContent, dataview[recordIndex]);
                            zpl += label + "\n";

                            _labelIndex++;
                            recordIndex = (_labelIndex / labelsPerRecord);
                        }
                        else
                            break;
                    }
                    if (recordIndex > _endRecordIndex)
                        break;
                }
                zpl += "^XZ";
            }

            return zpl;
        }

        private string ReplaceVariables(string template, Newtonsoft.Json.Linq.JToken dgrCurrent)
        {
            string label = template;
            string startMark = @"##";
            string endMark = @"##";
            foreach (var variable in _labelVariables)
            {
                var variableName = variable.Replace(startMark, "").Replace(endMark, "");
                if (null != dgrCurrent[variableName])
                {
                    string formattedVlur = dgrCurrent[variableName].ToString();
                    label = label.Replace(variable, formattedVlur.Replace("~", @"\7E").Replace("^", @"\5E")
                        .Replace("á", @"\A0")
                        .Replace("é", @"\82")
                        .Replace("í", @"\A1")
                        .Replace("ó", @"\A2")
                        .Replace("ú", @"\A3")
                        .Replace("ñ", @"\A4")
                        .Replace("Ñ", @"\A5")
                        .Replace("ü", @"\81"));
                }
            }
            return label;
        }
        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            try
            {
                Utils.RefreshLang(this, _dataStoreService, "CreatePrintInventoryPage");

                if (_location == null)
                {
                    //_location = await _restClient.GetLocations(Settings.WorkgroupInvMaintPolicies);
                }

                if (ListLocation1 == null)
                {
                    ListLocation1 = await _restClient.GetAllLocation1List();
                    /*
                    var locationPart1List = _location.Select(l => l.storage_location_part1).Distinct().ToList<string>();
                    locationPart1List.Sort();
                    var locationPart2List = _location.Select(l => l.storage_location_part2).Distinct().ToList<string>();
                    locationPart2List.Sort();
                    var locationPart3List = _location.Select(l => l.storage_location_part3).Distinct().ToList<string>();
                    locationPart3List.Sort();
                    var locationPart4List = _location.Select(l => l.storage_location_part4).Distinct().ToList<string>();
                    locationPart4List.Sort();

                    ListLocation1 = locationPart1List;*/

                    ListLocation2 = new List<string>();
                    ListLocation3 = new List<string>();
                    ListLocation4 = new List<string>();
                    //Location1 = Settings.Location1;
                }

                if (InventoryMaintPolicyList == null)
                {
                    var tempInventoryMaintPolicyList = await _restClient.GetLookupList("inventory_maint_policy_lookup");
                    //Apply filters
                    if (!string.IsNullOrEmpty(Settings.WorkgroupInvMaintPolicies))
                    {
                        var filterList = Settings.WorkgroupInvMaintPolicies.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = tempInventoryMaintPolicyList.Count - 1; i >= 0; i--)
                        {
                            if (!filterList.Any(x => x.Equals(tempInventoryMaintPolicyList[i].ValueMember.ToString())))
                            {
                                tempInventoryMaintPolicyList.RemoveAt(i);
                            }
                        }
                    }
                    InventoryMaintPolicyList = tempInventoryMaintPolicyList;

                    if (InventoryMaintPolicyList != null && InventoryMaintPolicyList.Count > 0)
                        InventoryMaintPolicy = InventoryMaintPolicyList[0];
                }
                if (InventoryFormList == null)
                {
                    InventoryFormList = await _restClient.GetCodeValueByGroupName("GERMPLASM_FORM");
                    if (InventoryFormList != null && InventoryFormList.Count > 0)
                        InventoryForm = InventoryFormList[0];
                }
                if (QuantityUnitList == null)
                {
                    QuantityUnitList = await _restClient.GetCodeValueByGroupName("UNIT_OF_QUANTITY");
                    if (QuantityUnitList != null && QuantityUnitList.Count > 0)
                        QuantityUnit = QuantityUnitList[0];
                }
                if (PropagatorCooperatorList == null)
                {
                    PropagatorCooperatorList = await _restClient.GetCooperatorLookupListByWorkgroup(Settings.WorkgroupId);
                    if (PropagatorCooperatorList != null && PropagatorCooperatorList.Count > 0)
                        PropagatorCooperator = PropagatorCooperatorList[0];
                }
                if (RegenerationList == null)
                {
                    RegenerationList = await _restClient.GetLookupList("method_lookup");
                }
                if (PreservationList == null)
                {
                    PreservationList = await _restClient.GetLookupList("method_lookup");
                }

                if (ActionJustificationList == null)
                {
                    ActionJustificationList = await _restClient.GetInventoryActionJustificationCodeValue();
                }

                if (PrinterList == null)
                {
                    PrinterList = await _restClient.GetPrinterList();
                }
                if (Printer == null)
                {
                    Printer = PrinterList.FirstOrDefault(p => Settings.Printer.Equals(p.PrinterName));
                    if (Printer == null)
                        Settings.Printer = string.Empty;
                }
                if (LabelTemplateList == null)
                {
                    LabelTemplateList = await _restClient.GetInventoryLabelTemplateList(null);
                }

                if (parameters.ContainsKey("InventoryThumbnailList"))
                {
                    _inventoryList = (List<InventoryThumbnail>)parameters["InventoryThumbnailList"];

                    //Initialize task status for all items 
                    _inventoryList.ForEach(x => x.TaskStatus = "Pending");
                }

                if (_discountMethodId == 0)
                {
                    var appSettingDiscount = _dataStoreService.GetAppSettingsDisplayMember(Settings.LangId, "DISCOUNT");
                    if (appSettingDiscount != null)
                    {
                        _discountMethodId = int.Parse(appSettingDiscount.setting_value);
                    }
                    else
                    {
                        throw new Exception("App setting \"DISCOUNT\" is not defined in \"get_mob_app_settings\" dataview ");
                    }
                    var methodsIds = await _restClient.SearchKeys("@method.method_id = " + _discountMethodId, "method");
                    if (methodsIds == null)
                    {
                        throw new Exception("Method for \"DISCOUNT\" set in \"get_mob_app_settings\" is not found.\nUse \"get_method\" dataview to register it");
                    }
                    if (!methodsIds.Contains(_discountMethodId))
                    {
                        throw new Exception("Method for \"DISCOUNT\" set in \"get_mob_app_settings\" is not found.\nUse \"get_method\" dataview to register it");
                    }
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
    }
}
