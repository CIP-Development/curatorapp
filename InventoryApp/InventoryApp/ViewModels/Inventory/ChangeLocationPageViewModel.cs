﻿using ImTools;
using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using InventoryApp.Services;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;

namespace InventoryApp.ViewModels
{
    public class ChangeLocationPageViewModel : ViewModelBase
    {
        #region Properties
        private List<string> _listLocation1;
        public List<string> ListLocation1
        {
            get { return _listLocation1; }
            set { SetProperty(ref _listLocation1, value); }
        }
        private List<string> _listLocation2;
        public List<string> ListLocation2
        {
            get { return _listLocation2; }
            set { SetProperty(ref _listLocation2, value); }
        }
        private List<string> _listLocation3;
        public List<string> ListLocation3
        {
            get { return _listLocation3; }
            set { SetProperty(ref _listLocation3, value); }
        }
        private List<string> _listLocation4;
        public List<string> ListLocation4
        {
            get { return _listLocation4; }
            set { SetProperty(ref _listLocation4, value); }
        }
        private string _location1;
        public string Location1
        {
            get { return _location1; }
            set { SetProperty(ref _location1, value); }
        }

        private string _location2;
        public string Location2
        {
            get { return _location2; }
            set { SetProperty(ref _location2, value); }
        }

        private string _location3;
        public string Location3
        {
            get { return _location3; }
            set { SetProperty(ref _location3, value); }
        }

        private string _location4;
        public string Location4
        {
            get { return _location4; }
            set { SetProperty(ref _location4, value); }
        }

        private string _location1Text;
        public string Location1Text
        {
            get { return _location1Text; }
            set { SetProperty(ref _location1Text, value); }
        }

        private string _location2Text;
        public string Location2Text
        {
            get { return _location2Text; }
            set { SetProperty(ref _location2Text, value); }
        }

        private string _location3Text;
        public string Location3Text
        {
            get { return _location3Text; }
            set { SetProperty(ref _location3Text, value); }
        }

        private string _location4Text;
        public string Location4Text
        {
            get { return _location4Text; }
            set { SetProperty(ref _location4Text, value); }
        }

        private bool _isPicker1;
        public bool IsPicker1
        {
            get { return _isPicker1; }
            set { SetProperty(ref _isPicker1, value); }
        }

        private bool _isPicker2;
        public bool IsPicker2
        {
            get { return _isPicker2; }
            set { SetProperty(ref _isPicker2, value); }
        }

        private bool _isPicker3;
        public bool IsPicker3
        {
            get { return _isPicker3; }
            set { SetProperty(ref _isPicker3, value); }
        }

        private bool _isPicker4;
        public bool IsPicker4
        {
            get { return _isPicker4; }
            set { SetProperty(ref _isPicker4, value); }
        }

        private bool _createInventoryAction;
        public bool CreateInventoryAction
        {
            get { return _createInventoryAction; }
            set { SetProperty(ref _createInventoryAction, value); }
        }
        #endregion

        #region LangProperties
        private string _UxTitle;
        public string UxTitle
        {
            get { return _UxTitle; }
            set { SetProperty(ref _UxTitle, value); }
        }
        private string _uxToolbarItemSave;
        public string UxToolbarItemSave
        {
            get { return _uxToolbarItemSave; }
            set { SetProperty(ref _uxToolbarItemSave, value); }
        }
        private string _uxLabelLocation1;
        public string UxLabelLocation1
        {
            get { return _uxLabelLocation1; }
            set { SetProperty(ref _uxLabelLocation1, value); }
        }
        private string _uxLabelLocation2;
        public string UxLabelLocation2
        {
            get { return _uxLabelLocation2; }
            set { SetProperty(ref _uxLabelLocation2, value); }
        }
        private string _uxLabelLocation3;
        public string UxLabelLocation3
        {
            get { return _uxLabelLocation3; }
            set { SetProperty(ref _uxLabelLocation3, value); }
        }
        private string _uxLabelLocation4;
        public string UxLabelLocation4
        {
            get { return _uxLabelLocation4; }
            set { SetProperty(ref _uxLabelLocation4, value); }
        }
        private string _uxLabelCreateAction;
        public string UxLabelCreateAction
        {
            get { return _uxLabelCreateAction; }
            set { SetProperty(ref _uxLabelCreateAction, value); }
        }
        #endregion

        IPageDialogService _pageDialogService { get; }
        IDataStoreService _dataStoreService;
        private List<Location> _location;
        private readonly RestClient _restClient;
        private List<InventoryThumbnail> _inventoryList;

        public ChangeLocationPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService, IDataStoreService dataStoreService)
            : base(navigationService)
        {
            _pageDialogService = pageDialogService;
            _dataStoreService = dataStoreService;
            _restClient = new RestClient();

            Title = "Change Location";

            SaveCommand = new DelegateCommand(OnSaveCommandExecuted);

            IsPicker1 = true;
            IsPicker2 = true;
            IsPicker3 = true;
            IsPicker4 = false;

            CreateInventoryAction = true;
            ListLocation1ChangedCommand = new DelegateCommand(OnListLocation1ChangedCommandExecuted);
            ListLocation2ChangedCommand = new DelegateCommand(OnListLocation2ChangedCommandExecuted);
        }

        public DelegateCommand SaveCommand { get; }
        private async void OnSaveCommandExecuted()
        {
            int errorCount = 0;
            foreach (InventoryThumbnail inventory in _inventoryList)
            {
                try
                {
                    var prevLocation = inventory.Location;
                    if (!IsPicker1)
                        inventory.storage_location_part1 = Location1Text;
                    else
                        inventory.storage_location_part1 = Location1;

                    if (!IsPicker2)
                        inventory.storage_location_part2 = Location2Text;
                    else
                        inventory.storage_location_part2 = Location2;

                    if (!IsPicker3)
                        inventory.storage_location_part3 = Location3Text;
                    else
                        inventory.storage_location_part3 = Location3;

                    if (!IsPicker4)
                        inventory.storage_location_part4 = Location4Text;
                    else
                        inventory.storage_location_part4 = Location4;

                    await _restClient.UpdateInventory(inventory);

                    if (CreateInventoryAction)
                    {
                        InventoryAction ia = new InventoryAction
                        {
                            inventory_action_id = -1,
                            inventory_id = inventory.inventory_id,
                            action_name_code = "TRANSFER",
                            quantity = null,
                            quantity_unit_code = null,
                            started_date = DateTime.Now,
                            method_id = null,
                            cooperator_id = Settings.UserCooperatorId,
                            note = "Transferred From " + prevLocation + " to " + inventory.Location
                        };
                        await _restClient.CreateInventoryAction(ia);
                    }
                }
                catch (Exception ex)
                {
                    errorCount++;
                    await _pageDialogService.DisplayAlertAsync(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "ChangeLocationPage", "MessageSaveResultsError"),
                        inventory.inventory_number + "\n\n" + ex.Message, "OK");
                }
            }

            if(errorCount == 0)
                await _pageDialogService.DisplayAlertAsync(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "ChangeLocationPage", "MessageSaveResults"),
                    _dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "ChangeLocationPage", "MessageSuccessfullySaved"), "OK");

            var list = string.Join(",", _inventoryList.Select(i => i.inventory_id));
            _inventoryList = await _restClient.Search("@inventory.inventory_id in (" + list + ")", "get_mob_inventory_thumbnail", "inventory");

            var navigationParams = new NavigationParameters();
            navigationParams.Add("InventoryThumbnailList", _inventoryList);
            await NavigationService.GoBackAsync(navigationParams);
        }

        public DelegateCommand ListLocation1ChangedCommand { get; }
        private async void OnListLocation1ChangedCommandExecuted()
        {
            try
            {
                if (Location1 != null)
                {
                    //Location1 = Settings.Location1;
                    ListLocation2 = _location.Where(l => l.storage_location_part1.Equals(Location1) && !string.IsNullOrEmpty(l.storage_location_part2))
                        .Select(l => l.storage_location_part2).Distinct().ToList();
                }
            }
            catch (Exception e)
            {
                await _pageDialogService.DisplayAlertAsync("Error", e.Message, "OK");
            }
        }
        public DelegateCommand ListLocation2ChangedCommand { get; }
        private async void OnListLocation2ChangedCommandExecuted()
        {
            try
            {
                if (Location2 != null)
                {
                    ListLocation3 = _location.Where(l => l.storage_location_part1.Equals(Location1) && l.storage_location_part2.Equals(Location2) && !string.IsNullOrEmpty(l.storage_location_part3))
                        .Select(l => l.storage_location_part3).Distinct().ToList();
                }
            }
            catch (Exception e)
            {
                await _pageDialogService.DisplayAlertAsync("Error", e.Message, "OK");
            }
        }

        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            try
            {
                Utils.RefreshLang(this, _dataStoreService, "ChangeLocationPage");

                if (_location == null)
                {
                    _location = await _restClient.GetLocations(Settings.WorkgroupInvMaintPolicies);
                }

                if (ListLocation1 == null)
                {
                    var locationPart1List = _location.Select(l => l.storage_location_part1).Distinct().ToList<string>();
                    locationPart1List.Sort();
                    var locationPart2List = _location.Select(l => l.storage_location_part2).Distinct().ToList<string>();
                    locationPart2List.Sort();
                    var locationPart3List = _location.Select(l => l.storage_location_part3).Distinct().ToList<string>();
                    locationPart3List.Sort();
                    var locationPart4List = _location.Select(l => l.storage_location_part4).Distinct().ToList<string>();
                    locationPart4List.Sort();

                    ListLocation1 = locationPart1List;
                    ListLocation2 = locationPart2List;
                    ListLocation3 = locationPart3List;
                    ListLocation4 = locationPart4List;
                    Location1 = Settings.Location1;
                }

                if (parameters.ContainsKey("InventoryThumbnailList"))
                {
                    _inventoryList = (List<InventoryThumbnail>)parameters["InventoryThumbnailList"];
                }
            }
            catch (Exception e)
            {
                await _pageDialogService.DisplayAlertAsync("Error", e.Message, "OK");
            }
        }
    }
    
}
