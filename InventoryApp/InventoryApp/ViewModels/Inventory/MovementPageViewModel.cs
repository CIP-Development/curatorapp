﻿using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using InventoryApp.Services;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;

namespace InventoryApp.ViewModels
{
    public class MovementPageViewModel : ViewModelBase
    {
        IPageDialogService _pageDialogService { get; }
        IDataStoreService _dataStoreService;
        private List<InventoryThumbnail> _inventoryList;
        private readonly RestClient _restClient;
        private string _quantityUnitCode;
        private int DISCOUNTID, INCREASEID;

        private string _quantityUnit;
        public string QuantityUnit
        {
            get { return _quantityUnit; }
            set { SetProperty(ref _quantityUnit, value); }
        }
        private string _note;
        public string Note
        {
            get { return _note; }
            set { SetProperty(ref _note, value); }
        }
        private int _quantity;
        public int Quantity
        {
            get { return _quantity; }
            set { SetProperty(ref _quantity, value); }
        }

        private List<ILookup> _actionCodeValueList;
        public List<ILookup> ActionCodeValueList
        {
            get { return _actionCodeValueList; }
            set { SetProperty(ref _actionCodeValueList, value); }
        }

        private List<Lookup> _methodList;
        public List<Lookup> MethodList
        {
            get { return _methodList; }
            set { SetProperty(ref _methodList, value); }
        }

        private CodeValueLookup _actionJustification;
        public CodeValueLookup ActionJustification
        {
            get { return _actionJustification; }
            set { SetProperty(ref _actionJustification, value); }
        }
        
        private Lookup _methodOperation;
        public Lookup MethodOperation
        {
            get { return _methodOperation; }
            set { SetProperty(ref _methodOperation, value); }
        }
        
        private DateTime _actionDate;
        public DateTime ActionDate
        {
            get { return _actionDate; }
            set { SetProperty(ref _actionDate, value); }
        }

        private bool _isCalculated;
        public bool IsCalculated
        {
            get { return _isCalculated; }
            set { SetProperty(ref _isCalculated, value, () => RaisePropertyChanged(nameof(IsNotCalculated))); }
        }
        public bool IsNotCalculated
        {
            get { return !IsCalculated; }
        }
        
        private decimal _seedsWeight;
        public decimal SeedsWeight
        {
            get { return _seedsWeight; }
            set { SetProperty(ref _seedsWeight, value); }
        }
        private decimal _seedsWeight100;
        public decimal SeedsWeight100
        {
            get { return _seedsWeight100; }
            set { SetProperty(ref _seedsWeight100, value); }
        }

        #region LangProperties
        private string _UxTitle;
        public string UxTitle
        {
            get { return _UxTitle; }
            set { SetProperty(ref _UxTitle, value); }
        }
        private string _UxToolbarItemSave;
        public string UxToolbarItemSave
        {
            get { return _UxToolbarItemSave; }
            set { SetProperty(ref _UxToolbarItemSave, value); }
        }
        private string _UxLabelOperation;
        public string UxLabelOperation
        {
            get { return _UxLabelOperation; }
            set { SetProperty(ref _UxLabelOperation, value); }
        }
        private string _UxLabelQuantity;
        public string UxLabelQuantity
        {
            get { return _UxLabelQuantity; }
            set { SetProperty(ref _UxLabelQuantity, value); }
        }
        private string _UxLabelByUsingWeight;
        public string UxLabelByUsingWeight
        {
            get { return _UxLabelByUsingWeight; }
            set { SetProperty(ref _UxLabelByUsingWeight, value); }
        }
        private string _UxLabel100SeedsWeight;
        public string UxLabel100SeedsWeight
        {
            get { return _UxLabel100SeedsWeight; }
            set { SetProperty(ref _UxLabel100SeedsWeight, value); }
        }
        private string _UxLabelTotalSeedsWeight;
        public string UxLabelTotalSeedsWeight
        {
            get { return _UxLabelTotalSeedsWeight; }
            set { SetProperty(ref _UxLabelTotalSeedsWeight, value); }
        }
        private string _UxButtonCalculate;
        public string UxButtonCalculate
        {
            get { return _UxButtonCalculate; }
            set { SetProperty(ref _UxButtonCalculate, value); }
        }
        private string _UxLabelQuantityUnit;
        public string UxLabelQuantityUnit
        {
            get { return _UxLabelQuantityUnit; }
            set { SetProperty(ref _UxLabelQuantityUnit, value); }
        }
        private string _UxLabelJustification;
        public string UxLabelJustification
        {
            get { return _UxLabelJustification; }
            set { SetProperty(ref _UxLabelJustification, value); }
        }
        private string _UxLabelActionDate;
        public string UxLabelActionDate
        {
            get { return _UxLabelActionDate; }
            set { SetProperty(ref _UxLabelActionDate, value); }
        }
        private string _UxLabelNote;
        public string UxLabelNote
        {
            get { return _UxLabelNote; }
            set { SetProperty(ref _UxLabelNote, value); }
        }
        #endregion

        public MovementPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService, IDataStoreService dataStoreService) 
            : base(navigationService)
        {
            _dataStoreService = dataStoreService;
            _pageDialogService = pageDialogService;
            _restClient = new RestClient();

            Title = "Increase / Discount";

            SaveCommand = new DelegateCommand(OnSaveCommandExecuted);
            CalculateCommand = new DelegateCommand(OnCalculateCommandExecuted);

            Note = "";
            _actionDate = DateTime.Now;
        }

        public DelegateCommand SaveCommand { get; }

        private async void OnSaveCommandExecuted()
        {
            //await _pageDialogService.DisplayAlertAsync("Inventory List Count", _inventoryList.Count.ToString(), "OK");
            int errorCount = 0;
            foreach (InventoryThumbnail inventory in _inventoryList)
            {
                try
                {
                    int quantity = (MethodOperation.value_member == DISCOUNTID) ? Math.Abs(Quantity) * -1 : Math.Abs(Quantity);
                    InventoryAction ia = new InventoryAction
                    {
                        inventory_action_id = -1,
                        inventory_id = inventory.inventory_id,
                        action_name_code = ActionJustification.value_member,
                        quantity = quantity,
                        quantity_unit_code = _quantityUnitCode,
                        started_date = ActionDate,
                        method_id = MethodOperation.value_member,
                        cooperator_id = Settings.UserCooperatorId,
                        note = Note
                    };
                    await _restClient.CreateInventoryAction(ia);

                    inventory.quantity_on_hand += quantity;
                    await _restClient.UpdateInventory(inventory);
                }
                catch (Exception ex)
                {
                    errorCount++;
                    await _pageDialogService.DisplayAlertAsync(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "MovementPage", "MessageSaveResultsError"),
                        inventory.inventory_number + "\n\n" + ex.Message, "OK");
                }
            }
            
            if(errorCount == 0)
                await _pageDialogService.DisplayAlertAsync(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "MovementPage", "MessageSaveResults"),
                    _dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "MovementPage", "MessageSuccessfullySaved"), "OK");

            var navigationParams = new NavigationParameters();
            navigationParams.Add("InventoryThumbnailList", _inventoryList);
            await NavigationService.GoBackAsync(navigationParams);
        }

        public DelegateCommand CalculateCommand { get; }
        private async void OnCalculateCommandExecuted()
        {
            try
            {
                if (SeedsWeight100 > 0)
                    Quantity = (int)(Math.Abs(SeedsWeight) / Math.Abs(SeedsWeight100) * 100);
            }
            catch (Exception ex)
            {
                await _pageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }

        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            try
            {
                Utils.RefreshLang(this, _dataStoreService, "MovementPage");

                if (MethodList == null) 
                {
                    List<Lookup> methodList= new List<Lookup>();
                    var appSettingIncrease = _dataStoreService.GetAppSettingsDisplayMember(Settings.LangId, "INCREASE");
                    if(appSettingIncrease != null)
                    {
                        INCREASEID = int.Parse(appSettingIncrease.setting_value);
                        methodList.Add(new Lookup() { value_member = int.Parse(appSettingIncrease.setting_value), display_member = appSettingIncrease.title });
                    }
                    else
                    {
                        throw new Exception("App setting \"INCREASE\" is not defined in \"get_mob_app_settings\" dataview ");
                    }
                    var appSettingDiscount = _dataStoreService.GetAppSettingsDisplayMember(Settings.LangId, "DISCOUNT");
                    if (appSettingDiscount != null)
                    {
                        DISCOUNTID = int.Parse(appSettingDiscount.setting_value);
                        methodList.Add(new Lookup() { value_member = int.Parse(appSettingDiscount.setting_value), display_member = appSettingDiscount.title });
                    }
                    else
                    {
                        throw new Exception("App setting \"DISCOUNT\" is not defined in \"get_mob_app_settings\" dataview ");
                    }
                    var methodsIds = await _restClient.SearchKeys("@method.method_id in (" + INCREASEID + "," + DISCOUNTID +")", "method");
                    if (methodsIds == null) {
                        throw new Exception("Methods for \"INCREASE\" and \"DISCOUNT\" set in \"get_mob_app_settings\" are not found.\nUse \"get_method\" dataview to register");
                    }
                    if (!methodsIds.Contains(INCREASEID))
                    {
                        throw new Exception("Method for \"INCREASE\" set in \"get_mob_app_settings\" is not found.\nUse \"get_method\" dataview to register it");
                    }
                    if (!methodsIds.Contains(DISCOUNTID))
                    {
                        throw new Exception("Method for \"DISCOUNT\" set in \"get_mob_app_settings\" is not found.\nUse \"get_method\" dataview to register it");
                    }
                    MethodList = methodList;
                }
                if(ActionCodeValueList == null)
                {
                    ActionCodeValueList = await _restClient.GetInventoryActionJustificationCodeValue();
                }

                if (parameters.ContainsKey("InventoryThumbnailList"))
                {
                    _inventoryList = (List<InventoryThumbnail>)parameters["InventoryThumbnailList"];
                    if (_inventoryList != null && _inventoryList.Count > 0)
                    {
                        _quantityUnitCode = _inventoryList[0].quantity_on_hand_unit_code;
                        QuantityUnit = _inventoryList[0].quantity_on_hand_unit;
                    }
                }
            }
            catch (Exception ex)
            {
                await _pageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
    }
}
