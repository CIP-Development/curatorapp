﻿using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using InventoryApp.Services;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Forms;

namespace InventoryApp.ViewModels
{
    public class LookupPickerPageViewModel : ViewModelBaseWithDialog
    {
        private IDataStoreService _dataStoreService;
        private EntityAttribute _attribute;
        private RestClient _restClient;

        private int _accessionId;

        private List<ILookup> _lookupList;
        public List<ILookup> LookupList
        {
            get { return _lookupList; }
            set { SetProperty(ref _lookupList, value); }
        }

        private string _searchText;
        public string SearchText
        {
            get { return _searchText; }
            set { SetProperty(ref _searchText, value); }
        }
        private bool _isSearchBarVisible;
        public bool IsSearchBarVisible
        {
            get { return _isSearchBarVisible; }
            set { SetProperty(ref _isSearchBarVisible, value); }
        }

        private bool _searchInWorkgroupInventories;
        public bool SearchInWorkgroupInventories
        {
            get { return _searchInWorkgroupInventories; }
            set { SetProperty(ref _searchInWorkgroupInventories, value); }
        }
        private bool _isFilterCheckVisible;
        public bool IsFilterCheckVisible
        {
            get { return _isFilterCheckVisible; }
            set { SetProperty(ref _isFilterCheckVisible, value); }
        }
        #region Lang
        private string _UxLabelResults;
        public string UxLabelResults
        {
            get { return _UxLabelResults; }
            set { SetProperty(ref _UxLabelResults, value); }
        }
        private string _UxLabelSearchInWorkgroupInventories;
        public string UxLabelSearchInWorkgroupInventories
        {
            get { return _UxLabelSearchInWorkgroupInventories; }
            set { SetProperty(ref _UxLabelSearchInWorkgroupInventories, value); }
        }
        private string _UxLabelSearchText;
        public string UxLabelSearchText
        {
            get { return _UxLabelSearchText; }
            set { SetProperty(ref _UxLabelSearchText, value); }
        }
        #endregion

        public LookupPickerPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService, IDataStoreService dataStoreService)
            : base(navigationService, pageDialogService)
        {
            _dataStoreService = dataStoreService;
            _restClient = new RestClient();

            SearchTextCommand = new DelegateCommand(OnSearchTextCommandExecuted);
            ItemTappedCommand = new DelegateCommand<object>(OnItemTappedCommandExecuted);

            SearchInWorkgroupInventories = true;
            IsFilterCheckVisible = true;
            SearchText = string.Empty;
        }

        public DelegateCommand SearchTextCommand { get; }
        private async void OnSearchTextCommandExecuted()
        {
            try
            {
                //LookupList = await _dataStoreService.GetAccessionLookupList(SearchText);
                switch (_attribute.ControlSource)
                {
                    case "accession_lookup":
                        if (SearchInWorkgroupInventories && !string.IsNullOrEmpty(Settings.WorkgroupInvMaintPolicies))
                            LookupList = await _restClient.GetAccessionLookUpListByAccessionNumberChecked(SearchText, Settings.WorkgroupInvMaintPolicies);
                        else
                            LookupList = await _restClient.GetAccessionLookUpListByAccessionNumber(SearchText);
                        break;
                    case "inventory_maint_policy_lookup":
                        LookupList = await _restClient.GetInventoryMaintPolicyLookupByMaintenanceName(SearchText);
                        break;
                    case "inventory_lookup":
                        if(SearchInWorkgroupInventories && !string.IsNullOrEmpty(Settings.WorkgroupInvMaintPolicies))
                            LookupList = await _restClient.GetInventoryLookUpListByInventoryNumberFiltered(SearchText, _accessionId, Settings.WorkgroupInvMaintPolicies); 
                        else
                            LookupList = await _restClient.GetInventoryLookUpListByInventoryNumber(SearchText, _accessionId);
                        break;
                    case "method_lookup":
                        LookupList = await _restClient.GetMethodLookupListByName(SearchText);
                        break;
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public DelegateCommand<Object> ItemTappedCommand { get; }
        private async void OnItemTappedCommandExecuted(Object param)
        {
            try
            {
                ILookup selectedTtem = (ILookup)param;
                _attribute.Value = selectedTtem.ValueMember;
                _attribute.DisplayValue = selectedTtem.DisplayMember;

                var navigationParams = new NavigationParameters
                {
                    { "LookupPicker", _attribute }
                };
                await NavigationService.GoBackAsync(navigationParams);
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public async override void OnNavigatedTo(INavigationParameters parameters)
        {
            try
            {
                Utils.RefreshLang(this, _dataStoreService, "LookupPickerPage");

                if (parameters.ContainsKey("EntityAttribute"))
                {
                    _attribute = (EntityAttribute)parameters["EntityAttribute"];

                    if (_attribute.ControlType.Equals("DROPDOWN"))
                    {
                        IsSearchBarVisible = false;
                        LookupList = await _restClient.GetCodeValueByGroupName(_attribute.ControlSource);
                    }
                    else if (_attribute.ControlType.Equals("LOOKUPPICKER")) 
                    {
                        IsSearchBarVisible = true;
                        LookupList = null;

                        if (_attribute.ControlSource.Equals("method_lookup"))
                        {
                            IsFilterCheckVisible = false;
                        }
                    }

                    if (_attribute.Name.Equals("parent_inventory_id"))
                    {
                        if (parameters.ContainsKey("AccessionId"))
                        {
                            _accessionId = (int)parameters["AccessionId"];
                            if (SearchInWorkgroupInventories && !string.IsNullOrEmpty(Settings.WorkgroupInvMaintPolicies))
                                LookupList = await _restClient.GetInventoryLookUpListByInventoryNumberFiltered("%", _accessionId, Settings.WorkgroupInvMaintPolicies);
                            else
                                LookupList = await _restClient.GetInventoryLookUpListByInventoryNumber("%", _accessionId);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
    }
}
