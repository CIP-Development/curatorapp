﻿using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using InventoryApp.Services;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InventoryApp.ViewModels
{
    public class AccessionLookupPickerPageViewModel : ViewModelBaseWithDialog
    {
        private IDataStoreService _dataStoreService;
        private EntityAttribute _attribute;
        private RestClient _restClient;

        private List<ILookup> _lookupList;
        public List<ILookup> LookupList
        {
            get { return _lookupList; }
            set { SetProperty(ref _lookupList, value); }
        }

        private string _searchText;
        public string SearchText
        {
            get { return _searchText; }
            set { SetProperty(ref _searchText, value); }
        }
        private bool _isSearchBarVisible;
        public bool IsSearchBarVisible
        {
            get { return _isSearchBarVisible; }
            set { SetProperty(ref _isSearchBarVisible, value); }
        }
        private bool _SearchInExistingInventories;
        public bool SearchInExistingInventories
        {
            get { return _SearchInExistingInventories; }
            set { SetProperty(ref _SearchInExistingInventories, value); }
        }

        #region Lang
        private string _UxLabelSearchInExistingInventories;
        public string UxLabelSearchInExistingInventories
        {
            get { return _UxLabelSearchInExistingInventories; }
            set { SetProperty(ref _UxLabelSearchInExistingInventories, value); }
        }
        private string _UxLabelSearchText;
        public string UxLabelSearchText
        {
            get { return _UxLabelSearchText; }
            set { SetProperty(ref _UxLabelSearchText, value); }
        }
        private string _UxLabelResults;
        public string UxLabelResults
        {
            get { return _UxLabelResults; }
            set { SetProperty(ref _UxLabelResults, value); }
        }
        #endregion

        public AccessionLookupPickerPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService, IDataStoreService dataStoreService)
            : base(navigationService, pageDialogService)
        {
            _dataStoreService = dataStoreService;
            //_dataStoreService.SyncCodeValueLookup();
            //_dataStoreService.SyncAccessionLookup();
            _restClient = new RestClient();

            SearchTextCommand = new DelegateCommand(OnSearchTextCommandExecuted);
            ItemTappedCommand = new DelegateCommand<object>(OnItemTappedCommandExecuted);

            SearchInExistingInventories = true;
        }

        public DelegateCommand SearchTextCommand { get; }
        private async void OnSearchTextCommandExecuted()
        {
            try
            {
                switch (_attribute.ControlSource)
                {
                    case "accession_lookup":
                        if (SearchInExistingInventories && !string.IsNullOrEmpty(Settings.WorkgroupInvMaintPolicies))
                            LookupList = await _restClient.GetAccessionLookUpListByAccessionNumberChecked(SearchText, Settings.WorkgroupInvMaintPolicies);
                        else
                            LookupList = await _restClient.GetAccessionLookUpListByAccessionNumber(SearchText);
                        break;
                    case "inventory_maint_policy_lookup":
                        LookupList = await _restClient.GetInventoryMaintPolicyLookupByMaintenanceName(SearchText);
                        break;
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public DelegateCommand<Object> ItemTappedCommand { get; }
        private async void OnItemTappedCommandExecuted(Object param)
        {
            try
            {
                ILookup selectedTtem = (ILookup)param;
                _attribute.Value = selectedTtem.ValueMember;
                _attribute.DisplayValue = selectedTtem.DisplayMember;

                var navigationParams = new NavigationParameters
                {
                    { "LookupPicker", _attribute }
                };
                await NavigationService.GoBackAsync(navigationParams);
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public async override void OnNavigatedTo(INavigationParameters parameters)
        {
            try
            {
                Utils.RefreshLang(this, _dataStoreService, "AccessionLookupPickerPage");

                if (parameters.ContainsKey("EntityAttribute"))
                {
                    _attribute = (EntityAttribute)parameters["EntityAttribute"];

                    if (_attribute.ControlType.Equals("DROPDOWN"))
                    {
                        IsSearchBarVisible = false;
                        LookupList = await _restClient.GetCodeValueByGroupName(_attribute.ControlSource);
                        //LookupList = await _dataStoreService.GetCodeValueList(_attribute.ControlSource);
                    }
                    else if (_attribute.ControlType.Equals("LOOKUPPICKER"))
                    {
                        IsSearchBarVisible = true;
                        LookupList = null;
                    }

                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
    }
}
