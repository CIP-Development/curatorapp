﻿using InventoryApp.Enums;
using InventoryApp.Helpers;
using InventoryApp.Models;
using InventoryApp.Services;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InventoryApp.ViewModels
{
    public class CropTraitObservationsPageViewModel : ViewModelBaseWithDialog
    {
        private IEnumerable<CropTrait> _cropTraitList = null;
        private IEnumerable<CropTraitCode> _cropTraitCodeList = null;
        private RestClient _restClient;
        private int _currentInventoryIndex = -1;

        private IEnumerable<InventoryThumbnail> _inventoryThumbnailList;
        public IEnumerable<InventoryThumbnail> InventoryThumbnailList
        {
            get { return _inventoryThumbnailList; }
            set { SetProperty(ref _inventoryThumbnailList, value); }
        }
        private InventoryThumbnail _currentInventory;
        public InventoryThumbnail CurrentInventory
        {
            get { return _currentInventory; }
            set { SetProperty(ref _currentInventory, value); }
        }
        private IEnumerable<EditingCropTraitObservation> _editingCropTraitObservations;
        public IEnumerable<EditingCropTraitObservation> EditingCropTraitObservations
        {
            get { return _editingCropTraitObservations; }
            set { SetProperty(ref _editingCropTraitObservations, value); }
        }
        
        public CropTraitObservationsPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
        : base(navigationService, pageDialogService)
        {
            _cropTraitCodeList = new List<CropTraitCode>();
            _restClient = new RestClient();

            InventoryThumbnailList = new List<InventoryThumbnail>();

            SaveCommand = new DelegateCommand(OnSaveCommand).ObservesProperty(() => IsNotBusy);
            PreviousInventoryCommand = new DelegateCommand(OnPreviousInventoryCommand, () =>
            {
                return _currentInventoryIndex > 0;
            });
            NextInventoryCommand = new DelegateCommand(OnNextInventoryCommand, () =>
            {
                return InventoryThumbnailList != null && _currentInventoryIndex < InventoryThumbnailList.Count() - 1;
            });
            NavigateBackCommand = new DelegateCommand(OnNavigateBackCommand);
        }

        public DelegateCommand SaveCommand { get; }
        private async void OnSaveCommand()
        {
            try
            {
                IsBusy = true;

                //Check some validations
                foreach (var editingCropTraitObservation in EditingCropTraitObservations)
                {
                    if (editingCropTraitObservation.IsCoded)
                    {
                        if (editingCropTraitObservation.SelectedCropTraitCode == null)
                        {
                            throw new Exception($"'{editingCropTraitObservation.CropTraitName}' is empty");
                        }
                    }
                }

                //Update current inventory
                foreach (var cropTraitObservation in CurrentInventory.CropTraitObservations)
                {
                    var editingCropTraitObservation = EditingCropTraitObservations.FirstOrDefault(x => x.CropTraitId == cropTraitObservation.Value.crop_trait_id);
                    if(editingCropTraitObservation != null)
                    {
                        if (editingCropTraitObservation.IsCoded)
                        {
                            cropTraitObservation.Value.code = editingCropTraitObservation.SelectedCropTraitCode.CodeValue;
                            cropTraitObservation.Value.crop_trait_code_id = editingCropTraitObservation.SelectedCropTraitCode.CropTraitCodeId;
                            cropTraitObservation.Value.DisplayText = editingCropTraitObservation.SelectedCropTraitCode.CodeValue;
                        }
                        else if (editingCropTraitObservation.DataType.Equals(CropTraitDataTypeEnum.Numeric.GetString()))
                        {
                            cropTraitObservation.Value.numeric_value = string.IsNullOrEmpty(editingCropTraitObservation.DisplayValue)
                                ? null
                                : (decimal?)decimal.Parse(editingCropTraitObservation.DisplayValue);
                            cropTraitObservation.Value.DisplayText = editingCropTraitObservation.DisplayValue;
                        }
                        else
                        {
                            cropTraitObservation.Value.string_value = editingCropTraitObservation.DisplayValue;
                            cropTraitObservation.Value.DisplayText = editingCropTraitObservation.DisplayValue;
                        }
                    }
                }

                List<string> errors = new List<string>();
                //Save changes in server
                foreach (var obs in CurrentInventory.CropTraitObservations)
                {
                    try
                    {
                        if (obs.Value.crop_trait_observation_id > 0) // Update
                        {
                            _ = await _restClient.UpdateCropTraitObservation(obs.Value);
                        }
                        else // Create
                        {
                            var cropTraitObservationId = await _restClient.CreateCropTraitObservation(new Models.Database.CropTraitObservationInsertDto
                            {
                                crop_trait_observation_id = 0,
                                crop_trait_id = obs.Value.crop_trait_id,
                                crop_trait_code_id = obs.Value.crop_trait_code_id,
                                inventory_id = CurrentInventory.inventory_id,
                                numeric_value = obs.Value.numeric_value,
                                string_value = obs.Value.string_value,
                                method_id = 1,
                                is_archived = "N",
                                created_date = DateTime.Now,
                                created_by = Settings.UserCooperatorId,
                                owned_date = DateTime.Now,
                                owned_by = Settings.UserCooperatorId,
                            });
                            obs.Value.crop_trait_observation_id = int.Parse(cropTraitObservationId);
                            obs.Value.inventory_id = CurrentInventory.inventory_id;
                        }
                    }
                    catch (Exception saveEx)
                    {
                        errors.Add(saveEx.Message);
                    }
                }
                if (errors.Any())
                {
                    throw new Exception(string.Join(Environment.NewLine, errors));
                }

                IsBusy = false;
                await PageDialogService.DisplayAlertAsync("Saving result", "Saved succesfully", "OK");
            }
            catch (Exception ex)
            {
                IsBusy = false;
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public DelegateCommand PreviousInventoryCommand { get; }
        private async void OnPreviousInventoryCommand()
        {
            try
            {
                if (_currentInventoryIndex >= 1)
                {
                    CurrentInventory = InventoryThumbnailList.ElementAt(--_currentInventoryIndex);
                    Title = $"{_currentInventoryIndex + 1} of {InventoryThumbnailList.Count()} Inventories";
                    LoadCropTraitsObservations();
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public DelegateCommand NextInventoryCommand { get; }
        private async void OnNextInventoryCommand()
        {
            try
            {
                if (_currentInventoryIndex <= InventoryThumbnailList.Count() - 2)
                {
                    CurrentInventory = InventoryThumbnailList.ElementAt(++_currentInventoryIndex);
                    Title = $"{_currentInventoryIndex + 1} of {InventoryThumbnailList.Count()} Inventories";
                    LoadCropTraitsObservations();
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public DelegateCommand NavigateBackCommand { get; }
        private async void OnNavigateBackCommand()
        {
            try
            {
                INavigationParameters parameters = new NavigationParameters
                {
                    { "RefreshDatagrid" , true}
                };
                var result = await NavigationService.GoBackAsync(parameters);
                if (result.Exception != null)
                {
                    throw result.Exception;
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        private void LoadCropTraitsObservations()
        {
            var tempEditingCropTraitObservations = new List<EditingCropTraitObservation>();

            foreach (var cropTraitObservation in CurrentInventory.CropTraitObservations)
            {
                var cropTrait = _cropTraitList.First(x => x.CropTraitId == cropTraitObservation.Key);

                var currentCropTraitCodeList = new List<CropTraitCode>();
                CropTraitCode selectedCropTraitCode = null;
                if (cropTrait.IsCoded.Equals('Y'))
                {
                    currentCropTraitCodeList = _cropTraitCodeList.Where(x => x.CropTraitId == cropTrait.CropTraitId).ToList();
                    selectedCropTraitCode = currentCropTraitCodeList.FirstOrDefault(x => x.CropTraitCodeId == cropTraitObservation.Value.crop_trait_code_id);
                }

                tempEditingCropTraitObservations.Add(new EditingCropTraitObservation
                {
                    CropTraitId = cropTrait.CropTraitId,
                    CropTraitName = cropTrait.CropTraitName,
                    DisplayValue = cropTraitObservation.Value.DisplayText,
                    IsCoded = cropTrait.IsCoded.Equals('Y'),
                    DataType = cropTrait.DataTypeCode,
                    CropTraitCodeList = currentCropTraitCodeList,
                    SelectedCropTraitCode = selectedCropTraitCode
                });
            }

            EditingCropTraitObservations = tempEditingCropTraitObservations;

            PreviousInventoryCommand.RaiseCanExecuteChanged();
            NextInventoryCommand.RaiseCanExecuteChanged();
        }
        public override async void Initialize(INavigationParameters parameters)
        {
            try
            {
                IsBusy = true;
                if (!parameters.ContainsKey("CropTraitList"))
                {
                    throw new Exception("CropTraitList is empty");
                }
                if (!parameters.ContainsKey("InventoryThumbnailList"))
                {
                    throw new Exception("InventoryThumbnailList is empty");
                }
                if (!parameters.ContainsKey("CurrentInventory"))
                {
                    throw new Exception("CurrentInventory is empty");
                }

                _cropTraitList = parameters.GetValue<IEnumerable<CropTrait>>("CropTraitList");
                if (_cropTraitList.Any())
                {
                    //Get all crop trait codes
                    var cropTraitIds = _cropTraitList.Select(x => x.CropTraitId).ToArray();
                    _cropTraitCodeList = await _restClient.GetCropTraitCodeList(string.Join(",", cropTraitIds));
                }

                if (parameters.ContainsKey("InventoryThumbnailList"))
                {
                    InventoryThumbnailList = parameters.GetValue<IEnumerable<InventoryThumbnail>>("InventoryThumbnailList");
                    if (!InventoryThumbnailList.Any())
                    {
                        throw new Exception("InventoryThumbnailList is empty");
                    }
                    Title = $"{InventoryThumbnailList.Count()} Items";
                }

                CurrentInventory = parameters.GetValue<InventoryThumbnail>("CurrentInventory");

                int inventoryIndex = 0;
                bool inventoryFound = false;
                var inventoryComparer = EqualityComparer<InventoryThumbnail>.Default;
                foreach (InventoryThumbnail inventory in InventoryThumbnailList)
                {
                    if (inventoryComparer.Equals(inventory, CurrentInventory))
                    {
                        inventoryFound = true;
                        break;
                    }
                    inventoryIndex++;
                }
                if (!inventoryFound)
                {
                    throw new Exception("CurrentInvetory not found in InventoryThumbnailList");
                }
                _currentInventoryIndex = inventoryIndex;
                Title = $"{_currentInventoryIndex + 1} of {InventoryThumbnailList.Count()} Inventories";

                LoadCropTraitsObservations();

                IsBusy = false;
            }
            catch (Exception ex)
            {
                IsBusy = false;
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
    }
}
