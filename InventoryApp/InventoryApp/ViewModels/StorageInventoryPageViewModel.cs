﻿using InventoryApp.Extensions;
using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using InventoryApp.Models.Database;
using InventoryApp.Models.LocalStorage;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InventoryApp.ViewModels
{
    public class StorageInventoryPageViewModel : ViewModelBaseWithDialog
    {
        private readonly IOrderRequestRepository _orderRequestRepository;
        private readonly IRestService _restService;

        private OrderRequestDb _selectedOrderRequestDb;
        public OrderRequestDb SelectedOrderRequestDb
        {
            get { return _selectedOrderRequestDb; }
            set { SetProperty(ref _selectedOrderRequestDb, value); }
        }
        private IEnumerable<OrderRequestItem> _inventoryList;
        public IEnumerable<OrderRequestItem> InventoryList
        {
            get { return _inventoryList; }
            set { SetProperty(ref _inventoryList, value); }
        }
        private OrderRequestItem _SelectedInventory;
        public OrderRequestItem SelectedInventory
        {
            get { return _SelectedInventory; }
            set { SetProperty(ref _SelectedInventory, value); }
        }
        private bool _IsSelectingParentInventory;
        public bool IsSelectingParentInventory
        {
            get { return _IsSelectingParentInventory; }
            set { SetProperty(ref _IsSelectingParentInventory, value); }
        }
        public StorageInventoryPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService,
            IOrderRequestRepository orderRequestRepository,
            IRestService restService)
            : base(navigationService, pageDialogService)
        {
            _restService = restService;
            _orderRequestRepository = orderRequestRepository;

            SelectParentInventoryCommand = new DelegateCommand(ExecuteSelectParentInventoryCommand);
            SelectInventoryCommand = new DelegateCommand(ExecuteSelectInventoryCommand);
        }

        public DelegateCommand SelectParentInventoryCommand { get; }
        private void ExecuteSelectParentInventoryCommand()
        {
            IsSelectingParentInventory = !IsSelectingParentInventory;
        }
        public DelegateCommand SelectInventoryCommand { get; }
        private void ExecuteSelectInventoryCommand()
        {
            IsSelectingParentInventory = false;
        }
        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            try
            {
                if (!IsInitialized)
                {
                    if (Settings.SelectedRegenerationOrderRequestId == -1)
                        throw new Exception("Seleccionar solicitud en Ajustes de regeneración");

                    SelectedOrderRequestDb = await _orderRequestRepository.GetOrderRequestById(Settings.SelectedRegenerationOrderRequestId);
                    if (SelectedOrderRequestDb == null)
                        throw new Exception("Solicitud no encontrada\nIr a Ajustes de regeneración");

                    InventoryList = await _restService.GetOrderRequestItems(SelectedOrderRequestDb.OrderRequestId);

                    IsInitialized = true;
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
        }
    }
}
