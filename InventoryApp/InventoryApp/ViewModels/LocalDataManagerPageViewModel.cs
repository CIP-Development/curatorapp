﻿using InventoryApp.Extensions;
using InventoryApp.Interfaces;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InventoryApp.ViewModels
{
    public class LocalDataManagerPageViewModel : ViewModelBaseWithDialog
    {
        IRestService _restService;
        ICodeValueLookupLocalRepository _codeValueLookupLocalRepository;

        public LocalDataManagerPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService,
            IRestService restService, ICodeValueLookupLocalRepository codeValueLookupLocalRepository)
            : base(navigationService, pageDialogService)
        {
            _restService = restService;
            _codeValueLookupLocalRepository = codeValueLookupLocalRepository;

            UpdateAllCommand = new DelegateCommand(ExecuteUpdateAllCommand).ObservesProperty(() => IsNotBusy);
            ViewCodeValueListCommand = new DelegateCommand<string>(ExecuteViewCodeValueListCommand).ObservesCanExecute(() => IsNotBusy);
            UpdateCodeValueListCommand = new DelegateCommand<string>(ExecuteUpdateCodeValueListCommand).ObservesCanExecute(() => IsNotBusy);
        }

        public DelegateCommand UpdateAllCommand { get; }
        private async void ExecuteUpdateAllCommand()
        {
            try
            {
                IsBusy = true;

                var germplasmFormList = await _restService.GetCodeValueLookupList("GERMPLASM_FORM");
                var inventoryContainerTypeList = await _restService.GetCodeValueLookupList("INVENTORY_CONTAINER_TYPE");
                var unitOfQuantityList = await _restService.GetCodeValueLookupList("UNIT_OF_QUANTITY");
                var inventoryActionList = await _restService.GetCodeValueLookupList("INVENTORY_ACTION");
                var regenerationLocalityList = await _restService.GetCodeValueLookupList("REGENERATION_LOCALITY");
                var inventoryPollinationVectorList = await _restService.GetCodeValueLookupList("INVENTORY_POLLINATION_VECTOR");

                await _codeValueLookupLocalRepository.DeleteByGroupNameAsync("GERMPLASM_FORM");
                await _codeValueLookupLocalRepository.DeleteByGroupNameAsync("INVENTORY_CONTAINER_TYPE");
                await _codeValueLookupLocalRepository.DeleteByGroupNameAsync("UNIT_OF_QUANTITY");
                await _codeValueLookupLocalRepository.DeleteByGroupNameAsync("INVENTORY_ACTION");
                await _codeValueLookupLocalRepository.DeleteByGroupNameAsync("REGENERATION_LOCALITY");
                await _codeValueLookupLocalRepository.DeleteByGroupNameAsync("INVENTORY_POLLINATION_VECTOR");

                await _codeValueLookupLocalRepository.InsertManyAsync(germplasmFormList);
                await _codeValueLookupLocalRepository.InsertManyAsync(inventoryContainerTypeList);
                await _codeValueLookupLocalRepository.InsertManyAsync(unitOfQuantityList);
                await _codeValueLookupLocalRepository.InsertManyAsync(inventoryActionList);
                await _codeValueLookupLocalRepository.InsertManyAsync(regenerationLocalityList);
                await _codeValueLookupLocalRepository.InsertManyAsync(inventoryPollinationVectorList);

                await PageDialogService.DisplayAlertAsync("Resultado de actualizar todo",
                    $"GERMPLASM_FORM : {germplasmFormList.Count()}\n" +
                    $"INVENTORY_CONTAINER_TYPE : {inventoryContainerTypeList.Count()}\n" +
                    $"UNIT_OF_QUANTITY: {unitOfQuantityList.Count()}\n" +
                    $"INVENTORY_ACTION: {inventoryActionList.Count()}\n" +
                    $"REGENERATION_LOCALITY: {regenerationLocalityList.Count()}\n" +
                    $"INVENTORY_POLLINATION_VECTOR: {inventoryPollinationVectorList.Count()}\n",
                    "OK");
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand<string> ViewCodeValueListCommand { get; }
        private async void ExecuteViewCodeValueListCommand(string groupName)
        {
            try
            {
                IsBusy = true;

                var codeValueLookupList = await _codeValueLookupLocalRepository.GetByGroupNameAsync(groupName);
                var codeValueAsStringList = codeValueLookupList.Select(x => $"{x.value_member} : {x.display_member}").ToArray();
                await PageDialogService.DisplayAlertAsync(groupName, string.Join("\n", codeValueAsStringList), "OK");
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand<string> UpdateCodeValueListCommand { get; }
        private async void ExecuteUpdateCodeValueListCommand(string groupName)
        {
            try
            {
                IsBusy = true;

                var codeValueList = await _restService.GetCodeValueLookupList(groupName);
                await _codeValueLookupLocalRepository.DeleteByGroupNameAsync(groupName);
                await _codeValueLookupLocalRepository.InsertManyAsync(codeValueList);

                await PageDialogService.DisplayAlertAsync($"Resultado de actualizar",
                    $"{groupName} : {codeValueList.Count()}",
                    "OK");
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
