﻿using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using InventoryApp.Services;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InventoryApp.ViewModels
{
    public class WelcomePageViewModel : ViewModelBaseWithDialog
    {
        private readonly RestClient _restClient;
        IDataStoreService _dataStoreService;

        #region Properties
        private List<CooperatorGroup> _listWorkgroup;
        public List<CooperatorGroup> ListWorkgroup
        {
            get { return _listWorkgroup; }
            set { SetProperty(ref _listWorkgroup, value); }
        }
        private List<string> _listLocation1;
        public List<string> ListLocation1
        {
            get { return _listLocation1; }
            set { SetProperty(ref _listLocation1, value); }
        }
        private CooperatorGroup _workgroup;
        public CooperatorGroup Workgroup
        {
            get { return _workgroup; }
            set { SetProperty(ref _workgroup, value); }
        }
        private string _location1;
        public string Location1
        {
            get { return _location1; }
            set { SetProperty(ref _location1, value); }
        }
        
        private int _cooperatorGroupIndex;
        public int CooperatorGroupIndex
        {
            get { return _cooperatorGroupIndex; }
            set { SetProperty(ref _cooperatorGroupIndex, value); }
        }
        #endregion

        #region LangProperties
        private string _UxTitle;
        public string UxTitle
        {
            get { return _UxTitle; }
            set { SetProperty(ref _UxTitle, value); }
        }
        private string _uxLabelWorkgroup;
        public string UxLabelWorkgroup
        {
            get { return _uxLabelWorkgroup; }
            set { SetProperty(ref _uxLabelWorkgroup, value); }
        }
        private string _uxLabelDefaultLocation1;
        public string UxLabelDefaultLocation1
        {
            get { return _uxLabelDefaultLocation1; }
            set { SetProperty(ref _uxLabelDefaultLocation1, value); }
        }
        private string _uxButtonSearchInventory;
        public string UxButtonSearchInventory
        {
            get { return _uxButtonSearchInventory; }
            set { SetProperty(ref _uxButtonSearchInventory, value); }
        }
        private string _uxButtonRegisterNewInventory;
        public string UxButtonRegisterNewInventory
        {
            get { return _uxButtonRegisterNewInventory; }
            set { SetProperty(ref _uxButtonRegisterNewInventory, value); }
        }
        private string _uxButtonInventoryList;
        public string UxButtonInventoryList
        {
            get { return _uxButtonInventoryList; }
            set { SetProperty(ref _uxButtonInventoryList, value); }
        }
        private string _uxButtonInventoryViability;
        public string UxButtonInventoryViability
        {
            get { return _uxButtonInventoryViability; }
            set { SetProperty(ref _uxButtonInventoryViability, value); }
        }
        private string _UxButtonSearchUserLists;
        public string UxButtonSearchUserLists
        {
            get { return _UxButtonSearchUserLists; }
            set { SetProperty(ref _UxButtonSearchUserLists, value); }
        }
        #endregion

        public WelcomePageViewModel(INavigationService navigationService, IPageDialogService pageDialogService, IDataStoreService dataStoreService) 
            : base(navigationService, pageDialogService)
        {
            _restClient = new RestClient();
            _dataStoreService = dataStoreService;

            Title = "Home";

            NavigateCommand = new DelegateCommand<string>(OnNavigateCommandExecuted);
            LogoutCommand = new DelegateCommand(OnLogoutCommandExecuted);

            ListWorkGroupChangedCommand = new DelegateCommand(OnListWorkGroupChangedCommand);
            ListLocationChangedCommand = new DelegateCommand(OnListLocationChangedCommand);

        }

        public DelegateCommand LogoutCommand { get; }
        private async void OnLogoutCommandExecuted()
        {
            Settings.UserToken = string.Empty;
            await NavigationService.NavigateAsync("/LoginPage");
        }

        public DelegateCommand<string> NavigateCommand { get; }
        private async void OnNavigateCommandExecuted(string path)
        {
            try
            {
                if (Workgroup == null)
                    throw new Exception("Workgroup is empty");

                await NavigationService.NavigateAsync(path, null, false, true);
            }
            catch (Exception e)
            {
                await PageDialogService.DisplayAlertAsync("Error", e.Message, "OK");
            }
        }

        public DelegateCommand ListWorkGroupChangedCommand { get; }
        private async void OnListWorkGroupChangedCommand()
        {
            try
            {
                if (Workgroup != null)
                {
                    Settings.WorkgroupId = Workgroup.sys_group_id;
                    Settings.WorkgroupName = Workgroup.group_name;
                    Settings.WorkgroupInvMaintPolicies = Workgroup.inv_maint_policy_ids;
                    Settings.WorkgroupInventoryDataview = Workgroup.inventory_dataview;
                    Settings.WorkgroupInventoryThumbnailDataview = Workgroup.inventory_thumbnail_dataview;
                    Settings.WorkgroupCooperatorId = Workgroup.workgroup_cooperator_id ?? -1;
                }
            }
            catch (Exception e)
            {
                await PageDialogService.DisplayAlertAsync("Error", e.Message, "OK");
            }
        }
        public DelegateCommand ListLocationChangedCommand { get; }
        private async void OnListLocationChangedCommand()
        {
            try
            {
                if (Location1 != null)
                    Settings.Location1 = Location1;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }

        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            try
            {
                //Load workgroups
                if (ListWorkgroup == null)
                {
                    ListWorkgroup = await _restClient.GetWorkGroups(Settings.UserCooperatorId);
                    Workgroup = ListWorkgroup.FirstOrDefault(g => g.group_name.Equals(Settings.WorkgroupName));
                    if (Workgroup == null)
                    {
                        Settings.WorkgroupId = -1;
                        Settings.WorkgroupName = string.Empty;
                        Settings.WorkgroupInvMaintPolicies = null;
                        Settings.WorkgroupInventoryDataview = null;
                        Settings.WorkgroupInventoryThumbnailDataview = null;
                        Settings.WorkgroupCooperatorId = -1;
                    }
                    else
                    {
                        Settings.WorkgroupId = Workgroup.sys_group_id;
                        Settings.WorkgroupInvMaintPolicies = Workgroup.inv_maint_policy_ids;
                        Settings.WorkgroupInventoryDataview = Workgroup.inventory_dataview;
                        Settings.WorkgroupInventoryThumbnailDataview = Workgroup.inventory_thumbnail_dataview;
                        Settings.WorkgroupCooperatorId = Workgroup.workgroup_cooperator_id ?? -1;
                    }
                }
                //Load Location 1
                if (ListLocation1 == null)
                {
                    ListLocation1 = await _restClient.GetAllLocation1List();
                    Location1 = Settings.Location1;
                }
                
                if (_dataStoreService.GroupSettingList == null)
                {
                    await _dataStoreService.RefreshGroupSettings();
                }

                if (_dataStoreService.AppLangResourceList == null)
                {
                    _dataStoreService.RefreshAppLangResources();
                }
                Utils.RefreshLang(this, _dataStoreService, "WelcomePage");
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
            finally
            {
                if (_dataStoreService.GroupSettingList == null)
                    _dataStoreService.GroupSettingList = new List<GroupSetting>();
            }
        }
    }
}
