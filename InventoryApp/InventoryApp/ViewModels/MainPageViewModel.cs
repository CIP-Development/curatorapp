﻿using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using Xamarin.Essentials;

namespace InventoryApp.ViewModels
{
    public class MainPageViewModel : ViewModelBaseWithDialog
    {
        private IDataStoreService _dataStoreService;

        private List<MenuItem> _menuList;
        public List<MenuItem> MenuList
        {
            get { return _menuList; }
            set { SetProperty(ref _menuList, value); }
        }

        private MenuItem _selectedMenu;
        public MenuItem SelectedMenuItem
        {
            get { return _selectedMenu; }
            set { SetProperty(ref _selectedMenu, value); }
        }

        #region Lang
        private string _UxTitle;
        public string UxTitle
        {
            get { return _UxTitle; }
            set { SetProperty(ref _UxTitle, value); }
        }
        private string _UxLabelWelcomeUser;
        public string UxLabelWelcomeUser
        {
            get { return _UxLabelWelcomeUser; }
            set { SetProperty(ref _UxLabelWelcomeUser, value); }
        }
        private string _UxLabelConnectedTo;
        public string UxLabelConnectedTo
        {
            get { return _UxLabelConnectedTo; }
            set { SetProperty(ref _UxLabelConnectedTo, value); }
        }
        private string _UxButtonLogout;
        public string UxButtonLogout
        {
            get { return _UxButtonLogout; }
            set { SetProperty(ref _UxButtonLogout, value); }
        }
        #endregion
        public MainPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService, IDataStoreService dataStoreService)
            : base(navigationService, pageDialogService)
        {
            _dataStoreService = dataStoreService;
            string offlineColor = "#D5D5D5";
            MenuList = new List<MenuItem> { 
                new MenuItem { Text = _dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "MainPage", "UxLabelHome"), Path = "NavigationPage/WelcomePage"} ,
                new MenuItem { Text = "Buscar inventarios", Path = "NavigationPage/InventoriesPage/SearchInventoriesPage"} ,
                new MenuItem { Text = _dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "MainPage", "UxLabelInventoryList"), Path = "NavigationPage/InventoriesPage" },
                //new MenuItem { Text = "Registro de adquisiciones ", Path = "NavigationPage/InventoriesPage/InventoryPage"} ,
                new MenuItem { Text = "Registro de adquisiciones ", Path = "NavigationPage/InventoriesPage/CreateAdquisitionInventoryPage"} ,
                new MenuItem { Text = "Solicitudes de regeneración / viabilidad", Path = "NavigationPage/RegenerationOrderRequestsPage", BackgroundColor = offlineColor},
                new MenuItem { Text = _dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "MainPage", "UxLabelViabilityTest"),
                    Path = "NavigationPage/SearchInventoryViabilityPage", BackgroundColor = offlineColor },
                new MenuItem { Text = "Ajustes de regeneración", Path = "NavigationPage/CrossToHarvestPage", BackgroundColor = offlineColor },
                new MenuItem { Text = "   Siembra y transplantes", Path = "NavigationPage/PlantingPage", BackgroundColor = offlineColor },
                new MenuItem { Text = "   Monitoreo", Path = "NavigationPage/RegenerationMonitoringPage", BackgroundColor = offlineColor },
                new MenuItem { Text = "   Cruzamiento", Path = "NavigationPage/CrossingPage", BackgroundColor = offlineColor },
                new MenuItem { Text = "   Cosecha de frutos", Path = "NavigationPage/HarvestFruitPage", BackgroundColor = offlineColor },
                new MenuItem { Text = "   Cosecha de tubérculos / raíces", Path = "NavigationPage/HarvestTuberPage", BackgroundColor = offlineColor },
                new MenuItem { Text = "   Maceración", Path = "NavigationPage/MacerationPage", BackgroundColor = offlineColor },
                new MenuItem { Text = "   Almacenamiento", Path = "NavigationPage/StorageInventoryPage", BackgroundColor = offlineColor },
                new MenuItem { Text = "   Reportes",
                    Path = "https://app.powerbi.com/groups/me/apps/6e2075f1-3eef-4f56-b71a-c9cf21e1e8e3/reports/cd2980e8-b4cd-4538-b81f-bddefda0dab8/ReportSection?ctid=6afa0e00-fa14-40b7-8a2e-22a7f8c357d5",
                    BackgroundColor = offlineColor },
                new MenuItem { Text = "Administrador de datos locales", Path = "LocalDataManagerPage" },
                new MenuItem { Text = _dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "MainPage", "UxLabelSettings"), Path = "NavigationPage/SettingsPage" }
                /*new MenuItem { Text = _dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "MainPage", "UxLabelAbout"), Path = "NavigationPage/About" }*/
            };

            NavigateCommand = new DelegateCommand<string>(OnNavigateCommandExecuted);
            LogoutCommand = new DelegateCommand(OnLogoutCommandExecuted);

            ItemTappedCommand = new DelegateCommand(OnItemTappedCommandExecuted);

            UxLabelWelcomeUser = "Welcome {0}";
            UxLabelConnectedTo = "Connected to {0}";
        }

        public DelegateCommand ItemTappedCommand { get; }
        private async void OnItemTappedCommandExecuted()
        {
            if (!string.IsNullOrEmpty(SelectedMenuItem.Path))
            {
                bool isUrl = Uri.TryCreate(SelectedMenuItem.Path, UriKind.Absolute, out Uri uriResult)
                    && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);
                if (isUrl)
                    await Browser.OpenAsync(SelectedMenuItem.Path, BrowserLaunchMode.SystemPreferred);
                else
                    await NavigationService.NavigateAsync(SelectedMenuItem.Path, null);
            }
        }

        public DelegateCommand<string> NavigateCommand { get; }
        private async void OnNavigateCommandExecuted(string path)
        {
            await NavigationService.NavigateAsync(path);
        }
        public DelegateCommand LogoutCommand { get; }
        private async void OnLogoutCommandExecuted()
        {
            try
            {
                _dataStoreService.InventoryList = null;
                await NavigationService.NavigateAsync("/LoginPage");
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public override void OnNavigatedTo(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("SelectedMenuItemIndex") && parameters["SelectedMenuItemIndex"] != null && !string.IsNullOrWhiteSpace(parameters["SelectedMenuItemIndex"].ToString()))
            {
                int selectedHomeMenuItemIndex = int.TryParse(parameters["SelectedMenuItemIndex"].ToString(), out selectedHomeMenuItemIndex) ? selectedHomeMenuItemIndex : -1;
                if (selectedHomeMenuItemIndex > -1 && selectedHomeMenuItemIndex < _menuList.Count)
                    SelectedMenuItem = _menuList[selectedHomeMenuItemIndex];
            }
            SelectedMenuItem = MenuList.First();

            Utils.RefreshLang(this, _dataStoreService, "MainPage");
            UxLabelWelcomeUser = string.Format(UxLabelWelcomeUser, Settings.Username);
            UxLabelConnectedTo = string.Format(UxLabelConnectedTo, Settings.Server);
        }
    }
}
