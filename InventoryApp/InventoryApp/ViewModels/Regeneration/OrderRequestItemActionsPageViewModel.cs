﻿using InventoryApp.Extensions;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InventoryApp.ViewModels
{
    public class OrderRequestItemActionsPageViewModel : ViewModelBaseWithDialog
    {
        private readonly IRestService _restService;

        private IEnumerable<OrderRequestItem> _selectedOrderRequestItemList;
        public IEnumerable<OrderRequestItem> SelectedOrderRequestItemList
        {
            get { return _selectedOrderRequestItemList; }
            set { SetProperty(ref _selectedOrderRequestItemList, value); }
        }
        private OrderRequestItem _currentOrderRequestItem;
        public OrderRequestItem CurrentOrderRequestItem
        {
            get { return _currentOrderRequestItem; }
            set { SetProperty(ref _currentOrderRequestItem, value); }
        }
        private IEnumerable<OrderRequestItemAction> _orderRequestItemActionList;
        public IEnumerable<OrderRequestItemAction> OrderRequestItemActionList
        {
            get { return _orderRequestItemActionList; }
            set { SetProperty(ref _orderRequestItemActionList, value); }
        }
        public OrderRequestItemActionsPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService,
            IRestService restService)
            : base(navigationService, pageDialogService)
        {
            _restService = restService;

            Title = "Order item actions";

            CancelCommand = new DelegateCommand(ExecuteCancelCommand).ObservesCanExecute(() => IsNotBusy);
            SaveCommand = new DelegateCommand(ExecuteSaveCommand).ObservesCanExecute(() => IsNotBusy);
        }

        public DelegateCommand CancelCommand { get; }
        private async void ExecuteCancelCommand()
        {
            _ = await NavigationService.GoBackAsync();
        }
        public DelegateCommand SaveCommand { get; }
        private async void ExecuteSaveCommand()
        {
            try
            {
                IsBusy = true;

                string json = JsonConvert.SerializeObject(CurrentOrderRequestItem, Formatting.Indented);
                var continueSaving = await PageDialogService.DisplayAlertAsync("Are you sure to continue", json, "OK", "CANCEL");
                if (!continueSaving)
                    return;

                await PageDialogService.DisplayAlertAsync("Result", $"Successfully updated", "OK");

                var navigationResult = await NavigationService.GoBackAsync(new NavigationParameters
                {
                    { "RefreshItems", true }
                });
                if (!navigationResult.Success)
                    throw navigationResult.Exception;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public override void Initialize(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("SelectedOrderRequestItemList"))
            {
                SelectedOrderRequestItemList = parameters.GetValue<IEnumerable<OrderRequestItem>>("SelectedOrderRequestItemList");
            }
            else
            {
                throw new Exception($"{nameof(SelectedOrderRequestItemList)} is required");
            }
        }
        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            try
            {
                IsBusy = true;

                Title = $"Acciones de items : {SelectedOrderRequestItemList.Count()} item(s)";

                var orderRequestItemIds = SelectedOrderRequestItemList.Select(x => x.order_request_item_id).ToArray();
                OrderRequestItemActionList = await _restService.GetOrderRequestItemActionAsync(orderRequestItemIds);
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
