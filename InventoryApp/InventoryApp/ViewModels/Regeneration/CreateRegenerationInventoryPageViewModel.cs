﻿using InventoryApp.Extensions;
using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using InventoryApp.Models.Database;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InventoryApp.ViewModels
{
    public class CreateRegenerationInventoryPageViewModel : ViewModelBaseWithDialog
    {
        private readonly IRestService _restService;
        public IEnumerable<OrderRequestItem> SelectedOrderRequestItems { get; set; }


        private IEnumerable<CodeValue> _formTypeCodeListt;
        public IEnumerable<CodeValue> FormTypeCodeList
        {
            get { return _formTypeCodeListt; }
            set { SetProperty(ref _formTypeCodeListt, value); }
        }
        private IEnumerable<CodeValue> _containerCodeList;
        public IEnumerable<CodeValue> ContainerCodeList
        {
            get { return _containerCodeList; }
            set { SetProperty(ref _containerCodeList, value); }
        }
        private CodeValue _selectedFormTypeCode;
        public CodeValue SelectedFormTypeCode
        {
            get { return _selectedFormTypeCode; }
            set { SetProperty(ref _selectedFormTypeCode, value); }
        }
        private CodeValue _selectedContainerCode;
        public CodeValue SelectedContainerCode
        {
            get { return _selectedContainerCode; }
            set { SetProperty(ref _selectedContainerCode, value); }
        }
        private string _storageLocation1;
        public string StorageLocation1
        {
            get { return _storageLocation1; }
            set { SetProperty(ref _storageLocation1, value); }
        }
        private string _storageLocation2;
        public string StorageLocation2
        {
            get { return _storageLocation2; }
            set { SetProperty(ref _storageLocation2, value); }
        }
        private string _storageLocation3;
        public string StorageLocation3
        {
            get { return _storageLocation3; }
            set { SetProperty(ref _storageLocation3, value); }
        }
        private string _storageLocation4;
        public string StorageLocation4
        {
            get { return _storageLocation4; }
            set { SetProperty(ref _storageLocation4, value); }
        }
        private string _notes;
        public string Notes
        {
            get { return _notes; }
            set { SetProperty(ref _notes, value); }
        }
        private IEnumerable<ILookup> _containerTypeCodeValueList;
        public IEnumerable<ILookup> ContainerTypeCodeValueList
        {
            get { return _containerTypeCodeValueList; }
            set { SetProperty(ref _containerTypeCodeValueList, value); }
        }
        private ILookup _selectedContainerTypeCodeValue;
        public ILookup SelectedContainerTypeCodeValue
        {
            get { return _selectedContainerTypeCodeValue; }
            set
            {
                var notified = SetProperty(ref _selectedContainerTypeCodeValue, value);
                //if (notified)
                //    NewInventory.storage_location_part4 = value == null ? null : (string)value.ValueMember;
            }
        }

        public CreateRegenerationInventoryPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService,
            IRestService restService)
            : base(navigationService, pageDialogService)
        {
            _restService = restService;

            CancelCommand = new DelegateCommand(ExecuteCancelCommand).ObservesCanExecute(() => IsNotBusy);
            SaveCommand = new DelegateCommand(ExecuteSaveCommand).ObservesCanExecute(() => IsNotBusy);
        }

        public DelegateCommand CancelCommand { get; }
        private async void ExecuteCancelCommand()
        {
            await NavigationService.GoBackAsync();
        }
        public DelegateCommand SaveCommand { get; }
        private async void ExecuteSaveCommand()
        {
            try
            {
                IsBusy = true;

                foreach (var orderRequestItem in SelectedOrderRequestItems)
                {
                    var orderItemInventory = await _restService.GetInventoryAsync(orderRequestItem.inventory_id);

                    var newInventory = new Inventory()
                    {
                        accession_id = orderItemInventory.accession_id,
                        form_type_code = SelectedFormTypeCode.Code,
                        inventory_maint_policy_id = orderItemInventory.inventory_maint_policy_id,
                        inventory_number_part1 = orderRequestItem.order_request_id.ToString(),
                        inventory_number_part2 = orderItemInventory.inventory_id,
                        inventory_number_part3 = "1",
                        is_available = "N",
                        is_distributable = "N",
                        is_auto_deducted = "N",
                        parent_inventory_id = orderItemInventory.inventory_id,
                        pathogen_status_code = string.Empty,
                        quantity_on_hand = orderRequestItem.quantity_shipped,
                        quantity_on_hand_unit_code = orderRequestItem.quantity_shipped_unit_code,
                        storage_location_part1 = StorageLocation1,
                        storage_location_part2 = StorageLocation2,
                        storage_location_part3 = StorageLocation3,
                        storage_location_part4 = StorageLocation4,
                        note = Notes,
                        availability_status_code = "NOT-AVAIL",
                        workgroup_cooperator_id = Settings.WorkgroupCooperatorId,
                    };

                    var newInventoryIdStr = await _restService.CreateInventoryAsync(newInventory);
                    newInventory.inventory_id = int.Parse(newInventoryIdStr);

                    var newOrderRequestItemAction = new OrderRequestItemAction()
                    {
                        order_request_item_id = orderRequestItem.order_request_item_id,
                        action_name_code = "REG_INV",
                        action_cost = null,
                        action_information = newInventoryIdStr,
                        started_date = DateTime.Now,
                        started_date_code = "MM/dd/yyyy",
                        completed_date = null,
                        completed_date_code = null,
                        cooperator_id = Settings.UserCooperatorId,
                        note = "Regeneration inventory created",
                    };
                    var newOrderRequestItemActionIdStr = await _restService.CreateOrderRequestItemActionAsync(newOrderRequestItemAction);
                }

                await PageDialogService.DisplayAlertAsync("Resultado de crear inventarios de regeneración", "Se crearon los inventarios correctamente", "OK");
                await NavigationService.GoBackAsync();
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public override void Initialize(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("SelectedOrderRequestItems"))
            {
                SelectedOrderRequestItems = parameters.GetValue<IEnumerable<OrderRequestItem>>("SelectedOrderRequestItems");
            }
            else
                throw new Exception("SelectedOrderRequestItems is required");

            FormTypeCodeList = CodeValueFactory.FormTypeList;
            ContainerCodeList = CodeValueFactory.InventoryContainerList;
            SelectedFormTypeCode = FormTypeCodeList.FirstOrDefault(x => x.Code.Equals("RE"));
            SelectedContainerCode = ContainerCodeList.FirstOrDefault();
        }
        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            try
            {
                IsBusy = true;

                ContainerTypeCodeValueList = await _restService.GetCodeValueLookupList("INVENTORY_CONTAINER_TYPE");
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
