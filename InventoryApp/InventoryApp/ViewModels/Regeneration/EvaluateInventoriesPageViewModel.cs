﻿using InventoryApp.Extensions;
using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using InventoryApp.Models.Database;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InventoryApp.ViewModels
{
    public class EvaluateInventoriesPageViewModel : ViewModelBaseWithDialog
    {
        private readonly IDialogService _dialogService;
        private IRestService _restService;
        private IEnumerable<CropTraitObservation> _cropTraitObservations;

        private IEnumerable<InventoryThumbnail> _inventoryThumbnailList;
        public IEnumerable<InventoryThumbnail> InventoryThumbnailList
        {
            get { return _inventoryThumbnailList; }
            set { SetProperty(ref _inventoryThumbnailList, value); }
        }
        private InventoryThumbnail _selectedInventoryThumbnail;
        public InventoryThumbnail SelectedInventoryThumbnail
        {
            get { return _selectedInventoryThumbnail; }
            set { SetProperty(ref _selectedInventoryThumbnail, value); }
        }
        private IEnumerable<Lookup> _regenerationStatusCodeList;
        public IEnumerable<Lookup> RegenerationStatusCodeList
        {
            get { return _regenerationStatusCodeList; }
            set { SetProperty(ref _regenerationStatusCodeList, value); }
        }
        private Lookup _selectedRegenarionStatusCode;
        public Lookup SelectedRegenarionStatusCode
        {
            get { return _selectedRegenarionStatusCode; }
            set { SetProperty(ref _selectedRegenarionStatusCode, value); }
        }
        private string _notes;
        public string Notes
        {
            get { return _notes; }
            set { SetProperty(ref _notes, value); }
        }
        private int _inventoryThumbnailListCount;
        public int InventoryThumbnailListCount
        {
            get { return _inventoryThumbnailListCount; }
            set { SetProperty(ref _inventoryThumbnailListCount, value); }
        }
        private string _searchText;
        public string SearchText
        {
            get { return _searchText; }
            set { SetProperty(ref _searchText, value); }
        }
        public CropTraitObservation CropTraitObservationRegStatus { get; set; }
        public CropTraitObservation CropTraitObservationRegNotes { get; set; }
        public EvaluateInventoriesPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService,
            IRestService restService, IDialogService dialogService)
            : base(navigationService, pageDialogService)
        {
            _restService = restService;
            _dialogService = dialogService;

            Title = "Evaluar inventarios";

            RegenerationStatusCodeList = new List<Lookup>()
            {
                new Lookup { value_member = 2785, display_member = "No viable"},
                new Lookup { value_member = 2784, display_member = "Viable"}
            };

            CancelCommand = new DelegateCommand(ExecuteCancelCommand);
            SaveCommand = new DelegateCommand(ExecuteSaveCommand).ObservesCanExecute(() => IsNotBusy);
            InventorySelectedCommand = new DelegateCommand(ExecuteInventorySelectedCommand).ObservesCanExecute(() => IsNotBusy);
            EditCommand = new DelegateCommand(ExecuteEditCommand).ObservesCanExecute(() => IsNotBusy);
            SearchCommand = new DelegateCommand(ExecuteSearchCommand).ObservesCanExecute(() => IsNotBusy);
        }
        public DelegateCommand CancelCommand { get; }
        private async void ExecuteCancelCommand()
        {
            await NavigationService.GoBackAsync();
        }
        public DelegateCommand SaveCommand { get; }
        private async void ExecuteSaveCommand()
        {
            try
            {
                IsBusy = true;

                foreach (var inventoryThumbnail in InventoryThumbnailList)
                {
                    CropTraitObservationRegStatus = _cropTraitObservations
                        .Where(x => x.inventory_id == inventoryThumbnail.inventory_id && x.crop_trait_id == 2011)
                        .FirstOrDefault();
                    CropTraitObservationRegNotes = _cropTraitObservations
                        .Where(x => x.inventory_id == inventoryThumbnail.inventory_id && x.crop_trait_id == 2013)
                        .FirstOrDefault();

                    if (CropTraitObservationRegStatus != null)
                    {
                        var cropTraitObservationRegStatus = new CropTraitObservationInsertDto()
                        {
                            crop_trait_observation_id = CropTraitObservationRegStatus.crop_trait_observation_id,
                            crop_trait_id = 2011,
                            inventory_id = inventoryThumbnail.inventory_id,
                            crop_trait_code_id = inventoryThumbnail.CropTraitObservations[2011].crop_trait_code_id,
                            numeric_value = null,
                            string_value = null,
                            is_archived = "N",
                            method_id = 1,
                            workgroup_cooperator_id = Settings.WorkgroupCooperatorId,
                        };
                        var ctoRegStatusIdStr = await _restService.UpdateCropTraitObservationAsync(cropTraitObservationRegStatus);
                    }
                    else if(inventoryThumbnail.CropTraitObservations[2011].crop_trait_code_id != null)
                    {
                        var cropTraitObservationRegStatus = new CropTraitObservationInsertDto()
                        {
                            crop_trait_observation_id = 0,
                            crop_trait_id = 2011,
                            inventory_id = inventoryThumbnail.inventory_id,
                            crop_trait_code_id = inventoryThumbnail.CropTraitObservations[2011].crop_trait_code_id,
                            numeric_value = null,
                            string_value = null,
                            is_archived = "N",
                            method_id = 1,
                            workgroup_cooperator_id = Settings.WorkgroupCooperatorId,
                        };
                        var ctoRegStatusIdStr = await _restService.CreateCropTraitObservationAsync(cropTraitObservationRegStatus);
                    }

                    if(CropTraitObservationRegNotes != null)
                    {
                        var cropTraitObservationRegNotes = new CropTraitObservationInsertDto()
                        {
                            crop_trait_observation_id = CropTraitObservationRegNotes.crop_trait_observation_id,
                            crop_trait_id = 2013,
                            inventory_id = inventoryThumbnail.inventory_id,
                            crop_trait_code_id = null,
                            numeric_value = null,
                            string_value = !string.IsNullOrEmpty(inventoryThumbnail.CropTraitObservations[2013].string_value) ? inventoryThumbnail.CropTraitObservations[2013].string_value : " ", // To avoid empty string validation
                            is_archived = "N",
                            method_id = 1,
                            workgroup_cooperator_id = Settings.WorkgroupCooperatorId,
                        };
                        var ctoRegNotesIdStr = await _restService.UpdateCropTraitObservationAsync(cropTraitObservationRegNotes);
                    }
                    else
                    {
                        var cropTraitObservationRegNotes = new CropTraitObservationInsertDto()
                        {
                            crop_trait_observation_id = 0,
                            crop_trait_id = 2013,
                            inventory_id = inventoryThumbnail.inventory_id,
                            crop_trait_code_id = null,
                            numeric_value = null,
                            string_value = !string.IsNullOrEmpty(inventoryThumbnail.CropTraitObservations[2013].string_value) ? inventoryThumbnail.CropTraitObservations[2013].string_value : " ", // To avoid empty string validation
                            is_archived = "N",
                            method_id = 1,
                            workgroup_cooperator_id = Settings.WorkgroupCooperatorId,
                        };
                        var ctoRegNotesIdStr = await _restService.CreateCropTraitObservationAsync(cropTraitObservationRegNotes);
                    }
                    
                }

                await PageDialogService.DisplayAlertAsync("Resultado de guardar", "Se guardaron todos los descriptores correctamente", "OK");
                //await NavigationService.GoBackAsync();
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand InventorySelectedCommand { get; }
        private async void ExecuteInventorySelectedCommand()
        {
            try
            {
                IsBusy = true;

                if( SelectedInventoryThumbnail != null)
                {
                    CropTraitObservationRegStatus = _cropTraitObservations
                        .Where(x => x.inventory_id == SelectedInventoryThumbnail.inventory_id && x.crop_trait_id == 2011)
                        .FirstOrDefault();

                    CropTraitObservationRegNotes = _cropTraitObservations
                        .Where(x => x.inventory_id == SelectedInventoryThumbnail.inventory_id && x.crop_trait_id == 2013)
                        .FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand EditCommand { get; }
        private async void ExecuteEditCommand()
        {
            try
            {
                IsBusy = true;

                if (SelectedInventoryThumbnail == null)
                {
                    SelectedInventoryThumbnail = InventoryThumbnailList.First();

                    CropTraitObservationRegStatus = _cropTraitObservations
                        .Where(x => x.inventory_id == SelectedInventoryThumbnail.inventory_id && x.crop_trait_id == 2011)
                        .FirstOrDefault();

                    CropTraitObservationRegNotes = _cropTraitObservations
                        .Where(x => x.inventory_id == SelectedInventoryThumbnail.inventory_id && x.crop_trait_id == 2013)
                        .FirstOrDefault();
                }

                IDialogParameters dialogParameters = new DialogParameters()
                {
                    {"SelectedInventoryThumbnail", SelectedInventoryThumbnail },
                    {"CropTraitObservationRegStatus", CropTraitObservationRegStatus },
                    {"CropTraitObservationRegNotes", CropTraitObservationRegNotes }
                };
                var dialogResult = await _dialogService.ShowDialogAsync("AddOrEditRegenerationDescriptorsDialog", dialogParameters);
                if (dialogResult.Parameters.ContainsKey(""))
                {

                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand SearchCommand { get; }
        private async void ExecuteSearchCommand()
        {
            try
            {
                IsBusy = true;

                if (!string.IsNullOrEmpty(SearchText))
                {
                    var inventoryFound = InventoryThumbnailList.FirstOrDefault( x => x.inventory_id.ToString().Equals(SearchText));
                    if (inventoryFound != null)
                    {
                        SelectedInventoryThumbnail = inventoryFound;

                        CropTraitObservationRegStatus = _cropTraitObservations
                        .Where(x => x.inventory_id == SelectedInventoryThumbnail.inventory_id && x.crop_trait_id == 2011)
                        .FirstOrDefault();

                        CropTraitObservationRegNotes = _cropTraitObservations
                            .Where(x => x.inventory_id == SelectedInventoryThumbnail.inventory_id && x.crop_trait_id == 2013)
                            .FirstOrDefault();

                        IDialogParameters dialogParameters = new DialogParameters()
                        {
                            {"SelectedInventoryThumbnail", SelectedInventoryThumbnail },
                            {"CropTraitObservationRegStatus", CropTraitObservationRegStatus },
                            {"CropTraitObservationRegNotes", CropTraitObservationRegNotes }
                        };
                        var dialogResult = await _dialogService.ShowDialogAsync("AddOrEditRegenerationDescriptorsDialog", dialogParameters);
                    }
                    else
                    {
                        throw new Exception("Inventario no encontrado");
                    }
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public override async void Initialize(INavigationParameters parameters)
        {
            try
            {
                IsBusy = true;

                if (parameters.ContainsKey("InventoryThumbnailList"))
                {
                    InventoryThumbnailList = parameters.GetValue<IEnumerable<InventoryThumbnail>>("InventoryThumbnailList");
                    InventoryThumbnailListCount = InventoryThumbnailList.Count();
                    Title = $"Evaluar {InventoryThumbnailList.Count()} inventarios";

                    foreach (var inventoryThumbnail in InventoryThumbnailList)
                    {
                        inventoryThumbnail.CropTraitObservations = new Dictionary<int, CropTraitObservation>
                        {
                            {
                                2011, new CropTraitObservation(){ DisplayText = ""}
                            },
                            {
                                2013, new CropTraitObservation(){ DisplayText = ""}
                            }
                        };
                    }
                }
                else
                    throw new Exception("InventoryThumbnailList is required");

                var inventoryIds = InventoryThumbnailList.Select(x => x.inventory_id).ToArray();
                _cropTraitObservations = await _restService.GetCropTraitObservationByInventoriesAndCropTraitsAsync(string.Join(",", inventoryIds), "2011,2013", 1);

                foreach (var inventoryThumbnail in InventoryThumbnailList)
                {
                    var tempCropTraitObservationRegStatus = _cropTraitObservations
                        .Where(x => x.inventory_id == inventoryThumbnail.inventory_id && x.crop_trait_id == 2011)
                        .FirstOrDefault();
                    if (tempCropTraitObservationRegStatus != null)
                    {
                        var tempRegenarionStatusCode = RegenerationStatusCodeList.FirstOrDefault(x => x.value_member == tempCropTraitObservationRegStatus.crop_trait_code_id);
                        inventoryThumbnail.CropTraitObservations[2011].DisplayText = tempRegenarionStatusCode.display_member;
                        inventoryThumbnail.CropTraitObservations[2011].crop_trait_code_id = tempCropTraitObservationRegStatus.crop_trait_code_id;
                    }
                    var tempCropTraitObservationRegNotes = _cropTraitObservations
                        .Where(x => x.inventory_id == inventoryThumbnail.inventory_id && x.crop_trait_id == 2013)
                        .FirstOrDefault();
                    if (tempCropTraitObservationRegNotes != null)
                    {
                        inventoryThumbnail.CropTraitObservations[2013].DisplayText = tempCropTraitObservationRegNotes.string_value;
                        inventoryThumbnail.CropTraitObservations[2011].string_value = tempCropTraitObservationRegNotes.string_value;
                    }
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
