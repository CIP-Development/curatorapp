﻿using InventoryApp.Extensions;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryApp.ViewModels
{
    public class RegenerationOrderRequestsPageViewModel : ViewModelBaseWithDialog
    {
        private readonly IRestService _restService;
        private ObservableCollection<OrderRequest> _orderRequestList;
        public ObservableCollection<OrderRequest> OrderRequestList
        {
            get { return _orderRequestList; }
            set { SetProperty(ref _orderRequestList, value); }
        }
        private OrderRequest _selectedOrderRequest;
        public OrderRequest SelectedOrderRequest
        {
            get { return _selectedOrderRequest; }
            set { SetProperty(ref _selectedOrderRequest, value); }
        }
        public RegenerationOrderRequestsPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService,
            IRestService restService)
            : base(navigationService, pageDialogService)
        {
            _restService = restService;

            Title = "Solicitudes de regeneración / viabilidad";
        }
        private DelegateCommand _searchOrderRequestCommandfieldName;
        public DelegateCommand SearchOrderRequestCommand =>
            _searchOrderRequestCommandfieldName ?? (_searchOrderRequestCommandfieldName = new DelegateCommand(ExecuteSearchOrderRequestCommand));

        private void ExecuteSearchOrderRequestCommand()
        {

        }
        private DelegateCommand _createOrderRequestCommand;
        public DelegateCommand CreateOrderRequestCommand =>
            _createOrderRequestCommand ?? (_createOrderRequestCommand = new DelegateCommand(ExecuteCreateOrderRequestCommand));

        private async void ExecuteCreateOrderRequestCommand()
        {
            var navigationResult = await NavigationService.NavigateAsync("RegenerationOrderRequestPage");
            if (!navigationResult.Success)
                throw navigationResult.Exception;
        }
        private DelegateCommand _refreshOrderRequestsCommand;
        public DelegateCommand RefreshOrderRequestsCommand =>
            _refreshOrderRequestsCommand ?? (_refreshOrderRequestsCommand = new DelegateCommand(ExecuteRefreshOrderRequestsCommand));

        private async void ExecuteRefreshOrderRequestsCommand()
        {
            try
            {
                IsBusy = true;

                await LoadRequests();
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        private DelegateCommand _orderRequestSelectedCommand;
        public DelegateCommand OrderRequestSelectedCommand =>
            _orderRequestSelectedCommand ?? (_orderRequestSelectedCommand = new DelegateCommand(ExecuteOrderRequestSelectedCommand)
            .ObservesCanExecute(() => IsNotBusy));

        private async void ExecuteOrderRequestSelectedCommand()
        {
            try
            {
                IsBusy = true;

                if (SelectedOrderRequest == null)
                    return;

                var navigationResult = await NavigationService.NavigateAsync("RegenerationOrderRequestWithDetailsPage", new NavigationParameters
                {
                    { "OrderRequest", SelectedOrderRequest}
                });

                if (!navigationResult.Success)
                    throw navigationResult.Exception;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        private async Task LoadRequests()
        {
            var tempOrderRequestList = await _restService.GetOrderRequests(new[] { "" });
            OrderRequestList = new ObservableCollection<OrderRequest>(tempOrderRequestList.OrderByDescending(x => x.ordered_date));
        }
        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            try
            {
                IsBusy = true;

                if(OrderRequestList == null)
                {
                    await LoadRequests();
                }

                if (parameters.ContainsKey("Refresh"))
                {
                    await LoadRequests();
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
