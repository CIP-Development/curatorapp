﻿using InventoryApp.Extensions;
using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using InventoryApp.Models.Database;
using InventoryApp.Services;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InventoryApp.ViewModels
{
    public class OrderRequestItemActionPageViewModel : ViewModelBaseWithDialog
    {
        private readonly IDialogService _dialogService;
        private readonly IRestService _restService;
        private readonly RestClient _restClient;

        private IEnumerable<OrderRequestItem> _selectedOrderRequestItemList;
        public IEnumerable<OrderRequestItem> SelectedOrderRequestItemList
        {
            get { return _selectedOrderRequestItemList; }
            set { SetProperty(ref _selectedOrderRequestItemList, value); }
        }

        private OrderRequestItemAction _currentOrderRequestItemAction;
        public OrderRequestItemAction CurrentOrderRequestItemAction
        {
            get { return _currentOrderRequestItemAction; }
            set { SetProperty(ref _currentOrderRequestItemAction, value); }
        }
        private IEnumerable<CodeValue> _actionList;
        public IEnumerable<CodeValue> ActionList
        {
            get { return _actionList; }
            set { SetProperty(ref _actionList, value); }
        }
        private CodeValue _selectedAction;
        public CodeValue SelectedAction
        {
            get { return _selectedAction; }
            set
            {
                SetProperty(ref _selectedAction, value);
                CurrentOrderRequestItemAction.action_name_code = value?.Code;
                IsDiscountActionSelected = value != null && value.Code.Equals("OUT_STORAGE");
            }
        }
        private bool _isDiscountActionSelected;
        public bool IsDiscountActionSelected
        {
            get { return _isDiscountActionSelected; }
            set { SetProperty(ref _isDiscountActionSelected, value); }
        }
        private bool _isInventoryDiscountNeeded;
        public bool IsInventoryDiscountNeeded
        {
            get { return _isInventoryDiscountNeeded; }
            set { SetProperty(ref _isInventoryDiscountNeeded, value); }
        }
        private IEnumerable<Lookup> _inventoryActionMethodList;
        public IEnumerable<Lookup> InventoryActionMethodList
        {
            get { return _inventoryActionMethodList; }
            set { SetProperty(ref _inventoryActionMethodList, value); }
        }
        private Lookup _selectedInventoryActionMethod;
        public Lookup SelectedInventoryActionMethod
        {
            get { return _selectedInventoryActionMethod; }
            set { SetProperty(ref _selectedInventoryActionMethod, value); }
        }
        private bool _startedDateIsEmpty;
        public bool StartedDateIsEmpty
        {
            get { return _startedDateIsEmpty; }
            set { SetProperty(ref _startedDateIsEmpty, value); }
        }
        private bool _completedDateIsEmpty;
        public bool CompletedDateIsEmpty
        {
            get { return _completedDateIsEmpty; }
            set { SetProperty(ref _completedDateIsEmpty, value); }
        }
        private DateTime _startedDate;
        public DateTime StartedDate
        {
            get { return _startedDate; }
            set { SetProperty(ref _startedDate, value); }
        }
        private DateTime _completedDate;
        public DateTime CompletedDate
        {
            get { return _completedDate; }
            set { SetProperty(ref _completedDate, value); }
        }

        public OrderRequestItemActionPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService,
            IDialogService dialogService, IRestService restService)
            : base(navigationService, pageDialogService)
        {
            _dialogService = dialogService;
            _restService = restService;
            _restClient = new RestClient();

            Title = "Agregar acción de item de solicitud";
            CurrentOrderRequestItemAction = new OrderRequestItemAction();

            ShowDebugInfoCommand = new DelegateCommand(ExecuteShowDebugInfoCommand);
            CancelCommand = new DelegateCommand(ExecuteCancelCommand);
            SaveCommand = new DelegateCommand(ExecuteSaveCommand).ObservesProperty(() => IsNotBusy);
            OpenLookupPickerCommand = new DelegateCommand(ExecuteOpenLookupPickerCommand).ObservesProperty(() => IsNotBusy);

            ActionList = CodeValueFactory.OrderRequestItemActionList;

            StartedDate = DateTime.Now;
            CurrentOrderRequestItemAction.started_date_code = "MM/dd/yyyy";
            CompletedDate = StartedDate;
            CurrentOrderRequestItemAction.completed_date_code = "MM/dd/yyyy";
            StartedDateIsEmpty = false;
            CompletedDateIsEmpty = true;

            IsInventoryDiscountNeeded = true;
        }
        public DelegateCommand ShowDebugInfoCommand { get; }
        private async void ExecuteShowDebugInfoCommand()
        {
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(CurrentOrderRequestItemAction, Newtonsoft.Json.Formatting.Indented);
            await PageDialogService.DisplayAlertAsync("", json , "OK");
        }
        public DelegateCommand CancelCommand { get; }
        private async void ExecuteCancelCommand()
        {
            await NavigationService.GoBackAsync();
        }
        public DelegateCommand SaveCommand { get; }
        private async void ExecuteSaveCommand()
        {
            try
            {
                IsBusy = true;

                if (StartedDateIsEmpty)
                {
                    CurrentOrderRequestItemAction.started_date = null;
                    CurrentOrderRequestItemAction.started_date_code = null;
                }
                else
                {
                    CurrentOrderRequestItemAction.started_date = StartedDate;
                    CurrentOrderRequestItemAction.started_date_code = "MM/dd/yyyy";
                }
                if (CompletedDateIsEmpty)
                {
                    CurrentOrderRequestItemAction.completed_date = null;
                    CurrentOrderRequestItemAction.completed_date_code = null;
                }
                else
                {
                    CurrentOrderRequestItemAction.completed_date = CompletedDate;
                    CurrentOrderRequestItemAction.completed_date_code = "MM/dd/yyyy";
                }
#if DEBUG
                var json = Newtonsoft.Json.JsonConvert.SerializeObject(CurrentOrderRequestItemAction, Newtonsoft.Json.Formatting.Indented);
                var continueSaving = await PageDialogService.DisplayAlertAsync("new order request item", json, "OK", "CANCEL");
                if (!continueSaving)
                    return;
#endif
                if (IsDiscountActionSelected && IsInventoryDiscountNeeded)
                {
                    if (_selectedInventoryActionMethod == null)
                        throw new Exception("Seleccionar justificación de descuento");

                    foreach (var orderRequestItem in SelectedOrderRequestItemList)
                    {
                        if (orderRequestItem.quantity_shipped == null)
                            throw new Exception($"El item {orderRequestItem.inventory_number} no se ha especificado la cantidad a descontar");
                        if (orderRequestItem.quantity_shipped > orderRequestItem.quantity_on_hand)
                            throw new Exception($"El item {orderRequestItem.inventory_number} no tiene la cantidad suficiente");
                    }
                }

                foreach (var orderRequestItem in SelectedOrderRequestItemList)
                {
                    CurrentOrderRequestItemAction.order_request_item_id = orderRequestItem.order_request_item_id;
                    await _restService.CreateOrderRequestItemActionAsync(CurrentOrderRequestItemAction);

                    if (IsDiscountActionSelected && IsInventoryDiscountNeeded)
                    {
                        Inventory i = await _restClient.ReadInventory(orderRequestItem.inventory_id); //TODO: Patch inventory
                        i.quantity_on_hand -= orderRequestItem.quantity_shipped;
                        await _restClient.UpdateInventory(i);

                        InventoryAction ia = new InventoryAction
                        {
                            inventory_action_id = -1,
                            inventory_id = i.inventory_id,
                            action_name_code = "WITHDRAWAL",
                            quantity = orderRequestItem.quantity_shipped * -1,
                            quantity_unit_code = i.quantity_on_hand_unit_code,
                            started_date = DateTime.Now,
                            method_id = (int)_selectedInventoryActionMethod.ValueMember,
                            cooperator_id = CurrentOrderRequestItemAction.cooperator_id,
                            owned_by = Settings.UserCooperatorId,
                            note = "(order_request_id=" + orderRequestItem.order_request_id + ")"
                        };
                        await _restClient.CreateInventoryAction(ia);
                    }
                }

                await PageDialogService.DisplayAlertAsync("Resultado de guardar", "Guardado correctamente", "OK");
                
                var navigationResult = await NavigationService.GoBackAsync(new NavigationParameters
                {
                    { "RefreshItems", true }
                });
                if (!navigationResult.Success)
                    throw navigationResult.Exception;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand OpenLookupPickerCommand { get; }
        private async void ExecuteOpenLookupPickerCommand()
        {
            var dialogResult = await _dialogService.ShowDialogAsync("LookupPickerDialog");
            if (dialogResult.Parameters.ContainsKey("SelectedEntity"))
            {
                var selectedCooperatorLookup = dialogResult.Parameters.GetValue<Lookup>("SelectedEntity");
                CurrentOrderRequestItemAction.cooperator_id = (int)selectedCooperatorLookup.ValueMember;
                CurrentOrderRequestItemAction.cooperator_name = selectedCooperatorLookup.DisplayMember;
            }
        }

        public override async void Initialize(INavigationParameters parameters)
        {
            try
            {
                IsBusy = true;

                if (parameters.ContainsKey("SelectedOrderRequestItemList"))
                {
                    SelectedOrderRequestItemList = parameters.GetValue<IEnumerable<OrderRequestItem>>("SelectedOrderRequestItemList");
                }
                else
                {
                    throw new Exception("SelectedOrderRequestItemList is required");
                }

                InventoryActionMethodList = await _restService.GetMethodLookupByStudyReasonCodeAsync("MOVJUST");
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
