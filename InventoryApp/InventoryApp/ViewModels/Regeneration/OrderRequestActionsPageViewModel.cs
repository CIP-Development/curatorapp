﻿using InventoryApp.Extensions;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace InventoryApp.ViewModels
{
    public class OrderRequestActionsPageViewModel : ViewModelBaseWithDialog
    {
        private readonly IRestService _restService;
        public OrderRequest SelectedOrderRequest { get; set; }

        private ObservableCollection<OrderRequestAction> _orderRequestActionList;
        public ObservableCollection<OrderRequestAction> OrderRequestActionList
        {
            get { return _orderRequestActionList; }
            set { SetProperty(ref _orderRequestActionList, value); }
        }
        public OrderRequestActionsPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService,
            IRestService restService)
            : base(navigationService, pageDialogService)
        {
            _restService = restService;

            Title = "Order request actions";
            OrderRequestActionList = new ObservableCollection<OrderRequestAction>();

            ShowDebugInfoCommand = new DelegateCommand(ExecuteShowDebugInfoCommand).ObservesCanExecute(() => IsNotBusy);
            AddOrderRequestActionCommand = new DelegateCommand(ExecuteAddOrderRequestActionCommand).ObservesCanExecute(() => IsNotBusy);
        }
        public DelegateCommand ShowDebugInfoCommand { get; }
        private async void ExecuteShowDebugInfoCommand()
        {
            try
            {
                IsBusy = true;

                var json = Newtonsoft.Json.JsonConvert.SerializeObject(OrderRequestActionList, Newtonsoft.Json.Formatting.Indented);
                await PageDialogService.DisplayAlertAsync("", json, "OK");
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand AddOrderRequestActionCommand { get; }
        private async void ExecuteAddOrderRequestActionCommand()
        {
            try
            {
                IsBusy = true;

                var navigationResult = await NavigationService.NavigateAsync("OrderRequestActionPage", new NavigationParameters
                {
                    {"OrderRequest", SelectedOrderRequest }
                });
                if (!navigationResult.Success)
                    throw navigationResult.Exception;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public override void Initialize(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("OrderRequest"))
            {
                SelectedOrderRequest = parameters.GetValue<OrderRequest>("OrderRequest");
                Title = $"Acciones de solicitud : {SelectedOrderRequest.local_number} ";
            }
            else
            {
                throw new Exception($"{nameof(OrderRequest)} is required");
            }
        }
        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            try
            {
                IsBusy = true;

                var tempOrderRequestActionList = await _restService.GetOrderRequestActionAsync(SelectedOrderRequest.order_request_id);
                OrderRequestActionList = new ObservableCollection<OrderRequestAction>(tempOrderRequestActionList);
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
