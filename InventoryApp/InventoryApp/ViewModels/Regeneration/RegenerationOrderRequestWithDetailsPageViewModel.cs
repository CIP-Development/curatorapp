﻿using InventoryApp.Extensions;
using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using InventoryApp.Models.Database;
using InventoryApp.Models.LocalStorage;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryApp.ViewModels
{
    public class RegenerationOrderRequestWithDetailsPageViewModel : ViewModelBaseWithDialog
    {
        private readonly IRestService _restService;
        private readonly IOrderRequestRepository _orderRequestRepository;
        private readonly IInventoryLocalRepository _inventoryLocalRepository;
        private readonly ICropTraitObservationLocalRepository _cropTraitObservationLocalRepository;

        private OrderRequest _currentOrderRequest;
        public OrderRequest CurrentOrderRequest
        {
            get { return _currentOrderRequest; }
            set { SetProperty(ref _currentOrderRequest, value); }
        }
        private ObservableCollection<OrderRequestItem> _items;
        public ObservableCollection<OrderRequestItem> Items
        {
            get { return _items; }
            set { SetProperty(ref _items, value); }
        }
        private ObservableCollection<object> _selectedItems;
        public ObservableCollection<object> SelectedItems
        {
            get { return _selectedItems; }
            set
            {
                SetProperty(ref _selectedItems, value);
            }
        }
        private bool _hasItemsSelected;
        public bool HasItemsSelected
        {
            get { return _hasItemsSelected; }
            set { SetProperty(ref _hasItemsSelected, value); }
        }
        public RegenerationOrderRequestWithDetailsPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService,
            IRestService restService, IOrderRequestRepository orderRequestRepository, IInventoryLocalRepository inventoryLocalRepository,
            ICropTraitObservationLocalRepository cropTraitObservationLocalRepository)
            : base(navigationService, pageDialogService)
        {
            _restService = restService;
            _orderRequestRepository = orderRequestRepository;
            _inventoryLocalRepository = inventoryLocalRepository;
            _cropTraitObservationLocalRepository = cropTraitObservationLocalRepository;

            HasItemsSelected = false;
            SelectedItems = new ObservableCollection<object>();

            AddInventoriesCommand = new DelegateCommand(ExecuteAddInventoriesCommand).ObservesCanExecute(() => IsNotBusy);
            AddOrderRequestItemActionCommand = new DelegateCommand(ExecuteAddOrderRequestItemActionCommand).ObservesCanExecute(() => IsNotBusy);
            OrderRequestItemSelectedCommand = new DelegateCommand(ExecuteOrderRequestItemSelectedCommand);
            RemoveOrderRequestItemCommand = new DelegateCommand(ExecuteRemoveOrderRequestItemCommand).ObservesCanExecute(() => IsNotBusy);
            CancelOrderRequestItemCommand = new DelegateCommand(ExecuteCancelOrderRequestItemCommand).ObservesCanExecute(() => IsNotBusy);
            ShippedOrderRequestItemCommand = new DelegateCommand(ExecuteShippedOrderRequestItemCommand).ObservesCanExecute(() => IsNotBusy);
            EditOrderRequestItemCommand = new DelegateCommand(ExecuteEditOrderRequestItemCommand).ObservesCanExecute(() => IsNotBusy);
            ViewOrderRequestItemActionsCommand = new DelegateCommand(ExecuteViewOrderRequestItemActionsCommand).ObservesCanExecute(() => IsNotBusy);
            ChangeInventoryCommand = new DelegateCommand(ExecuteChangeInventoryCommand).ObservesCanExecute(() => IsNotBusy);
            ViewOrderRequestActionsCommand = new DelegateCommand(ExecuteViewOrderRequestActionsCommand).ObservesCanExecute(() => IsNotBusy);
            CreateWithdrawInventoriesCommand = new DelegateCommand(ExecuteCreateWithdrawInventoriesCommand).ObservesCanExecute(() => IsNotBusy);
            SelectionChangedCommand = new DelegateCommand(ExecuteSelectionChangedCommand).ObservesCanExecute(() => IsNotBusy);

            DownloadToLocalStorageCommand = new DelegateCommand(ExecuteDownloadToLocalStorageCommand).ObservesCanExecute(() => IsNotBusy);
        }
        private DelegateCommand<string> _showDebugInfoCommand;
        public DelegateCommand<string> ShowDebugInfoCommand =>
            _showDebugInfoCommand ?? (_showDebugInfoCommand = new DelegateCommand<string>(ExecuteShowDebugInfoCommand));

        private async void ExecuteShowDebugInfoCommand(string parameter)
        {
            try
            {
                IsBusy = true;

                string json = string.Empty;
                switch (parameter)
                {
                    case "order_request":
                        json = JsonConvert.SerializeObject(_currentOrderRequest, Formatting.Indented);
                        break;
                    case "order_request_item":
                        json = JsonConvert.SerializeObject(_items, Formatting.Indented);
                        break;
                    default:
                        throw new Exception("Not supported");
                }
                await Xamarin.Essentials.Clipboard.SetTextAsync(JsonConvert.SerializeObject(json));
                await PageDialogService.DisplayAlertAsync("", json, "OK");
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        private DelegateCommand _editOrderRequestCommand;
        public DelegateCommand EditOrderRequestCommand =>
            _editOrderRequestCommand ?? (_editOrderRequestCommand = new DelegateCommand(ExecuteEditOrderRequestCommand)
            .ObservesCanExecute(() => IsNotBusy));

        private async void ExecuteEditOrderRequestCommand()
        {
            try
            {
                IsBusy = true;

                if (CurrentOrderRequest == null)
                    return;

                var navigationResult = await NavigationService.NavigateAsync("RegenerationOrderRequestPage", new NavigationParameters
                {
                    { "OrderRequest", CurrentOrderRequest}
                });

                if (!navigationResult.Success)
                    throw navigationResult.Exception;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand AddInventoriesCommand { get; }
        private async void ExecuteAddInventoriesCommand()
        {
            try
            {
                IsBusy = true;

                var navigationResult = await NavigationService.NavigateAsync("SearchInventoriesPage");
                if (!navigationResult.Success)
                    throw navigationResult.Exception;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand AddOrderRequestItemActionCommand { get; }
        private async void ExecuteAddOrderRequestItemActionCommand()
        {
            try
            {
                IsBusy = true;

                if (!SelectedItems.Any())
                    throw new Exception("SelectedItems is empty");

                var navigationResult = await NavigationService.NavigateAsync("OrderRequestItemActionPage", new NavigationParameters
                {
                    {"SelectedOrderRequestItemList", SelectedItems.Cast<OrderRequestItem>()}
                });
                if (!navigationResult.Success)
                {
                    throw navigationResult.Exception;
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand OrderRequestItemSelectedCommand { get; }
        private void ExecuteOrderRequestItemSelectedCommand()
        {
            if (SelectedItems != null && SelectedItems.Any())
                HasItemsSelected = true;
            else
                HasItemsSelected = false;
        }
        public DelegateCommand SelectionChangedCommand { get; }
        private void ExecuteSelectionChangedCommand()
        {
            if (SelectedItems != null && SelectedItems.Any())
                HasItemsSelected = true;
            else
                HasItemsSelected = false;
        }
        public DelegateCommand RemoveOrderRequestItemCommand { get; }
        private async void ExecuteRemoveOrderRequestItemCommand()
        {
            try
            {
                IsBusy = true;

                if (!SelectedItems.Any())
                    throw new Exception("SelectedItems is empty");

                var selectedItems = SelectedItems.Cast<OrderRequestItem>();
                
                if(selectedItems.Any(x => !x.status_code.Equals("NEW")))
                    throw new Exception("Items with a status other than NEW cannot be deleted");
                if (selectedItems.Any(x => x.order_request_item_action_date != null))
                    throw new Exception("Items with actions registered cannot be deleted");

                foreach (var orderRequestItem in selectedItems)
                {
                    await _restService.DeleteOrderRequestItemAsync(orderRequestItem.order_request_item_id);
                }

                SelectedItems.Clear();
                HasItemsSelected = false;

                await LoadItems();
                await PageDialogService.DisplayAlertAsync("Result message", "Succesfully removed", "OK");
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand CancelOrderRequestItemCommand { get; }
        private async void ExecuteCancelOrderRequestItemCommand()
        {
            try
            {
                IsBusy = true;

                if (!SelectedItems.Any())
                    throw new Exception("SelectedItems is empty");

                var selectedItems = SelectedItems.Cast<OrderRequestItem>();

                foreach (var orderRequestItem in selectedItems)
                {
                    orderRequestItem.status_code = "CANCEL";
                    orderRequestItem.status_date = DateTime.Now;

                    await _restService.UpdateOrderRequestItemAsync(orderRequestItem);
                }

                await LoadItems();
                await PageDialogService.DisplayAlertAsync("Result message", "Succesfully cancelled", "OK");
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand ShippedOrderRequestItemCommand { get; }
        private async void ExecuteShippedOrderRequestItemCommand()
        {
            try
            {
                IsBusy = true;

                if (!SelectedItems.Any())
                    throw new Exception("SelectedItems is empty");

                var selectedItems = SelectedItems.Cast<OrderRequestItem>();

                foreach (var orderRequestItem in selectedItems)
                {
                    orderRequestItem.status_code = "SHIPPED";
                    orderRequestItem.status_date = DateTime.Now;

                    await _restService.UpdateOrderRequestItemAsync(orderRequestItem);
                }

                await LoadItems();
                await PageDialogService.DisplayAlertAsync("Result message", "Succesfully marked as shipped", "OK");
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand EditOrderRequestItemCommand { get; }
        private async void ExecuteEditOrderRequestItemCommand()
        {
            try
            {
                IsBusy = true;

                var navigationParams = new NavigationParameters
                {
                    { "SelectedOrderRequestItemList", SelectedItems.Cast<OrderRequestItem>() }
                };
                var navigationResult = await NavigationService.NavigateAsync("RegenerationOrderRequestItemPage", navigationParams);
                if (!navigationResult.Success)
                    throw navigationResult.Exception;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand ViewOrderRequestItemActionsCommand { get; }
        private async void ExecuteViewOrderRequestItemActionsCommand()
        {
            try
            {
                IsBusy = true;

                var navigationParams = new NavigationParameters
                {
                    { "SelectedOrderRequestItemList", SelectedItems.Cast<OrderRequestItem>() }
                };
                var navigationResult = await NavigationService.NavigateAsync("OrderRequestItemActionsPage", navigationParams);
                if (!navigationResult.Success)
                    throw navigationResult.Exception;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand ChangeInventoryCommand { get; }
        private async void ExecuteChangeInventoryCommand()
        {
            try
            {
                IsBusy = true;

                var navigationParams = new NavigationParameters
                {
                    { "SelectedOrderRequestItemList", SelectedItems.Cast<OrderRequestItem>() }
                };
                var navigationResult = await NavigationService.NavigateAsync("ChangeOrderRequestItemInventoryPage", navigationParams);
                if (!navigationResult.Success)
                    throw navigationResult.Exception;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand ViewOrderRequestActionsCommand { get; }
        private async void ExecuteViewOrderRequestActionsCommand()
        {
            try
            {
                IsBusy = true;

                var navigationResult = await NavigationService.NavigateAsync("OrderRequestActionsPage", new NavigationParameters
                {
                    {"OrderRequest", CurrentOrderRequest }
                });
                if (!navigationResult.Success)
                    throw navigationResult.Exception;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand CreateWithdrawInventoriesCommand { get; }
        private async void ExecuteCreateWithdrawInventoriesCommand()
        {
            try
            {
                IsBusy = true;

                if (!SelectedItems.Any())
                    throw new Exception("SelectedItems is empty");

                var selectedItems = SelectedItems.Cast<OrderRequestItem>();

                //if (selectedItems.Any(x => !x.status_code.Equals("SHIPPED")))
                //    throw new Exception("Cant create inventories if selected items has a status other than SHIPPED");

                var navigationResult = await NavigationService.NavigateAsync("CreateRegenerationInventoryPage", new NavigationParameters
                {
                    {"SelectedOrderRequestItems", selectedItems }
                });
                if (!navigationResult.Success)
                    throw navigationResult.Exception;

            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand DownloadToLocalStorageCommand { get; }
        private async void ExecuteDownloadToLocalStorageCommand()
        {
            try
            {
                IsBusy = true;

                var localOrderRequest = await _orderRequestRepository.GetOrderRequestById(CurrentOrderRequest.order_request_id);
                if (localOrderRequest != null)
                    throw new Exception("La solicitud ya se ha descargado.\n\nSi desea volver a descargar es necesario eliminar los datos locales de la solicitud en \"Ajustes de regeneración\"");

                //Get all regeneration inventories
                var regenerationInventories = await _restService.GetRegenerationInventoriesAsync(CurrentOrderRequest.order_request_id);

                if (!regenerationInventories.Any())
                    throw new Exception("La solicitud no tiene inventarios de regeneración.\n\nPara crearlos debe seleccionar los items de la solicitud y presionar el botón \"Crear inventarios de regeneración\"");

                var dbInventories = regenerationInventories.Select(x => new InventoryDb
                {
                    AccessionId = x.accession_id,
                    AccessionNumber = x.accession_number,
                    AvailabilityStatusCode = x.availability_status_code,
                    Container = x.container,
                    ContainerCode = x.container_code,
                    CreatedBy = x.created_by,
                    CreatedDate = x.created_date,
                    FormTypeCode = x.form_type_code,
                    FormType = x.form_type,
                    Guid = Guid.NewGuid().ToString(),
                    InventoryId = x.inventory_id,
                    InventoryMaintPolicyId = x.inventory_maint_policy_id,
                    InventoryNumber = x.inventory_number,
                    InventoryNumberPart1 = x.inventory_number_part1,
                    InventoryNumberPart2 = x.inventory_number_part2,
                    InventoryNumberPart3 = x.inventory_number_part3,
                    IsAutoDeducted = x.is_auto_deducted,
                    IsAvailable = x.is_available,
                    IsDistributable = x.is_distributable,
                    ModifiedBy = x.modified_by,
                    ModifiedDate = x.modified_date,
                    Note = x.note,
                    OrderRequestId = CurrentOrderRequest.order_request_id,
                    OwnedBy = x.owned_by,
                    OwnedDate = x.owned_date,
                    ParentInventoryId = x.parent_inventory_id,
                    PropagationDate = x.propagation_date,
                    PropagationDateCode = x.propagation_date_code,
                    QuantityOnHand = x.quantity_on_hand,
                    QuantityOnHandUnitCode = x.quantity_on_hand_unit_code,
                    QuantityOnHandUnit = x.quantity_on_hand_unit,
                    StorageLocationPart1 = x.storage_location_part1,
                    StorageLocationPart2 = x.storage_location_part2,
                    StorageLocationPart3 = x.storage_location_part3,
                    StorageLocationPart4 = x.storage_location_part4,
                    AccessionName = x.acc_name_cul,
                    CollectingNumber = x.acc_name_col,
                    TaxonomySpeciesCode = x.taxonomy_species_code,
                    TaxonomySpeciesName = x.taxonomy_species_name,
                }).ToArray();
                
                var cropTraitObservations = await _restService.GetCropTraitObservationByOrderRequestIdAsync(CurrentOrderRequest.order_request_id);
                var dbCropTraitObservations = cropTraitObservations.Select(x => new CropTraitObservationDb
                {
                    Code = x.code,
                    CreatedBy = x.created_by,
                    CreatedDate = x.created_date,
                    CropTraitCodeId = x.crop_trait_code_id,
                    CropTraitId = x.crop_trait_id,
                    CropTraitObservationId = x.crop_trait_observation_id,
                    DisplayText = x.crop_trait_code_id > 0 ? x.code : (x.numeric_value != null ? x.numeric_value.GetValueOrDefault().ToString() : x.string_value),
                    Guid = Guid.NewGuid(),
                    InventoryGuid = dbInventories.First(iDb => iDb.InventoryId == x.inventory_id).Guid,
                    InventoryId = x.inventory_id,
                    ModifiedBy = x.modified_by,
                    ModifiedDate = x.modified_date,
                    NumericValue = x.numeric_value,
                    StringValue = x.string_value,
                }).ToArray();
                
                var rowsAffected = await _orderRequestRepository.InsertItemAsync(new OrderRequestDb
                {
                    OrderRequestId = CurrentOrderRequest.order_request_id,
                    LocalNumber = CurrentOrderRequest.local_number,
                    OrderTypeCode = CurrentOrderRequest.order_type_code,
                    LastSyncedDate = DateTime.UtcNow,
                });
                if (rowsAffected == 0)
                    throw new Exception($"{nameof(OrderRequestDb)} not inserted");
                var inventoryRowsAffected = await _inventoryLocalRepository.InsertInventoryManyAsync(dbInventories);
                if (inventoryRowsAffected == 0)
                    throw new Exception($"regeneration inventories of \"{nameof(OrderRequestDb)}\" not inserted");
                if (dbCropTraitObservations.Any())
                {
                    var ctoRows = await _cropTraitObservationLocalRepository.InsertCropTraitObservationManyAsync(dbCropTraitObservations);
                    if (ctoRows == 0)
                        throw new Exception($"regeneration inventory crop trait observations of {nameof(OrderRequestDb)} not inserted");
                }

                await PageDialogService.DisplayAlertAsync("Resultado de descarga", "La solicitud se ha descargado correctamente", "OK");
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        private async Task LoadItems()
        {
            var tempItems = await _restService.GetOrderRequestItems(CurrentOrderRequest.order_request_id);
            Items = new ObservableCollection<OrderRequestItem>(tempItems);

            SelectedItems.Clear();
            HasItemsSelected = false;
        }
        private DelegateCommand _SelectAllCommand;
        public DelegateCommand SelectAllCommand =>
            _SelectAllCommand ?? (_SelectAllCommand = new DelegateCommand(ExecuteSelectAllCommand));

        void ExecuteSelectAllCommand()
        {
            SelectedItems = new ObservableCollection<object>(Items);
        }
        private DelegateCommand _UnselectAllCommand;
        public DelegateCommand UnselectAllCommand =>
            _UnselectAllCommand ?? (_UnselectAllCommand = new DelegateCommand(ExecuteUnselectAllCommand));

        void ExecuteUnselectAllCommand()
        {
            SelectedItems.Clear();
        }
        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            try
            {
                IsBusy = true;

                base.OnNavigatedTo(parameters);

                if (parameters.ContainsKey("OrderRequest"))
                {
                    CurrentOrderRequest = parameters.GetValue<OrderRequest>("OrderRequest");
                    Title = $"{CurrentOrderRequest.local_number}";

                    await LoadItems();
                }
                else if(CurrentOrderRequest == null)
                    throw new Exception("OrderRequest is required");

                if (parameters.ContainsKey("InventoryThumbnailList"))
                {
                    var selectedInventories = parameters.GetValue<IEnumerable<InventoryThumbnail>>("InventoryThumbnailList");

                    foreach (var selectedInventory in selectedInventories)
                    {
                        var alreadyAdded = Items.Any(x => x.inventory_id == selectedInventory.inventory_id);
                        if (!alreadyAdded)
                        {
                            var seqNumber = Items.Any() ? Items.Max(x => x.sequence_number) + 1 : 1;

                            var newOrderRequestItem = new OrderRequestItem()
                            {
                                order_request_id = CurrentOrderRequest.order_request_id,
                                inventory_id = selectedInventory.inventory_id,
                                status_code = "NEW",
                                distribution_form_code = selectedInventory.form_type_code,
                                name = "-",
                                sequence_number = seqNumber,
                                status_date = DateTime.Now,
                                workgroup_cooperator_id = Settings.WorkgroupCooperatorId > 0 ? Settings.WorkgroupCooperatorId : (int?)null
                            };

                            var newOrderRequestItemId = await _restService.CreateOrderRequestItemAsync(newOrderRequestItem);
                            newOrderRequestItem.order_request_item_id = int.Parse(newOrderRequestItemId);
                            Items.Add(newOrderRequestItem);
                        }
                    }

                    await LoadItems();
                }

                if (parameters.ContainsKey("RefreshItems") && CurrentOrderRequest != null)
                {
                    await LoadItems();
                }
                if (parameters.ContainsKey("RefreshOrderRequest") && CurrentOrderRequest != null)
                {
                    CurrentOrderRequest = await _restService.GetOrderRequestAsync(CurrentOrderRequest.order_request_id);
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
