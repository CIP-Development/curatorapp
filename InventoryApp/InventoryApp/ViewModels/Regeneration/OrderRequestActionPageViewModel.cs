﻿using InventoryApp.Extensions;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InventoryApp.ViewModels
{
    public class OrderRequestActionPageViewModel : ViewModelBaseWithDialog
    {
        private readonly IDialogService _dialogService;
        private readonly IRestService _restService;

        public OrderRequest SelectedOrderRequest { get; set; }

        private OrderRequestAction _currentOrderRequestAction;
        public OrderRequestAction CurrentOrderRequestAction
        {
            get { return _currentOrderRequestAction; }
            set { SetProperty(ref _currentOrderRequestAction, value); }
        }
        private IEnumerable<CodeValue> _actionList;
        public IEnumerable<CodeValue> ActionList
        {
            get { return _actionList; }
            set { SetProperty(ref _actionList, value); }
        }
        private CodeValue _selectedAction;
        public CodeValue SelectedAction
        {
            get { return _selectedAction; }
            set
            {
                SetProperty(ref _selectedAction, value);
                CurrentOrderRequestAction.action_name_code = value?.Code;
            }
        }

        public OrderRequestActionPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService,
            IDialogService dialogService, IRestService restService)
            : base(navigationService, pageDialogService)
        {
            _dialogService = dialogService;
            _restService = restService;

            Title = "Agregar acción de solicitud";
            CurrentOrderRequestAction = new OrderRequestAction();

            CancelCommand = new DelegateCommand(ExecuteCancelCommand).ObservesCanExecute(() => IsNotBusy);
            SaveCommand = new DelegateCommand(ExecuteSaveCommand).ObservesCanExecute(() => IsNotBusy);
            ShowDebugInfoCommand = new DelegateCommand(ExecuteShowDebugInfoCommand).ObservesCanExecute(() => IsNotBusy);
            OpenLookupPickerCommand = new DelegateCommand(ExecuteOpenLookupPickerCommand).ObservesProperty(() => IsNotBusy);

            ActionList = CodeValueFactory.OrderRequestActionList;
            CurrentOrderRequestAction.started_date = DateTime.Now;
            CurrentOrderRequestAction.started_date_code = "MM/dd/yyyy";
            CurrentOrderRequestAction.completed_date_code = "MM/dd/yyyy";
        }
        public DelegateCommand ShowDebugInfoCommand { get; }
        private async void ExecuteShowDebugInfoCommand()
        {
            try
            {
                IsBusy = true;

                var json = Newtonsoft.Json.JsonConvert.SerializeObject(CurrentOrderRequestAction, Newtonsoft.Json.Formatting.Indented);
                await PageDialogService.DisplayAlertAsync("", json, "OK");
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand CancelCommand { get; }
        private async void ExecuteCancelCommand()
        {
            await NavigationService.GoBackAsync();
        }
        public DelegateCommand SaveCommand { get; }
        private async void ExecuteSaveCommand()
        {
            try
            {
                IsBusy = true;

                
                CurrentOrderRequestAction.order_request_id = SelectedOrderRequest.order_request_id;
                var newOrderRequestActionId = await _restService.CreateOrderRequestActionAsync(CurrentOrderRequestAction);
                
                await PageDialogService.DisplayAlertAsync("Result message", "Succesfully saved", "OK");

                var navigationResult = await NavigationService.GoBackAsync(new NavigationParameters
                {
                    { "Refresh", true }
                });
                if (!navigationResult.Success)
                    throw navigationResult.Exception;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand OpenLookupPickerCommand { get; }
        private async void ExecuteOpenLookupPickerCommand()
        {
            var dialogResult = await _dialogService.ShowDialogAsync("LookupPickerDialog");
            if (dialogResult.Parameters.ContainsKey("SelectedEntity"))
            {
                var selectedCooperatorLookup = dialogResult.Parameters.GetValue<Lookup>("SelectedEntity");
                CurrentOrderRequestAction.cooperator_id = (int)selectedCooperatorLookup.ValueMember;
                CurrentOrderRequestAction.cooperator_name = selectedCooperatorLookup.DisplayMember;
            }
        }
        public override async void Initialize(INavigationParameters parameters)
        {
            try
            {
                if (parameters.ContainsKey("OrderRequest"))
                {
                    SelectedOrderRequest = parameters.GetValue<OrderRequest>("OrderRequest");
                    CurrentOrderRequestAction.order_request_id = SelectedOrderRequest.order_request_id;
                }
                else
                {
                    throw new Exception($"{nameof(OrderRequest)} is required");
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
        }
    }
}
