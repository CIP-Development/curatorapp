﻿using InventoryApp.Interfaces;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using InventoryApp.Extensions;
using InventoryApp.Models;
using InventoryApp.Helpers;
using Newtonsoft.Json;
using Prism.Services.Dialogs;

namespace InventoryApp.ViewModels
{
    public class RegenerationOrderRequestPageViewModel : ViewModelBaseWithDialog
    {
        IRestService _restService;
        private readonly IDialogService _dialogService;
        bool _isNew = false;
        private IEnumerable<CodeValueMap> _intendedUseCodeMapList = Enumerable.Empty<CodeValueMap>();

        private OrderRequest _currentOrderRequest;
        public OrderRequest CurrentOrderRequest
        {
            get { return _currentOrderRequest; }
            set { SetProperty(ref _currentOrderRequest, value); }
        }
        private IEnumerable<CodeValue> _orderTypeList;
        public IEnumerable<CodeValue> OrderTypeList
        {
            get { return _orderTypeList; }
            set { SetProperty(ref _orderTypeList, value); }
        }
        private IEnumerable<CodeValue> _orderIntendedUseList;
        public IEnumerable<CodeValue> OrderIntendedUseList
        {
            get { return _orderIntendedUseList; }
            set { SetProperty(ref _orderIntendedUseList, value); }
        }
        private CodeValue _selectedOrderType;
        public CodeValue SelectedOrderType
        {
            get { return _selectedOrderType; }
            set { SetProperty(ref _selectedOrderType, value); }
        }
        private CodeValue _selectedOrderIntendedUse;
        public CodeValue SelectedOrderIntendedUse
        {
            get { return _selectedOrderIntendedUse; }
            set { SetProperty(ref _selectedOrderIntendedUse, value); }
        }

        public RegenerationOrderRequestPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService,
            IRestService restService, IDialogService dialogService)
            : base(navigationService, pageDialogService)
        {
            _restService = restService;
            _dialogService = dialogService;

            CancelCommand = new DelegateCommand(ExecuteCancelCommand);
            SelectedOrderTypeChangedCommmand = new DelegateCommand(ExecuteSelectedOrderTypeChangedCommmand).ObservesCanExecute(() => IsNotBusy);
        }
        private DelegateCommand _showDebugInfoCommand;
        public DelegateCommand ShowDebugInfoCommand =>
            _showDebugInfoCommand ?? (_showDebugInfoCommand = new DelegateCommand(ExecuteShowDebugInfoCommand));

        private async void ExecuteShowDebugInfoCommand()
        {
            try
            {
                IsBusy = true;

                string json = JsonConvert.SerializeObject(_currentOrderRequest, Formatting.Indented);
                await Xamarin.Essentials.Clipboard.SetTextAsync(JsonConvert.SerializeObject(json));
                await PageDialogService.DisplayAlertAsync("", json, "OK");
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand CancelCommand { get; }
        private async void ExecuteCancelCommand()
        {
            await NavigationService.GoBackAsync();
        }
        private DelegateCommand<string> _openLookupPickerCommand;
        public DelegateCommand<string> OpenLookupPickerCommand =>
            _openLookupPickerCommand ?? (_openLookupPickerCommand = new DelegateCommand<string>(ExecuteOpenLookupPickerCommand));

        private async void ExecuteOpenLookupPickerCommand(string propertyName)
        {
            try
            {
                IsBusy = true;

                var dialogResult = await _dialogService.ShowDialogAsync("LookupPickerDialog");
                if (dialogResult.Parameters.ContainsKey("SelectedEntity"))
                {
                    var selectedCooperatorLookup = dialogResult.Parameters.GetValue<Lookup>("SelectedEntity");
                    switch (propertyName)
                    {
                        case "final_recipient_cooperator":
                            CurrentOrderRequest.final_recipient_cooperator_id = (int)selectedCooperatorLookup.ValueMember;
                            CurrentOrderRequest.final_recipient_cooperator_name = selectedCooperatorLookup.DisplayMember;

                            if (CurrentOrderRequest.ship_to_cooperator_id == null)
                            {
                                CurrentOrderRequest.ship_to_cooperator_id = (int)selectedCooperatorLookup.ValueMember;
                                CurrentOrderRequest.ship_to_cooperator_name = selectedCooperatorLookup.DisplayMember;
                            }
                            if(CurrentOrderRequest.requestor_cooperator_id == null)
                            {
                                CurrentOrderRequest.requestor_cooperator_id = (int)selectedCooperatorLookup.ValueMember;
                                CurrentOrderRequest.requestor_cooperator_name = selectedCooperatorLookup.DisplayMember;
                            }
                            break;
                        case "ship_to_cooperator":
                            CurrentOrderRequest.ship_to_cooperator_id = (int)selectedCooperatorLookup.ValueMember;
                            CurrentOrderRequest.ship_to_cooperator_name = selectedCooperatorLookup.DisplayMember;
                            break;
                        case "requestor_cooperator":
                            CurrentOrderRequest.requestor_cooperator_id = (int)selectedCooperatorLookup.ValueMember;
                            CurrentOrderRequest.requestor_cooperator_name = selectedCooperatorLookup.DisplayMember;
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        private DelegateCommand _saveCommand;
        public DelegateCommand SaveCommand =>
            _saveCommand ?? (_saveCommand = new DelegateCommand(ExecuteSaveCommand).ObservesProperty(() => IsNotBusy));

        private async void ExecuteSaveCommand()
        {
            try
            {
                IsBusy = true;

                if (string.IsNullOrEmpty(CurrentOrderRequest.local_number))
                    throw new Exception("Local number is empty");
                if (SelectedOrderType == null)
                    throw new Exception("Order type is empty");
                if (SelectedOrderIntendedUse == null)
                    throw new Exception("Order intended use is empty");

                if (_isNew)
                {
                    CurrentOrderRequest.order_type_code = SelectedOrderType.Code;
                    CurrentOrderRequest.intended_use_code = SelectedOrderIntendedUse.Code;
                    CurrentOrderRequest.created_date = DateTime.Now;
                    CurrentOrderRequest.created_by = Settings.UserCooperatorId;
                    CurrentOrderRequest.owned_date = DateTime.Now;
                    CurrentOrderRequest.owned_by = Settings.UserCooperatorId;
                    CurrentOrderRequest.requestor_cooperator_id = CurrentOrderRequest.requestor_cooperator_id ?? 155383;
                    CurrentOrderRequest.final_recipient_cooperator_id = (int)CurrentOrderRequest.requestor_cooperator_id;
                    CurrentOrderRequest.workgroup_cooperator_id = Settings.WorkgroupCooperatorId > 0 ? Settings.WorkgroupCooperatorId : (int?)null;
#if DEBUG
                    string json = JsonConvert.SerializeObject(CurrentOrderRequest, Formatting.Indented);
                    var continueSaving = await PageDialogService.DisplayAlertAsync("new order request", json, "OK", "CANCEL");
                    if (!continueSaving)
                        return;
#endif
                    string newOrderRequestID = await _restService.CreateOrderRequestAsync(_currentOrderRequest);

                    await PageDialogService.DisplayAlertAsync("Resultado de guardar", $"Solicitud creada\n{newOrderRequestID}", "OK");

                    var navigationResult = await NavigationService.GoBackAsync(new NavigationParameters
                    {
                        { "Refresh", true }
                    });
                    if (!navigationResult.Success)
                        throw navigationResult.Exception;
                }
                else
                {
                    CurrentOrderRequest.order_type_code = SelectedOrderType.Code;
                    CurrentOrderRequest.intended_use_code = SelectedOrderIntendedUse.Code;
                    CurrentOrderRequest.requestor_cooperator_id = CurrentOrderRequest.requestor_cooperator_id ?? 155383;
#if DEBUG
                    string json = JsonConvert.SerializeObject(_currentOrderRequest, Formatting.Indented);
                    var continueSaving = await PageDialogService.DisplayAlertAsync("edit order request", json, "OK", "CANCEL");
                    if (!continueSaving)
                        return;
#endif 
                    string newOrderRequestID = await _restService.UpdateOrderRequestAsync(_currentOrderRequest);
                    await PageDialogService.DisplayAlertAsync("Resultado de guardar", $"Solicitud actualizada correctamente", "OK");

                    var navigationResult = await NavigationService.GoBackAsync(new NavigationParameters
                    {
                        { "RefreshOrderRequest", true }
                    });
                    if (!navigationResult.Success)
                        throw navigationResult.Exception;
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand SelectedOrderTypeChangedCommmand { get; }
        private async void ExecuteSelectedOrderTypeChangedCommmand()
        {
            if (!IsInitialized)
                return;

            try
            {
                IsBusy = true;

                if (SelectedOrderType == null)
                    return;
                
                OrderIntendedUseList = _intendedUseCodeMapList
                    .Where(x => x.ParentCode.Equals(SelectedOrderType.Code))
                    .Select(x => new CodeValue(x.Code, x.Value))
                    .ToArray();
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public override async void Initialize(INavigationParameters parameters)
        {
            try
            {
                OrderTypeList = CodeValueFactory.RegenerationOrderRequestTypeList;
                //OrderIntendedUseList = CodeValueFactory.OrderIntendedUseList;

                _intendedUseCodeMapList = await _restService.GetCodeValueLookupMapList(Settings.LangId);

                if (parameters.ContainsKey("OrderRequest"))
                {
                    CurrentOrderRequest = parameters.GetValue<OrderRequest>("OrderRequest");
                    SelectedOrderType = OrderTypeList.FirstOrDefault(x => x.Code.Equals(CurrentOrderRequest.order_type_code));

                    OrderIntendedUseList = _intendedUseCodeMapList
                        .Where(x => x.ParentCode.Equals(SelectedOrderType.Code))
                        .Select(x => new CodeValue(x.Code, x.Value))
                        .ToArray();

                    SelectedOrderIntendedUse = OrderIntendedUseList.FirstOrDefault(x => x.Code.Equals(CurrentOrderRequest.intended_use_code));
                    Title = $"Editar solicitud";
                }
                else
                {
                    CurrentOrderRequest = new OrderRequest();
                    Title = "Crear solicitud";
                    _isNew = true;
                }

                IsInitialized = true;
            }
            catch(Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {

            }
        }
        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            try
            {
                IsBusy = true;

                base.OnNavigatedTo(parameters);

            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
