﻿using InventoryApp.Extensions;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using InventoryApp.Models.Database;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace InventoryApp.ViewModels
{
    public class ChangeOrderRequestItemInventoryPageViewModel : ViewModelBaseWithDialog
    {
        private readonly IRestService _restService;
        private IEnumerable<Inventory> _inventoryList;

        private IEnumerable<OrderRequestItem> _selectedOrderRequestItemList;
        public IEnumerable<OrderRequestItem> SelectedOrderRequestItemList
        {
            get { return _selectedOrderRequestItemList; }
            set { SetProperty(ref _selectedOrderRequestItemList, value); }
        }
        private OrderRequestItem _currentOrderRequestItem;
        public OrderRequestItem CurrentOrderRequestItem
        {
            get { return _currentOrderRequestItem; }
            set { SetProperty(ref _currentOrderRequestItem, value); }
        }
        private int _selectedOrderRequestItemListCount;
        public int SelectedOrderRequestItemListCount
        {
            get { return _selectedOrderRequestItemListCount; }
            set { SetProperty(ref _selectedOrderRequestItemListCount, value); }
        }
        private ObservableCollection<Inventory> _FilteredInventoryList;
        public ObservableCollection<Inventory> FilteredInventoryList
        {
            get { return _FilteredInventoryList; }
            set { SetProperty(ref _FilteredInventoryList, value); }
        }
        private Inventory _selectedInventory;
        public Inventory SelectedInventory
        {
            get { return _selectedInventory; }
            set { SetProperty(ref _selectedInventory, value); }
        }
        public ChangeOrderRequestItemInventoryPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService,
            IRestService restService)
            : base(navigationService, pageDialogService)
        {
            _restService = restService;

            Title = "Cambiar inventario";
            FilteredInventoryList = new ObservableCollection<Inventory>();

            CancelCommand = new DelegateCommand(ExecuteCancelCommand).ObservesCanExecute(() => IsNotBusy);
            SaveCommand = new DelegateCommand(ExecuteSaveCommand).ObservesCanExecute(() => IsNotBusy);
            OrderRequestItemSelectedCommand = new DelegateCommand(ExecuteOrderRequestItemSelectedCommand).ObservesCanExecute(() => IsNotBusy);
            InventorySelectedCommand = new DelegateCommand(ExecuteInventorySelectedCommand).ObservesCanExecute(() => IsNotBusy);
        }
        private DelegateCommand<string> _showDebugInfoCommand;
        public DelegateCommand<string> ShowDebugInfoCommand =>
            _showDebugInfoCommand ?? (_showDebugInfoCommand = new DelegateCommand<string>(ExecuteShowDebugInfoCommand));

        private async void ExecuteShowDebugInfoCommand(string parameter)
        {
            try
            {
                IsBusy = true;

                string json = string.Empty;
                
                switch (parameter ?? string.Empty)
                {
                    case "order_request_item":
                        json = JsonConvert.SerializeObject(_selectedOrderRequestItemList, Formatting.Indented);
                        break;
                    default:
                        throw new Exception("Not supported");
                }
                await Xamarin.Essentials.Clipboard.SetTextAsync(JsonConvert.SerializeObject(json));
                await PageDialogService.DisplayAlertAsync("", json, "OK");
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand CancelCommand { get; }
        private async void ExecuteCancelCommand()
        {
            _ = await NavigationService.GoBackAsync();
        }
        public DelegateCommand SaveCommand { get; }
        private async void ExecuteSaveCommand()
        {
            try
            {
                IsBusy = true;

                string json = JsonConvert.SerializeObject(CurrentOrderRequestItem, Formatting.Indented);
                var continueSaving = await PageDialogService.DisplayAlertAsync("Are you sure to continue", json, "OK", "CANCEL");
                if (!continueSaving)
                    return;

                foreach (var orderRequestItem in SelectedOrderRequestItemList)
                {
                    var result = await _restService.UpdateOrderRequestItemAsync(orderRequestItem);
                }

                await PageDialogService.DisplayAlertAsync("Result", $"Successfully updated", "OK");

                var navigationResult = await NavigationService.GoBackAsync(new NavigationParameters
                {
                    { "RefreshItems", true }
                });
                if (!navigationResult.Success)
                    throw navigationResult.Exception;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand OrderRequestItemSelectedCommand { get; }
        private async void ExecuteOrderRequestItemSelectedCommand()
        {
            try
            {
                IsBusy = true;

                SelectedInventory = null;
                FilteredInventoryList = new ObservableCollection<Inventory>(_inventoryList.Where(x => x.accession_id == CurrentOrderRequestItem.accession_id));
                SelectedInventory = FilteredInventoryList.FirstOrDefault(x => x.inventory_id == CurrentOrderRequestItem.inventory_id);
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand InventorySelectedCommand { get; }
        private async void ExecuteInventorySelectedCommand()
        {
            try
            {
                IsBusy = true;

                if(SelectedInventory != null)
                {
                    CurrentOrderRequestItem.inventory_id = SelectedInventory.inventory_id;
                    CurrentOrderRequestItem.quantity_on_hand = SelectedInventory.quantity_on_hand;
                    CurrentOrderRequestItem.quantity_on_hand_unit_code = SelectedInventory.quantity_on_hand_unit_code;
                    CurrentOrderRequestItem.name = SelectedInventory.inventory_number;
                    CurrentOrderRequestItem.storage_location_part1 = SelectedInventory.storage_location_part1;
                    CurrentOrderRequestItem.storage_location_part2 = SelectedInventory.storage_location_part2;
                    CurrentOrderRequestItem.storage_location_part3 = SelectedInventory.storage_location_part3;
                    CurrentOrderRequestItem.storage_location_part4 = SelectedInventory.storage_location_part4;
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public override void Initialize(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("SelectedOrderRequestItemList"))
            {
                SelectedOrderRequestItemList = parameters.GetValue<IEnumerable<OrderRequestItem>>("SelectedOrderRequestItemList");
                SelectedOrderRequestItemListCount = SelectedOrderRequestItemList.Count();
                CurrentOrderRequestItem = SelectedOrderRequestItemList.FirstOrDefault();
            }
            else
            {
                throw new Exception($"{nameof(SelectedOrderRequestItemList)} is required");
            }
        }
        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            try
            {
                IsBusy = true;

                //Title = $"Change order item inventory of {SelectedOrderRequestItemList.Count()} item(s)";

                var accessionIds = SelectedOrderRequestItemList.Select(x => x.accession_id).ToArray();
                _inventoryList = await _restService.GetInventoriesByAccessionsAsync(accessionIds);

                FilteredInventoryList = new ObservableCollection<Inventory>(_inventoryList.Where(x => x.accession_id == CurrentOrderRequestItem.accession_id));
                SelectedInventory = FilteredInventoryList.FirstOrDefault(x => x.inventory_id == CurrentOrderRequestItem.inventory_id);
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
