﻿using InventoryApp.Extensions;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InventoryApp.ViewModels
{
    public class RegenerationOrderRequestItemPageViewModel : ViewModelBaseWithDialog
    {
        private readonly IRestService _restService;

        private IEnumerable<OrderRequestItem> _selectedOrderRequestItemList;
        public IEnumerable<OrderRequestItem> SelectedOrderRequestItemList
        {
            get { return _selectedOrderRequestItemList; }
            set { SetProperty(ref _selectedOrderRequestItemList, value); }
        }
        private OrderRequestItem _currentOrderRequestItem;
        public OrderRequestItem CurrentOrderRequestItem
        {
            get { return _currentOrderRequestItem; }
            set { SetProperty(ref _currentOrderRequestItem, value); }
        }
        private IEnumerable<CodeValue> _quantityUnitList;
        public IEnumerable<CodeValue> QuantityUnitList
        {
            get { return _quantityUnitList; }
            set { SetProperty(ref _quantityUnitList, value); }
        }
        private CodeValue _selectedQuantityUnit;
        public CodeValue SelectedQuantityUnit
        {
            get { return _selectedQuantityUnit; }
            set { SetProperty(ref _selectedQuantityUnit, value); }
        }
        //public bool IsSingleSelection { get; set; }
        private bool _isSingleSelection;
        public bool IsSingleSelection
        {
            get { return _isSingleSelection; }
            set { SetProperty(ref _isSingleSelection, value); }
        }
        public RegenerationOrderRequestItemPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService,
            IRestService restService)
            : base(navigationService, pageDialogService)
        {
            _restService = restService;

            CancelCommand = new DelegateCommand(ExecuteCancelCommand).ObservesCanExecute(() => IsNotBusy);
            SaveCommand = new DelegateCommand(ExecuteSaveCommand).ObservesCanExecute(() => IsNotBusy);

            QuantityUnitList = CodeValueFactory.QuantityUnitList;
        }
        private DelegateCommand _showDebugInfoCommand;
        public DelegateCommand ShowDebugInfoCommand =>
            _showDebugInfoCommand ?? (_showDebugInfoCommand = new DelegateCommand(ExecuteShowDebugInfoCommand));
        private async void ExecuteShowDebugInfoCommand()
        {
            try
            {
                IsBusy = true;

                string json = JsonConvert.SerializeObject(_currentOrderRequestItem, Formatting.Indented);
                await Xamarin.Essentials.Clipboard.SetTextAsync(JsonConvert.SerializeObject(json));
                await PageDialogService.DisplayAlertAsync("", json, "OK");
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        
        public DelegateCommand CancelCommand { get; }
        private async void ExecuteCancelCommand()
        {
            _ = await NavigationService.GoBackAsync();
        }
        public DelegateCommand SaveCommand { get; }
        private async void ExecuteSaveCommand()
        {
            try
            {
                IsBusy = true;

                CurrentOrderRequestItem.quantity_shipped_unit_code = SelectedQuantityUnit?.Code;
#if DEBUG
                string json = JsonConvert.SerializeObject(CurrentOrderRequestItem, Formatting.Indented);
                var continueSaving = await PageDialogService.DisplayAlertAsync("Are you sure to continue", json, "OK", "CANCEL");
                if (!continueSaving)
                    return;
#endif

                if (IsSingleSelection)
                {
                    if (string.IsNullOrEmpty(CurrentOrderRequestItem.name))
                        CurrentOrderRequestItem.name = "-";
                    string result = await _restService.UpdateOrderRequestItemAsync(CurrentOrderRequestItem);
                }
                else
                {
                    foreach (var orderRequestItem in SelectedOrderRequestItemList)
                    {
                        orderRequestItem.quantity_shipped = CurrentOrderRequestItem.quantity_shipped;
                        orderRequestItem.quantity_shipped_unit_code = CurrentOrderRequestItem.quantity_shipped_unit_code;
                        orderRequestItem.note = CurrentOrderRequestItem.note;
                        string result = await _restService.UpdateOrderRequestItemAsync(orderRequestItem);
                    }
                }
                
                await PageDialogService.DisplayAlertAsync("Resultado de guardar", $"Se actualizó correctamente", "OK");

                var navigationResult = await NavigationService.GoBackAsync(new NavigationParameters
                {
                    { "RefreshItems", true }
                });
                if (!navigationResult.Success)
                    throw navigationResult.Exception;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public override void Initialize(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("SelectedOrderRequestItemList"))
            {
                SelectedOrderRequestItemList = parameters.GetValue<IEnumerable<OrderRequestItem>>("SelectedOrderRequestItemList");

                if (SelectedOrderRequestItemList.Count() == 1)
                {
                    IsSingleSelection = true;
                    CurrentOrderRequestItem = SelectedOrderRequestItemList.FirstOrDefault();
                    Title = $"Editar Item de solicitud {CurrentOrderRequestItem.sequence_number} : {CurrentOrderRequestItem.inventory_number}";
                    SelectedQuantityUnit = QuantityUnitList.FirstOrDefault(x => x.Code.Equals(_currentOrderRequestItem.quantity_shipped_unit_code));
                }
                else
                {
                    IsSingleSelection = false;
                    Title = $"Editar {SelectedOrderRequestItemList.Count()} Items de solicitud";
                    CurrentOrderRequestItem = new OrderRequestItem();
                }
            }
            else
            {
                throw new Exception($"{nameof(SelectedOrderRequestItemList)} is required");
            }
        }
    }
}
