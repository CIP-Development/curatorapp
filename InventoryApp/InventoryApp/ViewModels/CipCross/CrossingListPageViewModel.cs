﻿using InventoryApp.Extensions;
using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Models.LocalStorage;
using Prism.Commands;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InventoryApp.ViewModels
{
    public class CrossingListPageViewModel : ViewModelBaseWithDialog
    {
        private readonly IOrderRequestRepository _orderRequestRepository;
        private readonly IInventoryLocalRepository _inventoryLocalRepository;
        private readonly ICrossingLocalRepository _crossingLocalRepository;

        #region Properties
        private OrderRequestDb _selectedOrderRequestDb;
        public OrderRequestDb SelectedOrderRequestDb
        {
            get { return _selectedOrderRequestDb; }
            set { SetProperty(ref _selectedOrderRequestDb, value); }
        }
        private IEnumerable<CrossingDb> _crossingDbList;
        public IEnumerable<CrossingDb> CrossingDbList
        {
            get { return _crossingDbList; }
            set { SetProperty(ref _crossingDbList, value); }
        }
        private CrossingDb _selectedCrossingDb;
        public CrossingDb SelectedCrossingDb
        {
            get { return _selectedCrossingDb; }
            set { SetProperty(ref _selectedCrossingDb, value); }
        }
        private int _crossingDbListCount;
        public int CrossingDbListCount
        {
            get { return _crossingDbListCount; }
            set { SetProperty(ref _crossingDbListCount, value); }
        }
        private string _searchText;
        public string SearchText
        {
            get { return _searchText; }
            set { SetProperty(ref _searchText, value); }
        }
        #endregion

        public CrossingListPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService,
            IOrderRequestRepository orderRequestRepository, IInventoryLocalRepository inventoryLocalRepository, ICrossingLocalRepository crossingLocalRepository)
            : base(navigationService, pageDialogService)
        {
            _orderRequestRepository = orderRequestRepository;
            _inventoryLocalRepository = inventoryLocalRepository;
            _crossingLocalRepository = crossingLocalRepository;

            EditCommand = new DelegateCommand(ExecuteEditCommand).ObservesCanExecute(() => IsNotBusy);
            SearchCommand = new DelegateCommand(ExecuteSearchCommand).ObservesCanExecute(() => IsNotBusy);
        }
        public DelegateCommand EditCommand { get; }
        private async void ExecuteEditCommand()
        {
            try
            {
                IsBusy = true;

                if (SelectedCrossingDb == null)
                    return;

                var navResult = await NavigationService.NavigateAsync("EditCrossingPage",
                    new NavigationParameters
                    {
                        { "SelectedCrossingDb" , SelectedCrossingDb}
                    });
                if (!navResult.Success)
                    throw navResult.Exception;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand SearchCommand { get; }
        private async void ExecuteSearchCommand()
        {
            try
            {
                IsBusy = true;
                
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public override async void Initialize(INavigationParameters parameters)
        {
            try
            {
                IsBusy = true;

                if (Settings.SelectedRegenerationOrderRequestId == -1)
                    throw new Exception("Seleccionar solicitud en Ajustes de regeneración");
                
                SelectedOrderRequestDb = await _orderRequestRepository.GetOrderRequestById(Settings.SelectedRegenerationOrderRequestId);
                if (SelectedOrderRequestDb == null)
                    throw new Exception("Solicitud no encontrada\nIr a Ajustes de regeneración");

                CrossingDbList = await _crossingLocalRepository.GetCrossingByOrderRequestIdAsync(SelectedOrderRequestDb.OrderRequestId);
                CrossingDbListCount = CrossingDbList.Count();

                IsInitialized = true;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            if (!IsInitialized)
                return;
            try
            {
                IsBusy = true;

                if (parameters.ContainsKey("Refresh"))
                {
                    CrossingDbList = await _crossingLocalRepository.GetCrossingByOrderRequestIdAsync(SelectedOrderRequestDb.OrderRequestId);
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
