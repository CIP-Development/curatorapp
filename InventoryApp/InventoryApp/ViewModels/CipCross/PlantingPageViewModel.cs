﻿using InventoryApp.Extensions;
using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using InventoryApp.Models.Database;
using InventoryApp.Models.LocalStorage;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryApp.ViewModels
{
    public class PlantingPageViewModel : ViewModelBaseWithDialog
    {
        private readonly IOrderRequestRepository _orderRequestRepository;
        private readonly IInventoryLocalRepository _inventoryLocalRepository;
        private readonly IInventoryActionLocalRepository _inventoryActionLocalRepository;

        private OrderRequestDb _selectedOrderRequestDb;
        public OrderRequestDb SelectedOrderRequestDb
        {
            get { return _selectedOrderRequestDb; }
            set { SetProperty(ref _selectedOrderRequestDb, value); }
        }
        private string _searchText;
        public string SearchText
        {
            get { return _searchText; }
            set { SetProperty(ref _searchText, value); }
        }
        private Inventory _currentInventory;
        public Inventory CurrentInventory
        {
            get { return _currentInventory; }
            set { SetProperty(ref _currentInventory, value); }
        }
        private bool _hasResult;
        public bool HasResult
        {
            get { return _hasResult; }
            set { SetProperty(ref _hasResult, value); }
        }
        private ObservableCollection<InventoryActionDb> _inventoryActionList;
        public ObservableCollection<InventoryActionDb> InventoryActionList
        {
            get { return _inventoryActionList; }
            set { SetProperty(ref _inventoryActionList, value); }
        }
        private string _searchFilter;
        public string SearchFilter
        {
            get { return _searchFilter; }
            set { SetProperty(ref _searchFilter, value); }
        }
        public PlantingPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService,
            IInventoryLocalRepository inventoryLocalRepository, IOrderRequestRepository orderRequestRepository,
            IInventoryActionLocalRepository inventoryActionLocalRepository)
            : base(navigationService, pageDialogService)
        {
            _orderRequestRepository = orderRequestRepository;
            _inventoryLocalRepository = inventoryLocalRepository;
            _inventoryActionLocalRepository = inventoryActionLocalRepository;

            SearchCommand = new DelegateCommand(ExecuteSearchCommand).ObservesCanExecute(() => IsNotBusy);
            TransplantCommmand = new DelegateCommand(ExecuteTransplantCommmand).ObservesCanExecute(() => IsNotBusy);
            EvaluateCommmand = new DelegateCommand(ExecuteEvaluateCommmand).ObservesCanExecute(() => IsNotBusy);

            HasResult = false;
            SearchFilter = "InventoryGuid";
        }
        public DelegateCommand TransplantCommmand { get; }
        private async void ExecuteTransplantCommmand()
        {
            try
            {
                IsBusy = true;

                if (CurrentInventory == null)
                    return;

                var selectedItems = new List<Inventory>();
                selectedItems.Add(CurrentInventory);
                var navigationResult = await NavigationService.NavigateAsync("TransplantInventoriesPage", new NavigationParameters
                {
                    {"SelectedInventories", selectedItems }
                });
                if (!navigationResult.Success)
                    throw navigationResult.Exception;

            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public DelegateCommand EvaluateCommmand { get; }
        private async void ExecuteEvaluateCommmand()
        {
            try
            {
                IsBusy = true;

                if (CurrentInventory == null)
                    return;

                var selectedItems = new List<Inventory>();
                selectedItems.Add(CurrentInventory);
                var navigationResult = await NavigationService.NavigateAsync("EditRegenerationInventoryPage", new NavigationParameters
                {
                    {"SelectedInventories", selectedItems }
                });
                if (!navigationResult.Success)
                    throw navigationResult.Exception;

            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        
        public DelegateCommand SearchCommand { get; }
        private async void ExecuteSearchCommand()
        {
            try
            {
                IsBusy = true;

                if (string.IsNullOrWhiteSpace(SearchText))
                    return;

                await SearchInventory();
            }
            catch (Exception ex)
            {
                HasResult = false;
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        private async Task SearchInventory()
        {
            CurrentInventory = null;
            InventoryDb dbInventory = null;

            switch (SearchFilter)
            {
                case "InventoryGuid":
                    dbInventory = await _inventoryLocalRepository.GetInventoryByGuidAsync(SearchText);
                    break;
                case "InventoryNumber":
                    dbInventory = await _inventoryLocalRepository.GetInventoryByInventoryNumberAsync(SearchText);
                    break;
                case "LotId":
                    int lotId;
                    if (int.TryParse(SearchText, out lotId))
                    {
                        var dbResults = await _inventoryLocalRepository.GetByLotAsync(lotId);
                        dbInventory = dbResults.FirstOrDefault();
                    }
                    else
                        throw new Exception("Formato inválido.\nDebe ser un número entero");
                    break;
                default:
                    throw new Exception("Filtro de búsqueda no soportado");
            }
            if (dbInventory == null)
            {
                HasResult = false;
                await PageDialogService.DisplayAlertAsync("Resultado de búsqueda", "No se ha encontrado ningún inventario", "OK");
            }
            else
            {
                CurrentInventory = new Inventory
                {
                    accession_id = dbInventory.AccessionId,
                    accession_number = dbInventory.AccessionNumber,
                    acc_name_col = dbInventory.CollectingNumber,
                    acc_name_cul = dbInventory.AccessionName,
                    availability_status_code = dbInventory.AvailabilityStatusCode,
                    container_code = dbInventory.ContainerCode,
                    created_by = dbInventory.CreatedBy,
                    created_date = dbInventory.CreatedDate,
                    form_type_code = dbInventory.FormTypeCode,
                    inventory_id = dbInventory.InventoryId,
                    inventory_maint_policy_id = dbInventory.InventoryMaintPolicyId,
                    inventory_number = dbInventory.InventoryNumber,
                    inventory_number_part1 = dbInventory.InventoryNumberPart1,
                    inventory_number_part2 = dbInventory.InventoryNumberPart2,
                    inventory_number_part3 = dbInventory.InventoryNumberPart3,
                    is_auto_deducted = dbInventory.IsAutoDeducted,
                    is_available = dbInventory.IsAvailable,
                    is_distributable = dbInventory.IsDistributable,
                    modified_by = dbInventory.ModifiedBy,
                    modified_date = dbInventory.ModifiedDate,
                    note = dbInventory.Note,
                    quantity_on_hand = dbInventory.QuantityOnHand,
                    quantity_on_hand_unit_code = dbInventory.QuantityOnHandUnitCode,
                    storage_location_part1 = dbInventory.StorageLocationPart1,
                    storage_location_part2 = dbInventory.StorageLocationPart2,
                    storage_location_part3 = dbInventory.StorageLocationPart3,
                    storage_location_part4 = dbInventory.StorageLocationPart4,
                    taxonomy_species_code = dbInventory.TaxonomySpeciesCode,
                    workgroup_cooperator_id = dbInventory.OwnedBy,
                    order_request_id = dbInventory.OrderRequestId,
                    Guid = dbInventory.Guid,
                    form_type = dbInventory.FormType,
                    container = dbInventory.Container,
                    taxonomy_species_name = dbInventory.TaxonomySpeciesName,
                };
                HasResult = true;

                var tempInventoryActions = await _inventoryActionLocalRepository.GetByInventoryGuidAsync(dbInventory.Guid);
                InventoryActionList = new ObservableCollection<InventoryActionDb>(tempInventoryActions);
            }
        }

        public override async void Initialize(INavigationParameters parameters)
        {
            try
            {
                IsBusy = true;

                if (Settings.SelectedRegenerationOrderRequestId == -1)
                    throw new Exception("Seleccionar solicitud en Ajustes de regeneración");

                SelectedOrderRequestDb = await _orderRequestRepository.GetOrderRequestById(Settings.SelectedRegenerationOrderRequestId);
                if (SelectedOrderRequestDb == null)
                    throw new Exception("Solicitud no encontrada\nIr a Ajustes de regeneración");

                IsInitialized = true;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            if (!IsInitialized)
                return;

            try
            {
                IsBusy = true;

                if (parameters.ContainsKey("Refresh"))
                {
                    await SearchInventory();
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
