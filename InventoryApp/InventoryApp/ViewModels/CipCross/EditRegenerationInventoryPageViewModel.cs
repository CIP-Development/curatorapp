﻿using InventoryApp.Extensions;
using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using InventoryApp.Models.Database;
using InventoryApp.Models.LocalStorage;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InventoryApp.ViewModels
{
    public class EditRegenerationInventoryPageViewModel : ViewModelBaseWithDialog
    {
        private readonly IInventoryLocalRepository _inventoryLocalRepository;
        private InventoryDb _selectedInventoryDb;

        public IEnumerable<InventoryDb> SelectedInventories { get; set; }

        private IEnumerable<CodeValue> _formTypeCodeListt;
        public IEnumerable<CodeValue> FormTypeCodeList
        {
            get { return _formTypeCodeListt; }
            set { SetProperty(ref _formTypeCodeListt, value); }
        }
        private IEnumerable<CodeValue> _containerCodeList;
        public IEnumerable<CodeValue> ContainerCodeList
        {
            get { return _containerCodeList; }
            set { SetProperty(ref _containerCodeList, value); }
        }
        private CodeValue _selectedFormTypeCode;
        public CodeValue SelectedFormTypeCode
        {
            get { return _selectedFormTypeCode; }
            set { SetProperty(ref _selectedFormTypeCode, value); }
        }
        private CodeValue _selectedContainerCode;
        public CodeValue SelectedContainerCode
        {
            get { return _selectedContainerCode; }
            set { SetProperty(ref _selectedContainerCode, value); }
        }
        private IEnumerable<CodeValue> _quantityUnitCodeList;
        public IEnumerable<CodeValue> QuantityUnitCodeList
        {
            get { return _quantityUnitCodeList; }
            set { SetProperty(ref _quantityUnitCodeList, value); }
        }
        private CodeValue _selectedQuantityUnitCode;
        public CodeValue SelectedQuantityUnitCode
        {
            get { return _selectedQuantityUnitCode; }
            set { SetProperty(ref _selectedQuantityUnitCode, value); }
        }
        private string _storageLocation1;
        public string StorageLocation1
        {
            get { return _storageLocation1; }
            set { SetProperty(ref _storageLocation1, value); }
        }
        private string _storageLocation2;
        public string StorageLocation2
        {
            get { return _storageLocation2; }
            set { SetProperty(ref _storageLocation2, value); }
        }
        private string _storageLocation3;
        public string StorageLocation3
        {
            get { return _storageLocation3; }
            set { SetProperty(ref _storageLocation3, value); }
        }
        private string _storageLocation4;
        public string StorageLocation4
        {
            get { return _storageLocation4; }
            set { SetProperty(ref _storageLocation4, value); }
        }
        private string _notes;
        public string Notes
        {
            get { return _notes; }
            set { SetProperty(ref _notes, value); }
        }
        private decimal _quantity;
        public decimal Quantity
        {
            get { return _quantity; }
            set { SetProperty(ref _quantity, value); }
        }
        private bool _isMother;
        public bool IsMother
        {
            get { return _isMother; }
            set { SetProperty(ref _isMother, value); }
        }
        
        public EditRegenerationInventoryPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService,
            IInventoryLocalRepository inventoryLocalRepository)
            : base(navigationService, pageDialogService)
        {
            _inventoryLocalRepository = inventoryLocalRepository;

            CancelCommand = new DelegateCommand(ExecuteCancelCommand).ObservesCanExecute(() => IsNotBusy);
            SaveCommand = new DelegateCommand(ExecuteSaveCommand).ObservesCanExecute(() => IsNotBusy);
        }
        public DelegateCommand CancelCommand { get; }
        private async void ExecuteCancelCommand()
        {
            try
            {
                var navResult = await NavigationService.GoBackAsync();
                if (!navResult.Success)
                    throw navResult.Exception;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
        }
        public DelegateCommand SaveCommand { get; }
        private async void ExecuteSaveCommand()
        {
            try
            {
                IsBusy = true;

                if (SelectedContainerCode == null)
                    throw new Exception("Contenedor está vacío");
                if (SelectedFormTypeCode == null)
                    throw new Exception("Tipo de forma está vacío");
                if (SelectedQuantityUnitCode == null)
                    throw new Exception("Unidad de cantidad está vacía");

                _selectedInventoryDb.ContainerCode = SelectedContainerCode.Code;
                _selectedInventoryDb.Container = SelectedContainerCode.Value;
                if (!_selectedInventoryDb.FormTypeCode.Equals(SelectedFormTypeCode.Code))
                {
                    _selectedInventoryDb.FormTypeCode = SelectedFormTypeCode.Code;
                    _selectedInventoryDb.FormType = SelectedFormTypeCode.Value;
                    _selectedInventoryDb.InventoryNumber = $"{_selectedInventoryDb.InventoryNumberPart1} {_selectedInventoryDb.InventoryNumberPart2} {_selectedInventoryDb.InventoryNumberPart3} {_selectedInventoryDb.FormTypeCode}";
                }
                _selectedInventoryDb.IsMother = IsMother;
                _selectedInventoryDb.ModifiedBy = Settings.UserCooperatorId;
                _selectedInventoryDb.ModifiedDate = DateTime.UtcNow;
                _selectedInventoryDb.Note = Notes;
                _selectedInventoryDb.QuantityOnHand = Quantity;
                _selectedInventoryDb.QuantityOnHandUnitCode = SelectedQuantityUnitCode.Code;
                _selectedInventoryDb.QuantityOnHandUnit = SelectedQuantityUnitCode.Value;
                _selectedInventoryDb.StorageLocationPart1 = StorageLocation1;
                _selectedInventoryDb.StorageLocationPart2 = StorageLocation2;
                _selectedInventoryDb.StorageLocationPart3 = StorageLocation3;
                _selectedInventoryDb.StorageLocationPart4 = StorageLocation4;

                var rows = await _inventoryLocalRepository.UpdateInventoryAsync(_selectedInventoryDb);

                var navResult = await NavigationService.GoBackAsync(new NavigationParameters
                {
                    { "ModifiedInventoryDb", _selectedInventoryDb }
                });
                if (!navResult.Success)
                    throw navResult.Exception;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public override void Initialize(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("SelectedInventory"))
            {
                //SelectedInventories = parameters.GetValue<IEnumerable<InventoryDb>>("SelectedInventories");
                _selectedInventoryDb = parameters.GetValue<InventoryDb>("SelectedInventory");

                FormTypeCodeList = CodeValueFactory.FormTypeList;
                ContainerCodeList = CodeValueFactory.InventoryContainerList;
                QuantityUnitCodeList = CodeValueFactory.QuantityUnitList;

                SelectedFormTypeCode = FormTypeCodeList.FirstOrDefault(x => x.Code.Equals(_selectedInventoryDb.FormTypeCode));
                SelectedContainerCode = ContainerCodeList.FirstOrDefault(x => x.Code.Equals(_selectedInventoryDb.ContainerCode));
                SelectedQuantityUnitCode = QuantityUnitCodeList.FirstOrDefault(x => x.Code.Equals(_selectedInventoryDb.QuantityOnHandUnitCode));

                Quantity = _selectedInventoryDb.QuantityOnHand.GetValueOrDefault(0);
                IsMother = _selectedInventoryDb.IsMother;

                StorageLocation1 = _selectedInventoryDb.StorageLocationPart1;
                StorageLocation2 = _selectedInventoryDb.StorageLocationPart2;
                StorageLocation3 = _selectedInventoryDb.StorageLocationPart3;
                StorageLocation4 = _selectedInventoryDb.StorageLocationPart4;
                Notes = _selectedInventoryDb.Note;
            }
            else
                throw new Exception("SelectedInventory is required");
        }
    }
}
