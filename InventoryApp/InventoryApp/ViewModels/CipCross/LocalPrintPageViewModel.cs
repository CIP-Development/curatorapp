﻿using InventoryApp.Extensions;
using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Models.LocalStorage;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Zebra.Sdk.Comm;
using Zebra.Sdk.Printer;

namespace InventoryApp.ViewModels
{
    public class LocalPrintPageViewModel : ViewModelBaseWithDialog
    {
        private IEnumerable<InventoryDb> _selectedInventoryDbList;
        public string BluetoothPrinterMac { get => Settings.BluetoothPrinterMac; }

        private string _connectionStatus;
        public string ConnectionStatus
        {
            get { return _connectionStatus; }
            set { SetProperty(ref _connectionStatus, value); }
        }
        private string _connectionStatusColor;
        public string ConnectionStatusColor
        {
            get { return _connectionStatusColor; }
            set { SetProperty(ref _connectionStatusColor, value); }
        }
        private int _numberOfCopies;
        public int NumberOfCopies
        {
            get { return _numberOfCopies; }
            set { SetProperty(ref _numberOfCopies, value); }
        }
        private IEnumerable<LabelTemplateDb> _labelTemplateDbList;
        public IEnumerable<LabelTemplateDb> LabelTemplateDbList
        {
            get { return _labelTemplateDbList; }
            set { SetProperty(ref _labelTemplateDbList, value); }
        }
        private LabelTemplateDb _selectedLabelTemplateDb;
        public LabelTemplateDb SelectedLabelTemplateDb
        {
            get { return _selectedLabelTemplateDb; }
            set { SetProperty(ref _selectedLabelTemplateDb, value); }
        }
        public LocalPrintPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService, pageDialogService)
        {
            PrintCommand = new DelegateCommand(ExecutePrintCommand).ObservesCanExecute(() => IsNotBusy);

            ConnectionStatus = "No conectado";
            ConnectionStatusColor = "Red";
            NumberOfCopies = 1;
            LabelTemplateDbList = LabelTemplateDbFactory.LabelTemplateList;
        }
        public DelegateCommand PrintCommand { get; }
        private async void ExecutePrintCommand()
        {
            Connection connection = null;
            try
            {
                IsBusy = true;

                if (SelectedLabelTemplateDb == null)
                    throw new Exception("Plantilla de etiqueta está vacía");

                try
                {
                    connection = DependencyService.Get<IConnectionManager>().GetBluetoothConnection(Settings.BluetoothPrinterMac);
                }
                catch (NotImplementedException)
                {
                    throw new NotImplementedException("Bluetooth connection not supported on this platform");
                }

                if (connection == null)
                    return;

                await DisplayConnectionStatusAsync("Conectando...", "Goldenrod", 1500);
                connection.Open();
                await DisplayConnectionStatusAsync("Conectado", "Green", 1500);

                //await DisplayConnectionStatusAsync("Determinando lenguaje de impresora...", "Goldenrod", 1500);
                //PrinterLanguage printerLanguage = ZebraPrinterFactory.GetInstance(connection).PrinterControlLanguage;
                //await DisplayConnectionStatusAsync("Lenguaje de impresora : " + printerLanguage.ToString(), "Blue", 1500);

                //if(printerLanguage != PrinterLanguage.ZPL)
                //    throw new ZebraPrinterLanguageUnknownException();

                ConnectionStatus = "Enviando datos...";
                ConnectionStatusColor = "Goldenrod";

                

                //Replace values
                foreach (InventoryDb currentInventoryDb in _selectedInventoryDbList)
                {
                    var zpl = SelectedLabelTemplateDb.Zpl;

                    switch (SelectedLabelTemplateDb.TemplateCode)
                    {
                        case "PLATE":
                            zpl = string.Format(zpl,
                                NumberOfCopies,
                                currentInventoryDb.AccessionNumber,
                                string.Empty,
                                currentInventoryDb.InventoryNumber,
                                currentInventoryDb.InventoryNumberPart2,
                                "bulk",
                                "spp",
                                DateTime.UtcNow.Year);
                            break;
                        case "GENERAL":
                            zpl = string.Format(zpl,
                                NumberOfCopies,
                                currentInventoryDb.AccessionNumber, $"{currentInventoryDb.InventoryNumberPart2} {currentInventoryDb.InventoryNumberPart3}",
                                currentInventoryDb.StorageLocationPart1, DateTime.Now.ToString("MM/dd/yy"),
                                currentInventoryDb.Guid);
                            break;
                        case "HARVEST":
                            zpl = string.Format(zpl,
                                NumberOfCopies,
                                currentInventoryDb.CollectingNumber,
                                currentInventoryDb.Extensions.ContainsKey("HarvestPlantCount") ? currentInventoryDb.Extensions["HarvestPlantCount"] : string.Empty,
                                currentInventoryDb.Extensions.ContainsKey("HarvestTuberCount") ? currentInventoryDb.Extensions["HarvestTuberCount"] : string.Empty,
                                currentInventoryDb.Extensions.ContainsKey("HarvestTuberWeigth") ? currentInventoryDb.Extensions["HarvestTuberWeigth"] : string.Empty,
                                DateTime.Now.ToString("MM/dd/yy"),
                                currentInventoryDb.StorageLocationPart1,
                                currentInventoryDb.Extensions.ContainsKey("HarvestTuberOriginForm") ? currentInventoryDb.Extensions["HarvestTuberOriginForm"] : string.Empty,
                                currentInventoryDb.Guid);
                            break;
                        case "CROSSING":
                            zpl = string.Format(zpl,
                                NumberOfCopies,
                                currentInventoryDb.CollectingNumber,
                                currentInventoryDb.Extensions.ContainsKey("CrossingOrder") ? currentInventoryDb.Extensions["CrossingOrder"] : string.Empty,
                                currentInventoryDb.Extensions.ContainsKey("CrossingFlowerCount") ? currentInventoryDb.Extensions["CrossingFlowerCount"] : string.Empty,
                                DateTime.Now.ToString("MM/dd/yy"),
                                currentInventoryDb.StorageLocationPart1,
                                currentInventoryDb.Extensions.ContainsKey("CrossingGuid") ? currentInventoryDb.Extensions["CrossingGuid"] : string.Empty);
                            break;
                        case "HARVEST-FRUITS":
                            zpl = string.Format(zpl,
                                NumberOfCopies,
                                currentInventoryDb.CollectingNumber,
                                currentInventoryDb.Extensions.ContainsKey("CrossingType") ? currentInventoryDb.Extensions["CrossingType"] : string.Empty,
                                DateTime.Now.ToString("MM/dd/yy"),
                                currentInventoryDb.StorageLocationPart1,
                                currentInventoryDb.Extensions.ContainsKey("HarvestFlowerCount") ? currentInventoryDb.Extensions["HarvestFlowerCount"] : string.Empty,
                                currentInventoryDb.Extensions.ContainsKey("HarvestFruitCount") ? currentInventoryDb.Extensions["HarvestFruitCount"] : string.Empty,
                                currentInventoryDb.Extensions.ContainsKey("CrossingGuid") ? currentInventoryDb.Extensions["CrossingGuid"] : string.Empty);
                            break;
                        case "POT":
                            var brotherNum = string.Empty;
                            if (currentInventoryDb.InventoryNumberPart3.Contains("."))
                            {
                                brotherNum = currentInventoryDb.InventoryNumberPart3.Split('.').Last();
                            }
                            zpl = string.Format(zpl,
                                NumberOfCopies,
                                currentInventoryDb.AccessionNumber, currentInventoryDb.InventoryNumber,
                                currentInventoryDb.StorageLocationPart1, DateTime.Now.ToString("MM/dd/yy"),
                                brotherNum, $"{currentInventoryDb.AccessionNumber};{brotherNum}");
                            break;
                        default:
                            throw new Exception("Label template not supported");
                    }

#if DEBUG
                    await Xamarin.Essentials.Clipboard.SetTextAsync(zpl);
#endif
                    connection.Write(Encoding.UTF8.GetBytes(zpl));

                    await Task.Delay(1000);
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                try
                {
                    connection?.Close();

                    await DisplayConnectionStatusAsync("Desconectando...", "Goldenrod", 1000);
                    ConnectionStatus = "No conectado";
                    ConnectionStatusColor = "Red";
                }
                catch (ConnectionException) { }
                IsBusy = false;
            }
        }
        private async Task DisplayConnectionStatusAsync(string statusMessage, string color, int displayTime)
        {
            //UpdateConnectionStatus(statusMessage, color);
            ConnectionStatus = statusMessage;
            ConnectionStatusColor = color;
            await Task.Delay(displayTime);
        }

        public override void Initialize(INavigationParameters parameters)
        {
            if (parameters.ContainsKey("SelectedInventoryDbList"))
            {
                _selectedInventoryDbList = parameters.GetValue<IEnumerable<InventoryDb>>("SelectedInventoryDbList");
            }
            else
                throw new Exception("SelectedInventoryDbList is required");
        }
    }
}
