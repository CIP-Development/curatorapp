﻿using InventoryApp.Extensions;
using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Models.LocalStorage;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InventoryApp.ViewModels
{
    public class HarvestFruitPageViewModel : ViewModelBaseWithDialog
    {
        private readonly IOrderRequestRepository _orderRequestRepository;
        private readonly IInventoryLocalRepository _inventoryLocalRepository;
        private readonly IInventoryActionLocalRepository _inventoryActionLocalRepository;
        private readonly ICrossingLocalRepository _crossingLocalRepository;

        #region Properties
        private IEnumerable<OrderRequestDb> _orderRequestDbList;
        public IEnumerable<OrderRequestDb> OrderRequestDbList
        {
            get { return _orderRequestDbList; }
            set { SetProperty(ref _orderRequestDbList, value); }
        }
        private OrderRequestDb _selectedOrderRequestDb;
        public OrderRequestDb SelectedOrderRequestDb
        {
            get { return _selectedOrderRequestDb; }
            set { SetProperty(ref _selectedOrderRequestDb, value); }
        }
        private IEnumerable<CrossingDb> _crossingDbList;
        public IEnumerable<CrossingDb> CrossingDbList
        {
            get { return _crossingDbList; }
            set { SetProperty(ref _crossingDbList, value); }
        }
        private CrossingDb _selectedCrossingDb;
        public CrossingDb SelectedCrossingDb
        {
            get { return _selectedCrossingDb; }
            set { SetProperty(ref _selectedCrossingDb, value); }
        }
        private int _crossingDbListCount;
        public int CrossingDbListCount
        {
            get { return _crossingDbListCount; }
            set { SetProperty(ref _crossingDbListCount, value); }
        }
        private IEnumerable<int> _flowersCountList;
        public IEnumerable<int> FlowersCountList
        {
            get { return _flowersCountList; }
            set { SetProperty(ref _flowersCountList, value); }
        }
        private int _selectedFlowersCount;
        public int SelectedFlowersCount
        {
            get { return _selectedFlowersCount; }
            set { SetProperty(ref _selectedFlowersCount, value); }
        }
        private IEnumerable<int> _fruitsCountList;
        public IEnumerable<int> FruitsCountList
        {
            get { return _fruitsCountList; }
            set { SetProperty(ref _fruitsCountList, value); }
        }
        private int _selectedFruitCount;
        public int SelectedFruitCount
        {
            get { return _selectedFruitCount; }
            set { SetProperty(ref _selectedFruitCount, value); }
        }
        private string _crossingNameWithOrder;
        public string CrossingNameWithOrder
        {
            get { return _crossingNameWithOrder; }
            set { SetProperty(ref _crossingNameWithOrder, value); }
        }

        private string _searchText;
        public string SearchText
        {
            get { return _searchText; }
            set { SetProperty(ref _searchText, value); }
        }
        private DateTime _harvestDate;
        public DateTime HarvestDate
        {
            get { return _harvestDate; }
            set { SetProperty(ref _harvestDate, value); }
        }

        private bool _considerDateRange;
        public bool ConsiderDateRange
        {
            get { return _considerDateRange; }
            set { SetProperty(ref _considerDateRange, value); }
        }
        private DateTime _harvestStartDate;
        public DateTime HarvestStartDate
        {
            get { return _harvestStartDate; }
            set { SetProperty(ref _harvestStartDate, value); }
        }
        private DateTime _harvestFinishDate;
        public DateTime HarvestFinishDate
        {
            get { return _harvestFinishDate; }
            set { SetProperty(ref _harvestFinishDate, value); }
        }
        private string _harvestDateRangeDescription;
        public string HarvestDateRangeDescription
        {
            get { return _harvestDateRangeDescription; }
            set { SetProperty(ref _harvestDateRangeDescription, value); }
        }
        #endregion
        public HarvestFruitPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService,
            IOrderRequestRepository orderRequestRepository, IInventoryLocalRepository inventoryLocalRepository,
            IInventoryActionLocalRepository inventoryActionLocalRepository, ICrossingLocalRepository crossingLocalRepository)
            : base(navigationService, pageDialogService)
        {
            _orderRequestRepository = orderRequestRepository;
            _inventoryLocalRepository = inventoryLocalRepository;
            _inventoryActionLocalRepository = inventoryActionLocalRepository;
            _crossingLocalRepository = crossingLocalRepository;

            SelectedOrderRequestDbChangedCommand = new DelegateCommand(ExecuteSelectedOrderRequestDbChangedCommand).ObservesCanExecute(() => IsNotBusy);
            CrossingSelectedCommand = new DelegateCommand(ExecuteCrossingSelectedCommand).ObservesCanExecute(() => IsNotBusy);
            SearchCommand = new DelegateCommand(ExecuteSearchCommand).ObservesCanExecute(() => IsNotBusy);
            SaveAndPrintCommand = new DelegateCommand(ExecuteSaveAndPrintCommand).ObservesCanExecute(() => IsNotBusy);

            //CrossingTypeCodeList = CodeValueFactory.CrossingTypeList;
            FruitsCountList = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25 };
            FlowersCountList = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25 };

            var today = DateTime.Now.Date;
            HarvestDate = today;
            HarvestStartDate = today;
            HarvestFinishDate = today;
        }

        public DelegateCommand SelectedOrderRequestDbChangedCommand { get; }
        private async void ExecuteSelectedOrderRequestDbChangedCommand()
        {
            if (!IsInitialized)
                return;
            try
            {
                IsBusy = true;

                if (SelectedOrderRequestDb == null)
                    return;

                CrossingDbList = await _crossingLocalRepository.GetCrossingByOrderRequestIdAsync(SelectedOrderRequestDb.OrderRequestId);
                CrossingDbListCount = CrossingDbList.Count();
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand CrossingSelectedCommand { get; }
        private async void ExecuteCrossingSelectedCommand()
        {
            try
            {
                IsBusy = true;

                if (SelectedCrossingDb == null)
                    return;

                CrossingNameWithOrder = SelectedCrossingDb.CrossingName;

                string maleOrder = "?";
                if (SelectedCrossingDb.CrossingTypeCode != null)
                {
                    if (SelectedCrossingDb.CrossingTypeCode.Equals("SC"))
                    {
                        maleOrder = SelectedCrossingDb.MaleOrder.ToString();
                    }
                    else
                    {
                        maleOrder = SelectedCrossingDb.CrossingTypeCode;
                    }
                }
                CrossingNameWithOrder = $"{SelectedCrossingDb.CrossingName} {SelectedCrossingDb.FemaleOrder}-{maleOrder}";

                var intSuperTotalFlores = 0;
                var intSuperTotalBayas = 0;
                if (ConsiderDateRange)
                {
                    foreach (var itemDb in CrossingDbList)
                    {
                        if (itemDb.HarvestDate == null)
                            continue;

                        var crossingDate = itemDb.HarvestDate.Value.Date;
                        if (SelectedCrossingDb.FemaleAccessionNumber.Equals(itemDb.FemaleAccessionNumber) &&
                            SelectedCrossingDb.CrossingTypeCode.Equals(itemDb.CrossingTypeCode) &&
                            crossingDate >= HarvestStartDate.Date && crossingDate <= HarvestFinishDate.Date)
                        {
                            if(itemDb.HarvestedFruits != null)
                            {
                                //intSuperTotalFlores += itemDb.HarvestPollinatedFlowers.Value;
                                intSuperTotalFlores += itemDb.PollinatedFlowers.Value;
                                intSuperTotalBayas += itemDb.HarvestedFruits.Value;
                            }
                        }
                    }

                    HarvestDateRangeDescription = $@"{SelectedCrossingDb.FemaleAccessionNumber} \ T.Flores: {intSuperTotalFlores} T.Frutos: {intSuperTotalBayas}";
                }
                else
                {
                    SelectedFlowersCount = FlowersCountList.FirstOrDefault(x => x == SelectedCrossingDb.PollinatedFlowers);
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand SearchCommand { get; }
        private async void ExecuteSearchCommand()
        {
            try
            {
                IsBusy = true;

                SelectedCrossingDb = CrossingDbList.FirstOrDefault(x => x.Guid.Equals(SearchText));
                if (SelectedCrossingDb == null)
                    await PageDialogService.DisplayAlertAsync("Resultado de búsqueda","No se encontró el id", "OK");
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand SaveAndPrintCommand { get; }
        private async void ExecuteSaveAndPrintCommand()
        {
            try
            {
                IsBusy = true;
                if (SelectedCrossingDb == null)
                    return;

                var SelectedInventoryDb = await _inventoryLocalRepository.GetInventoryByGuidAsync(SelectedCrossingDb.FemaleParentGuid);

                if (ConsiderDateRange)
                {
                    SelectedCrossingDb.HarvestSummary = HarvestDateRangeDescription;

                    //TODO: Create harvest resume action related to SelectedInventory
                }
                else
                {
                    if (SelectedFruitCount == 0)
                        throw new Exception("La cantidad de frutos está vacía");

                    if (SelectedFlowersCount > 0 && SelectedFruitCount > SelectedFlowersCount)
                        throw new Exception("La cantidad de frutos debe ser menor igual a la cantidad de flores polinizadas");

                    SelectedCrossingDb.HarvestPollinatedFlowers = SelectedFlowersCount;
                    SelectedCrossingDb.HarvestedFruits = SelectedFruitCount;
                    SelectedCrossingDb.HarvestDate = HarvestDate;
                    SelectedCrossingDb.HarvestCooperatorId = Settings.UserCooperatorId;
                    SelectedCrossingDb.HarvestCooperator = Settings.Username;
                    //SelectedCrossingDb.HarvestPrintedDate = DateTime.Now;

                    var utcNow = DateTime.UtcNow;
                    var fruitHarvestAction = new InventoryActionDb
                    {
                        InventoryActionId = -1,
                        Guid = Guid.NewGuid().ToString(),

                        InventoryGuid = SelectedCrossingDb.Guid,
                        InventoryId = SelectedCrossingDb.InventoryId,

                        ActionNameCode = "FRUIT-HARVEST",
                        ActionName = "Cosecha de frutos",
                        ActionDate = HarvestDate,
                        ActionDateCode = "MM/dd/yyyy",
                        CooperatorId = Settings.UserCooperatorId,
                        CooperatorName = Settings.Username,
                        FormCode = "BE",
                        Form = "Baya",
                        Quantity = SelectedFruitCount,
                        QuantityUnit = "Conteo",
                        QuantityUnitCode = "ct",

                        OwnedBy = Settings.WorkgroupCooperatorId,
                        OwnedDate = utcNow,
                        ModifiedBy = Settings.UserCooperatorId,
                        ModifiedDate = utcNow,
                        CreatedBy = Settings.UserCooperatorId,
                        CreatedDate = utcNow,
                    };
                    await _inventoryActionLocalRepository.InsertAsync(fruitHarvestAction);

                    SelectedCrossingDb.HarvestedFruits = SelectedFruitCount;
                    SelectedCrossingDb.HarvestDate = HarvestDate;
                }

                await PageDialogService.DisplayAlertAsync("Resultado de guardar", "Se registró la cosecha correctamente", "OK");

                SelectedInventoryDb.Extensions.Clear();
                SelectedInventoryDb.Extensions.Add("CrossingGuid", SelectedCrossingDb.Guid);
                SelectedInventoryDb.Extensions.Add("CrossingType", SelectedCrossingDb.CrossingType);
                SelectedInventoryDb.Extensions.Add("HarvestFlowerCount", SelectedFlowersCount.ToString());
                SelectedInventoryDb.Extensions.Add("HarvestFruitCount", SelectedFruitCount.ToString());
                await NavigationService.NavigateAsync("LocalPrintPage", new NavigationParameters
                {
                    { "SelectedInventoryDbList", new List<InventoryDb>{ SelectedInventoryDb} }
                }, false, true);

                SelectedCrossingDb = null;
                SelectedFlowersCount = -1;
                SelectedFruitCount = -1;
                HarvestDateRangeDescription = string.Empty;
                CrossingNameWithOrder = string.Empty;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public override async void Initialize(INavigationParameters parameters)
        {
            try
            {
                IsBusy = true;

                if (Settings.SelectedRegenerationOrderRequestId == -1)
                    throw new Exception("Seleccionar solicitud en Ajustes de regeneración");

                SelectedOrderRequestDb = await _orderRequestRepository.GetOrderRequestById(Settings.SelectedRegenerationOrderRequestId);
                if (SelectedOrderRequestDb == null)
                    throw new Exception("Solicitud no encontrada\nIr a Ajustes de regeneración");

                CrossingDbList = await _crossingLocalRepository.GetCrossingByOrderRequestIdAsync(SelectedOrderRequestDb.OrderRequestId);
                CrossingDbListCount = CrossingDbList.Count();

                IsInitialized = true;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
