﻿using InventoryApp.Extensions;
using InventoryApp.Interfaces;
using InventoryApp.Models.LocalStorage;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryApp.ViewModels
{
    public class LocalInventoryActionsPageViewModel : ViewModelBaseWithDialog
    {
        private readonly IInventoryActionLocalRepository _inventoryActionLocalRepository;
        private InventoryDb _inventoryDb;

        private ObservableCollection<InventoryActionDb> _inventoryActionDbList;
        public ObservableCollection<InventoryActionDb> InventoryActionDbList
        {
            get { return _inventoryActionDbList; }
            set { SetProperty(ref _inventoryActionDbList, value); }
        }
        private InventoryActionDb _selectedInventoryActionDb;
        public InventoryActionDb SelectedInventoryActionDb
        {
            get { return _selectedInventoryActionDb; }
            set { SetProperty(ref _selectedInventoryActionDb, value); }
        }
        public LocalInventoryActionsPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService,
            IInventoryActionLocalRepository inventoryActionLocalRepository)
            : base(navigationService, pageDialogService)
        {
            _inventoryActionLocalRepository = inventoryActionLocalRepository;

            AddInventoryActionCommand = new DelegateCommand(ExecuteAddInventoryActionCommand).ObservesCanExecute(() => IsNotBusy);

            InventoryActionDbList = new ObservableCollection<InventoryActionDb>();
        }
        public DelegateCommand AddInventoryActionCommand { get; }
        private async void ExecuteAddInventoryActionCommand()
        {
            try
            {
                IsBusy = true;

                var navResult = await NavigationService.NavigateAsync("AddInventoryActionLocalPage", new NavigationParameters
                {
                    {"InventoryDb", _inventoryDb }
                });
                if (!navResult.Success)
                    throw navResult.Exception;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        private async Task LoadInventoryActionList()
        {
            var tempInventoryActionList = await _inventoryActionLocalRepository.GetByInventoryGuidAsync(_inventoryDb.Guid);
            InventoryActionDbList = new ObservableCollection<InventoryActionDb>(tempInventoryActionList);
        }
        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            try
            {
                IsBusy = true;

                if (parameters.ContainsKey("InventoryDb"))
                {
                    _inventoryDb = parameters.GetValue<InventoryDb>("InventoryDb");
                    await LoadInventoryActionList();
                }
                if (parameters.ContainsKey("Refresh"))
                {
                    await LoadInventoryActionList();
                }
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
