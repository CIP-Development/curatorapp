﻿using InventoryApp.Extensions;
using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using InventoryApp.Models.LocalStorage;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryApp.ViewModels
{
    public class RegenerationMonitoringPageViewModel : ViewModelBaseWithDialog
    {
        private readonly IOrderRequestRepository _orderRequestRepository;
        private readonly IInventoryLocalRepository _inventoryLocalRepository;
        private readonly ICropTraitObservationLocalRepository _cropTraitObservationLocalRepository;
        private readonly IAccessionInvAttachLocalRepository _accessionInvAttachLocalRepository;
        private readonly IInventoryActionLocalRepository _inventoryActionLocalRepository;
        
        private readonly IDialogService _dialogService;
        private IEnumerable<CropTraitObservationDb> _cropTraitObservationDbList;
        public CropTraitObservation CropTraitObservationRegStatus { get; set; }
        public CropTraitObservation CropTraitObservationRegNotes { get; set; }
        private bool _IsMultipleAndHasItemsSelected;
        public bool IsMultipleAndHasItemsSelected
        {
            get { return _IsMultipleAndHasItemsSelected; }
            set { SetProperty(ref _IsMultipleAndHasItemsSelected, value); }
        }

        #region Properties
        private OrderRequestDb _selectedOrderRequestDb;
        public OrderRequestDb SelectedOrderRequestDb
        {
            get { return _selectedOrderRequestDb; }
            set { SetProperty(ref _selectedOrderRequestDb, value); }
        }
        private IEnumerable<InventoryDb> _InventoryDbList;
        public IEnumerable<InventoryDb> InventoryDbList
        {
            get { return _InventoryDbList; }
            set { SetProperty(ref _InventoryDbList, value); }
        }
        private InventoryDb _selectedInventoryDb;
        public InventoryDb SelectedInventoryDb
        {
            get { return _selectedInventoryDb; }
            set { SetProperty(ref _selectedInventoryDb, value); }
        }
        private ObservableCollection<InventoryDbWithTraits> _inventoryDbWithTraitsList;
        public ObservableCollection<InventoryDbWithTraits> InventoryDbWithTraitsList
        {
            get { return _inventoryDbWithTraitsList; }
            set { SetProperty(ref _inventoryDbWithTraitsList, value); }
        }
        private InventoryDbWithTraits _selectedInventoryDbWithTraits;
        public InventoryDbWithTraits SelectedInventoryDbWithTraits
        {
            get { return _selectedInventoryDbWithTraits; }
            set { SetProperty(ref _selectedInventoryDbWithTraits, value); }
        }
        private int _inventoryDbListCount;
        public int InventoryDbListCount
        {
            get { return _inventoryDbListCount; }
            set { SetProperty(ref _inventoryDbListCount, value); }
        }
        private string _searchText;
        public string SearchText
        {
            get { return _searchText; }
            set { SetProperty(ref _searchText, value); }
        }

        private ObservableCollection<object> _selectedInventoryDbWithTraitsList;
        public ObservableCollection<object> SelectedInventoryDbWithTraitsList
        {
            get { return _selectedInventoryDbWithTraitsList; }
            set { SetProperty(ref _selectedInventoryDbWithTraitsList, value); }
        }
        #endregion

        private Syncfusion.SfDataGrid.XForms.SelectionMode _selectionMode;
        public Syncfusion.SfDataGrid.XForms.SelectionMode SelectionMode
        {
            get { return _selectionMode; }
            set { SetProperty(ref _selectionMode, value); }
        }
        private bool _isMultipleSelectionMode;
        public bool IsMultipleSelectionMode
        {
            get { return _isMultipleSelectionMode; }
            set { SetProperty(ref _isMultipleSelectionMode, value); }
        }
        public RegenerationMonitoringPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService,
            IOrderRequestRepository orderRequestRepository, IInventoryLocalRepository inventoryLocalRepository,
            ICropTraitObservationLocalRepository cropTraitObservationLocalRepository, IAccessionInvAttachLocalRepository accessionInvAttachLocalRepository,
            IInventoryActionLocalRepository inventoryActionLocalRepository, IDialogService dialogService)
            : base(navigationService, pageDialogService)
        {
            _orderRequestRepository = orderRequestRepository;
            _inventoryLocalRepository = inventoryLocalRepository;
            _cropTraitObservationLocalRepository = cropTraitObservationLocalRepository;
            _accessionInvAttachLocalRepository = accessionInvAttachLocalRepository;
            _inventoryActionLocalRepository = inventoryActionLocalRepository;
            _dialogService = dialogService;

            SelectionMode = Syncfusion.SfDataGrid.XForms.SelectionMode.Single;
            SelectedInventoryDbWithTraitsList = new ObservableCollection<object>();
            //SelectedInventoryDbWithTraits = null;
            IsMultipleSelectionMode = false;

            SearchCommand = new DelegateCommand(ExecuteSearchCommand).ObservesCanExecute(() => IsNotBusy);
            EvaluateCommand = new DelegateCommand(ExecuteEvaluateCommand).ObservesCanExecute(() => IsNotBusy);
            EditCommand = new DelegateCommand(ExecuteEditCommand).ObservesCanExecute(() => IsNotBusy);
            PrintCommand = new DelegateCommand(ExecutePrintCommand).ObservesCanExecute(() => IsNotBusy);
            AddImageCommand = new DelegateCommand(ExecuteAddImageCommand).ObservesCanExecute(() => IsNotBusy);
            AddInventoryActionCommand = new DelegateCommand(ExecuteAddInventoryActionCommand).ObservesCanExecute(() => IsNotBusy);
            ShowImagesCommand = new DelegateCommand(ExecuteShowImagesCommand).ObservesCanExecute(() => IsNotBusy);
            ShowInventoryActionsCommand = new DelegateCommand(ExecuteShowInventoryActionsCommand).ObservesCanExecute(() => IsNotBusy);

            ChangeSelectionModeCommand = new DelegateCommand(ExecuteChangeSelectionModeCommand).ObservesCanExecute(() => IsNotBusy);
            InventoryDbSelectedCommand = new DelegateCommand(ExecuteInventoryDbSelectedCommand).ObservesCanExecute(() => IsNotBusy);
            RefreshCommand = new DelegateCommand(ExecuteRefreshCommand).ObservesCanExecute(() => IsNotBusy);
        }

        public DelegateCommand SearchCommand { get; }
        private async void ExecuteSearchCommand()
        {
            try
            {
                IsBusy = true;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand EvaluateCommand { get; }
        private async void ExecuteEvaluateCommand()
        {
            try
            {
                IsBusy = true;

                if (SelectedInventoryDbWithTraits == null)
                {
                    SelectedInventoryDbWithTraits = InventoryDbWithTraitsList.First();
                }

                IDialogParameters dialogParameters = new DialogParameters()
                {
                    {"SelectedInventory", SelectedInventoryDbWithTraits },
                };
                var dialogResult = await _dialogService.ShowDialogAsync("AddOrEditRegenerationDescriptorsDialog", dialogParameters);
                if (dialogResult.Parameters.ContainsKey(""))
                {

                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand EditCommand { get; }
        private async void ExecuteEditCommand()
        {
            try
            {
                IsBusy = true;

                if (SelectedInventoryDbWithTraits == null)
                    return;

                var navigationResult = await NavigationService.NavigateAsync("EditRegenerationInventoryPage", new NavigationParameters
                {
                    {"SelectedInventory", SelectedInventoryDbWithTraits.InventoryDb }
                });
                if (!navigationResult.Success)
                    throw navigationResult.Exception;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public DelegateCommand PrintCommand { get; }
        private async void ExecutePrintCommand()
        {
            try
            {
                IsBusy = true;

                if(!SelectedInventoryDbWithTraitsList.Any())
                    return;

                var selectedInventoryDbs = SelectedInventoryDbWithTraitsList.Cast<InventoryDbWithTraits>().Select(x => x.InventoryDb).ToArray();
                var navigationResult = await NavigationService.NavigateAsync("LocalPrintPage",
                    new NavigationParameters
                    {
                        { "SelectedInventoryDbList", selectedInventoryDbs }
                    });
                if (!navigationResult.Success)
                    throw navigationResult.Exception;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand AddImageCommand { get; }
        private async void ExecuteAddImageCommand()
        {
            try
            {
                IsBusy = true;

                string PhotoPath;
                var photo = await Xamarin.Essentials.MediaPicker.PickPhotoAsync();

                // canceled
                if (photo == null)
                {
                    PhotoPath = null;
                    return;
                }
                // save the file into local storage
                var newFile = Path.Combine(Xamarin.Essentials.FileSystem.CacheDirectory, photo.FileName);
                using (var stream = await photo.OpenReadAsync())
                using (var newStream = File.OpenWrite(newFile))
                    await stream.CopyToAsync(newStream);

                PhotoPath = newFile;

                var dbResult = await _accessionInvAttachLocalRepository.InsertAsync(
                    new AccessionInvAttachDb
                    {
                        AccessionInvAttachId = -1,
                        AttachCooperatorId = Settings.UserCooperatorId,
                        AttachDate = DateTime.Now,
                        AttachDateCode = "MM/dd/yyyy",
                        CategoryCode = "IMAGE",
                        CreatedBy = Settings.UserCooperatorId,
                        CreatedDate = DateTime.Now,
                        Guid = Guid.NewGuid().ToString(),
                        InventoryGuid = SelectedInventoryDbWithTraits.InventoryDb.Guid,
                        InventoryId = SelectedInventoryDbWithTraits.InventoryDb.InventoryId,
                        IsWebVisible = "N",
                        ModifiedBy = Settings.UserCooperatorId,
                        ModifiedDate = DateTime.Now,
                        Note = string.Empty,
                        OwnedBy = Settings.WorkgroupCooperatorId,
                        ThumbnailVirtualPath = "",
                        Title = photo.FileName,
                        VirtualPath = PhotoPath,
                    });
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand ShowImagesCommand { get; }
        private async void ExecuteShowImagesCommand()
        {
            try
            {
                IsBusy = true;

                var navigationResult = await NavigationService.NavigateAsync("AccessionInvAttachImagesPage",
                    new NavigationParameters
                    {
                        { "InventoryDb" , SelectedInventoryDbWithTraits.InventoryDb}
                    });
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand ChangeSelectionModeCommand { get; }
        private void ExecuteChangeSelectionModeCommand()
        {
            try
            {
                IsBusy = true;

                if (SelectionMode == Syncfusion.SfDataGrid.XForms.SelectionMode.Single)
                {
                    SelectionMode = Syncfusion.SfDataGrid.XForms.SelectionMode.Multiple;
                    IsMultipleSelectionMode = true;
                }
                else
                {
                    SelectionMode = Syncfusion.SfDataGrid.XForms.SelectionMode.Single;
                    IsMultipleSelectionMode = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand ShowInventoryActionsCommand { get; }
        private async void ExecuteShowInventoryActionsCommand()
        {
            try
            {
                IsBusy = true;

                var navResult = await NavigationService.NavigateAsync("LocalInventoryActionsPage",
                    new NavigationParameters
                    {
                        { "InventoryDb" , SelectedInventoryDbWithTraits.InventoryDb}
                    });
                if (!navResult.Success)
                    throw navResult.Exception;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand RefreshCommand { get; }
        private async void ExecuteRefreshCommand()
        {
            try
            {
                IsBusy = true;

                await LoadInventoryItemList();
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand AddInventoryActionCommand { get; }
        private async void ExecuteAddInventoryActionCommand()
        {
            try
            {
                IsBusy = true;

                if (!SelectedInventoryDbWithTraitsList.Any())
                    return;

                var selectedInventoryDbs = SelectedInventoryDbWithTraitsList.Cast<InventoryDbWithTraits>().Select(x => x.InventoryDb).ToArray();
                var navigationResult = await NavigationService.NavigateAsync("AddInventoryActionLocalPage",
                    new NavigationParameters
                    {
                        { "SelectedInventoryDbList", selectedInventoryDbs }
                    });
                if (!navigationResult.Success)
                    throw navigationResult.Exception;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand InventoryDbSelectedCommand { get; }
        private async void ExecuteInventoryDbSelectedCommand()
        {
            try
            {
                IsBusy = true;

                IsMultipleAndHasItemsSelected = IsMultipleSelectionMode && _selectedInventoryDbWithTraitsList.Any();
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        private async Task LoadInventoryItemList()
        {
            InventoryDbList = await _inventoryLocalRepository.GetInventoriesByOrderRequestIdAsync(SelectedOrderRequestDb.OrderRequestId);

            var tempInventoryDbWithTraitsList = InventoryDbList.Select(x => new InventoryDbWithTraits
            {
                InventoryDb = x,
                CropTraitObservations = new Dictionary<int, CropTraitObservation>
                {
                    { 2011, new CropTraitObservation(){ DisplayText = ""} },
                    { 2013, new CropTraitObservation(){ DisplayText = ""} }
                }
            }).ToArray();

            var inventoryGuids = InventoryDbList.Select(x => x.Guid).ToArray();
            _cropTraitObservationDbList = await _cropTraitObservationLocalRepository.GetCropTraitObservationByManyInventoryGuidAsync(inventoryGuids);
            var inventoryAttachmentsCounts = await _accessionInvAttachLocalRepository.GetCountsByManyInventoryGuidAsync(inventoryGuids);
            var inventoryActionsCounts = await _inventoryActionLocalRepository.GetCountsByManyInventoryGuidAsync(inventoryGuids);

            foreach (var inventorythumbnail in tempInventoryDbWithTraitsList)
            {
                //Load descriptors
                var tempcroptraitobservationregstatus = _cropTraitObservationDbList
                    .FirstOrDefault(x => x.InventoryGuid.Equals(inventorythumbnail.InventoryDb.Guid) && x.CropTraitId == 2011);
                if (tempcroptraitobservationregstatus != null)
                {
                    inventorythumbnail.CropTraitObservations[2011].DisplayText = tempcroptraitobservationregstatus.DisplayText;
                    inventorythumbnail.CropTraitObservations[2011].crop_trait_code_id = tempcroptraitobservationregstatus.CropTraitCodeId;
                    inventorythumbnail.CropTraitObservations[2011].Guid = tempcroptraitobservationregstatus.Guid;
                }
                var tempcroptraitobservationregnotes = _cropTraitObservationDbList
                    .FirstOrDefault(x => x.InventoryGuid.Equals(inventorythumbnail.InventoryDb.Guid) && x.CropTraitId == 2013);
                if (tempcroptraitobservationregnotes != null)
                {
                    inventorythumbnail.CropTraitObservations[2013].DisplayText = tempcroptraitobservationregnotes.StringValue;
                    inventorythumbnail.CropTraitObservations[2013].string_value = tempcroptraitobservationregnotes.StringValue;
                    inventorythumbnail.CropTraitObservations[2013].Guid = tempcroptraitobservationregstatus.Guid;
                }

                //Load attachments count
                var inventoryAttatchCount = inventoryAttachmentsCounts.FirstOrDefault(x => x.InventoryGuid.Equals(inventorythumbnail.InventoryDb.Guid));
                inventorythumbnail.InventoryImageCount = inventoryAttatchCount == null ? 0 : inventoryAttatchCount.Count;
                //Load actions count
                var inventoryActionCount = inventoryActionsCounts.FirstOrDefault(x => x.InventoryGuid.Equals(inventorythumbnail.InventoryDb.Guid));
                inventorythumbnail.InventoryActionCount = inventoryActionCount == null ? 0 : inventoryActionCount.Count;
            }

            InventoryDbWithTraitsList = new ObservableCollection<InventoryDbWithTraits>(tempInventoryDbWithTraitsList);
        }
        public override async void Initialize(INavigationParameters parameters)
        {
            try
            {
                IsBusy = true;

                var orderRequestList = await _orderRequestRepository.GetOrderRequestsAsync();

                if (Settings.SelectedRegenerationOrderRequestId == -1)
                    throw new Exception("Seleccionar solicitud en Ajustes de regeneración");

                SelectedOrderRequestDb = orderRequestList.FirstOrDefault(x => x.OrderRequestId == Settings.SelectedRegenerationOrderRequestId);

                await LoadInventoryItemList();

                IsInitialized = true;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            if (!IsInitialized)
                return;
            try
            {
                IsBusy = true;

                if (parameters.ContainsKey("ModifiedInventoryDb"))
                {
                    var modifiedInventoryDb = parameters.GetValue<InventoryDb>("ModifiedInventoryDb");

                    var inventoryDbWithTraits = InventoryDbWithTraitsList.FirstOrDefault(x => x.InventoryDb.Guid.Equals(modifiedInventoryDb.Guid));
                    if (inventoryDbWithTraits != null)
                    {
                        inventoryDbWithTraits.InventoryDb = null; //To refresh datagrid row
                        inventoryDbWithTraits.InventoryDb = modifiedInventoryDb;
                    }
                    else
                        throw new Exception($"No se ha encontrado el inventario con Guid:{modifiedInventoryDb.Guid}");
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
