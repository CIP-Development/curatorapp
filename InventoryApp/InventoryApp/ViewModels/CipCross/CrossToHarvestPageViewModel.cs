﻿using InventoryApp.Extensions;
using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using InventoryApp.Models.Database;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryApp.ViewModels
{
    public class CrossToHarvestPageViewModel : ViewModelBaseWithDialog
    {
        private readonly IRestService _restService;
        private readonly IOrderRequestRepository _orderRequestRepository;
        private readonly IInventoryLocalRepository _inventoryLocalRepository;
        private readonly ICropTraitObservationLocalRepository _cropTraitObservationLocalRepository;
        private readonly IInventoryActionLocalRepository _inventoryActionLocalRepository;
        private readonly IAccessionInvAttachLocalRepository _accessionInvAttachLocalRepository;
        private readonly ICrossingLocalRepository _crossingLocalRepository;
        private readonly ICodeValueLookupLocalRepository _codeValueLookupLocalRepository;

        private IEnumerable<OrderRequest> _orderRequestList;
        public IEnumerable<OrderRequest> OrderRequestList
        {
            get { return _orderRequestList; }
            set { SetProperty(ref _orderRequestList, value); }
        }
        private OrderRequest _selectedOrderRequest;
        public OrderRequest SelectedOrderRequest
        {
            get { return _selectedOrderRequest; }
            set { SetProperty(ref _selectedOrderRequest, value); }
        }
        public IEnumerable<string> LocationList => CodeValueFactory.RegenerationLocationList;
        private string _selectedLocation;
        public string SelectedLocation
        {
            get { return _selectedLocation; }
            set
            {
                SetProperty(ref _selectedLocation, value);
                if(value != null && !value.Equals(Settings.SelectedRegenerationLocation))
                    Settings.SelectedRegenerationLocation = value;
            }
        }
        public bool IsHarvestGeneral
        { 
            get { return Settings.IsHarvestGeneral; }
            set { Settings.IsHarvestGeneral = value; }
        }
        public string BluetoothPrinterMac
        {
            get { return Settings.BluetoothPrinterMac; }
            set
            {
                Settings.BluetoothPrinterMac = value;
                RaisePropertyChanged(nameof(BluetoothPrinterMac));
            }
        }
        private IEnumerable<CodeValue> _regenerationLocalityCodeList;
        public IEnumerable<CodeValue> RegenerationLocalityCodeList
        {
            get { return _regenerationLocalityCodeList; }
            set { SetProperty(ref _regenerationLocalityCodeList, value); }
        }
        private CodeValue _selectedRegenerationLocalityCode;
        public CodeValue SelectedRegenerationLocalityCode
        {
            get { return _selectedRegenerationLocalityCode; }
            set
            {
                SetProperty(ref _selectedRegenerationLocalityCode, value);
                if (value != null && !value.Code.Equals(Settings.SelectedRegenerationLocation))
                    Settings.SelectedRegenerationLocation = value.Code;
            }
        }

        public CrossToHarvestPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService,
            IRestService restService, IOrderRequestRepository orderRequestRepository, IInventoryLocalRepository inventoryLocalRepository,
            ICropTraitObservationLocalRepository cropTraitObservationLocalRepository, IInventoryActionLocalRepository inventoryActionLocalRepository,
            IAccessionInvAttachLocalRepository accessionInvAttachLocalRepository, ICodeValueLookupLocalRepository codeValueLookupLocalRepository,
            ICrossingLocalRepository crossingLocalRepository)
            : base(navigationService, pageDialogService)
        {
            _restService = restService;
            _orderRequestRepository = orderRequestRepository;
            _inventoryLocalRepository = inventoryLocalRepository;
            _cropTraitObservationLocalRepository = cropTraitObservationLocalRepository;
            _inventoryActionLocalRepository = inventoryActionLocalRepository;
            _accessionInvAttachLocalRepository = accessionInvAttachLocalRepository;
            _codeValueLookupLocalRepository = codeValueLookupLocalRepository;
            _crossingLocalRepository = crossingLocalRepository;

            SelectedOrderRequestChangedCommand = new DelegateCommand(ExecuteSelectedOrderRequestChangedCommand).ObservesCanExecute(() => IsNotBusy);
            DeleteOrderRequestCommand = new DelegateCommand(ExecuteDeleteOrderRequestCommand).ObservesCanExecute(() => IsNotBusy);
            SyncRegenerationOrderRequestCommand = new DelegateCommand(ExecuteSyncRegenerationOrderRequestCommand).ObservesCanExecute(() => IsNotBusy);
            ExploreOrderRequestCommand = new DelegateCommand(ExecuteExploreOrderRequestCommand).ObservesCanExecute(() => IsNotBusy);

            SetLocalPrinterCommand = new DelegateCommand(ExecuteSetLocalPrinterCommand).ObservesCanExecute(() => IsNotBusy);
        }
        public DelegateCommand SelectedOrderRequestChangedCommand { get; }
        private async void ExecuteSelectedOrderRequestChangedCommand()
        {
            if (!IsInitialized)
                return;

            try
            {
                IsBusy = true;

                if (SelectedOrderRequest == null)
                    return;

                Settings.SelectedRegenerationOrderRequestId = SelectedOrderRequest.order_request_id;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand DeleteOrderRequestCommand { get;}
        private async void ExecuteDeleteOrderRequestCommand()
        {
            try
            {
                IsBusy = true;

                if (SelectedOrderRequest == null)
                    return;

                _ = await _crossingLocalRepository.DeleteAllAsync();
                _ = await _cropTraitObservationLocalRepository.DeteleAllCropTraitObservationAsync();
                _ = await _inventoryLocalRepository.DeleteInventoryByOrderRequestIdAsync(SelectedOrderRequest.order_request_id);
                _ = await _orderRequestRepository.DeleteOrderRequestByIdAsync(SelectedOrderRequest.order_request_id);

                await LoadOrderRequests();
                Settings.SelectedRegenerationOrderRequestId = -1;
                _selectedOrderRequest = null;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand SyncRegenerationOrderRequestCommand { get; }
        private async void ExecuteSyncRegenerationOrderRequestCommand()
        {
            try
            {
                IsBusy = true;

                var lastInventoryRepositoryModified = await _inventoryLocalRepository.GetLastModifiedDate();
                var lastCropTraitObservationRepositoryModified = await _cropTraitObservationLocalRepository.GetLastModifiedDate();
                var lastInventoryActionRepositoryModified = await _inventoryActionLocalRepository.GetLastModifiedDate();
                var lastInventoryAttachmentRepositoryModified = await _accessionInvAttachLocalRepository.GetLastModifiedDate();

                var datetimeList = new List<DateTime> {lastInventoryRepositoryModified, lastCropTraitObservationRepositoryModified,
                    lastInventoryActionRepositoryModified, lastInventoryAttachmentRepositoryModified};
                var lastModified = datetimeList.Max();

                if (SelectedOrderRequest.last_synced_date == null || lastModified > SelectedOrderRequest.last_synced_date)
                {
                    // Sync inventories
                    var dbInventories = await _inventoryLocalRepository.GetPendingInventoriesByOrderRequestIdAsync(
                        SelectedOrderRequest.order_request_id,
                        SelectedOrderRequest.last_synced_date.GetValueOrDefault());

                    foreach (var dbInventory in dbInventories)
                    {
                        var inventory = new Inventory()
                        {
                            accession_id = dbInventory.AccessionId,
                            availability_status_code = dbInventory.AvailabilityStatusCode,
                            container_code = dbInventory.ContainerCode,
                            created_by = dbInventory.CreatedBy,
                            created_date = dbInventory.CreatedDate,
                            form_type_code = dbInventory.FormTypeCode,
                            inventory_id = dbInventory.InventoryId,
                            inventory_maint_policy_id = dbInventory.InventoryMaintPolicyId,
                            //inventory_number = dbInventory.InventoryNumber,
                            inventory_number_part1 = dbInventory.InventoryNumberPart1,
                            inventory_number_part2 = dbInventory.InventoryNumberPart2,
                            inventory_number_part3 = dbInventory.InventoryNumberPart3,
                            is_auto_deducted = dbInventory.IsAutoDeducted,
                            is_available = dbInventory.IsAvailable,
                            is_distributable = dbInventory.IsDistributable,
                            modified_by = dbInventory.ModifiedBy,
                            modified_date = dbInventory.ModifiedDate,
                            note = dbInventory.Note,
                            quantity_on_hand = dbInventory.QuantityOnHand,
                            quantity_on_hand_unit_code = dbInventory.QuantityOnHandUnitCode,
                            storage_location_part1 = dbInventory.StorageLocationPart1,
                            storage_location_part2 = dbInventory.StorageLocationPart2,
                            storage_location_part3 = dbInventory.StorageLocationPart3,
                            storage_location_part4 = dbInventory.ContainerCode,
                            parent_inventory_id = dbInventory.ParentInventoryId,
                            workgroup_cooperator_id = Settings.WorkgroupCooperatorId,
                            pollination_method_code = dbInventory.PollinationMethodCode,
                            pollination_vector_code = dbInventory.PollinationVectorCode,
                            //order_request_id = dbInventory.OrderRequestId,
                        };

                        if(dbInventory.InventoryId > 0) //Update
                        {
                            //TODO: Download first and check possible conflicts
                            var result = await _restService.UpdateInventoryAsync(inventory);
                        }
                        else //Insert
                        {
                            var newInventoryIdStr = await _restService.CreateInventoryAsync(inventory);

                            var rows1 = await _inventoryLocalRepository.PatchInventoryIdAsync(dbInventory.Guid, int.Parse(newInventoryIdStr));
                            var rows5 = await _inventoryLocalRepository.PatchParentInventoryIdAsync(dbInventory.Guid, int.Parse(newInventoryIdStr));
                            var rows2 = await _cropTraitObservationLocalRepository.PatchInventoryIdAsync(dbInventory.Guid, int.Parse(newInventoryIdStr));
                            var rows3 = await _inventoryActionLocalRepository.PatchInventoryIdAsync(dbInventory.Guid, int.Parse(newInventoryIdStr));
                            var rows4 = await _accessionInvAttachLocalRepository.PatchInventoryIdAsync(dbInventory.Guid, int.Parse(newInventoryIdStr));
                            
                            //update parent inventory id in current inventory list
                            foreach (var dbInventory2 in dbInventories.Where(x => !string.IsNullOrEmpty(x.ParentInventoryGuid) && x.ParentInventoryGuid.Equals(dbInventory.Guid)))
                            {
                                dbInventory2.ParentInventoryId = int.Parse(newInventoryIdStr);
                            }
                        }
                    }

                    // Sync crop trait observations
                    var dbCropTraitObservations = await _cropTraitObservationLocalRepository.GetPendingCropTraitObservationByOrderRequestIdAsync(
                        SelectedOrderRequest.order_request_id,
                        SelectedOrderRequest.last_synced_date.GetValueOrDefault());

                    foreach (var dbCropTraitObservation in dbCropTraitObservations)
                    {
                        var cropTraitObservation = new CropTraitObservationInsertDto
                        {
                            crop_trait_code_id = dbCropTraitObservation.CropTraitCodeId,
                            crop_trait_id = dbCropTraitObservation.CropTraitId,
                            crop_trait_observation_id = dbCropTraitObservation.CropTraitObservationId,
                            inventory_id = dbCropTraitObservation.InventoryId,
                            numeric_value = dbCropTraitObservation.NumericValue,
                            string_value = dbCropTraitObservation.StringValue,
                            created_by = dbCropTraitObservation.CreatedBy,
                            created_date = dbCropTraitObservation.CreatedDate,
                            is_archived = "N",
                            method_id = 1,
                            workgroup_cooperator_id = Settings.WorkgroupCooperatorId,
                        };
                        if(cropTraitObservation.crop_trait_observation_id > 0)
                        {
                            var result = await _restService.UpdateCropTraitObservationAsync(cropTraitObservation);
                        }
                        else
                        {
                            var newCropTraitObservationIdStr = await _restService.CreateCropTraitObservationAsync(cropTraitObservation);
                            var rows1 = await _cropTraitObservationLocalRepository.PatchCropTraitObservationIdAsync(dbCropTraitObservation.Guid, int.Parse(newCropTraitObservationIdStr));
                        }
                    }

                    // Sync inventories actions
                    var pendingInventoryActionDbs = await _inventoryActionLocalRepository.GetPendingByOrderRequestIdAsync(
                        SelectedOrderRequest.order_request_id,
                        SelectedOrderRequest.last_synced_date.GetValueOrDefault());
                    foreach (var dbInventoryAction in pendingInventoryActionDbs)
                    {
                        var inventoryAction = new InventoryAction
                        {
                            inventory_action_id = dbInventoryAction.InventoryActionId,
                            inventory_id = dbInventoryAction.InventoryId,
                            started_date = dbInventoryAction.ActionDate,
                            started_date_code = "MM/dd/yyyy",
                            action_name_code = dbInventoryAction.ActionNameCode,
                            cooperator_id = dbInventoryAction.CooperatorId,
                            form_code = dbInventoryAction.FormCode,
                            method_id = dbInventoryAction.MethodId,
                            note = dbInventoryAction.Note,
                            quantity = dbInventoryAction.Quantity,
                            quantity_unit_code = dbInventoryAction.QuantityUnitCode,
                            owned_by = Settings.WorkgroupCooperatorId,
                        };
                        if (inventoryAction.inventory_action_id > 0)
                        {
                            var result = await _restService.UpdateInventoryActionAsync(inventoryAction);
                        }
                        else
                        {
                            var newInventoryActionStr = await _restService.CreateInventoryActionAsync(inventoryAction);
                            var rows1 = await _inventoryActionLocalRepository.PatchInventoryActionIdAsync(dbInventoryAction.Guid, int.Parse(newInventoryActionStr));
                        }
                    }

                    // Sync inventories images
                    var pendingInventoryAttachmentDbs = await _accessionInvAttachLocalRepository.GetPendingByOrderRequestIdAsync(
                        SelectedOrderRequest.order_request_id,
                        SelectedOrderRequest.last_synced_date.GetValueOrDefault());
                    foreach (var dbAccessionInvAttach in pendingInventoryAttachmentDbs)
                    {
                        //var inventoryAction = new AccessionInvAttach
                        //{
                        //    inventory_action_id = dbInventoryAction.InventoryActionId,
                        //    inventory_id = dbInventoryAction.InventoryId,
                        //    action_date = dbInventoryAction.ActionDate,
                        //    action_name_code = dbInventoryAction.ActionNameCode,
                        //    cooperator_id = dbInventoryAction.CooperatorId,
                        //    form_code = dbInventoryAction.FormCode,
                        //    method_id = dbInventoryAction.MethodId,
                        //    note = dbInventoryAction.Note,
                        //    quantity = dbInventoryAction.Quantity,
                        //    quantity_unit_code = dbInventoryAction.QuantityUnitCode,
                        //    owned_by = Settings.WorkgroupCooperatorId,
                        //};
                        //if (inventoryAction.inventory_action_id > 0)
                        //{
                        //    var result = await _restService.UpdateInventoryActionAsync(inventoryAction);
                        //}
                        //else
                        //{
                        //    var newInventoryActionStr = await _restService.CreateInventoryActionAsync(inventoryAction);
                        //    var rows1 = await _inventoryActionLocalRepository.PatchInventoryActionIdAsync(dbInventoryAction.Guid, int.Parse(newInventoryActionStr));
                        //}
                    }

                    // Update sync date
                    await _orderRequestRepository.PatchLastSyncedDateAsync(SelectedOrderRequest.order_request_id, lastModified);
                    SelectedOrderRequest.last_synced_date = lastModified;

                    //Newtonsoft.Json.JsonConvert.SerializeObject(dbInventories, Newtonsoft.Json.Formatting.Indented),
                    await PageDialogService.DisplayAlertAsync("Resultado de sincronización",
                        $"Se subieron los registros correctamente",
                        "OK");
                }
                else
                {
                    await PageDialogService.DisplayAlertAsync("Mensaje",
                        $"Todos los cambios están sincronizados",
                        "OK");
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        private async Task LoadOrderRequests()
        {
            var orderRequestBds = await _orderRequestRepository.GetOrderRequestsAsync();
            OrderRequestList = orderRequestBds.Select(x => new OrderRequest
            {
                order_request_id = x.OrderRequestId,
                local_number = x.LocalNumber,
                order_type_code = x.OrderTypeCode,
                last_synced_date = x.LastSyncedDate,
            }).ToList();
        }
        public DelegateCommand ExploreOrderRequestCommand { get; }
        private async void ExecuteExploreOrderRequestCommand()
        {
            try
            {
                IsBusy = true;

                var navResult = await NavigationService.NavigateAsync("DatabaseExplorerPage");
                if (!navResult.Success)
                    throw navResult.Exception;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public DelegateCommand SetLocalPrinterCommand { get; }
        private async void ExecuteSetLocalPrinterCommand()
        {
            try
            {
                IsBusy = true;

                var navigationResult = await NavigationService.NavigateAsync("BluetoothPrinterPickerPage");
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public override async void Initialize(INavigationParameters parameters)
        {
            try
            {
                IsBusy = true;

                await LoadOrderRequests();

                if(Settings.SelectedRegenerationOrderRequestId > 0)
                {
                    SelectedOrderRequest = OrderRequestList.FirstOrDefault(x => x.order_request_id == Settings.SelectedRegenerationOrderRequestId);
                }
                RegenerationLocalityCodeList = (await _codeValueLookupLocalRepository.GetByGroupNameAsync("REGENERATION_LOCALITY"))
                    .Select(x => new CodeValue { Code = x.value_member, Value = x.display_member })
                    .ToArray();

                if (!string.IsNullOrEmpty(Settings.SelectedRegenerationLocation))
                {
                    SelectedRegenerationLocalityCode = RegenerationLocalityCodeList.FirstOrDefault(x => x.Code.Equals(Settings.SelectedRegenerationLocation));
                    //SelectedLocation = Settings.SelectedRegenerationLocation;
                }

                IsInitialized = true;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            if (!IsInitialized)
                return;
            try
            {
                IsBusy = true;

                if (parameters.ContainsKey("SelectedPrinter"))
                {
                    BluetoothPrinterMac = parameters.GetValue<string>("SelectedPrinter");
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
