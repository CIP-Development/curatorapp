﻿using InventoryApp.Extensions;
using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using InventoryApp.Models.LocalStorage;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace InventoryApp.ViewModels
{
    public class AddInventoryActionLocalPageViewModel : ViewModelBaseWithDialog
    {
        private readonly IInventoryActionLocalRepository _inventoryActionLocalRepository;
        private readonly ICodeValueLookupLocalRepository _codeValueLookupLocalRepository;
        
        private IEnumerable<InventoryDb> _selectedInventoryDbList;
        
        private InventoryActionDb _currentInventoryActionDb;
        public InventoryActionDb CurrentInventoryActionDb
        {
            get { return _currentInventoryActionDb; }
            set { SetProperty(ref _currentInventoryActionDb, value); }
        }
        private IEnumerable<CodeValue> _inventoryActionList;
        public IEnumerable<CodeValue> InventoryActionList
        {
            get { return _inventoryActionList; }
            set { SetProperty(ref _inventoryActionList, value); }
        }
        private CodeValue _selectedInventoryAction;
        public CodeValue SelectedInventoryAction
        {
            get { return _selectedInventoryAction; }
            set
            {
                SetProperty(ref _selectedInventoryAction, value);
                if (value == null)
                {
                    CurrentInventoryActionDb.ActionNameCode = null;
                    CurrentInventoryActionDb.ActionName = string.Empty;
                }
                else
                {
                    CurrentInventoryActionDb.ActionNameCode = value.Code;
                    CurrentInventoryActionDb.ActionName = value.Value;
                }
            }
        }
        private IEnumerable<CodeValue> _quantityUnitList;
        public IEnumerable<CodeValue> QuantityUnitList
        {
            get { return _quantityUnitList; }
            set { SetProperty(ref _quantityUnitList, value); }
        }
        private CodeValue _selectedQuantityUnit;
        public CodeValue SelectedQuantityUnit
        {
            get { return _selectedQuantityUnit; }
            set
            {
                SetProperty(ref _selectedQuantityUnit, value);
                if (value == null)
                {
                    CurrentInventoryActionDb.QuantityUnitCode = null;
                    CurrentInventoryActionDb.QuantityUnit = string.Empty;
                }
                else
                {
                    CurrentInventoryActionDb.QuantityUnitCode = value.Code;
                    CurrentInventoryActionDb.QuantityUnit = value.Value;
                }
            }
        }
        private IEnumerable<CodeValue> _FormTypeList;
        public IEnumerable<CodeValue> FormTypeList
        {
            get { return _FormTypeList; }
            set { SetProperty(ref _FormTypeList, value); }
        }
        private CodeValue _selectedFormType;
        public CodeValue SelectedFormType
        {
            get { return _selectedFormType; }
            set
            {
                SetProperty(ref _selectedFormType, value);
                if (value == null)
                {
                    CurrentInventoryActionDb.FormCode = null;
                    CurrentInventoryActionDb.Form = string.Empty;
                }
                else
                {
                    CurrentInventoryActionDb.FormCode = value.Code;
                    CurrentInventoryActionDb.Form = value.Value;
                }
            }
        }
        public AddInventoryActionLocalPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService,
            IInventoryActionLocalRepository inventoryActionLocalRepository, ICodeValueLookupLocalRepository codeValueLookupLocalRepository)
            : base(navigationService, pageDialogService)
        {
            _inventoryActionLocalRepository = inventoryActionLocalRepository;
            _codeValueLookupLocalRepository = codeValueLookupLocalRepository;

            SaveCommand = new DelegateCommand(ExecuteSaveCommand).ObservesCanExecute(() => IsNotBusy);
            CurrentInventoryActionDb = new InventoryActionDb
            {
                ActionDate = DateTime.Now,
                ActionDateCode = "MM/dd/yyyy",
            };

            //InventoryActionList = CodeValueFactory.InventoryActionList.Where(x => new List<string> {
            //    "QUAR", "NOGROW", "NOFLOWER", "NOSEED", "PATHOGEN", "PHOTOGRAPH", "OTHER"
            //}.Contains(x.Code)).ToArray();
        }

        public DelegateCommand SaveCommand { get; }
        private async void ExecuteSaveCommand()
        {
            try
            {
                IsBusy = true;

                if (SelectedInventoryAction == null)
                    throw new Exception("Acción está vacía");

                var utcNow = DateTime.UtcNow;
                foreach (var inventoryDb in _selectedInventoryDbList)
                {
                    var newInventoryAction = new InventoryActionDb
                    {
                        InventoryActionId = -1,
                        Guid = Guid.NewGuid().ToString(),
                        InventoryId = inventoryDb.InventoryId,
                        InventoryGuid = inventoryDb.Guid,
                        InventoryNumber = inventoryDb.InventoryNumber,
                        ActionNameCode = SelectedInventoryAction.Code,
                        ActionName = SelectedInventoryAction.Value,
                        ActionDate = CurrentInventoryActionDb.ActionDate,
                        ActionDateCode = "MM/dd/yyyy",
                        Quantity = CurrentInventoryActionDb.Quantity,
                        QuantityUnitCode = SelectedQuantityUnit?.Code,
                        QuantityUnit = SelectedQuantityUnit?.Value,
                        FormCode = SelectedFormType?.Code,
                        Form = SelectedFormType?.Value,
                        MethodId = null,
                        MethodName = string.Empty,
                        Note = CurrentInventoryActionDb.Note,
                        CooperatorId = Settings.UserCooperatorId,
                        CooperatorName = Settings.Username,

                        CreatedBy = Settings.UserCooperatorId,
                        CreatedDate = utcNow,
                        ModifiedBy = Settings.UserCooperatorId,
                        ModifiedDate = utcNow,
                        OwnedBy = Settings.WorkgroupCooperatorId,
                        OwnedDate = utcNow,
                    };

                    await _inventoryActionLocalRepository.InsertAsync(newInventoryAction);
                }

                await PageDialogService.DisplayAlertAsync("Resutado de guardar", "Se registró correctamente", "OK");
                await NavigationService.GoBackAsync(new NavigationParameters
                {
                    { "Refresh", true }
                });
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public override async void Initialize(INavigationParameters parameters)
        {
            try
            {
                IsBusy = true;
                if (parameters.ContainsKey("SelectedInventoryDbList"))
                {
                    _selectedInventoryDbList = parameters.GetValue<IEnumerable<InventoryDb>>("SelectedInventoryDbList");
                }
                else if (parameters.ContainsKey("InventoryDb"))
                {
                    var selectedInventoryDb = parameters.GetValue<InventoryDb>("InventoryDb");
                    _selectedInventoryDbList = new List<InventoryDb> { selectedInventoryDb };
                }
                if (_selectedInventoryDbList == null || !_selectedInventoryDbList.Any())
                {
                    throw new Exception("InventoryDb is required");
                }

                InventoryActionList = (await _codeValueLookupLocalRepository.GetByGroupNameAsync("INVENTORY_ACTION"))
                    .Select(x => new CodeValue{ Code = x.value_member, Value = x.display_member })
                    .ToArray();

                QuantityUnitList = (await _codeValueLookupLocalRepository.GetByGroupNameAsync("UNIT_OF_QUANTITY"))
                    .Select(x => new CodeValue{ Code = x.value_member, Value = x.display_member })
                    .ToArray();

                FormTypeList = (await _codeValueLookupLocalRepository.GetByGroupNameAsync("GERMPLASM_FORM"))
                    .Select(x => new CodeValue { Code = x.value_member, Value = x.display_member })
                    .ToArray();

                IsInitialized = true;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}