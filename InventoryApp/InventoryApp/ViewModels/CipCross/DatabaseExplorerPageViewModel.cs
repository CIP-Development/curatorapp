﻿using InventoryApp.Extensions;
using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Models.LocalStorage;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InventoryApp.ViewModels
{
    public class DatabaseExplorerPageViewModel : ViewModelBaseWithDialog
    {
        IInventoryLocalRepository _inventoryLocalRepository;
        ICropTraitObservationLocalRepository _cropTraitObservationLocalRepository;
        IAccessionInvAttachLocalRepository _accessionInvAttachLocalRepository;
        IInventoryActionLocalRepository _inventoryActionLocalRepository;

        private IEnumerable<InventoryDb> _inventoryDbList;
        public IEnumerable<InventoryDb> InventoryDbList
        {
            get { return _inventoryDbList; }
            set { SetProperty(ref _inventoryDbList, value); }
        }
        private IEnumerable<CropTraitObservationDb> _cropTraitObservationDbList;
        public IEnumerable<CropTraitObservationDb> CropTraitObservationDbList
        {
            get { return _cropTraitObservationDbList; }
            set { SetProperty(ref _cropTraitObservationDbList, value); }
        }
        private IEnumerable<AccessionInvAttachDb> _accessionInvAttachDbList;
        public IEnumerable<AccessionInvAttachDb> AccessionInvAttachDbList
        {
            get { return _accessionInvAttachDbList; }
            set { SetProperty(ref _accessionInvAttachDbList, value); }
        }
        private IEnumerable<InventoryActionDb> _inventoryActionDbList;
        public IEnumerable<InventoryActionDb> InventoryActionDbList
        {
            get { return _inventoryActionDbList; }
            set { SetProperty(ref _inventoryActionDbList, value); }
        }
        public DatabaseExplorerPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService,
            IInventoryLocalRepository inventoryLocalRepository, ICropTraitObservationLocalRepository cropTraitObservationLocalRepository,
            IAccessionInvAttachLocalRepository accessionInvAttachLocalRepository, IInventoryActionLocalRepository inventoryActionLocalRepository)
            : base(navigationService, pageDialogService)
        {
            _inventoryLocalRepository = inventoryLocalRepository;
            _cropTraitObservationLocalRepository = cropTraitObservationLocalRepository;
            _accessionInvAttachLocalRepository = accessionInvAttachLocalRepository;
            _inventoryActionLocalRepository = inventoryActionLocalRepository;
        }
        public override async void Initialize(INavigationParameters parameters)
        {
            try
            {
                IsBusy = true;

                InventoryDbList = await _inventoryLocalRepository.GetInventoriesByOrderRequestIdAsync(Settings.SelectedRegenerationOrderRequestId);
                //CropTraitObservationDbList = await _cropTraitObservationLocalRepository.GetCropTraitObservationByOrderRequestIdAsync(Settings.SelectedRegenerationOrderRequestId);
                CropTraitObservationDbList = await _cropTraitObservationLocalRepository.GetAllCropTraitObservationAsync();
                AccessionInvAttachDbList = await _accessionInvAttachLocalRepository.GetAllAsync();
                InventoryActionDbList = await _inventoryActionLocalRepository.GetAllAsync();

                IsInitialized = true;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
