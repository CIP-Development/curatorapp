﻿using InventoryApp.Extensions;
using InventoryApp.Interfaces;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Zebra.Sdk.Printer.Discovery;

namespace InventoryApp.ViewModels
{
    public class BluetoothPrinterPickerPageViewModel : ViewModelBaseWithDialog
    {
        private ObservableCollection<DiscoveredPrinter> _discoveredPrinters;
        public ObservableCollection<DiscoveredPrinter> DiscoveredPrinters
        {
            get { return _discoveredPrinters; }
            set { SetProperty(ref _discoveredPrinters, value); }
        }
        private DiscoveredPrinter _selectedPrinter;
        public DiscoveredPrinter SelectedPrinter
        {
            get { return _selectedPrinter; }
            set { SetProperty(ref _selectedPrinter, value); }
        }

        public BluetoothPrinterPickerPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService)
            : base(navigationService, pageDialogService)
        {
            DiscoveredPrinters = new ObservableCollection<DiscoveredPrinter>();

            RefreshCommand = new DelegateCommand(ExecuteRefreshCommand).ObservesCanExecute(() => IsNotBusy);
            AcceptCommand = new DelegateCommand(ExecuteAcceptCommand).ObservesCanExecute(() => IsNotBusy);
        }

        public DelegateCommand RefreshCommand { get; }
        private async void ExecuteRefreshCommand()
        {
            try
            {
                IsBusy = true;

                DiscoveredPrinters.Clear();
                DiscoveryHandlerImplementation discoveryHandler = new DiscoveryHandlerImplementation(this);
                if (Xamarin.Essentials.DeviceInfo.Platform == Xamarin.Essentials.DevicePlatform.UWP)
                {
                    var pairedDevices = await Xamarin.Forms.DependencyService.Get<IConnectionManager>().GetPairedBluetoothDevices();

                    foreach (var device in pairedDevices)
                    {
                        DiscoveredPrinters.Add(device);
                    }
                    IsBusy = false;
                }
                else if (Xamarin.Essentials.DeviceInfo.Platform == Xamarin.Essentials.DevicePlatform.Android)
                {
                    Xamarin.Forms.DependencyService.Get<IConnectionManager>().FindBluetoothPrinters(discoveryHandler);
                }
                else
                    throw new Exception("Bluetooh printing not supported");
                
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
                IsBusy = false;
            }
            finally
            {
                //IsBusy = false;
            }
        }
        public DelegateCommand AcceptCommand { get; }
        private async void ExecuteAcceptCommand()
        {
            try
            {
                IsBusy = true;

                if (SelectedPrinter == null)
                    return;

                _ = await NavigationService.GoBackAsync(new NavigationParameters
                {
                    {"SelectedPrinter", SelectedPrinter.Address }
                });
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        private class DiscoveryHandlerImplementation : DiscoveryHandler
        {

            private BluetoothPrinterPickerPageViewModel discoveryDemoPage;

            public DiscoveryHandlerImplementation(BluetoothPrinterPickerPageViewModel discoveryDemoPage)
            {
                this.discoveryDemoPage = discoveryDemoPage;
            }

            public void DiscoveryError(string message)
            {
                //Xamarin.Forms.Device.BeginInvokeOnMainThread(async () => {
                //    await discoveryDemoPage.DisplayAlert("Discovery Error", message, "OK");
                //});
            }

            public void DiscoveryFinished()
            {
                discoveryDemoPage.IsBusy = false;
                //Device.BeginInvokeOnMainThread(() => {
                //    discoveryDemoPage.SetInputEnabled(true);
                //});
            }

            public void FoundPrinter(DiscoveredPrinter printer)
            {
                discoveryDemoPage.DiscoveredPrinters.Add(printer);
                //Device.BeginInvokeOnMainThread(() => {
                //    discoveryDemoPage.discoveredPrinters.Add(printer);
                //});
            }
        }
    }
}
