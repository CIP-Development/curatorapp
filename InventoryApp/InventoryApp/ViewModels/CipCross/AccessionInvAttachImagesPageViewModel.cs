﻿using InventoryApp.Extensions;
using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Models.LocalStorage;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;

namespace InventoryApp.ViewModels
{
    public class AccessionInvAttachImagesPageViewModel : ViewModelBaseWithDialog
    {
        IAccessionInvAttachLocalRepository _accessionInvAttachLocalRepository;
        private InventoryDb _selectedInventoryDb;

        private ObservableCollection<AccessionInvAttachDb> _accessionInvAttachList;
        public ObservableCollection<AccessionInvAttachDb> AccessionInvAttachList
        {
            get { return _accessionInvAttachList; }
            set { SetProperty(ref _accessionInvAttachList, value); }
        }
        public AccessionInvAttachImagesPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService,
            IAccessionInvAttachLocalRepository accessionInvAttachLocalRepository)
            : base(navigationService, pageDialogService)
        {
            _accessionInvAttachLocalRepository = accessionInvAttachLocalRepository;

            AddImageCommand = new DelegateCommand(ExecuteAddImageCommand).ObservesCanExecute(() => IsNotBusy);
        }

        public DelegateCommand AddImageCommand { get; }
        private async void ExecuteAddImageCommand()
        {
            try
            {
                IsBusy = true;

                string PhotoPath;
                var photo = await Xamarin.Essentials.MediaPicker.PickPhotoAsync();

                // canceled
                if (photo == null)
                {
                    PhotoPath = null;
                    return;
                }
                // save the file into local storage
                var newFile = Path.Combine(Xamarin.Essentials.FileSystem.CacheDirectory, photo.FileName);
                using (var stream = await photo.OpenReadAsync())
                using (var newStream = File.OpenWrite(newFile))
                    await stream.CopyToAsync(newStream);

                PhotoPath = newFile;

                var note = await PageDialogService.DisplayPromptAsync("Agregar imagen", "Notas de la imagen", "OK", "Cancelar");
                if (note == null)
                    return;

                var newImage = new AccessionInvAttachDb
                {
                    AccessionInvAttachId = -1,
                    AttachCooperatorId = Settings.UserCooperatorId,
                    AttachDate = DateTime.Now,
                    AttachDateCode = "MM/dd/yyyy",
                    CategoryCode = "IMAGE",
                    CreatedBy = Settings.UserCooperatorId,
                    CreatedDate = DateTime.Now,
                    Guid = Guid.NewGuid().ToString(),
                    InventoryGuid = _selectedInventoryDb.Guid,
                    InventoryId = _selectedInventoryDb.InventoryId,
                    IsWebVisible = "N",
                    ModifiedBy = Settings.UserCooperatorId,
                    ModifiedDate = DateTime.Now,
                    Note = note,
                    OwnedBy = Settings.WorkgroupCooperatorId,
                    ThumbnailVirtualPath = "",
                    Title = photo.FileName,
                    VirtualPath = PhotoPath,
                };
                var dbResult = await _accessionInvAttachLocalRepository.InsertAsync(newImage);

                if(dbResult > 0)
                {
                    AccessionInvAttachList.Add(newImage);
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            try
            {
                IsBusy = false;

                _selectedInventoryDb = parameters.GetValue<InventoryDb>("InventoryDb");

                var tempAccessionInvAttachList = await _accessionInvAttachLocalRepository.GetByInventoryGuidAsync(_selectedInventoryDb.Guid);
                AccessionInvAttachList = new ObservableCollection<AccessionInvAttachDb>(tempAccessionInvAttachList);
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
