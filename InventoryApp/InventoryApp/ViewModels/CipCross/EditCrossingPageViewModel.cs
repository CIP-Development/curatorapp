﻿using InventoryApp.Extensions;
using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using InventoryApp.Models.LocalStorage;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InventoryApp.ViewModels
{
    public class EditCrossingPageViewModel : ViewModelBaseWithDialog
    {
        private readonly IInventoryLocalRepository _inventoryLocalRepository;
        private readonly ICrossingLocalRepository _crossingLocalRepository;

        private CrossingDb _selectedCrossingDb;
        public CrossingDb SelectedCrossingDb
        {
            get { return _selectedCrossingDb; }
            set { SetProperty(ref _selectedCrossingDb, value); }
        }
        public IEnumerable<CodeValue> CrossingTypeCodeList => CodeValueFactory.CrossingTypeList;
        private CodeValue _selectedCrossingType;
        public CodeValue SelectedCrossingType
        {
            get { return _selectedCrossingType; }
            set
            {
                SetProperty(ref _selectedCrossingType, value);
                if (value != null && value.Code.Equals("SC"))
                {
                    IsSibCrossSelected = true;
                }
                else
                {
                    IsSibCrossSelected = false;
                }
                RaisePropertyChanged(nameof(Order));
            }
        }
        private IEnumerable<int> _FemaleOrderList;
        public IEnumerable<int> FemaleOrderList
        {
            get { return _FemaleOrderList; }
            set { SetProperty(ref _FemaleOrderList, value); }
        }
        private int _selectedFemaleOrder;
        public int SelectedFemaleOrder
        {
            get { return _selectedFemaleOrder; }
            set
            {
                SetProperty(ref _selectedFemaleOrder, value);
                RaisePropertyChanged(nameof(Order));
            }
        }
        private IEnumerable<int> _maleOrderList;
        public IEnumerable<int> MaleOrderList
        {
            get { return _maleOrderList; }
            set { SetProperty(ref _maleOrderList, value); }
        }
        private int _selectedMaleOrder;
        public int SelectedMaleOrder
        {
            get { return _selectedMaleOrder; }
            set
            {
                SetProperty(ref _selectedMaleOrder, value);
                RaisePropertyChanged(nameof(Order));
            }
        }
        public string Order
        {
            get
            {
                string maleOrder = "?";
                if (SelectedCrossingType != null && !_isSibCrossSelected)
                {
                    maleOrder = SelectedCrossingType.Code;
                }
                else if (SelectedCrossingType != null && _isSibCrossSelected && _selectedMaleOrder > 0)
                {
                    maleOrder = _selectedMaleOrder.ToString();
                }
                return $"{(_selectedFemaleOrder > 0 ? _selectedFemaleOrder.ToString() : "?")} - {maleOrder}";
            }
        }
        private IEnumerable<int> _flowersCountList;
        public IEnumerable<int> FlowersCountList
        {
            get { return _flowersCountList; }
            set { SetProperty(ref _flowersCountList, value); }
        }
        private int _selectedFlowersCount;
        public int SelectedFlowersCount
        {
            get { return _selectedFlowersCount; }
            set { SetProperty(ref _selectedFlowersCount, value); }
        }
        private bool _isSibCrossSelected;
        public bool IsSibCrossSelected
        {
            get { return _isSibCrossSelected; }
            set { SetProperty(ref _isSibCrossSelected, value); }
        }
        
        public EditCrossingPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService,
            IInventoryLocalRepository inventoryLocalRepository, ICrossingLocalRepository crossingLocalRepository)
            : base(navigationService, pageDialogService)
        {
            _inventoryLocalRepository = inventoryLocalRepository;
            _crossingLocalRepository = crossingLocalRepository;

            FlowersCountList = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 };

            SaveCommand = new DelegateCommand(ExecuteSaveCommand).ObservesCanExecute(() => IsNotBusy);
        }
        public DelegateCommand SaveCommand { get; }
        private async void ExecuteSaveCommand()
        {
            try
            {
                IsBusy = true;

                SelectedCrossingDb.CrossingType = SelectedCrossingType.Value;
                SelectedCrossingDb.CrossingTypeCode = SelectedCrossingType.Code;
                SelectedCrossingDb.FemaleOrder = SelectedFemaleOrder;
                SelectedCrossingDb.MaleOrder = SelectedMaleOrder;
                SelectedCrossingDb.PollinatedFlowers = SelectedFlowersCount;
                
                SelectedCrossingDb.ModifiedBy = Settings.UserCooperatorId;
                SelectedCrossingDb.ModifiedDate = DateTime.UtcNow;
                //SelectedCrossingDb.CrossingDate = CrossingDate;
                //SelectedCrossingDb.PrintedDate = DateTime.Now;

                var updatedRows = await _crossingLocalRepository.UpdateInventoryAsync(SelectedCrossingDb);
                if (updatedRows > 0)
                {
                    await PageDialogService.DisplayAlertAsync("Resultado de guardar", $"Se actualizó el cruzamiento satisfactoriamente\nGuid : {SelectedCrossingDb.Guid}", "OK");

                    _ = await NavigationService.GoBackAsync(new NavigationParameters
                    {
                        { "Refresh", true }
                    });
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public override async void Initialize(INavigationParameters parameters)
        {
            try
            {
                IsBusy = true;

                if (parameters.ContainsKey("SelectedCrossingDb"))
                {
                    SelectedCrossingDb = parameters.GetValue<CrossingDb>("SelectedCrossingDb");
                    
                    var femaleParentInventoryDb = await _inventoryLocalRepository.GetInventoryByGuidAsync(SelectedCrossingDb.FemaleParentGuid);

                    var tempFemaleOrderList = Enumerable.Range(1, (int)femaleParentInventoryDb.QuantityOnHand).ToArray();
                    //_allOrderRequestInventories
                    //.Where(x => x.InventoryNumberPart2 == SelectedInventoryDb.InventoryNumberPart2 &&
                    //    !string.IsNullOrEmpty(x.InventoryNumberPart3) && x.InventoryNumberPart3.Contains('.'))
                    //.Select(x => x.InventoryNumberPart3.Split('.').Last());
                    FemaleOrderList = tempFemaleOrderList.ToArray();
                    MaleOrderList = tempFemaleOrderList.ToArray();

                    SelectedCrossingType = CrossingTypeCodeList.FirstOrDefault(x => x.Code.Equals(SelectedCrossingDb.CrossingTypeCode));
                    SelectedFemaleOrder = SelectedCrossingDb.FemaleOrder;
                    SelectedMaleOrder = SelectedCrossingDb.MaleOrder;
                    SelectedFlowersCount = SelectedCrossingDb.PollinatedFlowers.GetValueOrDefault(-1);
                }
                else
                    throw new Exception("SelectedCrossingDb is required");
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
