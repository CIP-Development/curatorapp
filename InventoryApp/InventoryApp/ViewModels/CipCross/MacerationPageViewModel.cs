﻿using InventoryApp.Extensions;
using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Models.LocalStorage;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace InventoryApp.ViewModels
{
    public class MacerationPageViewModel : ViewModelBaseWithDialog
    {
        private readonly IOrderRequestRepository _orderRequestRepository;
        private readonly IInventoryLocalRepository _inventoryLocalRepository;
        private readonly ICrossingLocalRepository _crossingLocalRepository;

        private OrderRequestDb _selectedOrderRequestDb;
        public OrderRequestDb SelectedOrderRequestDb
        {
            get { return _selectedOrderRequestDb; }
            set { SetProperty(ref _selectedOrderRequestDb, value); }
        }
        private IEnumerable<CrossingDb> _crossingDbList;
        public IEnumerable<CrossingDb> CrossingDbList
        {
            get { return _crossingDbList; }
            set { SetProperty(ref _crossingDbList, value); }
        }
        private int _crossingDbListCount;
        public int CrossingDbListCount
        {
            get { return _crossingDbListCount; }
            set { SetProperty(ref _crossingDbListCount, value); }
        }
        private CrossingDb _selectedCrossingDb;
        public CrossingDb SelectedCrossingDb
        {
            get { return _selectedCrossingDb; }
            set { SetProperty(ref _selectedCrossingDb, value); }
        }
        private string _SearchText;
        public string SearchText
        {
            get { return _SearchText; }
            set { SetProperty(ref _SearchText, value); }
        }
        private string _MacerationText;
        public string MacerationText
        {
            get { return _MacerationText; }
            set { SetProperty(ref _MacerationText, value); }
        }
        public MacerationPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService,
            IOrderRequestRepository orderRequestRepository, IInventoryLocalRepository inventoryLocalRepository, ICrossingLocalRepository crossingLocalRepository)
            : base(navigationService, pageDialogService)
        {
            _orderRequestRepository = orderRequestRepository;
            _inventoryLocalRepository = inventoryLocalRepository;
            _crossingLocalRepository = crossingLocalRepository;


            SearchCommand = new DelegateCommand(ExecuteSearchCommand).ObservesCanExecute(() => IsNotBusy);
            SelectCommand = new DelegateCommand(ExecuteSelectCommand).ObservesCanExecute(() => IsNotBusy);
        }

        public DelegateCommand SearchCommand { get; }
        private async void ExecuteSearchCommand()
        {
            try
            {
                IsBusy = true;

                var SelectedCrossingDb = CrossingDbList.FirstOrDefault(x => x.Guid.Equals(SearchText));

                if(SelectedCrossingDb != null)
                {
                    if (Settings.IsHarvestGeneral)
                    {
                        var crossings = CrossingDbList.Where(x => x.FemaleAccessionNumber.Equals(SelectedCrossingDb.FemaleAccessionNumber) &&
                        x.CrossingTypeCode != null && x.CrossingTypeCode.Equals(SelectedCrossingDb.CrossingTypeCode) &&
                        x.PollinatedFlowers != null && x.HarvestedFruits != null);
                        //x.HarvestPollinatedFlowers != null && x.HarvestedFruits != null);

                        int totalPollinatedFlowers = (int)crossings.Select(x => x.PollinatedFlowers).Sum();
                        //int totalPollinatedFlowers = (int)crossings.Select(x => x.HarvestPollinatedFlowers).Sum();
                        int totalFruits = (int)crossings.Select(x => x.HarvestedFruits).Sum();
                        MacerationText = $"{SelectedCrossingDb.FemaleAccessionNumber}|{SelectedCrossingDb.CrossingTypeCode}|{totalPollinatedFlowers}|{totalFruits}|{SearchText}|Especie";
                    }
                }
                else
                {
                    await PageDialogService.DisplayAlertAsync("Resultado de búsqueda", "Id no encontrado", "OK");
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public DelegateCommand SelectCommand { get; }
        private async void ExecuteSelectCommand()
        {
            try
            {
                IsBusy = true;

                if (SelectedCrossingDb == null)
                    return;

                MacerationText = SelectedCrossingDb.HarvestSummary;

                //MacerationText = SelectedCrossingDb.HarvestSummary;

                //If HarverstGeneral
                //Get Collector, species, CrossType

                //Obtener TotalFloresPolinizadas, TotalFrutos por Collector-TipoCruzamiento

                //TextBox_Maceration.Text = strColector + "|" + strTipoCruzamiento + "|" + strTotalFloresPolinizadas + "|" + strTotalFrutos + "|" + ID + "|" + strEspecie
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public override async void Initialize(INavigationParameters parameters)
        {
            try
            {
                IsBusy = true;

                if (Settings.SelectedRegenerationOrderRequestId == -1)
                    throw new Exception("Seleccionar solicitud en Ajustes de regeneración");

                SelectedOrderRequestDb = await _orderRequestRepository.GetOrderRequestById(Settings.SelectedRegenerationOrderRequestId);
                if (SelectedOrderRequestDb == null)
                    throw new Exception("Solicitud no encontrada\nIr a Ajustes de regeneración");

                CrossingDbList = await _crossingLocalRepository.GetCrossingByOrderRequestIdAsync(SelectedOrderRequestDb.OrderRequestId);
                CrossingDbListCount = CrossingDbList.Count();

                IsInitialized = true;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayErrorAlertAsync(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
