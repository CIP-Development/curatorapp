﻿using ImTools;
using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using InventoryApp.Models.Database;
using InventoryApp.Services;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;

namespace InventoryApp.ViewModels
{
    public class CreateInventoryViabilityPageViewModel : ViewModelBaseWithDialog
    {
        private RestService _restService;
        private RestClient _restClient;
        private int _discountMethodId = 0;
        IDialogService DialogService { get; }
        private IDataStoreService _dataStoreService;

        #region Properties
        private int _orderRequestId;
        public int OrderRequestId
        {
            get { return _orderRequestId; }
            set { SetProperty(ref _orderRequestId, value); }
        }
        private int _inventoryId;
        public int InventoryId
        {
            get { return _inventoryId; }
            set { SetProperty(ref _inventoryId, value); }
        }
        private List<InventoryViabilityRule> _inventoryViabilityRuleList;
        public List<InventoryViabilityRule> InventoryViabilityRuleList
        {
            get { return _inventoryViabilityRuleList; }
            set { SetProperty(ref _inventoryViabilityRuleList, value); }
        }
        private InventoryViabilityRule _inventoryViabilityRuleI;
        public InventoryViabilityRule InventoryViabilityRule
        {
            get { return _inventoryViabilityRuleI; }
            set { SetProperty(ref _inventoryViabilityRuleI, value); }
        }
        private int _numberOfReplicates;
        public int NumberOfReplicates
        {
            get { return _numberOfReplicates; }
            set { SetProperty(ref _numberOfReplicates, value); }
        }
        private int _totalSeeds;
        public int TotalSeeds
        {
            get { return _totalSeeds; }
            set { SetProperty(ref _totalSeeds, value); }
        }
        private string _substrata;
        public string Substrata
        {
            get { return _substrata; }
            set { SetProperty(ref _substrata, value); }
        }
        private string _moisture;
        public string Moisture
        {
            get { return _moisture; }
            set { SetProperty(ref _moisture, value); }
        }
        private string _prechill;
        public string Prechill
        {
            get { return _prechill; }
            set { SetProperty(ref _prechill, value); }
        }
        private string _temperature;
        public string Temperature
        {
            get { return _temperature; }
            set { SetProperty(ref _temperature, value); }
        }
        private string _lighting;
        public string Lighting
        {
            get { return _lighting; }
            set { SetProperty(ref _lighting, value); }
        }
        private string _viabilityRuleNote;
        public string ViabilityRuleNote
        {
            get { return _viabilityRuleNote; }
            set { SetProperty(ref _viabilityRuleNote, value); }
        }
        private string _taxonomyNote;
        public string TaxonomyNote
        {
            get { return _taxonomyNote; }
            set { SetProperty(ref _taxonomyNote, value); }
        }
        private string _viabilityNote;
        public string ViabilityNote
        {
            get { return _viabilityNote; }
            set { SetProperty(ref _viabilityNote, value); }
        }
        private ObservableCollection<WrappedSelection<ViabilityOrderItem>> _inventoryList;
        public ObservableCollection<WrappedSelection<ViabilityOrderItem>> InventoryList
        {
            get { return _inventoryList; }
            set { SetProperty(ref _inventoryList, value); }
        }
        private ObservableCollection<object> _selectedViabilityOrderItems;
        public ObservableCollection<object> SelectedViabilityOrderItems
        {
            get { return _selectedViabilityOrderItems; }
            set { SetProperty(ref _selectedViabilityOrderItems, value); }
        }
        private bool _isAllSelected;
        public bool IsAllSelected
        {
            get { return _isAllSelected; }
            set
            {
                foreach (var wi in _inventoryList)
                {
                    wi.IsSelected = value;
                }
                if (value)
                    SelectedRowsCount = InventoryList.Count;
                else
                    SelectedRowsCount = 0;
                SetProperty(ref _isAllSelected, value);
            }
        }
        private int _selectedRowsCount;
        public int SelectedRowsCount
        {
            get { return _selectedRowsCount; }
            set { SetProperty(ref _selectedRowsCount, value); }
        }
        private bool _discount;
        public bool Discount
        {
            get { return _discount; }
            set { SetProperty(ref _discount, value); }
        }
        private bool _createAction;
        public bool CreateAction
        {
            get { return _createAction; }
            set { SetProperty(ref _createAction, value); }
        }
        private List<ILookup> _inventoryActionList;
        public List<ILookup> InventoryActionList
        {
            get { return _inventoryActionList; }
            set { SetProperty(ref _inventoryActionList, value); }
        }
        private CodeValueLookup _inventoryAction;
        public CodeValueLookup InventoryAction
        {
            get { return _inventoryAction; }
            set { SetProperty(ref _inventoryAction, value); }
        }
        private bool _isFromInventory;
        public bool IsFromInventory
        {
            get { return _isFromInventory; }
            set { SetProperty(ref _isFromInventory, value); }
        }
        private string _inventoryNumber;
        public string InventoryNumber
        {
            get { return _inventoryNumber; }
            set { SetProperty(ref _inventoryNumber, value); }
        }
        private string _inventoryTaxonName;
        public string InventoryTaxonName
        {
            get { return _inventoryTaxonName; }
            set { SetProperty(ref _inventoryTaxonName, value); }
        }
        #endregion

        #region Lang
        private string _UxTitle;
        public string UxTitle
        {
            get { return _UxTitle; }
            set { SetProperty(ref _UxTitle, value); }
        }
        private string _header1;
        public string Header1
        {
            get { return _header1; }
            set { SetProperty(ref _header1, value); }
        }
        private string _header2;
        public string Header2
        {
            get { return _header2; }
            set { SetProperty(ref _header2, value); }
        }
        private string _header3;
        public string Header3
        {
            get { return _header3; }
            set { SetProperty(ref _header3, value); }
        }
        private string _header4;
        public string Header4
        {
            get { return _header4; }
            set { SetProperty(ref _header4, value); }
        }
        private string _header5;
        public string Header5
        {
            get { return _header5; }
            set { SetProperty(ref _header5, value); }
        }
        private string _header6;
        public string Header6
        {
            get { return _header6; }
            set { SetProperty(ref _header6, value); }
        }
        private string _header7;
        public string Header7
        {
            get { return _header7; }
            set { SetProperty(ref _header7, value); }
        }

        private string _UxToolbarItemSave;
        public string UxToolbarItemSave
        {
            get { return _UxToolbarItemSave; }
            set { SetProperty(ref _UxToolbarItemSave, value); }
        }
        private string _UxToolbarItemPrint;
        public string UxToolbarItemPrint
        {
            get { return _UxToolbarItemPrint; }
            set { SetProperty(ref _UxToolbarItemPrint, value); }
        }
        private string _UxToolbarItemRefresh;
        public string UxToolbarItemRefresh
        {
            get { return _UxToolbarItemRefresh; }
            set { SetProperty(ref _UxToolbarItemRefresh, value); }
        }
        private string _UxTabViabilityTest;
        public string UxTabViabilityTest
        {
            get { return _UxTabViabilityTest; }
            set { SetProperty(ref _UxTabViabilityTest, value); }
        }
        private string _UxLabelViabilityRule;
        public string UxLabelViabilityRule
        {
            get { return _UxLabelViabilityRule; }
            set { SetProperty(ref _UxLabelViabilityRule, value); }
        }
        private string _UxLabelReplicates;
        public string UxLabelReplicates
        {
            get { return _UxLabelReplicates; }
            set { SetProperty(ref _UxLabelReplicates, value); }
        }
        private string _UxLabelTotalSeeds;
        public string UxLabelTotalSeeds
        {
            get { return _UxLabelTotalSeeds; }
            set { SetProperty(ref _UxLabelTotalSeeds, value); }
        }
        private string _UxLabelSubstrata;
        public string UxLabelSubstrata
        {
            get { return _UxLabelSubstrata; }
            set { SetProperty(ref _UxLabelSubstrata, value); }
        }
        private string _UxLabelMoisture;
        public string UxLabelMoisture
        {
            get { return _UxLabelMoisture; }
            set { SetProperty(ref _UxLabelMoisture, value); }
        }
        private string _UxLabelPrechill;
        public string UxLabelPrechill
        {
            get { return _UxLabelPrechill; }
            set { SetProperty(ref _UxLabelPrechill, value); }
        }
        private string _UxLabelTemperature;
        public string UxLabelTemperature
        {
            get { return _UxLabelTemperature; }
            set { SetProperty(ref _UxLabelTemperature, value); }
        }
        private string _UxLabelLighting;
        public string UxLabelLighting
        {
            get { return _UxLabelLighting; }
            set { SetProperty(ref _UxLabelLighting, value); }
        }
        private string _UxLabelRuleNotes;
        public string UxLabelRuleNotes
        {
            get { return _UxLabelRuleNotes; }
            set { SetProperty(ref _UxLabelRuleNotes, value); }
        }
        private string _UxLabelTaxonomyNotes;
        public string UxLabelTaxonomyNotes
        {
            get { return _UxLabelTaxonomyNotes; }
            set { SetProperty(ref _UxLabelTaxonomyNotes, value); }
        }
        private string _UxLabelViabilityTestNotes;
        public string UxLabelViabilityTestNotes
        {
            get { return _UxLabelViabilityTestNotes; }
            set { SetProperty(ref _UxLabelViabilityTestNotes, value); }
        }
        private string _UxLabelDiscountInventories;
        public string UxLabelDiscountInventories
        {
            get { return _UxLabelDiscountInventories; }
            set { SetProperty(ref _UxLabelDiscountInventories, value); }
        }
        private string _UxLabelCreateAction;
        public string UxLabelCreateAction
        {
            get { return _UxLabelCreateAction; }
            set { SetProperty(ref _UxLabelCreateAction, value); }
        }
        private string _UxLabelJustification;
        public string UxLabelJustification
        {
            get { return _UxLabelJustification; }
            set { SetProperty(ref _UxLabelJustification, value); }
        }
        private string _UxTabOrderRequestItems;
        public string UxTabOrderRequestItems
        {
            get { return _UxTabOrderRequestItems; }
            set { SetProperty(ref _UxTabOrderRequestItems, value); }
        }
        private string _UxLabelInventoryId;
        public string UxLabelInventoryId
        {
            get { return _UxLabelInventoryId; }
            set { SetProperty(ref _UxLabelInventoryId, value); }
        }
        private string _UxLabelInventoryNumber;
        public string UxLabelInventoryNumber
        {
            get { return _UxLabelInventoryNumber; }
            set { SetProperty(ref _UxLabelInventoryNumber, value); }
        }
        private string _UxLabelTaxon;
        public string UxLabelTaxon
        {
            get { return _UxLabelTaxon; }
            set { SetProperty(ref _UxLabelTaxon, value); }
        }
        private string _UxLabelOrderRequestId;
        public string UxLabelOrderRequestId
        {
            get { return _UxLabelOrderRequestId; }
            set { SetProperty(ref _UxLabelOrderRequestId, value); }
        }
        #endregion
        public CreateInventoryViabilityPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService, IDialogService dialogService, IDataStoreService dataStoreService) 
            : base(navigationService, pageDialogService)
        {
            DialogService = dialogService;
            _dataStoreService = dataStoreService;
            _restService = new RestService();
            _restClient = new RestClient();

            InventoryList = new ObservableCollection<WrappedSelection<ViabilityOrderItem>>();
            SelectedViabilityOrderItems = new ObservableCollection<object>();

            SaveCommand = new DelegateCommand(OnSaveCommand);
            InventoryViabilityRuleListChangedCommand = new DelegateCommand(OnInventoryViabilityRuleListChangedCommand);
            PrintCommand = new DelegateCommand(OnPrintCommand);
            RefreshCommand = new DelegateCommand(OnRefreshCommand);

            SelectItemCommand = new DelegateCommand<object>(OnSelectItemCommand);

            Header1 = "Inventory Number";
            Header2 = "Accession Number";
            
            Header4 = "Status";
            Header5 = "Tested date";
            Header6 = "% Viable";
            Header7 = "Storage location";
            Header3 = "Quantity";

            Discount = false;
            CreateAction = false;
        }
        private List<ViabilityOrderItem> GetSelection()
        {
            return _inventoryList.Where(item => item.IsSelected).Select(wrappedItem => wrappedItem.Item).ToList();
        }
        public DelegateCommand<Object> SelectItemCommand { get; }
        private async void OnSelectItemCommand(Object param)
        {
            try
            {
                WrappedSelection<ViabilityOrderItem> listItem = (WrappedSelection<ViabilityOrderItem>)param;
                if (listItem != null)
                {
                    listItem.IsSelected = !listItem.IsSelected;

                    if (listItem.IsSelected)
                        SelectedRowsCount++;
                    else
                        SelectedRowsCount--;
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public DelegateCommand SaveCommand { get; }
        private async void OnSaveCommand()
        {
            try
            {
                if (InventoryViabilityRule == null)
                    throw new Exception(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "CreateInventoryViabilityPage", "MessageErrorViabilityRuleIsEmpty"));
                if (Discount && CreateAction)
                {
                    if (InventoryAction == null)
                        throw new Exception(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "CreateInventoryViabilityPage", "MessageErrorInventoryActionIsEmpty"));
                }

                if (_inventoryId > 0) //Create from inventory
                {
                    var now = DateTime.Now;
                    InventoryViability newInventoryViability = new InventoryViability()
                    {
                        InventoryViabilityId = -1,
                        InventoryViabilityRuleId = InventoryViabilityRule.InventoryViabilityRuleId,
                        InventoryViabilityRuleName = InventoryViabilityRule.Name,
                        InventoryId = _inventoryId,
                        ReplicationCount = NumberOfReplicates,
                        TotalTestedCount = TotalSeeds,
                        TestedDateCode = "MM/dd/yyyy",
                        TestedDate = now,
                        Note = ViabilityNote
                    };
                    string result = await _restService.CreateInventoryViabilityAsync(newInventoryViability);

                    if (Discount)
                    {
                        Inventory i = await _restClient.ReadInventory(_inventoryId);
                        i.quantity_on_hand -= newInventoryViability.TotalTestedCount;
                        await _restClient.UpdateInventory(i);

                        if (CreateAction)
                        {
                            InventoryAction ia = new InventoryAction
                            {
                                inventory_action_id = -1,
                                inventory_id = i.inventory_id,
                                action_name_code = InventoryAction.value_member,
                                quantity = newInventoryViability.TotalTestedCount * -1,
                                quantity_unit_code = i.quantity_on_hand_unit_code,
                                started_date = now,
                                method_id = _discountMethodId,
                                cooperator_id = Settings.UserCooperatorId,
                                note = "Inventory viability rule name: " + InventoryViabilityRule.Name
                            };
                            await _restClient.CreateInventoryAction(ia);
                        }
                    }

                    await PageDialogService.DisplayAlertAsync(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "CreateInventoryViabilityPage", "MessageSaveResults"), 
                        string.Format(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "CreateInventoryViabilityPage", "MessageSuccessfullyCreatedFromInventory"), result), "OK");

                    var navigationParams = new NavigationParameters
                    {
                        { "InventoryViabilityId", int.Parse(result) }
                    };
                    await NavigationService.GoBackAsync(navigationParams);
                }
                else //Create from order
                {
                    if (!SelectedViabilityOrderItems.Any())
                        throw new Exception(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "CreateInventoryViabilityPage", "MessageErrorOrderRequestItemIsEmpty"));

                    string inventoryViabilityIds = string.Empty;
                    var now = DateTime.Now;
                    var wrappedItems = SelectedViabilityOrderItems.Cast<WrappedSelection<ViabilityOrderItem>>();
                    foreach (var item in wrappedItems.Select(x => x.Item))
                    {
                        InventoryViability newInventoryViability = new InventoryViability()
                        {
                            InventoryViabilityId = -1,
                            InventoryViabilityRuleId = InventoryViabilityRule.InventoryViabilityRuleId,
                            InventoryViabilityRuleName = InventoryViabilityRule.Name,
                            InventoryId = item.inventory_id,
                            ReplicationCount = NumberOfReplicates,
                            TotalTestedCount = TotalSeeds,
                            TestedDateCode = "MM/dd/yyyy",
                            TestedDate = now,
                            Note = ViabilityNote
                        };
                        string newInventoryViabilityIdStr = await _restService.CreateInventoryViabilityAsync(newInventoryViability);
                        
                        inventoryViabilityIds += (string.IsNullOrEmpty(inventoryViabilityIds) ? "" : ",") + newInventoryViabilityIdStr ;

                        //Create empty viability data to save order_request_item_id
                        var newInventoryViabilityData = new InventoryViabilityData()
                        {
                            CountDate = DateTime.Now,
                            CountNumber = -1, // To indicate invalid inventory viability data (for information only)
                            InventoryViabilityDataId = -1,
                            InventoryViabilityId = int.Parse(newInventoryViabilityIdStr),
                            NormalCount = 0,
                            Note = "Invalid inventory viability data",
                            OrderRequestItemId = item.order_request_item_id,
                            ReplicationNumber = 0,
                        };
                        string newInventoryViabilityDataIdStr = await _restService.CreateInventoryViabilityDataAsync(newInventoryViabilityData);

                        OrderRequestItemAction newOrderRequestItemAction = new OrderRequestItemAction()
                        {
                            action_information = newInventoryViabilityIdStr,
                            action_name_code = "VIABTEST",
                            cooperator_id = Settings.UserCooperatorId,
                            order_request_item_action_id = -1,
                            order_request_item_id = item.order_request_item_id,
                            started_date = now,
                            started_date_code = "MM/dd/yyyy",
                            completed_date = null,
                            note = "",
                            action_cost = 0,
                            completed_date_code = null,
                            owned_by = Settings.UserCooperatorId,
                            owned_date = now
                        };
                        await _restService.CreateOrderRequestItemActionAsync(newOrderRequestItemAction);

                        item.status_code = "VIAB";
                        string resultUpdateOrderItemRequest = await _restService.UpdateOrderItemRequest(item);

                        if (Discount)
                        {
                            Inventory i = await _restClient.ReadInventory(item.inventory_id);
                            i.quantity_on_hand -= newInventoryViability.TotalTestedCount;
                            await _restClient.UpdateInventory(i);

                            if(CreateAction)
                            {
                                InventoryAction ia = new InventoryAction
                                {
                                    inventory_action_id = -1,
                                    inventory_id = i.inventory_id,
                                    action_name_code = InventoryAction.value_member,
                                    quantity = newInventoryViability.TotalTestedCount * -1,
                                    quantity_unit_code = i.quantity_on_hand_unit_code,
                                    started_date = now,
                                    method_id = _discountMethodId,
                                    cooperator_id = Settings.UserCooperatorId,
                                    owned_by = Settings.UserCooperatorId,
                                    note = "Inventory viability rule name: " + InventoryViabilityRule.Name + "\n(order_request_id=" + OrderRequestId + ")"
                                };
                                await _restClient.CreateInventoryAction(ia);
                            }
                        }
                    }
                    await PageDialogService.DisplayAlertAsync(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "CreateInventoryViabilityPage", "MessageSaveResults"),
                        string.Format(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "CreateInventoryViabilityPage", "MessageSuccessfullyCreatedFromOrderRequest"), inventoryViabilityIds), "OK");

                    var orderRequestIds = GetSelection().Select(i => i.order_request_item_id).ToArray();
                    foreach (var orderRequestItemId in orderRequestIds)
                    {
                        var wrappedInventory = InventoryList.FirstOrDefault(x => x.Item.order_request_item_id == orderRequestItemId);
                        wrappedInventory.Item = await _restService.ReadViabilityRequestItem(orderRequestItemId);
                    }
                }
                
                /*
                newInventoryViability.InventoryViabilityId = int.Parse(result);
                var navigationParams = new NavigationParameters();
                navigationParams.Add("InventoryViability", newInventoryViability);

                await NavigationService.GoBackAsync(navigationParams);
                */
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "CreateInventoryViabilityPage", "MessageSaveResultsError"), 
                    ex.Message, "OK");
            }
        }
        public DelegateCommand RefreshCommand { get; }//UxToolbarItemRefresh
        private async void OnRefreshCommand()
        {
            try
            {
                var inventoryList = await _restService.GetViabilityRequestItemList(OrderRequestId.ToString());
                if (inventoryList != null)
                {
                    SelectedViabilityOrderItems.Clear();
                    InventoryList.Clear();

                    foreach (var item in inventoryList)
                    {
                        InventoryList.Add(new WrappedSelection<ViabilityOrderItem>() { IsSelected = false, Item = item });
                    }
                }
                /*var selection = GetSelection();
                if (selection != null && selection.Count > 0)
                {
                    var idList = selection.Select(x => x.order_request_item_id).ToList();
                    string strIdList = string.Join(",", idList);

                    var inventoryThumbnailList = await _restClient.Search("@order_request_item.order_request_item_id in (" + strIdList + ")", "get_mob_viability_request_item", "order_request_item");

                    foreach (WrappedSelection<InventoryThumbnail> item in _inventoryList)
                    {
                        if (item.IsSelected)
                        {
                            item.Item = inventoryThumbnailList.FirstOrDefault(x => x.inventory_id == item.Item.inventory_id);
                        }
                    }
                }*/
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public DelegateCommand InventoryViabilityRuleListChangedCommand { get; }
        private async void OnInventoryViabilityRuleListChangedCommand()
        {
            try
            {
                if (InventoryViabilityRule != null)
                {
                    NumberOfReplicates = InventoryViabilityRule.NumberOfReplicates;
                    TotalSeeds = InventoryViabilityRule.SeedsPerReplicate * InventoryViabilityRule.NumberOfReplicates;
                    Substrata = InventoryViabilityRule.Substrata;
                    Moisture = InventoryViabilityRule.Moisture;
                    Prechill = InventoryViabilityRule.Prechill;
                    Temperature = InventoryViabilityRule.TemperatureRange;
                    Lighting = InventoryViabilityRule.Lighting;
                    ViabilityRuleNote = InventoryViabilityRule.Note;
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public DelegateCommand PrintCommand { get; }
        private async void OnPrintCommand()
        {
            try
            {
                if(!SelectedViabilityOrderItems.Any())
                    throw new Exception("Select at least one order request item");

                /*DialogParameters paremeters = new DialogParameters();
                paremeters.Add("ViabilityRequestItemList", GetSelection());
                DialogService.ShowDialog("PrintInventoryViabilityDialog", paremeters);*/

                var wrappedItems = SelectedViabilityOrderItems.Cast<WrappedSelection<ViabilityOrderItem>>();
                NavigationParameters parameters = new NavigationParameters
                {
                    { "ViabilityRequestItemList", wrappedItems.Select(x => x.Item).ToList() }
                };
                var navigationResult = await NavigationService.NavigateAsync("PrintInventoryViabilityPage", parameters, true, true);
                if (!navigationResult.Success)
                    throw navigationResult.Exception;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            try
            {
                Utils.RefreshLang(this, _dataStoreService, "CreateInventoryViabilityPage");

                if (parameters.ContainsKey("InventoryId"))
                {
                    UxTabOrderRequestItems = string.Empty;
                    InventoryId = int.Parse(parameters["InventoryId"].ToString());

                    var inventoryResult = await _restClient.Search("@inventory.inventory_id=" + InventoryId, "get_mob_inventory_thumbnail", "inventory");
                    if(inventoryResult != null && inventoryResult.Count == 1)
                    {
                        if(inventoryResult[0].inventory_number.EndsWith("**"))
                            throw new Exception(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "CreateInventoryViabilityPage", "MessageErrorInventoryIsSystemInventory"));

                        InventoryNumber = inventoryResult[0].inventory_number;
                        InventoryTaxonName = inventoryResult[0].taxonomy_species_code;
                    }
                    else
                    {
                        throw new Exception("Inventory not found");
                    }
                    IsFromInventory = true;
                }

                if(parameters.ContainsKey("OrderRequestId"))
                {
                    OrderRequestId = int.Parse(parameters["OrderRequestId"].ToString());
                    ViabilityNote = "\n(order_request_id=" + OrderRequestId + ")";

                    var inventoryList = await _restService.GetViabilityRequestItemList(OrderRequestId.ToString());
                    if(inventoryList != null)
                    {
                        foreach (var item in inventoryList)
                        {
                            if (item.inventory_number.EndsWith("**"))
                                throw new Exception(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "CreateInventoryViabilityPage", "MessageErrorOrderRequestHasSystemInventories"));
                            InventoryList.Add(new WrappedSelection<ViabilityOrderItem>() { IsSelected = false, Item = item });
                        }
                    }
                    IsFromInventory = false;
                }

                if (InventoryViabilityRuleList == null)
                {
                    InventoryViabilityRuleList = await _restService.SearchInventoryViabilityRuleAsync("@inventory_viability_rule.inventory_viability_rule_id > 0", null, "inventory_viability_rule");
                }
                if(InventoryActionList == null)
                {
                    var filter = _dataStoreService.GetSetting(Settings.WorkgroupId, "gui_settings", "viability_actions");
                    InventoryActionList = await _restClient.GetInventoryViabilityActionCodeList(filter);
                }
                if(_discountMethodId == 0)
                {
                    var appSettingDiscount = _dataStoreService.GetAppSettingsDisplayMember(Settings.LangId, "DISCOUNT");
                    if (appSettingDiscount != null)
                    {
                        _discountMethodId = int.Parse(appSettingDiscount.setting_value);
                    }
                    else
                    {
                        throw new Exception("App setting \"DISCOUNT\" is not defined in \"get_mob_app_settings\" dataview ");
                    }
                    var methodsIds = await _restClient.SearchKeys("@method.method_id = " + _discountMethodId , "method");
                    if (methodsIds == null)
                    {
                        throw new Exception("Method for \"DISCOUNT\" set in \"get_mob_app_settings\" is not found.\nUse \"get_method\" dataview to register it");
                    }
                    if (!methodsIds.Contains(_discountMethodId))
                    {
                        throw new Exception("Method for \"DISCOUNT\" set in \"get_mob_app_settings\" is not found.\nUse \"get_method\" dataview to register it");
                    }
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "CreateInventoryViabilityPage", "MessageNavigatedToError"), 
                    ex.Message, "OK");
                await NavigationService.GoBackAsync();
            }
        }
    }
}
