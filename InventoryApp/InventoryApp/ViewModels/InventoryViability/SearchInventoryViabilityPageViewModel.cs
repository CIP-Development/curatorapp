﻿using ImTools;
using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using InventoryApp.Services;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace InventoryApp.ViewModels
{
    public class SearchInventoryViabilityPageViewModel : ViewModelBaseWithDialog
    {
        private string _searchText;
        private InventoryThumbnail _inventoryThumbnail;
        private InventoryViability _inventoryViability;
        private RestClient _restClient;
        private IDataStoreService _dataStoreService;

        #region Properties

        private readonly RestService _restService;
        public string SearchText
        {
            get { return _searchText; }
            set { SetProperty(ref _searchText, value); }
        }
        private bool _isInventoryViability;
        public bool IsInventoryViability
        {
            get { return _isInventoryViability; }
            set { SetProperty(ref _isInventoryViability, value); }
        }
        private string _inventoryNumber;
        public string InventoryNumber
        {
            get { return _inventoryNumber; }
            set { SetProperty(ref _inventoryNumber, value); }
        }
        private string _inventoryViabilityPercent;
        public string InventoryViabilityPercent
        {
            get { return _inventoryViabilityPercent; }
            set { SetProperty(ref _inventoryViabilityPercent, value); }
        }
        private string _inventoryViabilityDate;
        public string InventoryViabilityDate
        {
            get { return _inventoryViabilityDate; }
            set { SetProperty(ref _inventoryViabilityDate, value); }
        }
        private string _inventoryTaxon;
        public string InventoryTaxon
        {
            get { return _inventoryTaxon; }
            set { SetProperty(ref _inventoryTaxon, value); }
        }
        private bool _isResultVisible;
        public bool IsResultVisible
        {
            get { return _isResultVisible; }
            set { SetProperty(ref _isResultVisible, value); }
        }
        private string _inventoryViabilityRule;
        public string InventoryViabilityRule
        {
            get { return _inventoryViabilityRule; }
            set { SetProperty(ref _inventoryViabilityRule, value); }
        }
        private string _inventoryViabilityReplicates;
        public string InventoryViabilityReplicates
        {
            get { return _inventoryViabilityReplicates; }
            set { SetProperty(ref _inventoryViabilityReplicates, value); }
        }
        private string _inventoryViabilityTotalSeeds;
        public string InventoryViabilityTotalSeeds
        {
            get { return _inventoryViabilityTotalSeeds; }
            set { SetProperty(ref _inventoryViabilityTotalSeeds, value); }
        }
        private string _inventoryViabilityNotes;
        public string InventoryViabilityNotes
        {
            get { return _inventoryViabilityNotes; }
            set { SetProperty(ref _inventoryViabilityNotes, value); }
        }
        private string _inventoryViabilityCounts;
        public string InventoryViabilityCounts
        {
            get { return _inventoryViabilityCounts; }
            set { SetProperty(ref _inventoryViabilityCounts, value); }
        }
        private string _inventoryViabilityLastCountDate;
        public string InventoryViabilityLastCountDate
        {
            get { return _inventoryViabilityLastCountDate; }
            set { SetProperty(ref _inventoryViabilityLastCountDate, value); }
        }
        private string _orderRequestText;
        public string OrderRequestText
        {
            get { return _orderRequestText; }
            set { SetProperty(ref _orderRequestText, value); }
        }
        private string _inventoryText;
        public string InventoryText
        {
            get { return _inventoryText; }
            set { SetProperty(ref _inventoryText, value); }
        }
        private int _inventoryId;
        public int InventoryId
        {
            get { return _inventoryId; }
            set { SetProperty(ref _inventoryId, value); }
        }
        private int _inventoryViabilityId;
        public int InventoryViabilityId
        {
            get { return _inventoryViabilityId; }
            set { SetProperty(ref _inventoryViabilityId, value); }
        }
        #endregion

        #region LangProperties
        private string _UxTitle;
        public string UxTitle
        {
            get { return _UxTitle; }
            set { SetProperty(ref _UxTitle, value); }
        }
        private string _UxToolbarItemNew;
        public string UxToolbarItemNew
        {
            get { return _UxToolbarItemNew; }
            set { SetProperty(ref _UxToolbarItemNew, value); }
        }
        private string _UxLabelViabilityTestId;
        public string UxLabelViabilityTestId
        {
            get { return _UxLabelViabilityTestId; }
            set { SetProperty(ref _UxLabelViabilityTestId, value); }
        }
        private string _UxLabelInventoryId;
        public string UxLabelInventoryId
        {
            get { return _UxLabelInventoryId; }
            set { SetProperty(ref _UxLabelInventoryId, value); }
        }
        private string _UxLabelInventoryNumber;
        public string UxLabelInventoryNumber
        {
            get { return _UxLabelInventoryNumber; }
            set { SetProperty(ref _UxLabelInventoryNumber, value); }
        }
        private string _UxLabelLastPercentViable;
        public string UxLabelLastPercentViable
        {
            get { return _UxLabelLastPercentViable; }
            set { SetProperty(ref _UxLabelLastPercentViable, value); }
        }
        private string _UxLabelLastTestDate;
        public string UxLabelLastTestDate
        {
            get { return _UxLabelLastTestDate; }
            set { SetProperty(ref _UxLabelLastTestDate, value); }
        }
        private string _UxLabelTaxon;
        public string UxLabelTaxon
        {
            get { return _UxLabelTaxon; }
            set { SetProperty(ref _UxLabelTaxon, value); }
        }
        private string _UxLabelViabilityRule;
        public string UxLabelViabilityRule
        {
            get { return _UxLabelViabilityRule; }
            set { SetProperty(ref _UxLabelViabilityRule, value); }
        }
        private string _UxLabelReplicates;
        public string UxLabelReplicates
        {
            get { return _UxLabelReplicates; }
            set { SetProperty(ref _UxLabelReplicates, value); }
        }
        private string _UxLabelTotalSeeds;
        public string UxLabelTotalSeeds
        {
            get { return _UxLabelTotalSeeds; }
            set { SetProperty(ref _UxLabelTotalSeeds, value); }
        }
        private string _UxLabelViabilityTestNotes;
        public string UxLabelViabilityTestNotes
        {
            get { return _UxLabelViabilityTestNotes; }
            set { SetProperty(ref _UxLabelViabilityTestNotes, value); }
        }
        private string _UxLabelCounts;
        public string UxLabelCounts
        {
            get { return _UxLabelCounts; }
            set { SetProperty(ref _UxLabelCounts, value); }
        }
        private string _UxLabelLastCountDate;
        public string UxLabelLastCountDate
        {
            get { return _UxLabelLastCountDate; }
            set { SetProperty(ref _UxLabelLastCountDate, value); }
        }

        private string _UxButtonEditInventoryViability;
        public string UxButtonEditInventoryViability
        {
            get { return _UxButtonEditInventoryViability; }
            set { SetProperty(ref _UxButtonEditInventoryViability, value); }
        }
        private string _UxButtonPrintLabels;
        public string UxButtonPrintLabels
        {
            get { return _UxButtonPrintLabels; }
            set { SetProperty(ref _UxButtonPrintLabels, value); }
        }
        private string _UxButtonViabilityData;
        public string UxButtonViabilityData
        {
            get { return _UxButtonViabilityData; }
            set { SetProperty(ref _UxButtonViabilityData, value); }
        }
        private string _UxButtonPostFinalResults;
        public string UxButtonPostFinalResults
        {
            get { return _UxButtonPostFinalResults; }
            set { SetProperty(ref _UxButtonPostFinalResults, value); }
        }

        private string _UxLabelCreateInventoryViabilityTest;
        public string UxLabelCreateInventoryViabilityTest
        {
            get { return _UxLabelCreateInventoryViabilityTest; }
            set { SetProperty(ref _UxLabelCreateInventoryViabilityTest, value); }
        }
        private string _UxButtonCreateFromOrder;
        public string UxButtonCreateFromOrder
        {
            get { return _UxButtonCreateFromOrder; }
            set { SetProperty(ref _UxButtonCreateFromOrder, value); }
        }
        private string _UxButtonCreateFromInventory;
        public string UxButtonCreateFromInventory
        {
            get { return _UxButtonCreateFromInventory; }
            set { SetProperty(ref _UxButtonCreateFromInventory, value); }
        }
        private string _UxLabelInventoryIdNumber;
        public string UxLabelInventoryIdNumber
        {
            get { return _UxLabelInventoryIdNumber; }
            set { SetProperty(ref _UxLabelInventoryIdNumber, value); }
        }
        private string _UxLabelOrderRequestId;
        public string UxLabelOrderRequestId
        {
            get { return _UxLabelOrderRequestId; }
            set { SetProperty(ref _UxLabelOrderRequestId, value); }
        }
        #endregion

        public SearchInventoryViabilityPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService, IDataStoreService dataStoreService)
            : base(navigationService, pageDialogService)
        {
            _isResultVisible = false;
            _restService = new RestService();
            _restClient = new RestClient();
            _dataStoreService = dataStoreService;

            NewInventoryViabilityCommand = new DelegateCommand(OnNewInventoryViabilityCommand);
            SearchInventoryViabilityCommand = new DelegateCommand(OnSearchInventoryViabilityCommand);

            EditInventoryViabilityCommand = new DelegateCommand(OnEditInventoryViabilityCommand);
            PrintLabelCommand = new DelegateCommand(OnPrintLabelCommand);
            OpenViabilityDataCommand = new DelegateCommand(OnOpenViabilityDataCommand);
            PostFinalResultsCommand = new DelegateCommand(OnPostFinalResultsCommand);

            CreateFromInventoryCommand = new DelegateCommand(OnCreateFromInventoryCommand);
            CreateFromOrderCommand = new DelegateCommand(OnCreateFromOrderCommand);

            _UxButtonEditInventoryViability = "Edit Viability Test";
            _UxButtonPrintLabels = "Print Labels";
            _searchText = "";
            IsInventoryViability = true;
        }
        public DelegateCommand NewInventoryViabilityCommand { get; }
        private async void OnNewInventoryViabilityCommand()
        {
            try
            {
                IsResultVisible = false;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public DelegateCommand SearchInventoryViabilityCommand { get; }
        private async void OnSearchInventoryViabilityCommand()
        {
            try
            {
                IsResultVisible = false;

                _inventoryThumbnail = null;
                _inventoryViability = null;
                Clean();

                //Search by restClient
                string query = "";
                SearchText = SearchText.Trim();
                if (!IsInventoryViability) //IsInventory
                {
                    if (!int.TryParse(SearchText, out int inventoryId)) //Search text is Inventory Number
                    {
                        List<ILookup> inventoryResult = await _restClient.GetInventoryLookUpListByInventoryNumber(SearchText, null);
                        if (inventoryResult != null)
                        {
                            if (inventoryResult.Count == 0)
                                throw new Exception("Inventory not found");
                            else if (inventoryResult.Count == 1)
                                inventoryId = (int)inventoryResult[0].ValueMember;
                            else
                                throw new Exception("More than one inventory found");
                        }
                        else
                        {
                            throw new Exception("Inventory not found");
                        }
                    }

                    query = "@inventory.inventory_id = " + inventoryId;

                    List<InventoryThumbnail> inventoryThumbnailListResult = await _restService.SearchInventoryThumbnailAsync(query, "get_mob_inventory_thumbnail", "inventory");
                    if (inventoryThumbnailListResult != null && inventoryThumbnailListResult.Count > 0)
                    {
                        _inventoryThumbnail = inventoryThumbnailListResult.FirstOrDefault();
                    }
                    else
                    {
                        throw new Exception("Inventory not found");
                    }

                    List<InventoryViability> inventoryViabilityList = await _restService.SearchInventoryViabilityAsync("@inventory_viability.inventory_id = " + _inventoryThumbnail.inventory_id,
                    null, "inventory_viability");
                    if (inventoryViabilityList != null && inventoryViabilityList.Count > 0)
                    {
                        IsResultVisible = true;

                        var orderedInventoryViabilityList = inventoryViabilityList.OrderByDescending(x => x.TestedDate).ThenByDescending(x => x.InventoryViabilityId);
                        _inventoryViability = orderedInventoryViabilityList.First();

                        InventoryId = _inventoryThumbnail.inventory_id;
                        InventoryNumber = _inventoryThumbnail.inventory_number;
                        InventoryViabilityPercent = _inventoryThumbnail.PercentViable.ToString();
                        InventoryViabilityDate = _inventoryThumbnail.TestedDate.HasValue ? _inventoryThumbnail.TestedDate.Value.ToString("MM/dd/yyyy") : "";
                        InventoryTaxon = _inventoryThumbnail.taxonomy_species_code;

                        InventoryViabilityId = _inventoryViability.InventoryViabilityId;
                        InventoryViabilityRule = _inventoryViability.InventoryViabilityRuleName;
                        InventoryViabilityReplicates = _inventoryViability.ReplicationCount.ToString();
                        InventoryViabilityTotalSeeds = _inventoryViability.TotalTestedCount.ToString();
                        InventoryViabilityNotes = _inventoryViability.Note;
                        InventoryViabilityCounts = _inventoryViability.counts.ToString();
                        InventoryViabilityLastCountDate = _inventoryViability.last_count_date.HasValue ? _inventoryViability.last_count_date.Value.ToString("MM/dd/yyyy") : "";
                    }
                    else
                    {
                        await PageDialogService.DisplayAlertAsync("Message", "Inventory Viability not found", "OK");
                    }
                }
                else //IsInventoryViability
                {
                    query = "@inventory_viability.inventory_viability_id = " + SearchText;
                    List<InventoryViability> inventoryViabilityList = await _restService.SearchInventoryViabilityAsync(query, null, "inventory_viability");
                    if (inventoryViabilityList != null && inventoryViabilityList.Count > 0)
                    {
                        _inventoryViability = inventoryViabilityList[0];

                        query = "@inventory.inventory_id = " + _inventoryViability.InventoryId;

                        List<InventoryThumbnail> inventoryThumbnailListResult = await _restService.SearchInventoryThumbnailAsync(query, "get_mob_inventory_thumbnail", "inventory");
                        if (inventoryThumbnailListResult != null && inventoryThumbnailListResult.Count > 0)
                        {
                            _inventoryThumbnail = inventoryThumbnailListResult.FirstOrDefault();

                            InventoryId = _inventoryThumbnail.inventory_id;
                            InventoryNumber = _inventoryThumbnail.inventory_number;
                            InventoryViabilityPercent = _inventoryThumbnail.PercentViable.ToString();
                            InventoryViabilityDate = _inventoryThumbnail.TestedDate.HasValue ? _inventoryThumbnail.TestedDate.Value.ToString("MM/dd/yyyy") : "";
                            InventoryTaxon = _inventoryThumbnail.taxonomy_species_code;

                            InventoryViabilityId = _inventoryViability.InventoryViabilityId;
                            InventoryViabilityRule = _inventoryViability.InventoryViabilityRuleName;
                            InventoryViabilityReplicates = _inventoryViability.ReplicationCount.ToString();
                            InventoryViabilityTotalSeeds = _inventoryViability.TotalTestedCount.ToString();
                            InventoryViabilityNotes = _inventoryViability.Note;
                            InventoryViabilityCounts = _inventoryViability.counts.ToString();
                            InventoryViabilityLastCountDate = _inventoryViability.last_count_date.HasValue ? _inventoryViability.last_count_date.Value.ToString("MM/dd/yyyy") : "";

                            IsResultVisible = true;
                        }
                        else
                        {
                            throw new Exception("Inventory not found");
                        }
                    }
                    else
                    {
                        throw new Exception("Inventory Viability not found");
                    }
                }

            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public DelegateCommand EditInventoryViabilityCommand { get; }
        private async void OnEditInventoryViabilityCommand()
        {
            try
            {
                if (_inventoryViability == null)
                {
                    await PageDialogService.DisplayAlertAsync("Message", "Inventory Viability not found", "OK");
                }
                else
                {
                    var navigationParams = new NavigationParameters();
                    navigationParams.Add("InventoryViability", _inventoryViability);
                    await NavigationService.NavigateAsync("ViabilityTestPage", navigationParams);
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public DelegateCommand OpenViabilityDataCommand { get; }
        private async void OnOpenViabilityDataCommand()
        {
            try
            {
                if (_inventoryViability == null)
                    throw new Exception("Inventory Viability is empty");
                var navigationParams = new NavigationParameters();
                navigationParams.Add("InventoryViability", _inventoryViability);
                await NavigationService.NavigateAsync("InventoryViabilityDataPage", navigationParams);
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public DelegateCommand PrintLabelCommand { get; }
        private async void OnPrintLabelCommand()
        {
            try
            {
                if (_inventoryViability == null)
                    throw new Exception("Inventory Viability is empty");

                var navigationParams = new NavigationParameters();
                navigationParams.Add("InventoryViability", _inventoryViability);
                await NavigationService.NavigateAsync("PrintInventoryViabilityPage", navigationParams, true, true);
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }

        public DelegateCommand PostFinalResultsCommand { get; }
        private async void OnPostFinalResultsCommand()
        {
            try
            {
                if (_inventoryViability == null)
                    throw new Exception("Inventory Viability is empty");

                List<InventoryViabilityData> inventoryViabilityDataList = await _restService.SearchInventoryViabilityDataAsync(
                        "@inventory_viability_data.inventory_viability_id = " + _inventoryViability.InventoryViabilityId, null, "inventory_viability_data");

                decimal normalCount = inventoryViabilityDataList.Sum(x => x.NormalCount.GetValueOrDefault(0));
                decimal abnormalCount = inventoryViabilityDataList.Sum(x => x.AbnormalCount.GetValueOrDefault(0));
                decimal allDormantCount = inventoryViabilityDataList.Sum(x => x.EstimatedDormantCount.GetValueOrDefault(0) + x.ConfirmedDormantCount.GetValueOrDefault(0) + x.DormantCount.GetValueOrDefault(0));
                decimal hardCount = inventoryViabilityDataList.Sum(x => x.HardCount.GetValueOrDefault(0));
                decimal emptyCount = inventoryViabilityDataList.Sum(x => x.EmptyCount.GetValueOrDefault(0));
                decimal infestedCount = inventoryViabilityDataList.Sum(x => x.InfestedCount.GetValueOrDefault(0));
                decimal deadCount = inventoryViabilityDataList.Sum(x => x.DeadCount.GetValueOrDefault(0));
                decimal unknownCount = inventoryViabilityDataList.Sum(x => x.UnknownCount.GetValueOrDefault(0));

                _inventoryViability.PercentNormal = normalCount  / _inventoryViability.TotalTestedCount * 100;
                _inventoryViability.PercentAbnormal = abnormalCount / _inventoryViability.TotalTestedCount * 100;
                _inventoryViability.PercentDormant = allDormantCount / _inventoryViability.TotalTestedCount * 100;
                _inventoryViability.PercentHard = hardCount / _inventoryViability.TotalTestedCount * 100;
                _inventoryViability.PercentEmpty = emptyCount / _inventoryViability.TotalTestedCount * 100;
                _inventoryViability.PercentInfested = infestedCount / _inventoryViability.TotalTestedCount * 100;
                _inventoryViability.PercentDead = deadCount / _inventoryViability.TotalTestedCount * 100;
                _inventoryViability.PercentUnknown = unknownCount / _inventoryViability.TotalTestedCount * 100;
                _inventoryViability.PercentViable = (normalCount + allDormantCount + hardCount) / _inventoryViability.TotalTestedCount * 100;

                var result = await _restService.UpdateInventoryViabilityAsync(_inventoryViability);
                InventoryViabilityPercent = "" + Math.Round((double)_inventoryViability.PercentViable, 2) + " %";

                //update order_request_item_status
                if (_inventoryViability.order_request_item_id.HasValue)
                {
                    var viabilityOrderItem = await _restService.ReadViabilityRequestItem((int)_inventoryViability.order_request_item_id);
                    viabilityOrderItem.status_code = "VIABEND";
                    viabilityOrderItem.status_date = DateTime.Now;
                    await _restService.UpdateOrderItemRequest(viabilityOrderItem);
                }

                InventoryAction ia = new InventoryAction
                {
                    inventory_action_id = -1,
                    inventory_id = _inventoryViability.InventoryId,
                    action_name_code = "ViabTested",
                    started_date = DateTime.Now,
                    method_id = null,
                    cooperator_id = Settings.UserCooperatorId,
                    note = "Viability Test - Percent viable: " + _inventoryViability.PercentViable + "\n(inventory_viability_id=" + _inventoryViability.InventoryViabilityId + ")"
                };
                await _restClient.CreateInventoryAction(ia);

                await PageDialogService.DisplayAlertAsync(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "SearchInventoryViabilityPage", "MessagePublishResults"),
                    _dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "SearchInventoryViabilityPage", "MessageSuccessfullyPublished"), "OK");
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "SearchInventoryViabilityPage", "MessagePublishResultsError"), 
                    ex.Message, "OK");
            }
        }
        public DelegateCommand CreateFromInventoryCommand { get; }
        private async void OnCreateFromInventoryCommand()
        {
            try
            {
                if (string.IsNullOrEmpty(InventoryText))
                    throw new Exception("Inventory is empty");

                if (!int.TryParse(InventoryText, out int inventoryId)) //Search text is Inventory Number
                {
                    List<ILookup> inventoryResult = await _restClient.GetInventoryLookUpListByInventoryNumber(InventoryText, null);
                    if (inventoryResult != null)
                    {
                        if (inventoryResult.Count == 0)
                            throw new Exception("Inventory not found");
                        else if (inventoryResult.Count == 1)
                            inventoryId = (int)inventoryResult[0].ValueMember;
                        else
                            throw new Exception("More than one inventory found");
                    }
                    else
                    {
                        throw new Exception("Inventory not found");
                    }
                }

                var navigationParams = new NavigationParameters();
                navigationParams.Add("InventoryId", inventoryId.ToString());
                await NavigationService.NavigateAsync("CreateInventoryViabilityPage", navigationParams);
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public DelegateCommand CreateFromOrderCommand { get; }
        private async void OnCreateFromOrderCommand()
        {
            try
            {
                if (string.IsNullOrEmpty(OrderRequestText))
                    throw new Exception("Order request is empty");
                var navigationParams = new NavigationParameters
                {
                    { "OrderRequestId", OrderRequestText }
                };
                await NavigationService.NavigateAsync("CreateInventoryViabilityPage", navigationParams);
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        private void Clean()
        {
            InventoryNumber = string.Empty;
            InventoryViabilityPercent = string.Empty;
            InventoryViabilityDate = string.Empty;
            InventoryTaxon = string.Empty;

            InventoryViabilityRule = string.Empty;
            InventoryViabilityReplicates = string.Empty;
            InventoryViabilityTotalSeeds = string.Empty;
            InventoryViabilityNotes = string.Empty;
            InventoryViabilityCounts = string.Empty; ;
            InventoryViabilityLastCountDate = string.Empty;
        }
        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            try
            {
                Utils.RefreshLang(this, _dataStoreService, "SearchInventoryViabilityPage");

                if (parameters.ContainsKey("InventoryViability"))
                {
                    var inventoryViability = (InventoryViability)parameters["InventoryViability"];
                    if (inventoryViability != null)
                    {
                        Clean();
                        var query = "@inventory_viability.inventory_viability_id = " + inventoryViability.InventoryViabilityId;
                        List<InventoryViability> inventoryViabilityList = await _restService.SearchInventoryViabilityAsync(query, null, "inventory_viability");
                        if (inventoryViabilityList != null && inventoryViabilityList.Count > 0)
                        {
                            _inventoryViability = inventoryViabilityList[0];

                            query = "@inventory.inventory_id = " + _inventoryViability.InventoryId;

                            List<InventoryThumbnail> inventoryThumbnailListResult = await _restService.SearchInventoryThumbnailAsync(query, "get_mob_inventory_thumbnail", "inventory");
                            if (inventoryThumbnailListResult != null && inventoryThumbnailListResult.Count > 0)
                            {
                                _inventoryThumbnail = inventoryThumbnailListResult.FirstOrDefault();

                                InventoryNumber = _inventoryThumbnail.inventory_number;
                                InventoryViabilityPercent = _inventoryThumbnail.PercentViable.ToString();
                                InventoryViabilityDate = _inventoryThumbnail.TestedDate.HasValue ? _inventoryThumbnail.TestedDate.Value.ToString("MM/dd/yyyy") : "";
                                InventoryTaxon = _inventoryThumbnail.taxonomy_species_code;

                                InventoryViabilityRule = _inventoryViability.InventoryViabilityRuleName;
                                InventoryViabilityReplicates = _inventoryViability.ReplicationCount.ToString();
                                InventoryViabilityTotalSeeds = _inventoryViability.TotalTestedCount.ToString();
                                InventoryViabilityNotes = _inventoryViability.Note;
                                InventoryViabilityCounts = _inventoryViability.counts.ToString();
                                InventoryViabilityLastCountDate = _inventoryViability.last_count_date.HasValue ? _inventoryViability.last_count_date.Value.ToString("MM/dd/yyyy") : "";

                                IsResultVisible = true;
                            }
                            else
                            {
                                throw new Exception("Inventory not found");
                            }
                        }
                        else
                        {
                            throw new Exception("Inventory Viability not found");
                        }
                    }
                }

                if (parameters.ContainsKey("InventoryViabilityId"))
                {
                    int inventoryViabilityId = (int) parameters["InventoryViabilityId"];
                    SearchText = inventoryViabilityId.ToString();
                    IsInventoryViability = true;
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
    }
}
