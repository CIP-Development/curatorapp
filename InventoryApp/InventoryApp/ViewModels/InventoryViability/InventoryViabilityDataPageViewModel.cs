﻿using ImTools;
using InventoryApp.Helpers;
using InventoryApp.Interfaces;
using InventoryApp.Models;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace InventoryApp.ViewModels
{
    public class InventoryViabilityDataPageViewModel : ViewModelBaseWithDialog
    {
        IDataStoreService _dataStoreService;
        private InventoryViability _inventoryViability;
        private List<InventoryViabilityData> _listInventoryViabilityData;
        private RestService _restService;
        private OrderRequestItemAction _inventoryViabilityOrderRequestItemAction;
        private int? _orderRequestItemId;

        #region Properties
        private ObservableCollection<DateTime> _listCountDate;
        public ObservableCollection<DateTime> ListCountDate
        {
            get { return _listCountDate; }
            set { SetProperty(ref _listCountDate, value); }
        }
        private DateTime _countDate;
        public DateTime CountDate
        {
            get { return _countDate; }
            set { SetProperty(ref _countDate, value); }
        }
        private bool _isCountDateEditing;
        public bool IsCountDateEditing
        {
            get { return _isCountDateEditing; }
            set { SetProperty(ref _isCountDateEditing, value); }
        }
        private DateTime _editingCountDate;
        public DateTime EditingCountDate
        {
            get { return _editingCountDate; }
            set { SetProperty(ref _editingCountDate, value); }
        }
        private List<InventoryViabilityData> _listInventoryViabilityDataByDate;
        public List<InventoryViabilityData> ListInventoryViabilityDataByDate
        {
            get { return _listInventoryViabilityDataByDate; }
            set { SetProperty(ref _listInventoryViabilityDataByDate, value); }
        }
        private InventoryViabilityData _inventoryViabilityData;
        public InventoryViabilityData InventoryViabilityData
        {
            get { return _inventoryViabilityData; }
            set { SetProperty(ref _inventoryViabilityData, value); }
        }
        private string _countNumber;
        public string CountNumber
        {
            get { return _countNumber; }
            set { SetProperty(ref _countNumber, value); }
        }
        private int _summarySeeds;
        public int SummarySeeds
        {
            get { return _summarySeeds; }
            set { SetProperty(ref _summarySeeds, value); }
        }
        private double _summaryCountedPercent;
        public double SummaryCountedPercent
        {
            get { return _summaryCountedPercent; }
            set { SetProperty(ref _summaryCountedPercent, value); }
        }
        private double _summaryNormalCount;
        public double SummaryNormalCount
        {
            get { return _summaryNormalCount; }
            set { SetProperty(ref _summaryNormalCount, value); }
        }
        private double _summaryAbnormalCount;
        public double SummaryAbnormalCount
        {
            get { return _summaryAbnormalCount; }
            set { SetProperty(ref _summaryAbnormalCount, value); }
        }
        private double _summaryDormantCount;
        public double SummaryDormantCount
        {
            get { return _summaryDormantCount; }
            set { SetProperty(ref _summaryDormantCount, value); }
        }
        private double _summaryHardCount;
        public double SummaryHardCount
        {
            get { return _summaryHardCount; }
            set { SetProperty(ref _summaryHardCount, value); }
        }
        private double _summaryEmptyCount;
        public double SummaryEmptyCount
        {
            get { return _summaryEmptyCount; }
            set { SetProperty(ref _summaryEmptyCount, value); }
        }
        private double _summaryInfestedCount;
        public double SummaryInfestedCount
        {
            get { return _summaryInfestedCount; }
            set { SetProperty(ref _summaryInfestedCount, value); }
        }
        private double _summaryDeadCount;
        public double SummaryDeadCount
        {
            get { return _summaryDeadCount; }
            set { SetProperty(ref _summaryDeadCount, value); }
        }
        private double _summaryUnknownCount;
        public double SummaryUnknownCount
        {
            get { return _summaryUnknownCount; }
            set { SetProperty(ref _summaryUnknownCount, value); }
        }
        private double _summaryEstimatedDormantCount;
        public double SummaryEstimatedDormantCount
        {
            get { return _summaryEstimatedDormantCount; }
            set { SetProperty(ref _summaryEstimatedDormantCount, value); }
        }
        private double _summaryTreatedDormantCount;
        public double SummaryTreatedDormantCount
        {
            get { return _summaryTreatedDormantCount; }
            set { SetProperty(ref _summaryTreatedDormantCount, value); }
        }
        private double _summaryConfirmedDormantCount;
        public double SummaryConfirmedDormantCount
        {
            get { return _summaryConfirmedDormantCount; }
            set { SetProperty(ref _summaryConfirmedDormantCount, value); }
        }
        #endregion

        #region LangProperties
        private string _UxTitle;
        public string UxTitle
        {
            get { return _UxTitle; }
            set { SetProperty(ref _UxTitle, value); }
        }
        private string _UxToolbarItemSave;
        public string UxToolbarItemSave
        {
            get { return _UxToolbarItemSave; }
            set { SetProperty(ref _UxToolbarItemSave, value); }
        }
        private string _UxLabelViabilityDate;
        public string UxLabelViabilityDate
        {
            get { return _UxLabelViabilityDate; }
            set { SetProperty(ref _UxLabelViabilityDate, value); }
        }
        private string _UxButtonDone;
        public string UxButtonDone
        {
            get { return _UxButtonDone; }
            set { SetProperty(ref _UxButtonDone, value); }
        }
        private string _UxButtonCancel;
        public string UxButtonCancel
        {
            get { return _UxButtonCancel; }
            set { SetProperty(ref _UxButtonCancel, value); }
        }
        private string _UxLabelRepetition;
        public string UxLabelRepetition
        {
            get { return _UxLabelRepetition; }
            set { SetProperty(ref _UxLabelRepetition, value); }
        }
        private string _UxLabelSeeds;
        public string UxLabelSeeds
        {
            get { return _UxLabelSeeds; }
            set { SetProperty(ref _UxLabelSeeds, value); }
        }
        private string _UxLabelCountedPercent;
        public string UxLabelCountedPercent
        {
            get { return _UxLabelCountedPercent; }
            set { SetProperty(ref _UxLabelCountedPercent, value); }
        }
        private string _UxLabelNormal;
        public string UxLabelNormal
        {
            get { return _UxLabelNormal; }
            set { SetProperty(ref _UxLabelNormal, value); }
        }
        private string _UxLabelAbnormal;
        public string UxLabelAbnormal
        {
            get { return _UxLabelAbnormal; }
            set { SetProperty(ref _UxLabelAbnormal, value); }
        }
        private string _UxLabelDormant;
        public string UxLabelDormant
        {
            get { return _UxLabelDormant; }
            set { SetProperty(ref _UxLabelDormant, value); }
        }
        private string _UxLabelEmpty;
        public string UxLabelEmpty
        {
            get { return _UxLabelEmpty; }
            set { SetProperty(ref _UxLabelEmpty, value); }
        }
        private string _UxLabelInfested;
        public string UxLabelInfested
        {
            get { return _UxLabelInfested; }
            set { SetProperty(ref _UxLabelInfested, value); }
        }
        private string _UxLabelDead;
        public string UxLabelDead
        {
            get { return _UxLabelDead; }
            set { SetProperty(ref _UxLabelDead, value); }
        }
        private string _UxLabelUnknown;
        public string UxLabelUnknown
        {
            get { return _UxLabelUnknown; }
            set { SetProperty(ref _UxLabelUnknown, value); }
        }
        private string _UxLabelEstimatedDormant;
        public string UxLabelEstimatedDormant
        {
            get { return _UxLabelEstimatedDormant; }
            set { SetProperty(ref _UxLabelEstimatedDormant, value); }
        }
        private string _UxLabelTreatedDormant;
        public string UxLabelTreatedDormant
        {
            get { return _UxLabelTreatedDormant; }
            set { SetProperty(ref _UxLabelTreatedDormant, value); }
        }
        private string _UxLabelConfirmedDormmant;
        public string UxLabelConfirmedDormmant
        {
            get { return _UxLabelConfirmedDormmant; }
            set { SetProperty(ref _UxLabelConfirmedDormmant, value); }
        }
        private string _UxLabelNote;
        public string UxLabelNote
        {
            get { return _UxLabelNote; }
            set { SetProperty(ref _UxLabelNote, value); }
        }
        private string _UxLabelViabilitySummary;
        public string UxLabelViabilitySummary
        {
            get { return _UxLabelViabilitySummary; }
            set { SetProperty(ref _UxLabelViabilitySummary, value); }
        }
        private string _UxLabelHard;
        public string UxLabelHard
        {
            get { return _UxLabelHard; }
            set { SetProperty(ref _UxLabelHard, value); }
        }
        #endregion
        public InventoryViabilityDataPageViewModel(INavigationService navigationService, IPageDialogService pageDialogService, IDataStoreService dataStoreService)
            : base(navigationService, pageDialogService)
        {
            _dataStoreService = dataStoreService;
            _restService = new RestService();

            AddCountDateCommand = new DelegateCommand(OnAddCountDateCommand);
            EditCountDateCommand = new DelegateCommand(OnEditCountDateCommand);
            DeleteCountDateCommand = new DelegateCommand(OnDeleteCountDateCommand);
            SaveCountDateCommand = new DelegateCommand(OnSaveCountDateCommand);
            CancelEditingCountDateCommand = new DelegateCommand(OnCancelEditingCountDateCommand);

            ListCountDateChangedCommand = new DelegateCommand(OnListCountDateChangedCommand);
            ListInventoryViabilityDataByDateChangedCommand = new DelegateCommand(OnListInventoryViabilityDataByDateChangedCommand);
            DataCountTextChangedCommand = new DelegateCommand(OnDataCountTextChangedCommand);

            SaveCommand = new DelegateCommand(OnSaveCommand);

            Title = "Inventory viability data";
            IsCountDateEditing = false;
            SummarySeeds = 0;
            SummaryCountedPercent = 0;
            SummaryNormalCount = 0;
            SummaryAbnormalCount = 0;
        }
        public DelegateCommand AddCountDateCommand { get; }
        private async void OnAddCountDateCommand()
        {
            try
            {
                int countNumber = ListCountDate.Count + 1;
                int replicationSeeds = (int)(_inventoryViability.TotalTestedCount / _inventoryViability.ReplicationCount);
                DateTime now = DateTime.Now;

                for (int iRep = 0; iRep < _inventoryViability.ReplicationCount; iRep++)
                {
                    _listInventoryViabilityData.Add(new InventoryViabilityData()
                    {
                        InventoryViabilityId = _inventoryViability.InventoryViabilityId,
                        OrderRequestItemId = _orderRequestItemId,
                        ReplicationNumber = iRep + 1,
                        ReplicationCount = replicationSeeds,
                        CountNumber = countNumber,
                        CountDate = now,
                        CounterCooperatorId = Settings.UserCooperatorId,
                        NormalCount = 0,
                        AbnormalCount = 0,
                        DormantCount = 0,
                        HardCount = 0,
                        EmptyCount = 0,
                        InfestedCount = 0,
                        UnknownCount = 0,
                        DeadCount = 0,
                        EstimatedDormantCount = 0,
                        TreatedDormantCount = 0,
                        ConfirmedDormantCount = 0
                    });
                }

                ListCountDate.Add(now);

                CountDate = now;
                OnListCountDateChangedCommand();
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public DelegateCommand EditCountDateCommand { get; }
        private async void OnEditCountDateCommand()
        {
            try
            {
                EditingCountDate = DateTime.Now;
                IsCountDateEditing = true;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public DelegateCommand DeleteCountDateCommand { get; }
        private async void OnDeleteCountDateCommand()
        {
            try
            {
                
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public DelegateCommand SaveCountDateCommand { get; }
        private async void OnSaveCountDateCommand()
        {
            try
            {
                var index = ListCountDate.IndexOf(CountDate);
                ListCountDate[index] = EditingCountDate;
                CountDate = EditingCountDate;
                //update inventory data

                IsCountDateEditing = false;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public DelegateCommand CancelEditingCountDateCommand { get; }
        private async void OnCancelEditingCountDateCommand()
        {
            try
            {
                IsCountDateEditing = false;
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public DelegateCommand ListCountDateChangedCommand { get; }
        private async void OnListCountDateChangedCommand()
        {
            try
            {
                if(CountDate != null)
                {
                    ListInventoryViabilityDataByDate = _listInventoryViabilityData.Where(x => x.CountDate == CountDate).ToList();

                    if(ListInventoryViabilityDataByDate.Count > 0)
                    {
                        InventoryViabilityData = ListInventoryViabilityDataByDate.FirstOrDefault();
                        CountNumber = InventoryViabilityData.CountNumber.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public DelegateCommand ListInventoryViabilityDataByDateChangedCommand { get; }
        private async void OnListInventoryViabilityDataByDateChangedCommand()
        {
            try
            {
                if (InventoryViabilityData != null)
                {
                    int replicateNumber = InventoryViabilityData.ReplicationNumber;
                    RefreshReplicateCount(replicateNumber);
                }
                else
                {
                    SummaryCountedPercent = SummaryNormalCount = SummaryAbnormalCount = SummaryDormantCount = SummaryHardCount = SummaryHardCount = SummaryEmptyCount = SummaryNormalCount = SummaryDeadCount = SummaryEstimatedDormantCount = SummaryTreatedDormantCount = SummaryConfirmedDormantCount = 0;
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        public DelegateCommand DataCountTextChangedCommand { get; }
        private async void OnDataCountTextChangedCommand()
        {
            try
            {
                if(InventoryViabilityData != null)
                {
                    int replicateNumber = InventoryViabilityData.ReplicationNumber;
                    RefreshReplicateCount(replicateNumber);
                    RefreshSummary();
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
        private void RefreshReplicateCount(int replicateNumber)
        {
            int totalSeeds = (int)InventoryViabilityData.ReplicationCount;
            int replicateCountedSeeds = 0;
            foreach (var item in _listInventoryViabilityData.Where(x => x.ReplicationNumber == replicateNumber))
            {
                var replicateSeeds = (item.NormalCount.HasValue ? item.NormalCount : 0) + (item.AbnormalCount.HasValue ? item.AbnormalCount : 0)
                                        + (item.DormantCount.HasValue ? item.DormantCount : 0) + (item.HardCount.HasValue ? item.HardCount : 0)
                                        + (item.EmptyCount.HasValue ? item.EmptyCount : 0) + (item.InfestedCount.HasValue ? item.InfestedCount : 0)
                                        + (item.DeadCount.HasValue ? item.DeadCount : 0) + (item.UnknownCount.HasValue ? item.UnknownCount : 0)
                                        + (item.EstimatedDormantCount.HasValue ? item.EstimatedDormantCount : 0) + (item.ConfirmedDormantCount.HasValue ? item.ConfirmedDormantCount : 0);
                replicateCountedSeeds += (int)replicateSeeds;
            }
            InventoryViabilityData.PercentCounted = Math.Round((replicateCountedSeeds * 1.0 / totalSeeds * 100), 2);
        }
        private void RefreshSummary()
        {
            int summaryCountedSeeds = 0;
            foreach (var item in _listInventoryViabilityData)
            {
                var replicateSeeds = (item.NormalCount.HasValue ? item.NormalCount : 0) + (item.AbnormalCount.HasValue ? item.AbnormalCount : 0)
                                        + (item.DormantCount.HasValue ? item.DormantCount : 0) + (item.HardCount.HasValue ? item.HardCount : 0)
                                        + (item.EmptyCount.HasValue ? item.EmptyCount : 0) + (item.InfestedCount.HasValue ? item.InfestedCount : 0)
                                        + (item.DeadCount.HasValue ? item.DeadCount : 0) + (item.UnknownCount.HasValue ? item.UnknownCount : 0)
                                        + (item.EstimatedDormantCount.HasValue ? item.EstimatedDormantCount : 0) + (item.ConfirmedDormantCount.HasValue ? item.ConfirmedDormantCount : 0);
                summaryCountedSeeds += (int)replicateSeeds;
            }
            SummaryCountedPercent = Math.Round(summaryCountedSeeds * 1.0 / SummarySeeds * 100, 2);

            SummaryNormalCount = (int)_listInventoryViabilityData.Sum(x => x.NormalCount);
            SummaryAbnormalCount = (int)_listInventoryViabilityData.Sum(x => x.AbnormalCount);
            SummaryDormantCount = (int)_listInventoryViabilityData.Sum(x => x.DormantCount);
            SummaryHardCount = (int)_listInventoryViabilityData.Sum(x => x.HardCount);
            SummaryEmptyCount = (int)_listInventoryViabilityData.Sum(x => x.EmptyCount);
            SummaryInfestedCount = (int)_listInventoryViabilityData.Sum(x => x.InfestedCount);
            SummaryDeadCount = (int)_listInventoryViabilityData.Sum(x => x.DeadCount);
            SummaryUnknownCount = (int)_listInventoryViabilityData.Sum(x => x.UnknownCount);
            SummaryEstimatedDormantCount = (int)_listInventoryViabilityData.Sum(x => x.EstimatedDormantCount);
            SummaryTreatedDormantCount = (int)_listInventoryViabilityData.Sum(x => x.TreatedDormantCount);
            SummaryConfirmedDormantCount = (int)_listInventoryViabilityData.Sum(x => x.ConfirmedDormantCount);
        }
        public DelegateCommand SaveCommand { get; }
        private async void OnSaveCommand()
        {
            try
            {
                foreach (var inventoryViabilityData in _listInventoryViabilityData)
                {
                    if(inventoryViabilityData.InventoryViabilityDataId > 0) //update
                    {
                        var rsp = await _restService.UpdateInventoryViabilityDataAsync(inventoryViabilityData);
                    }
                    else //create
                    {

                        var rsp = await _restService.CreateInventoryViabilityDataAsync(inventoryViabilityData);
                    }
                }
                await PageDialogService.DisplayAlertAsync(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "InventoryViabilityDataPage", "MessageSaveResults"),
                    _dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "InventoryViabilityDataPage", "MessageSuccessfullySaved"), "OK");

                var navigationParams = new NavigationParameters();
                navigationParams.Add("InventoryViability", _inventoryViability);
                await NavigationService.GoBackAsync(navigationParams);
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync(_dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "InventoryViabilityDataPage", "MessageSaveResultsError"), 
                    ex.Message, "OK");
            }
        }

        public override async void OnNavigatedTo(INavigationParameters parameters)
        {
            try
            {
                Utils.RefreshLang(this, _dataStoreService, "InventoryViabilityDataPage");

                if (parameters.ContainsKey("InventoryViability"))
                {
                    _inventoryViability = (InventoryViability)parameters["InventoryViability"];
                }

                if (_listInventoryViabilityData == null && _inventoryViability != null)
                {
                    var tempInventoryViabilityDataList = await _restService.SearchInventoryViabilityDataAsync(
                        "@inventory_viability_data.inventory_viability_id=" + _inventoryViability.InventoryViabilityId, null, "inventory_viability_data");

                    //_inventoryViabilityOrderRequestItemAction = await _restService.GetOrderRequestItemActionByInventoryViabilityIdAsync(_inventoryViability.InventoryViabilityId);
                    //if(_inventoryViabilityOrderRequestItemAction != null)
                    //{
                    //    _orderRequestItemId = int.Parse(_inventoryViabilityOrderRequestItemAction.action_information);
                    //}
                    _orderRequestItemId = tempInventoryViabilityDataList
                            .Where(x => x.CountNumber == -1)
                            .Select(x => x.OrderRequestItemId)
                            .FirstOrDefault();

                    _listInventoryViabilityData = tempInventoryViabilityDataList.Where(x => x.CountNumber > 0).ToList();
                    
                    if (_listInventoryViabilityData.Count == 0)
                    {
                        int replicationSeeds = (int) (_inventoryViability.TotalTestedCount / _inventoryViability.ReplicationCount);
                        DateTime now = DateTime.Now;
                        for (int iRep = 0; iRep < _inventoryViability.ReplicationCount; iRep++)
                        {
                            _listInventoryViabilityData.Add(new InventoryViabilityData() { 
                                InventoryViabilityId = _inventoryViability.InventoryViabilityId,
                                OrderRequestItemId = _orderRequestItemId,
                                ReplicationNumber = iRep + 1,
                                ReplicationCount = replicationSeeds,
                                CountNumber = 1,
                                CountDate = now,
                                CounterCooperatorId = Settings.UserCooperatorId,
                                NormalCount = 0,
                                AbnormalCount = 0,
                                DormantCount = 0,
                                HardCount = 0,
                                EmptyCount = 0,
                                InfestedCount = 0,
                                UnknownCount = 0,
                                DeadCount = 0,
                                EstimatedDormantCount = 0,
                                TreatedDormantCount = 0,
                                ConfirmedDormantCount = 0
                            });
                        }
                    }

                    //Populate count dates picker
                    ListCountDate = new ObservableCollection<DateTime>(_listInventoryViabilityData.Select(x => x.CountDate).Distinct().ToList());
                    if(ListCountDate.Count > 0)
                    {
                        CountDate = ListCountDate.LastOrDefault();
                        OnListCountDateChangedCommand();
                    }

                    //Summary
                    if(ListInventoryViabilityDataByDate != null)
                    {
                        SummarySeeds = (int) ListInventoryViabilityDataByDate.Sum(x => x.ReplicationCount);

                        int summaryCountedSeeds = 0;
                        foreach (var item in _listInventoryViabilityData)
                        {
                            var replicateSeeds = (item.NormalCount.HasValue ? item.NormalCount : 0) + (item.AbnormalCount.HasValue ? item.AbnormalCount : 0);
                            summaryCountedSeeds += (int)replicateSeeds;
                        }
                        SummaryCountedPercent = Math.Round((summaryCountedSeeds * 1.0 / SummarySeeds * 100), 2);
                    }
                }
            }
            catch (Exception ex)
            {
                await PageDialogService.DisplayAlertAsync("Error", ex.Message, "OK");
            }
        }
    }

}
