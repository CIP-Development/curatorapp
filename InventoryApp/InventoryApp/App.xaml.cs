﻿using Prism;
using Prism.Ioc;
using InventoryApp.ViewModels;
using InventoryApp.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace InventoryApp
{
    public partial class App
    {
        /* 
         * The Xamarin Forms XAML Previewer in Visual Studio uses System.Activator.CreateInstance.
         * This imposes a limitation in which the App class must have a default constructor. 
         * App(IPlatformInitializer initializer = null) cannot be handled by the Activator.
         */
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }

        protected override async void OnInitialized()
        {
            //Register Syncfusion license
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("NTAxMTcwQDMxMzkyZTMyMmUzMEJNV0F0TmRvM1pKOUd4dGhsWnFjNGFyTk4zbmJEcWQ0a1ZXQW5oTnVoOFU9");

            InitializeComponent();

            Xamarin.Essentials.VersionTracking.Track();

            await NavigationService.NavigateAsync("NavigationPage/LoginPage");
            //await NavigationService.NavigateAsync("MainPage/NavigationPage/InventoriesPage");
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterSingleton<Interfaces.IRestService, InventoryApp.Helpers.RestService>();
            containerRegistry.RegisterSingleton<Interfaces.IDataStoreService, InventoryApp.Helpers.DataStoreService>();
            containerRegistry.RegisterSingleton<Interfaces.IOrderRequestRepository, Helpers.OrderRequestDatabase>();
            containerRegistry.RegisterSingleton<Interfaces.IInventoryLocalRepository, Helpers.InventoryDatabase>();
            containerRegistry.RegisterSingleton<Interfaces.IInventoryActionLocalRepository, Helpers.InventoryActionDatabase>();
            containerRegistry.RegisterSingleton<Interfaces.ICrossingLocalRepository, Helpers.CrossingDatabase>();
            containerRegistry.RegisterSingleton<Interfaces.ICropTraitObservationLocalRepository, Helpers.CropTraitObservationDatabase>();
            containerRegistry.RegisterSingleton<Interfaces.IInventoryHarvestLocalRepository, Helpers.InventoryHarvestDatabase>();
            containerRegistry.RegisterSingleton<Interfaces.IAccessionInvAttachLocalRepository, Helpers.AccessionInvAttachDatabase>();
            containerRegistry.RegisterSingleton<Interfaces.ICodeValueLookupLocalRepository, Helpers.CodeValueLookupDatabase>();

            containerRegistry.RegisterDialog<Dialogs.EditServerListDialog, Dialogs.EditServerListDialogViewModel>();
            containerRegistry.RegisterDialog<Dialogs.ChangePasswordDialog, Dialogs.ChangePasswordViewModel>();
            containerRegistry.RegisterDialog<Dialogs.PrintInventoryViabilityDialog, Dialogs.PrintInventoryViabilityViewModel>();
            containerRegistry.RegisterDialog<Dialogs.LookupPickerDialog, Dialogs.LookupPickerDialogViewModel>();
            containerRegistry.RegisterDialog<Dialogs.AddOrEditRegenerationDescriptorsDialog, Dialogs.AddOrEditRegenerationDescriptorsDialogViewModel>();

            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<LoginPage, LoginPageViewModel>();
            containerRegistry.RegisterForNavigation<MainPage, MainPageViewModel>();
            containerRegistry.RegisterForNavigation<WelcomePage, WelcomePageViewModel>();
            containerRegistry.RegisterForNavigation<InventoriesPage, InventoriesPageViewModel>();
            containerRegistry.RegisterForNavigation<InventoryCollectionPage, InventoriesPageViewModel>();
            containerRegistry.RegisterForNavigation<ScanPage, ScanPageViewModel>();
            containerRegistry.RegisterForNavigation<InventoryPage, InventoryPageViewModel>();
            containerRegistry.RegisterForNavigation<LookupPickerPage, LookupPickerPageViewModel>();
            containerRegistry.RegisterForNavigation<ViabilityTestsPage, ViabilityTestsPageViewModel>();
            containerRegistry.RegisterForNavigation<ViabilityTestPage, ViabilityTestPageViewModel>();
            containerRegistry.RegisterForNavigation<ViabilityTestDataPage, ViabilityTestDataPageViewModel>();
            containerRegistry.RegisterForNavigation<ChangeLocationPage, ChangeLocationPageViewModel>();
            containerRegistry.RegisterForNavigation<MovementPage, MovementPageViewModel>();
            containerRegistry.RegisterForNavigation<PrintingPage, PrintingPageViewModel>();
            containerRegistry.RegisterForNavigation<ScanReaderPage, ScanReaderPageViewModel>();
            containerRegistry.RegisterForNavigation<SearchAccessionPage, SearchAccessionPageViewModel>();
            containerRegistry.RegisterForNavigation<SearchInventoriesPage, SearchInventoriesPageViewModel>();
            containerRegistry.RegisterForNavigation<SettingsPage, SettingsPageViewModel>();
            containerRegistry.RegisterForNavigation<PreviousInventoriesPage, PreviousInventoriesPageViewModel>();
            containerRegistry.RegisterForNavigation<InventoryActionsPage, InventoryActionsPageViewModel>();
            containerRegistry.RegisterForNavigation<SearchInventoryViabilityPage, SearchInventoryViabilityPageViewModel>();
            containerRegistry.RegisterForNavigation<CreateInventoryViabilityPage, CreateInventoryViabilityPageViewModel>();
            containerRegistry.RegisterForNavigation<InventoryViabilityDataPage, InventoryViabilityDataPageViewModel>();
            containerRegistry.RegisterForNavigation<PrintInventoryViabilityPage, PrintInventoryViabilityPageViewModel>();
            containerRegistry.RegisterForNavigation<AccessionLookupPickerPage, AccessionLookupPickerPageViewModel>();
            containerRegistry.RegisterForNavigation<CreatePrintInventoryPage, CreatePrintInventoryPageViewModel>();
            containerRegistry.RegisterForNavigation<CropTraitObservationsPage, CropTraitObservationsPageViewModel>();
            containerRegistry.RegisterForNavigation<ScanPageByReaderPage, ScanPageViewModel>();
            containerRegistry.RegisterForNavigation<RegenerationOrderRequestsPage, RegenerationOrderRequestsPageViewModel>();
            containerRegistry.RegisterForNavigation<RegenerationOrderRequestPage, RegenerationOrderRequestPageViewModel>();
            //containerRegistry.RegisterForNavigation<RegenerationRequestOrderDetailsPage, RegenerationRequestOrderDetailsPageViewModel>();
            containerRegistry.RegisterForNavigationOnIdiom<RegenerationOrderRequestWithDetailsPage, RegenerationOrderRequestWithDetailsPageViewModel>(
                desktopView: typeof(RegenerationRequestOrderDetailsPage));
            containerRegistry.RegisterForNavigation<OrderRequestItemActionPage, OrderRequestItemActionPageViewModel>();
            containerRegistry.RegisterForNavigation<RegenerationOrderRequestItemPage, RegenerationOrderRequestItemPageViewModel>();
            containerRegistry.RegisterForNavigation<OrderRequestItemActionsPage, OrderRequestItemActionsPageViewModel>();
            containerRegistry.RegisterForNavigation<ChangeOrderRequestItemInventoryPage, ChangeOrderRequestItemInventoryPageViewModel>();
            containerRegistry.RegisterForNavigation<OrderRequestActionsPage, OrderRequestActionsPageViewModel>();
            containerRegistry.RegisterForNavigation<OrderRequestActionPage, OrderRequestActionPageViewModel>();
            containerRegistry.RegisterForNavigation<EvaluateInventoriesPage, EvaluateInventoriesPageViewModel>();
            containerRegistry.RegisterForNavigation<CreateRegenerationInventoryPage, CreateRegenerationInventoryPageViewModel>();
            containerRegistry.RegisterForNavigation<CrossToHarvestPage, CrossToHarvestPageViewModel>();
            containerRegistry.RegisterForNavigation<CrossingPage, CrossingPageViewModel>();
            containerRegistry.RegisterForNavigation<CrossingListPage, CrossingListPageViewModel>(); 
            containerRegistry.RegisterForNavigation<HarvestFruitPage, HarvestFruitPageViewModel>();
            containerRegistry.RegisterForNavigation<HarvestTuberPage, HarvestTuberPageViewModel>();
            containerRegistry.RegisterForNavigation<MacerationPage, MacerationPageViewModel>();
            containerRegistry.RegisterForNavigation<PlantingPage, PlantingPageViewModel>();
            containerRegistry.RegisterForNavigation<TransplantInventoriesPage, TransplantInventoriesPageViewModel>();
            containerRegistry.RegisterForNavigation<EditRegenerationInventoryPage, EditRegenerationInventoryPageViewModel>();
            containerRegistry.RegisterForNavigation<RegenerationMonitoringPage, RegenerationMonitoringPageViewModel>();
            containerRegistry.RegisterForNavigation<DatabaseExplorerPage, DatabaseExplorerPageViewModel>();
            containerRegistry.RegisterForNavigation<EditCrossingPage, EditCrossingPageViewModel>();
            containerRegistry.RegisterForNavigation<LocalPrintPage, LocalPrintPageViewModel>();
            containerRegistry.RegisterForNavigation<BluetoothPrinterPickerPage, BluetoothPrinterPickerPageViewModel>();
            containerRegistry.RegisterForNavigation<AccessionInvAttachImagesPage, AccessionInvAttachImagesPageViewModel>();
            containerRegistry.RegisterForNavigation<CreateAdquisitionInventoryPage, CreateAdquisitionInventoryPageViewModel>();
            containerRegistry.RegisterForNavigation<AccessionPickerPage, AccessionPickerPageViewModel>();
            containerRegistry.RegisterForNavigation<LocalInventoryActionsPage, LocalInventoryActionsPageViewModel>();
            containerRegistry.RegisterForNavigation<AddInventoryActionLocalPage, AddInventoryActionLocalPageViewModel>();
            containerRegistry.RegisterForNavigation<LocalDataManagerPage, LocalDataManagerPageViewModel>();
            containerRegistry.RegisterForNavigation<StorageInventoryPage, StorageInventoryPageViewModel>();
        }
    }
}
