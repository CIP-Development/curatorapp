﻿using InventoryApp.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace InventoryApp.Interfaces
{
    public interface IDataStoreService
    {
        List<ILookup> GetCodeValueList(string groupName);
        bool SyncCodeValueLookup();

        List<ILookup> GetAccessionLookupList(string accessionNumber);
        bool SyncAccessionLookup();

        ObservableCollection<WrappedSelection<InventoryThumbnail>> InventoryList { get; set; }
        
        #region GroupSetting
        List<GroupSetting> GroupSettingList { get; set; }
        Task<bool> RefreshGroupSettings();
        GroupSetting GetSetting(int groupId, string dataview, string dataviewColumn);
        #endregion

        #region AppLangResource
        List<AppLangResource> AppLangResourceList{ get; set; }
        bool RefreshAppLangResources();
        Task<bool> SyncAppLangResourceList();
        string GetAppLangResourceDisplayMember(int langId, string formName, string resourceName);
        #endregion

        #region AppSetting
        List<AppSetting> AppSettingList { get; set; }
        Task<bool> SyncAppSettingList();
        AppSetting GetAppSettingsDisplayMember(int langId, string settingName);
        #endregion
    }
}
