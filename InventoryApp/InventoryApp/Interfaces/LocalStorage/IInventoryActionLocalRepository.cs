﻿using InventoryApp.Models.LocalStorage;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InventoryApp.Interfaces
{
    public interface IInventoryActionLocalRepository
    {
        Task<IEnumerable<InventoryActionDb>> GetAllAsync();
        Task<IEnumerable<InventoryActionDb>> GetByInventoryGuidAsync(string inventoryGuid);
        Task<int> InsertAsync(InventoryActionDb inventoryActionDb);
        Task<int> InsertManyAsync(IEnumerable<InventoryActionDb> inventoryActionDbs);
        Task<int> UpdateAsync(InventoryActionDb inventoryActionDb);
        Task<int> DeleteByInventoryGuidAsync(string inventoryGuid);
        Task<int> DeleteByOrderRequestIdAsync(int orderRequestId);
        Task<IEnumerable<InventoryGuidCount>> GetCountsByManyInventoryGuidAsync(string[] inventoryGuids);
        Task<DateTime> GetLastModifiedDate();
        Task<int> PatchInventoryIdAsync(string inventoryGuid, int newInventoryId);
        Task<IEnumerable<InventoryActionDb>> GetPendingByOrderRequestIdAsync(int orderRequestId, DateTime lastSynced);
        Task<int> PatchInventoryActionIdAsync(string guid, int newInventoryActionId);
        Task<IEnumerable<InventoryActionDb>> GetCrossingActionsByOrderRequestAsync(int orderRequestId);
        Task<IEnumerable<InventoryActionDb>> GetCrossingActionsByInventoryGuidAsync(string inventoryGuid);
    }
}
