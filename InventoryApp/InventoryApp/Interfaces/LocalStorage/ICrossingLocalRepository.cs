﻿using InventoryApp.Models.LocalStorage;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InventoryApp.Interfaces
{
    public interface ICrossingLocalRepository
    {
        Task<int> InsertInventoryAsync(CrossingDb InventoryDb);
        Task<int> UpdateInventoryAsync(CrossingDb InventoryDb);
        Task<CrossingDb> GetInventoryByGuidAsync(string guid);
        Task<IEnumerable<CrossingDb>> GetCrossingByOrderRequestIdAsync(int orderRequestId);
        Task<int> DeleteAllAsync();
        Task<IEnumerable<CrossingDb>> GetAllAsync();
        //Task<IEnumerable<int>> GetCountsByAccessionNumberAndCrossType(string accessionNumber, string crossType);
    }
}
