﻿using InventoryApp.Models.LocalStorage;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InventoryApp.Interfaces
{
    public interface IAccessionInvAttachLocalRepository
    {
        Task<IEnumerable<AccessionInvAttachDb>> GetByGuidAsync(string guid);
        Task<int> InsertAsync(AccessionInvAttachDb accessionInvAttachDb);
        Task<IEnumerable<AccessionInvAttachDb>> GetByInventoryGuidAsync(string inventoryGuid);
        Task<IEnumerable<AccessionInvAttachDb>> GetAllAsync();
        Task<IEnumerable<InventoryGuidCount>> GetCountsByManyInventoryGuidAsync(string[] inventoryGuids);
        Task<DateTime> GetLastModifiedDate();
        Task<int> PatchInventoryIdAsync(string inventoryGuid, int newInventoryId);
        Task<IEnumerable<AccessionInvAttachDb>> GetPendingByOrderRequestIdAsync(int orderRequestId, DateTime lastSynced);
        Task<int> PatchAccessionInvAttachIdAsync(string guid, int newAccessionInvAttatchId);
    }
}
