﻿using InventoryApp.Models.LocalStorage;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InventoryApp.Interfaces
{
    public interface IInventoryLocalRepository
    {
        Task<List<InventoryDb>> GetInventoriesAsync();
        Task<List<InventoryDb>> GetInventoriesByOrderRequestIdAsync(int orderRequestId);
        Task<IEnumerable<InventoryDb>> GetMotherInventoriesByOrderRequestIdAsync(int orderRequestId);
        Task<InventoryDb> GetInventoryByInventoryNumberAsync(string inventoryNumber);
        Task<InventoryDb> GetInventoryByInventoryNumberPartsAsync(string inventoryNumberPart1, int inventoryNumberPart2, string inventoryNumberPart3);
        Task<IEnumerable<InventoryDb>> GetByLotAsync(int lot);
        Task<InventoryDb> GetInventoryByGuidAsync(string guid);
        Task<int> InsertInventoryAsync(InventoryDb InventoryDb);
        Task<int> InsertInventoryManyAsync(IEnumerable<InventoryDb> InventoryDbs);
        Task<int> UpdateInventoryAsync(InventoryDb InventoryDb);
        Task<int> PatchQuantityAsync(string guid, decimal quantity, int modifiedBy, DateTime modifiedDate);
        Task<int> DeleteInventoryByIdAsync(int inventoryId);
        Task<int> DeleteInventoryByOrderRequestIdAsync(int orderRequestId);
        Task<int> GetMinorInventoryIdAsync();
        Task<DateTime> GetLastModifiedDate();
        Task<List<InventoryDb>> GetPendingInventoriesByOrderRequestIdAsync(int orderRequestId, DateTime lastSynced);
        Task<int> PatchInventoryIdAsync(string inventoryGuid, int newInventoryId);
        Task<int> PatchParentInventoryIdAsync(string parentInventoryGuid, int parentInventoryId);

        #region crossing
        Task<List<InventoryDb>> GetCrossingInventoriesByOrderRequestIdAsync(int orderRequestId);
        #endregion
    }
}
