﻿using InventoryApp.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InventoryApp.Interfaces
{
    public interface ICodeValueLookupLocalRepository
    {
        Task<IEnumerable<CodeValueLookup>> GetAllAsync();
        Task<IEnumerable<CodeValueLookup>> GetByGroupNameAsync(string groupName);
        //Task<int> InsertAsync(CodeValueLookup codeValueLookup);
        Task<int> InsertManyAsync(IEnumerable<CodeValueLookup> codeValueLookups);
        //Task<int> UpdateAsync(CodeValueLookup codeValueLookup);
        Task<int> DeleteByGroupNameAsync(string groupName);
    }
}
