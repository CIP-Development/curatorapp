﻿using InventoryApp.Models.LocalStorage;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InventoryApp.Interfaces
{
    public interface IInventoryHarvestLocalRepository
    {
        Task<int> InsertInventoryHarvestAsync(InventoryHarvestDb InventoryHarverstDb);
        Task<int> UpdateInventoryHarvestAsync(InventoryHarvestDb InventoryHarverstDb);
        Task<InventoryHarvestDb> GetInventoryHarvestByGuidAsync(string guid);
        Task<IEnumerable<InventoryHarvestDb>> GetInventoryHarvestByOrderRequestIdAsync(int orderRequestId);
        Task<int> DeleteAllAsync();
        Task<IEnumerable<InventoryHarvestDb>> GetAllAsync();
    }
}
