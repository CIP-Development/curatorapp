﻿using InventoryApp.Models.LocalStorage;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InventoryApp.Interfaces
{
    public interface ICropTraitObservationLocalRepository
    {
        Task<int> InsertCropTraitObservationAsync(CropTraitObservationDb cropTraitObservationDb);
        Task<int> InsertCropTraitObservationManyAsync(IEnumerable<CropTraitObservationDb> cropTraitObservationDbs);
        Task<int> UpdateCropTraitObservationAsync(CropTraitObservationDb cropTraitObservationDb);
        Task<CropTraitObservationDb> GetCropTraitObservationByGuidAsync(Guid guid);
        Task<IEnumerable<CropTraitObservationDb>> GetCropTraitObservationByInventoryGuidAsync(string inventoryGuid);
        Task<IEnumerable<CropTraitObservationDb>> GetCropTraitObservationByManyInventoryGuidAsync(string[] inventoryGuids);
        Task<IEnumerable<CropTraitObservationDb>> GetCropTraitObservationByOrderRequestIdAsync(int orderRequestId);
        Task<IEnumerable<CropTraitObservationDb>> GetAllCropTraitObservationAsync();
        Task<int> DeteleAllCropTraitObservationAsync();
        Task<DateTime> GetLastModifiedDate();
        Task<List<CropTraitObservationDb>> GetPendingCropTraitObservationByOrderRequestIdAsync(int orderRequestId, DateTime lastSynced);
        Task<int> PatchInventoryIdAsync(string inventoryGuid, int newInventoryId);
        Task<int> PatchCropTraitObservationIdAsync(Guid guid, int newCropTraitObservationId);
    }
}
