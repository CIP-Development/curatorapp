﻿using InventoryApp.Models;
using InventoryApp.Models.Database;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InventoryApp.Interfaces
{
    public interface IRestService
    {
        #region InventoryViability

        Task<List<InventoryViability>> RefreshInventoryViabilityAsync();
        Task SaveInventoryViability(InventoryViability item, bool isNewItem);
        Task DeleteInventoryViabilityAsync(string id);

        Task<string> CreateInventoryViabilityAsync(InventoryViability item);
        Task<string> UpdateInventoryViabilityAsync(InventoryViability item);
        #endregion

        Task<List<InventoryThumbnail>> SearchInventoryThumbnailAsync(string query, string dataview, string resolver);

        Task<List<InventoryViability>> SearchInventoryViabilityAsync(string query, string dataview, string resolver);
        Task<List<InventoryViabilityRule>> SearchInventoryViabilityRuleAsync(string query, string dataview, string resolver);

        #region InventoryViabilityData
        Task<List<InventoryViabilityData>> SearchInventoryViabilityDataAsync(string query, string dataview, string resolver);
        Task<string> CreateInventoryViabilityDataAsync(InventoryViabilityData item);
        Task<string> UpdateInventoryViabilityDataAsync(InventoryViabilityData item);
        #endregion

        #region Utils

        //Task<List<CooperatorGroup>> GetWorkGroups(int cooperatorId);
        //Task<List<string>> GetAllLocation1List();

        #endregion

        #region OrderRequest
        Task<IEnumerable<OrderRequest>> GetOrderRequests(string[] orderTypeCodes);
        Task<IEnumerable<OrderRequestItem>> GetOrderRequestItems(int orderRequestId);

        Task<OrderRequest> GetOrderRequestAsync(int orderRequestId);
        Task<string> CreateOrderRequestAsync(OrderRequest orderRequest);
        Task<string> UpdateOrderRequestAsync(OrderRequest orderRequest);
        Task<IEnumerable<Inventory>> GetRegenerationInventoriesAsync(int orderRequestId);
        #endregion

        #region OrderRequestAction
        Task<IEnumerable<OrderRequestAction>> GetOrderRequestActionAsync(int orderRequestId);
        Task<string> CreateOrderRequestActionAsync(OrderRequestAction orderRequestAction);
        #endregion

        #region OrderRequestItem
        Task<string> CreateOrderRequestItemAsync(OrderRequestItem orderRequestItem);
        Task<string> UpdateOrderRequestItemAsync(OrderRequestItem orderRequestItem);
        Task DeleteOrderRequestItemAsync(int orderRequestItemId);
        #endregion

        #region OrderRequestItemAction
        Task<string> CreateOrderRequestItemActionAsync(OrderRequestItemAction orderRequestItemAction);
        //Task<string> UpdateOrderRequestItemActionAsync(OrderRequestItemAction orderRequestItemAction);
        Task<IEnumerable<OrderRequestItemAction>> GetOrderRequestItemActionAsync(int[] orderRequestItemIds);
        Task<OrderRequestItemAction> GetOrderRequestItemActionByInventoryViabilityIdAsync(int inventoryViabilityId);
        #endregion

        #region Cooperator

        Task<IEnumerable<Lookup>> GetCooperatorLookupByWorkGroup(int? workgroupId);

        #endregion

        #region Inventory
        Task<IEnumerable<Inventory>> GetInventoriesByAccessionsAsync(int[] accessionIds);
        Task<Inventory> GetInventoryAsync(int inventoryId);
        Task<string> CreateInventoryAsync(Inventory inventory);
        Task<string> UpdateInventoryAsync(Inventory item);
        #endregion

        #region Method
        Task<IEnumerable<Lookup>> GetMethodLookupByStudyReasonCodeAsync(string studyReasonCode);
        #endregion

        #region CropTraitObservation
        Task<string> CreateCropTraitObservationAsync(CropTraitObservationInsertDto cropTraitObservation);
        Task<string> UpdateCropTraitObservationAsync(CropTraitObservationInsertDto cropTraitObservation);

        Task<IEnumerable<CropTraitObservation>> GetCropTraitObservationByInventoriesAndCropTraitsAsync(string inventoryIds, string cropTraitIds, int methodId = 1);
        Task<IEnumerable<CropTraitObservation>> GetCropTraitObservationByOrderRequestIdAsync(int regenerationOrderRequestId);
        #endregion

        #region InventoryAction
        Task<IEnumerable<InventoryAction>> GetInventoryActionsByInventoryIdAsync(int inventoryId);
        Task<string> CreateInventoryActionAsync(InventoryAction inventoryAction);
        Task<string> UpdateInventoryActionAsync(InventoryAction inventoryAction);
        Task<IEnumerable<InventoryAction>> GetInventoryActionsByInventoryIdAndActionCodesAsync(int inventoryId, string[] actionCodes);
        #endregion

        #region CodeValue
        Task<List<CodeValueLookup>> GetCodeValueLookupList(DateTime modifiedDate);
        Task<IEnumerable<CodeValueLookup>> GetCodeValueLookupList(string groupName);
        Task<IEnumerable<CodeValueMap>> GetCodeValueLookupMapList(int langId);
        #endregion 

        Task<IEnumerable<string>> GetInventoryStorageLocationPart4Distinct();
    }
}
