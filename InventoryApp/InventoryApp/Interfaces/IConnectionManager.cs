﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Zebra.Sdk.Comm;
using Zebra.Sdk.Printer.Discovery;

namespace InventoryApp.Interfaces
{
    public interface IConnectionManager
    {
        string BuildBluetoothConnectionChannelsString(string macAddress);

        void FindBluetoothPrinters(DiscoveryHandler discoveryHandler);

        Connection GetBluetoothConnection(string macAddress);

        StatusConnection GetBluetoothStatusConnection(string macAddress);

        MultichannelConnection GetMultichannelBluetoothConnection(string macAddress);
        Task<IEnumerable<DiscoveredPrinter>> GetPairedBluetoothDevices();
    }
}
