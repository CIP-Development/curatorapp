﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryApp.Models.LocalStorage
{
    [Table("inventory")]
    public class InventoryDb
    {
        public int InventoryId { get; set; }
        public string InventoryNumberPart1 { get; set; }
        public int? InventoryNumberPart2 { get; set; }
        public string InventoryNumberPart3 { get; set; }

        public string FormTypeCode { get; set; }
        public int InventoryMaintPolicyId { get; set; }
        public string IsDistributable { get; set; }

        public string StorageLocationPart1 { get; set; }
        public string StorageLocationPart2 { get; set; }
        public string StorageLocationPart3 { get; set; }
        public string StorageLocationPart4 { get; set; }
        public string IsAvailable { get; set; }
        public string AvailabilityStatusCode { get; set; }
        public decimal? QuantityOnHand { get; set; }
        public string QuantityOnHandUnitCode { get; set; }
        public string IsAutoDeducted { get; set; }
        public int AccessionId { get; set; }
        public int? ParentInventoryId { get; set; }
        public DateTime? PropagationDate { get; set; }
        public string PropagationDateCode { get; set; }
        public string PollinationMethodCode { get; set; }
        public string PollinationVectorCode { get; set; }
        public string Note { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime OwnedDate { get; set; }
        public int OwnedBy { get; set; }

        public int? OrderRequestId { get; set; }
        [PrimaryKey]
        public string Guid { get; set; }
        public string AccessionNumber { get; set; }
        public string InventoryNumber { get; set; }
        public string ContainerCode { get; set; }
        public bool IsMother { get; set; }
        public string Container { get; set; }
        public string FormType { get; set; }
        public string QuantityOnHandUnit { get; set; }
        public string CollectingNumber { get; set; }
        public string Doi { get; set; }
        public string TaxonomySpeciesCode { get; set; }
        public string TaxonomySpeciesName { get; set; }
        public string AccessionName { get; set; }
        public string ParentInventoryGuid { get; set; }
        public string ParentAccessionNumber { get; set; }

        public string this[string propertyName]
        {
            get
            {
                //var nameParts = propertyName.Split(new char[] {'_'}, StringSplitOptions.RemoveEmptyEntries);
                //var bufferPropertyName = new StringBuilder();
                //for (int i = 0; i < nameParts.Length; i++)
                //{
                //    bufferPropertyName.Append(char.ToUpper(nameParts[i][0]));
                //    if (nameParts[i].Length > 1)
                //    {
                //        bufferPropertyName.Append(nameParts[i].Substring(1));
                //    }
                //}
                //var formattedPropertyName = bufferPropertyName.ToString();

                switch (propertyName)
                {
                    case "accession_number": return AccessionNumber;
                    case "inventory_number": return InventoryNumber;
                    case "collecting_number": return CollectingNumber;
                    case "doi": return Doi;
                    case "taxonomy_species_code": return TaxonomySpeciesCode;
                    case "propagation_date": return PropagationDate == null ? string.Empty : PropagationDate.Value.ToString("yyyyMM");
                    case "inventory_id": return InventoryId.ToString();
                    case "inventory_number_part2": return InventoryNumberPart2.ToString();
                    case "pollination_method_code": return PollinationMethodCode;
                    case "guid": return Guid;
                    default: return string.Empty;
                }
            }
        }
        public string ClassName { get => "inventory"; }
        [Ignore]
        public Dictionary<string, string> Extensions { get; set; } = new Dictionary<string, string>();
    }
}
