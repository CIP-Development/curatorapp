﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryApp.Models.LocalStorage
{
    [Table("inventory_action")]
    public class InventoryActionDb
    {
        public int InventoryActionId { get; set; }
        public int InventoryId { get; set; }
        public string ActionNameCode { get; set; }
        public DateTime ActionDate { get; set; }
        public string ActionDateCode { get; set; }
        public decimal? Quantity { get; set; }
        public string QuantityUnitCode { get; set; }
        public string FormCode { get; set; }
        public int? CooperatorId { get; set; }
        public int? MethodId { get; set; }
        public string Note { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime OwnedDate { get; set; }
        public int OwnedBy { get; set; }

        public string InventoryNumber { get; set; }
        public string ActionName { get; set; }
        public string QuantityUnit { get; set; }
        public string CooperatorName { get; set; }
        public string MethodName { get; set; }
        [PrimaryKey]
        public string Guid { get; set; }
        public string InventoryGuid { get; set; }
        public string Form { get; set; }
    }
}
