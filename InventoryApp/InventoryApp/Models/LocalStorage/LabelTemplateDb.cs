﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InventoryApp.Models.LocalStorage
{
    public class LabelTemplateDb
    {
        public string TemplateCode { get; set; }
        public string TemplateName { get; set; }
        public string Zpl { get; set; }
        public IEnumerable<string> ParameterKeys { get; set; }
    }

    public static class LabelTemplateDbFactory
    {
        public static IEnumerable<LabelTemplateDb> LabelTemplateList => new List<LabelTemplateDb>()
        {
//            new LabelTemplateDb
//            {
//                TemplateCode = "PLATE",
//                TemplateName = "Placa",
//                Zpl = @"
//^XA
//^MMT
//^PW440
//^LL0208
//^LS0
//^FT25,40^A0N,37,36^FH\^FD{1}^FS
//^FT25,60^AAN,18,10^FH\^FD{2}^FS
//^FT25,90^A0N,23,24^FH\^FD{3}^FS
//^BY60,60^FT25,155^BXN,6,200,0,0,1,~
//^FH\^FD{4}^FS
//^FT25,175^A0N,16,16^FH\^FD{4}^FS
//^FT100,115^A0N,16,16^FH\^FD{5}^FS
//^FT100,135^A0N,16,16^FH\^FD{6}^FS
//^FT100,155^A0N,16,16^FH\^FD{7}^FS
//^PQ{0},0,1,Y^XZ",
//                ParameterKeys = Enumerable.Empty<string>()
//            },
            new LabelTemplateDb
            {
                TemplateCode = "GENERAL",
                TemplateName = "Etiqueta general de siembra y transplante",
                Zpl = @"
^XA
^MMT
^PW440
^LL0207
^LS0
^FT25,40^A0N,28,28^FH\^FD{1}^FS
^FT25,80^A0N,28,28^FH\^FD{2}^FS
^FT250,160^BXN,6,200
^FH\^FD{5}^FS
^FT25,120^A0N,28,28^FH\^FD{3}^FS
^FT25,160^A0N,28,28^FH\^FD{4}^FS
^PQ{0},0,1,Y^XZ",
                ParameterKeys = new List<string>{ "CopiesNro","AccessionNumber", "SpeciesCode", "Locality", "Date", "Barcode" } },
            
            new LabelTemplateDb
            {
                TemplateCode = "CROSSING",
                TemplateName = "Etiqueta de cruzamiento",
                Zpl = @"
^XA
^MMT
^PW406
^LL0207
^LS0
^FT25,40^A0N,28,28^FH\^FD{1}^FS
^FT25,75^A0N,28,28^FH\^FD{2}^FS
^FT25,110^A0N,28,28^FH\^FDT: {3}^FS
^FT25,145^A0N,28,28^FH\^FD{4}^FS
^FT25,180^A0N,28,28^FH\^FD{5}^FS
^FT250,185^BXN,6,200
^FH\^FD{6}^FS
^PQ{0},0,1,Y^XZ",
                ParameterKeys = new List<string>{ "CopiesNro"} },
            new LabelTemplateDb
            {
                TemplateCode = "HARVEST-FRUITS",
                TemplateName = "Etiqueta de cosecha de frutos",
                Zpl = @"
^XA
^MMT
^PW406
^LL0207
^LS0
^FT25,40^A0N,28,28^FH\^FD{1}^FS
^FT25,75^A0N,28,28^FH\^FD{2}^FS
^FT25,110^A0N,28,28^FH\^FD{3}^FS
^FT25,145^A0N,28,28^FH\^FD{4}^FS
^FT25,180^A0N,28,28^FH\^FDTF: {5}^FS
^FT125,180^A0N,28,28^FH\^FDTB: {6}^FS
^FT250,185^BXN,6,200
^FH\^FD{7}^FS
^PQ{0},0,1,Y^XZ",
                ParameterKeys = new List<string>{ "CopiesNro"} },
            new LabelTemplateDb
            {
                TemplateCode = "HARVEST",
                TemplateName = "Etiqueta de cosecha de tubérculos",
                Zpl = @"
^XA
^MMT
^PW406
^LL0207
^LS0
^FT25,40^A0N,28,28^FH\^FD{1}^FS
^FT25,75^A0N,28,28^FH\^FDP: {2}^FS
^FT125,75^A0N,28,28^FH\^FDT: {3}^FS
^FT25,110^A0N,28,28^FH\^FDPeso: {4}^FS
^FT25,145^A0N,28,28^FH\^FD{5}^FS
^FT25,180^A0N,28,28^FH\^FD{6}^FS
^FT250,40^A0N,28,28^FH\^FDProc: {7}^FS
^FT250,185^BXN,6,200
^FH\^FD{8}^FS
^PQ{0},0,1,Y^XZ",
                ParameterKeys = new List<string>{ "CopiesNro"} },
//            new LabelTemplateDb
//            {
//                TemplateCode = "POT",
//                TemplateName = "Maceta",
//                Zpl = @"
//^XA
//^MMT
//^PW440
//^LL0207
//^LS0
//^FT25,40^A0N,28,28^FH\^FD{1}^FS
//^FT25,80^A0N,28,28^FH\^FD{2}^FS
//^FT255,180^BQN,2,7
//^FH\^FDLA,{6}^FS
//^FT25,120^A0N,28,28^FH\^FD{3}^FS
//^FT25,160^A0N,28,28^FH\^FD{4}^FS
//^FT200,160^A0N,28,28^FH\^FD{5}^FS
//^PQ{0},0,1,Y^XZ",
//                ParameterKeys = new List<string>{ "AccessionNumber", "SpeciesCode", "Locality", "Date", "BrotherNumber", "Barcode" } },
        };
    }
}
