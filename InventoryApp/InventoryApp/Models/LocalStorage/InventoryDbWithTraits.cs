﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryApp.Models.LocalStorage
{
    public class InventoryDbWithTraits : BindableBase
    {
        private InventoryDb _inventoryDb;
        public InventoryDb InventoryDb
        {
            get { return _inventoryDb; }
            set { SetProperty(ref _inventoryDb, value); }
        }

        private Dictionary<int, CropTraitObservation> _CropTraitObservations = new Dictionary<int, CropTraitObservation>();
        public Dictionary<int, CropTraitObservation> CropTraitObservations
        {
            get { return _CropTraitObservations; }
            set { SetProperty(ref _CropTraitObservations, value); }
        }
        private int _inventoryImageCount;
        public int InventoryImageCount
        {
            get { return _inventoryImageCount; }
            set { SetProperty(ref _inventoryImageCount, value); }
        }
        private int _inventoryActionCount;
        public int InventoryActionCount
        {
            get { return _inventoryActionCount; }
            set { SetProperty(ref _inventoryActionCount, value); }
        }
    }
}
