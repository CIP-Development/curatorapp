﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryApp.Models.LocalStorage
{
    [Table("crossing")]
    public class CrossingDb : Prism.Mvvm.BindableBase
    {
        public int InventoryId { get; set; }
        public string CrossingName { get; set; }
        public int FemaleOrder { get; set; }
        public int MaleOrder { get; set; }
        public int? PollinatedFlowers { get; set; }
        public string Locality { get; set; }
        public DateTime? PrintedDate { get; set; }
        public DateTime? CrossingDate { get; set; }
        public int? CooperatorId { get; set; }
        public string CrossingTypeCode { get; set; }
        public int? FemaleInventoryId { get; set; }
        //public int? ParentInventoryId { get; set; }
        //public int? SourceInventoryId { get; set; }
        public int OrderRequestId { get; set; }

        #region Harvest
        public int? HarvestPollinatedFlowers { get; set; }
        //public DateTime? HarvestPrintedDate { get; set; }
        private int? _harvestedFruits;
        public int? HarvestedFruits
        {
            get { return _harvestedFruits; }
            set { SetProperty(ref _harvestedFruits, value); }
        }
        //public DateTime? HarvestDate { get; set; }
        private DateTime? _harvestDate;
        public DateTime? HarvestDate
        {
            get { return _harvestDate; }
            set { SetProperty(ref _harvestDate, value); }
        }
        public int HarvestCooperatorId { get; set; }
        public string HarvestCooperator { get; set; }
        public string HarvestSummary { get; set; }
        public string HarvestLocality { get; set; }
        #endregion

        #region HarvestTubers
        public int? TubersCount { get; set; }
        public string TuberOrigin { get; set; }
        public DateTime? TuberHarvestDate { get; set; }
        public string TuberHaverstLocality { get; set; }
        public int? TuberHarvestCooperatorId { get; set; }
        public string TuberHarvestCooperator { get; set; }

        #endregion

        #region Maceration
        public int? MacerationCooperatorId { get; set; }
        public string MacerationCooperator { get; set; }
        public DateTime? MacerationDate { get; set; }
        #endregion

        [PrimaryKey]
        public string Guid { get; set; }
        public string FemaleAccessionNumber { get; set; }
        //public string MaleAccessionNumber { get; set; }
        public string CooperatorName { get; set; }
        public string CrossingType { get; set; }
        public string FemaleParentGuid { get; set; }
        //public string MaleParentGuid { get; set; }

        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }
}
