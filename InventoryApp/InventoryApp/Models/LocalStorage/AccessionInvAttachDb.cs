﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryApp.Models.LocalStorage
{
    [Table("accession_inv_attach")]
    public class AccessionInvAttachDb
    {
        public int AccessionInvAttachId { get; set; }
        public int InventoryId { get; set; }
        public string VirtualPath { get; set; }
        public string ThumbnailVirtualPath { get; set; }
        //public int? sort_order { get; set; }
        public string Title { get; set; }
        //public string description { get; set; }
        //public string description_code { get; set; }
        //public string content_type { get; set; }
        public string CategoryCode { get; set; } //not-null
        //public string copyright_information { get; set; }
        public int? AttachCooperatorId { get; set; }
        public string IsWebVisible { get; set; } //not-null
        public DateTime AttachDate { get; set; }
        public string AttachDateCode { get; set; }
        public string Note { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; } //not-null
        public DateTime ModifiedDate { get; set; }
        public int ModifiedBy { get; set; }
        public int OwnedBy { get; set; }

        [PrimaryKey]
        public string Guid { get; set; }
        public string InventoryGuid { get; set; }
    }
}
