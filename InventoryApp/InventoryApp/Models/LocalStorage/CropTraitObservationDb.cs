﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryApp.Models.LocalStorage
{
    [Table("crop_trait_observation")]
    public class CropTraitObservationDb
    {
        public int CropTraitObservationId { get; set; }
        public int InventoryId { get; set; }
        public int CropTraitId { get; set; }
        public int? CropTraitCodeId { get; set; }
        public string Code { get; set; }
        public decimal? NumericValue { get; set; }
        public string StringValue { get; set; }
        public string DisplayText { get; set; }
        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedBy { get; set; }

        [PrimaryKey]
        public Guid Guid { get; set; }
        public string InventoryGuid { get; set; }
    }
}
