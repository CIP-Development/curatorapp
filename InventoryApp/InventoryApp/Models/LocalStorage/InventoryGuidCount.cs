﻿using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryApp.Models.LocalStorage
{
    public class InventoryGuidCount
    {
        public string InventoryGuid { get; set; }
        public int Count { get; set; }
    }
}
