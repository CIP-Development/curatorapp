﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryApp.Models
{
    public class BarcodeFormat
    {
        [JsonProperty("barcode_code")]
        public string Code { get; set; }
        [JsonProperty("barcode_title")]
        public string Title { get; set; }
        [JsonProperty("barcode_format")]
        public string Format { get; set; }
        [JsonProperty("barcode_query")]
        public string Query { get; set; }
        [JsonProperty("dataview_query")]
        public string QueryDataview { get; set; }
        [JsonProperty("dataview_parameters")]
        public string QueryDataviewParameters { get; set; }

    }
}
