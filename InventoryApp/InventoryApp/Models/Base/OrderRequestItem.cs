﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryApp.Models
{
    public class OrderRequestItem : BindableBase
    {
		public int order_request_item_id { get; set; }
		public int order_request_id { get; set; }
		public int? web_order_request_item_id { get; set; }
		public int? sequence_number { get; set; }
		//public string name { get; set; }
        private string _name;
        public string name
		{
            get { return _name; }
            set { SetProperty(ref _name, value); }
        }
        public decimal? quantity_shipped { get; set; }
		public string quantity_shipped_unit_code { get; set; }
		public string distribution_form_code { get; set; }
		public string status_code { get; set; }
		public DateTime? status_date { get; set; }
		//public int inventory_id { get; set; }
        private int _inventory_id;
        public int inventory_id
		{
            get { return _inventory_id; }
            set { SetProperty(ref _inventory_id, value); }
        }
        public string external_taxonomy { get; set; }
		public int? source_cooperator_id { get; set; }
		public string note { get; set; }
		public string web_user_note { get; set; }
		public DateTime created_date { get; set; }
		public int created_by { get; set; }
		public DateTime? modified_date { get; set; }
		public int? modified_by { get; set; }
		public DateTime owned_date { get; set; }
		public int owned_by { get; set; }

        //public decimal? quantity_on_hand { get; set; }
        private decimal? _quantity_on_hand;
        public decimal? quantity_on_hand
        {
            get { return _quantity_on_hand; }
            set { SetProperty(ref _quantity_on_hand, value); }
        }
        //public string quantity_on_hand_unit_code { get; set; }
        private string _quantity_on_hand_unit_code;
        public string quantity_on_hand_unit_code
        {
            get { return _quantity_on_hand_unit_code; }
            set { SetProperty(ref _quantity_on_hand_unit_code, value); }
        }
        public char is_distributable { get; set; }
        public string distribution_default_form_code { get; set; }
        public decimal? distribution_default_quantity { get; set; }
        public string distribution_unit_code { get; set; }
        public string storage_location_part1 { get; set; }
		public string storage_location_part2 { get; set; }
		public string storage_location_part3 { get; set; }
		//public string storage_location_part4 { get; set; }
        private string _storage_location_part4;
        public string storage_location_part4
        {
            get { return _storage_location_part4; }
            set
            {
                SetProperty(ref _storage_location_part4, value);
                RaisePropertyChanged(nameof(storage_location));
            }
        }
        public string plant_name { get; set; }
		public string storage_location { get { return $"{storage_location_part1}\\{storage_location_part2}\\{storage_location_part3}\\{storage_location_part4}"; } }
        public string order_request_item_action_name { get; set; }
        public DateTime? order_request_item_action_date { get; set; }
        public int accession_id { get; set; }
        public int? workgroup_cooperator_id { get; set; }
        public string distribution_form { get; set; }
        public string quantity_on_hand_unit { get; set; }
        public string quantity_shipped_unit { get; set; }
        public string inventory_number { get; set; }
        public string accession_number { get; set; }
        public string collecting_number { get; set; }
        public string accession_name { get; set; }
        public string taxonomy_species_name { get; set; }
    }
}
