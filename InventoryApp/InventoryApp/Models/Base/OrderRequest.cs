﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryApp.Models
{
    public class OrderRequest : BindableBase
    {
		public int order_request_id { get; set; }
		public int? original_order_request_id { get; set; }
		public int? web_order_request_id { get; set; }
		public string local_number { get; set; }
		public string order_type_code { get; set; }
		public DateTime? ordered_date { get; set; }
		public string intended_use_code { get; set; }
		public string intended_use_note { get; set; }
		public DateTime? completed_date { get; set; }
		public int? requestor_cooperator_id { get; set; }
		public int? ship_to_cooperator_id { get; set; }
		public int final_recipient_cooperator_id { get; set; }
		public string order_obtained_via { get; set; }
		public int? feedback_id { get; set; }
		public string special_instruction { get; set; }
		public string note { get; set; }
		public DateTime created_date { get; set; }
		public int created_by { get; set; }
		public DateTime? modified_date { get; set; }
		public int? modified_by { get; set; }
		public DateTime owned_date { get; set; }
		public int owned_by { get; set; }
		public string carrier { get; set; }
		public int? agreement_type { get; set; }

        private string _final_recipient_cooperator_name;
        public string final_recipient_cooperator_name
		{
            get { return _final_recipient_cooperator_name; }
            set { SetProperty(ref _final_recipient_cooperator_name, value); }
        }
        private string _ship_to_cooperator_name;
        public string ship_to_cooperator_name
		{
            get { return _ship_to_cooperator_name; }
            set { SetProperty(ref _ship_to_cooperator_name, value); }
        }
        private string _requestor_cooperator_name;
        public string requestor_cooperator_name
		{
            get { return _requestor_cooperator_name; }
            set { SetProperty(ref _requestor_cooperator_name, value); }
        }
		public string owner_name { get; set; }
        public string order_type { get; set; }
        public string intended_use { get; set; }
		public int? workgroup_cooperator_id { get; set; }
        public DateTime? last_synced_date { get; set; }
        public int order_request_item_count { get; set; }
    }
}
