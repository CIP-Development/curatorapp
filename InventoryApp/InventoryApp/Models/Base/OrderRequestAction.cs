﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryApp.Models
{
    public class OrderRequestAction : BindableBase
    {
		public int order_request_action_id { get; set; }
		public int order_request_id { get; set; }
		public string action_name_code { get; set; }
		public DateTime started_date { get; set; }
		public string started_date_code { get; set; }
		public DateTime? completed_date { get; set; }
		public string completed_date_code { get; set; }
		public string action_information { get; set; }
		public decimal? action_cost { get; set; }
		public int? cooperator_id { get; set; }
		public string note { get; set; }
		public DateTime created_date { get; set; }
		public int created_by { get; set; }
		public DateTime? modified_date { get; set; }
		public int? modified_by { get; set; }
		public DateTime owned_date { get; set; }
		public int owned_by { get; set; }

        public string order_request_action_name { get; set; }
        private string _cooperator_name;
        public string cooperator_name
		{
            get { return _cooperator_name; }
            set { SetProperty(ref _cooperator_name, value); }
        }
    }
}
