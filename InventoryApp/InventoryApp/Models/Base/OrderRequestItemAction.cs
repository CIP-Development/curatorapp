﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryApp.Models
{
    public class OrderRequestItemAction : BindableBase
    {
        public int order_request_item_action_id { get; set; }
        public int order_request_item_id { get; set; }
        //public int action_id { get; set; }
        public int cooperator_id { get; set; }
        //public DateTime action_date { get; set; }
        public string note { get; set; }
        public string action_name_code { get; set; }
        public DateTime? started_date { get; set; }
        public string started_date_code { get; set; }
        public DateTime? completed_date { get; set; }
        public string completed_date_code { get; set; }
        public string action_information { get; set; }
        public decimal? action_cost { get; set; }
        public int owned_by { get; set; }
        public DateTime owned_date { get; set; }

        private string _cooperator_name;
        public string cooperator_name
        {
            get { return _cooperator_name; }
            set { SetProperty(ref _cooperator_name, value); }
        }
        public string order_request_item_action_name { get; set; }
        public string inventory_number { get; set; }
        public int inventory_id { get; set; }

    }
}
