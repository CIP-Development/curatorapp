﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryApp.Models
{
    public class CodeValue
    {
        [JsonProperty("value_member")]
        public string Code { get; set; }
        [JsonProperty("display_member")]
        public string Value { get; set; }
        public CodeValue()
        {
        }
        public CodeValue(string code, string value)
        {
            Code = code;
            Value = value;
        }
    }

    public class CodeValueMap
    {
        [JsonProperty("parent_group_name")]
        public string ParentGroupName { get; set; }
        [JsonProperty("parent_value")]
        public string ParentCode { get; set; }
        [JsonProperty("value_member")]
        public string Code { get; set; }
        [JsonProperty("display_member")]
        public string Value { get; set; }
    }

    public static class CodeValueFactory
    {
        public static List<string> RegenerationLocationList = new List<string>
        {
            "Lima",
            "Huancayo",
            "San Ramón",
        };
        static List<CodeValue> _methodList = new List<CodeValue> {
            new CodeValue( "2", "Regeneration"),
            new CodeValue( "3", "Distribution"),
            new CodeValue( "4", "Correct Errors (daily work)"),
            new CodeValue( "5", "Inventory Adjustments (annual activity) "),
            new CodeValue( "6", "Other"),
            new CodeValue( "7", "Safety Duplicates International"),
            new CodeValue( "8", "DNA for Conservation"),
            new CodeValue( "9", "Svalbar")
        };
        static List<CodeValue> _actionNameCodeList = new List<CodeValue>{
            new CodeValue( "INCREASE", "Increase"),
            new CodeValue( "DISCOUNT", "Discount")
        };
        public static List<CodeValue> OrderIntendedUseList = new List<CodeValue>
        {
            // ORDER_INTENDED_USE
            //new CodeValue("1","Agriculture"),
            //new CodeValue("2","Breeding"),
            //new CodeValue("3","Commercial Sector"),
            //new CodeValue("4","Education"),
            //new CodeValue("5","Research"),
            // ORDER_INTENDED_USE_CIP
            new CodeValue("20","Identity verification"),
            new CodeValue("21","Regeneration/Viability testing"),
            new CodeValue("22","Characterization"),
            new CodeValue("23","Cryopreservation"),
            new CodeValue("24","Phytosanitary"),
            new CodeValue("26","Safety Backup"),
            new CodeValue("27","Repatriation"),
            new CodeValue("28","Research in Genebank"),
            new CodeValue("77","Rejuvenation"),
        };
        public static List<CodeValue> OrderRequestTypeList = new List<CodeValue>
        {
            new CodeValue("BA","Backup"),
            new CodeValue("DI","Distribution"),
            new CodeValue("GI","Germplasm introduction"),
            new CodeValue("GR","Germination"),
            new CodeValue("HE","Herbarium/reidentification"),
            new CodeValue("IO","Information only"),
            new CodeValue("NR","Non-research, non-educational"),
            new CodeValue("OB","Observation/evaluation"),
            new CodeValue("PT","Phytosanitary Testing"),
            new CodeValue("RE","Replenishment/regrow"),
            new CodeValue("RP","Repatriation"),
            new CodeValue("TR","Transfer"),
        };
        public static List<CodeValue> RegenerationOrderRequestTypeList = new List<CodeValue>
        {
            //new CodeValue("GR","Germination"),
            //new CodeValue("RE","Replenishment/regrow"),
            new CodeValue("GR","Prueba de viabilidad"),
            new CodeValue("RE","Regeneración"),
        };
        public static List<CodeValue> FormTypeList = new List<CodeValue>
        {
            //new CodeValue("**","**"),
            //new CodeValue("DN","ADN"),
            new CodeValue("BE","Baya"),
            new CodeValue("SP","Brote"),
            //new CodeValue("CR","Criopreservado"),
            new CodeValue("CT","Esqueje"),
            //new CodeValue("SH","Folio"),
            //new CodeValue("FO","Follaje"),
            //new CodeValue("FD","Liofilizado"),
            //new CodeValue("HE","Muestra de herbario"),
            //new CodeValue("HS","Semilla híbrida"),
            new CodeValue("IV","In vitro"),
            //new CodeValue("IO","Sólo la información"),
            //new CodeValue("LV","Hojas"),
            //new CodeValue("PI","Pistilos"),
            new CodeValue("PL","Planta"),
            //new CodeValue("PO","Polen"),
            //new CodeValue("RN","ARN"),
            //new CodeValue("RT","Rizoma"),
            new CodeValue("SD","Semilla"),
            //new CodeValue("SA","Starch"),
            //new CodeValue("LP","Tejido liofilizada"),
            new CodeValue("TU","Tubérculo"),
            new CodeValue("SL","Plántula"),
        };
        public static List<CodeValue> InventoryContainerList = new List<CodeValue>
        {
            new CodeValue("OTHER","Otro"),
            new CodeValue("PLATE","Placa"),
            new CodeValue("TRAY","Bandeja"),
            new CodeValue("JIFFY","Jiffy"),
            new CodeValue("POT","Maceta"),
        };
        public static List<CodeValue> CrossingTypeList = new List<CodeValue>
        {
            new CodeValue("SC","Sib-cross"),
            new CodeValue("AP","Autofecundación"),
            new CodeValue("BK","Bulk"),
            new CodeValue("LP","Libre polinización"),
        };


        public static List<CodeValue> OrderRequestActionList = new List<CodeValue>
        {
            new CodeValue("CANCEL","Cancel or abort order"),
            new CodeValue("CURALERTED","Curator alerted about order"),
            new CodeValue("CURATOR","Curator assigned"),
            new CodeValue("CURCLEARED","Curator cleared an order"),
            new CodeValue("CURWAIPROP","Order waiting for IP expiration"),
            new CodeValue("CURWAREGEN","Order waiting for regeneration"),
            new CodeValue("DONE","Completed order"),
            new CodeValue("ESSREQUEST","Endangered Species Statement requirement"),
            new CodeValue("FORWARD","Forward order to another site"),
            new CodeValue("GPAALERTED","Curator alerted Specialist on order"),
            new CodeValue("HOLD","Hold order pending action"),
            new CodeValue("IMPORT_PER","Import Permit requested"),
            new CodeValue("IMPPMTREQ","Requestor notified of need for permit"),
            new CodeValue("INSPECT","Order sent to APHIS"),
            new CodeValue("INSPECTASKED","Export requirements requested"),
            new CodeValue("INSPECTSITE","Site phyto inspection needed"),
            new CodeValue("NEW","New Order"),
            new CodeValue("ORDFILLED","Order filled ready to ship"),
            new CodeValue("PENDING","Order pending"),
            new CodeValue("PSHIP","Partial shipment"),
            new CodeValue("QUALITYPASSED","Quality test passed"),
            new CodeValue("QUALITYTEST","Quality test needed and sent"),
            new CodeValue("RECEIVED","Order was received by recipient"),
            new CodeValue("REQASKED","Requestor solicited for addional info"),
            new CodeValue("RETURNED","Returned order"),
            new CodeValue("SHIPPED","Order shipped"),
            new CodeValue("SMTAACCEPT","Documentation available, SMTA accepted"),
            new CodeValue("SMTASTATUS","SMTA status"),
            new CodeValue("SPLIT","Order split into sub-orders"),
        };
        public static List<CodeValue> OrderRequestItemActionList = new List<CodeValue>
        {
            //new CodeValue("OUT_STORAGE","Retrieve from storage"),
            //new CodeValue("PACK","Pack"),
            //new CodeValue("VERIFY_ITEM","Verifiy item"),
            new CodeValue("OUT_STORAGE","Retirar de cámara de conservación"),
            new CodeValue("PACK","Empaquetar"),
            new CodeValue("VERIFY_ITEM","Verificar item de solicitud"),
        };
        public static List<CodeValue> QuantityUnitList = new List<CodeValue>
        {
            new CodeValue("ct","Conteo"),
            new CodeValue("kc","Conteo x 1000"),
            new CodeValue("cu","Esquejes"),
            new CodeValue("gm","Gramos"),
            new CodeValue("kg","Kilogramos"),
            new CodeValue("ug","Microgramos"),
            new CodeValue("mg","Miligramos"),
            new CodeValue("ml","Mililitros"),
            new CodeValue("ng","Nanogramos"),
            new CodeValue("pk","Paquete"),
            new CodeValue("tb","Tubos"),
            new CodeValue("vl","Vial"),
        };
        public static List<CodeValue> InventoryActionList = new List<CodeValue>
        {
            //new CodeValue("REPLACED",""),
            //new CodeValue("LOWGERM",""),
            //new CodeValue("MOVED",""),
            //new CodeValue("AMESRENUMB",""),
            //new CodeValue("REPROP",""),
            //new CodeValue("DIED",""),
            //new CodeValue("EXHA",""),
            //new CodeValue("NOGERM",""),
            //new CodeValue("DISCARDED",""),
            //new CodeValue("AVAIL",""),
            //new CodeValue("STORE",""),
            //new CodeValue("DESTROYED",""),
            //new CodeValue("IVHSWT","100 seed weight in grams"),
            new CodeValue("QUAR","En cuarentena"),
            new CodeValue("IN TESTING","Actualmente en pruebas de patógenos"),
            //new CodeValue("AcqImp","Adquisicion/Importacion"),
            //new CodeValue("INTO25RH","Almacenado a 10C 25% HR"),
            //new CodeValue("ADDED","Añadido a la muestra de distribución"),
            new CodeValue("INCREASED","Aumentado"),
            //new CodeValue("BACKUP","Backed Up"),
            //new CodeValue("PULLED_BS","Balance sample(s) pulled for growing"),
            new CodeValue("HARVEST-BG","Beginning of harvest"),
            //new CodeValue("DIVIDED","Cloned by division"),
            //new CodeValue("CUTTING_R","Cloned by taking root cuttings"),
            //new CodeValue("CUTTING_S","Cloned by taking shoot cuttings"),
            new CodeValue("COMMENT","Comentario acerca del inventario"),
            //new CodeValue("ORDERCMT","Comment to the requestor"),
            //new CodeValue("SafInt","Copia de Seguridad - Internacional"),
            //new CodeValue("SafNat","Copia de Seguridad - Nacional"),
            //new CodeValue("CorErr","Correcion de errores (trabajo diario)"),
            new CodeValue("HARVESTED","Cosechado"),
            new CodeValue("STANDCOUNT","Count of plants in regeneration plot"),
            //new CodeValue("CTREVIEWED","Count reviewed"),
            //new CodeValue("CREATED","Created from pre-existing lot(s) manually"),
            //new CodeValue("REACTIVATD","Date Accession reactivated"),
            //new CodeValue("UNAVAIL","Date avail lot is first made unavailable"),
            //new CodeValue("LOWGERM_UP","Date distribution increase from germ."),
            //new CodeValue("PREPACKED","Date lot was prepackaged"),
            //new CodeValue("FLOWER_MAL","Date male flowers opened"),
            //new CodeValue("VERNALIN","Date material started vernalization"),
            //new CodeValue("MINUS20","Date placed in minus 20 degrees Cent."),
            //new CodeValue("INTO-18C","Date sample was placed in -18 C storage."),
            //new CodeValue("CAGED","Date the accession was caged"),
            //new CodeValue("WITHDRAWAL","Descuento"),
            //new CodeValue("DISCOUNT","Descuento de cantidad"),
            //new CodeValue("DisLos","Desechar/Perdida"),
            //new CodeValue("DISCARDSDT","Discarded seed type/types"),
            //new CodeValue("DisGer","Distribucion de germoplasma"),
            //new CodeValue("DISTRIBUTE","Distribuido al curador"),
            new CodeValue("SEEDDAMAGE","El campo Comentario ha causado daños de las semillas"),
            new CodeValue("ELISA","Elisa dio positivo. Dato sobre el % en el comentario"),
            new CodeValue("COMBINED","En combinación con otro lote"),
            new CodeValue("POTTED","En macetas"),
            new CodeValue("HARVEST-ED","End of harvest"),
            //new CodeValue("THERAPY","Enviada a terapia"),
            //new CodeValue("REGROW","Enviado para regeneración"),
            //new CodeValue("SVALBARD","Enviados a bóveda de semillas de Svalbard"),
            //new CodeValue("ESTABLISH","Establecido"),
            //new CodeValue("INTO-CRYO","Fecha de entrada de la muestra en criopreservación"),
            //new CodeValue("R","Fecha de liberación. Véase el comentario."),
            //new CodeValue("SITEREC","Fecha de recepción desde otro sitio"),
            //new CodeValue("INTO4C","Fecha en que la muestra se almacenó a 4 grados cent."),
            //new CodeValue("MINUS80","Fecha en que se coloca a menos 80 grados Cent."),
            //new CodeValue("ENDOPHYTE","Found endophyte"),
            //new CodeValue("YIELD10PLT","Gramos de las semillas producidas por 10 plantas"),
            //new CodeValue("GRAMS","Grams on hand at this date"),
            new CodeValue("YIELD","Grams produced from harvest"),
            //new CodeValue("GROWING","Growing at present time"),
            //new CodeValue("SCANNED","Image obtained with scanner"),
            new CodeValue("PRINTED","Impresion de etiqueta"),
            //new CodeValue("DEPOSIT","Incremento"),
            //new CodeValue("INCREASE","Incremento de cantidad"),
            //new CodeValue("GRAFTED","Injertado"),
            new CodeValue("INSPECTED","Inspeccionado"),
            //new CodeValue("AnnInv","Inventario anual"),
            //new CodeValue("FREEZEROUT","Inventario extraído del congelador"),
            //new CodeValue("FREEZER_IN","Inventario ubicado en el congelador"),
            new CodeValue("FLOWERED","Inventory flowered"),
            //new CodeValue("RELINKED","Inventory relinked to another accession"),
            new CodeValue("ONHAND","Inventory seed counts on this date"),
            //new CodeValue("CR","Liberación condicional"),
            //new CodeValue("UR","Liberación Incondicional"),
            //new CodeValue("PR","Liberación provicional"),
            //new CodeValue("RO","Liberado otro subclone de la accesión"),
            //new CodeValue("RELEASED","Liberado para su distribución"),
            new CodeValue("RENUMBERED","Lot number changed"),
            new CodeValue("LOTNUMBER","Lot number of inventory"),
            //new CodeValue("DUPERENUMB","Lot was renumbered due to duplication"),
            new CodeValue("TESTING","Material actualmente patógeno probado"),
            //new CodeValue("VERNALOUT","Material removed from vernalization"),
            new CodeValue("OBSERVED","Mediciones realizadas"),
            //new CodeValue("FOUR_C","Muestra en 4 grados centígrados"),
            new CodeValue("MIXED","Muestra mixta"),
            new CodeValue("NOGROW","No creció, pero germinó"),
            new CodeValue("NOFLOWER","No floreció ni produjo inflorencencias"),
            new CodeValue("NOSEED","No se produjeron semillas a partir de las inflorencias"),
            new CodeValue("NOTE","Nota en el campo de comentario"),
            new CodeValue("BALANCESAM","Number sampled in a balanced sample"),
            new CodeValue("BULKSAMPLE","Number Sampled Unequally"),
            new CodeValue("QUALITYOBS","Observation made on lot quality"),
            //new CodeValue("OthPro","Otros proyectos"),
            new CodeValue("PATHOGEN","Patógenos observados"),
            new CodeValue("PHOTOGRAPH","Fotografiado"),
            //new CodeValue("OVERWINTER","Plantas mantenidas por más de un año"),
            new CodeValue("PLANTED","Se plantó"),
            new CodeValue("POPHARVEST","Population size harvested"),
            //new CodeValue("POPCLEARED","Population size ok despite small"),
            //new CodeValue("RUST","Porcentaje de plantas con roya"),
            //new CodeValue("PHOTOSCAN","Previous photo scanned"),
            //new CodeValue("TESTED","Probado"),
            //new CodeValue("ViaReg","Prueba de viabilidad - inicial"),
            //new CodeValue("ViaMon","Prueba de viabilidad - monitoreo"),
            //new CodeValue("PULLPLANT","Pulled for planting"),
            //new CodeValue("PULLDISCAR","Pulled seed for planting discarded"),
            //new CodeValue("RECEIVEBPS","Receive date of a previous NPGS site"),
            //new CodeValue("RECEIVED","Received"),
            //new CodeValue("RECEIVEBK","Recibido como muestra de seguridad"),
            //new CodeValue("VOUCHERED","Recibo"),
            //new CodeValue("RegCon","Regeneración para conservación"),
            //new CodeValue("INACTIVATD","Removed accession from collection"),
            //new CodeValue("UNISOLATED","Removed from isolation"),
            //new CodeValue("EMBRYO","Rescate de embriones"),
            //new CodeValue("ROGUED","Rogued inventory"),
            new CodeValue("BULKED","Sample bulked with another to a new lot"),
            //new CodeValue("MOISTURED","Se midió el contenido de humedad"),
            //new CodeValue("MUTANT_CT","Se observaron fenotipos inusuales"),
            new CodeValue("ADDITIONAL","Se recibieron más esquejes para su almacenamiento"),
            //new CodeValue("SEEDMIXED","Seed packet has a seed mixture"),
            new CodeValue("CLEANED","Semilla limpiada"),
            //new CodeValue("ARTIC","Shipped to Svalbard Seed Vault in Norway"),
            //new CodeValue("HEAT-TREAT","Sometido a tratamiento térmico"),
            //new CodeValue("STORED","Stored by storage or curatorial staff"),
            //new CodeValue("INNOCULATE","Subclone inoculado (injertado)"),
            new CodeValue("DETILLERED","Tillers removed from plants."),
            new CodeValue("DNA SAMPLE","Tissue collected for DNA analyses"),
            new CodeValue("TRANSFER","Transferido"),
            new CodeValue("INTO-LN2","Transferido desde cultivo de tejidos."),
            //new CodeValue("TRANSFEROT","transferred out"),
            //new CodeValue("TRANSFERIN","Transferred to our station"),
            //new CodeValue("TRANSPLANT","Trasplantado"),
            //new CodeValue("COLD-TREAT","Tratado en frío"),
            //new CodeValue("TREATED","Treated"),
            //new CodeValue("DETREATED","Treatment partially removed"),
            //new CodeValue("CUT-BACK","Vegetation trimmed"),
            //new CodeValue("VERIFIED","Verificado"),
            //new CodeValue("VERIFIEDTX","Verificar taxomony"),
            new CodeValue("ViabTested","Viabilidad probada"),
            new CodeValue("HAR-VOLUME","Volumen de la cosecha en mililitros"),
            new CodeValue("OTHER","Otro"),
        };

        public static List<CodeValue> MethodList { get { return _methodList; } }
        public static List<CodeValue> ActionNameCodeList { get { return _actionNameCodeList; } }

        public static List<string> GetContainerTypeList()
        {
            return new List<string> { "Paper Envelope 8.6x5.8 cm", "Paper Envelope 18.2x10.2 cm", "Aluminium Envelope 8x12 cm", "Aluminium Envelope 21.5x17 cm", "Cardboard Folkote 29x42 cm", "Eppendorf" };
        }

    }
}
