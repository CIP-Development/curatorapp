﻿using Newtonsoft.Json;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryApp.Models
{
    public class InventoryViabilityData : BindableBase
    {
        [JsonProperty("inventory_viability_data_id")]
        public int InventoryViabilityDataId { get; set; }

        [JsonProperty("inventory_viability_id")]
        public int InventoryViabilityId { get; set; }

        [JsonProperty("counter_cooperator_id")]
        public int? CounterCooperatorId { get; set; }

        [JsonProperty("replication_number")]
        public int ReplicationNumber { get; set; }

        [JsonProperty("count_number")]
        public int CountNumber { get; set; }

        private DateTime _countDate;
        [JsonProperty("count_date")]
        public DateTime CountDate
        {
            get { return _countDate; }
            set { SetProperty(ref _countDate, value, () => RaisePropertyChanged(nameof(ShortDate))); }
        }

        private int? _normalCount;
        [JsonProperty("normal_count")]
        public int? NormalCount
        {
            get { return _normalCount; }
            set { SetProperty(ref _normalCount, value); }
        }

        private int? _abnormalCount;
        [JsonProperty("abnormal_count")]
        public int? AbnormalCount
        {
            get { return _abnormalCount; }
            set { SetProperty(ref _abnormalCount, value); }
        }

        private int? _dormant_count;
        [JsonProperty("dormant_count")]
        public int? DormantCount
        {
            get { return _dormant_count; }
            set { SetProperty(ref _dormant_count, value); }
        }
        private int? _hard_count;
        [JsonProperty("hard_count")]
        public int? HardCount
        {
            get { return _hard_count; }
            set { SetProperty(ref _hard_count, value); }
        }
        private int? _empty_count;
        [JsonProperty("empty_count")]
        public int? EmptyCount
        {
            get { return _empty_count; }
            set { SetProperty(ref _empty_count, value); }
        }
        private int? _infested_count;
        [JsonProperty("infested_count")]
        public int? InfestedCount
        {
            get { return _infested_count; }
            set { SetProperty(ref _infested_count, value); }
        }
        private int? _dead_count;
        [JsonProperty("dead_count")]
        public int? DeadCount
        {
            get { return _dead_count; }
            set { SetProperty(ref _dead_count, value); }
        }
        private int? _unknown_count;
        [JsonProperty("unknown_count")]
        public int? UnknownCount
        {
            get { return _unknown_count; }
            set { SetProperty(ref _unknown_count, value); }
        }
        private int? _estimated_dormant_count;
        [JsonProperty("estimated_dormant_count")]
        public int? EstimatedDormantCount
        {
            get { return _estimated_dormant_count; }
            set { SetProperty(ref _estimated_dormant_count, value); }
        }
        private int? _treated_dormant_count;
        [JsonProperty("treated_dormant_count")]
        public int? TreatedDormantCount
        {
            get { return _treated_dormant_count; }
            set { SetProperty(ref _treated_dormant_count, value); }
        }
        private int? _confirmed_dormant_count;
        [JsonProperty("confirmed_dormant_count")]
        public int? ConfirmedDormantCount
        {
            get { return _confirmed_dormant_count; }
            set { SetProperty(ref _confirmed_dormant_count, value); }
        }

        [JsonProperty("replication_count")]
        public int? ReplicationCount { get; set; }

        [JsonIgnore]
        private string _note;
        [JsonProperty("note")]
        public string Note
        {
            get { return _note; }
            set { SetProperty(ref _note, value); }
        }

        [JsonIgnore]
        public string ShortDate { get { return CountDate.ToString("MM/dd/yyyy"); } }
        
        private double _percentCounted;
        [JsonIgnore]
        public double PercentCounted
        {
            get { return _percentCounted; }
            set { SetProperty(ref _percentCounted, value); }
        }
        [JsonProperty("order_request_item_id")]
        public int? OrderRequestItemId { get; set; }
    }
}
