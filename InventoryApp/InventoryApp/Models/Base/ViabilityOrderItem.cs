﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryApp.Models
{
    public class ViabilityOrderItem : BindableBase
    {
        public int order_request_item_id { get; set; }
        public int order_request_id { get; set; }
        public int? sequence_number { get; set; }
        public decimal? quantity_shipped { get; set; }
        public string status_code { get; set; }
        public DateTime? status_date { get; set; }
        public int inventory_id { get; set; }
        public string note { get; set; }
        public string percent_viable { get; set; }
        public DateTime? tested_date { get; set; }
        public string accession_number { get; set; }
        public string inventory_number { get; set; }
        public string storage_location { get; set; }
        public decimal? quantity_on_hand { get; set; }
        public string status { get; set; }
        public int? inventory_viability_id { get; set; }
        public int inventory_viability_last_count_number { get; set; }
        public DateTime? inventory_viability_last_count_date { get; set; }
        public string accession_name { get; set; }
        public string collecting_number { get; set; }
        public string taxonomy_species_name { get; set; }
    }
}
