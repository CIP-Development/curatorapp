﻿using Newtonsoft.Json;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryApp.Models.Database
{
    public class Inventory : BindableBase
    {
        public override string ToString()
        {
            return inventory_number_part1 + "|" + inventory_number_part2 + "|" + inventory_number_part3;
        }

        public int inventory_id { get; set; }
        public string inventory_number_part1 { get; set; }
        public int? inventory_number_part2 { get; set; }

        public string inventory_number_part3 { get; set; }

        public string form_type_code { get; set; }
        public int inventory_maint_policy_id { get; set; }
        public string is_distributable { get; set; }

        public string storage_location_part1 { get; set; }
        public string storage_location_part2 { get; set; }
        public string storage_location_part3 { get; set; }
        public string storage_location_part4 { get; set; }

        /*
        public string latitude { get; set; }
        public string longitude { get; set; }
        */
        public string is_available { get; set; }
        public string web_availability_note { get; set; }
        public string availability_status_code { get; set; }
        //<!--
        public string availability_status_note { get; set; }
        public string availability_start_date { get; set; }
        public string availability_end_date { get; set; }
        //-->
        public decimal? quantity_on_hand { get; set; }
        public string quantity_on_hand_unit_code { get; set; }
        //<!--
        public string is_auto_deducted { get; set; }
        public string distribution_default_form_code { get; set; }
        public decimal? distribution_default_quantity { get; set; }
        public string distribution_unit_code { get; set; }
        public decimal? distribution_critical_quantity { get; set; }
        public decimal? regeneration_critical_quantity { get; set; }
        public string pathogen_status_code { get; set; }
        //-->
        public int accession_id { get; set; }
        public int? parent_inventory_id { get; set; }
        public int? backup_inventory_id { get; set; }
        /*
        public string rootstock { get; set; }
        */
        public decimal? hundred_seed_weight { get; set; }
        public string pollination_method_code { get; set; }
        public string pollination_vector_code { get; set; }
        public int? preservation_method_id { get; set; }
        public int? regeneration_method_id { get; set; }
        public string plant_sex_code { get; set; }
        public DateTime? propagation_date { get; set; }
        public string propagation_date_code { get; set; }
        public string note { get; set; }
        public DateTime created_date { get; set; }
        public int created_by { get; set; }
        public DateTime? modified_date { get; set; }
        public int? modified_by { get; set; }
        public DateTime owned_date { get; set; }
        public int owned_by { get; set; }
        
        public string acc_name_col { get; set; }
        public string acc_name_cul { get; set; }
        public string taxonomy_species_code { get; set; }
        public string taxonomy_species_name { get; set; }

        private string _accession_number;
        public string accession_number
        {
            get { return _accession_number; }
            set { SetProperty(ref _accession_number, value); }
        }
        public string inventory_number { get; set; }
        public string storage_location { get; set; }
        public int? workgroup_cooperator_id { get; set; }
        public int? order_request_id { get; set; }
        public string container_code { get; set; }
        public string container { get; set; }
        public string form_type { get; set; }
        public string quantity_on_hand_unit { get; set; }
        public string Guid { get; set; }
    }
}
