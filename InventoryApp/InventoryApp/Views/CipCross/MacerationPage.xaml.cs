﻿using InventoryApp.Helpers;
using Xamarin.Forms;

namespace InventoryApp.Views
{
    public partial class MacerationPage : ContentPage
    {
        public MacerationPage()
        {
            InitializeComponent();
        }

        private void ExportToExcel(object sender, System.EventArgs e)
        {
            try
            {
                UserInterfaceUtils.ExportToExcel(dataGrid);
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }
        }
    }
}
