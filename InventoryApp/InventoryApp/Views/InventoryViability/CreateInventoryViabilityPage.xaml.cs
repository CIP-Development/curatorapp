﻿using InventoryApp.Helpers;
using Xamarin.Forms;

namespace InventoryApp.Views
{
    public partial class CreateInventoryViabilityPage : TabbedPage
    {
        public CreateInventoryViabilityPage()
        {
            InitializeComponent();
        }
        private void ExportToExcel(object sender, System.EventArgs e)
        {
            try
            {
                UserInterfaceUtils.ExportToExcel(dataGrid);
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }
        }

        private void SelectAll(object sender, System.EventArgs e)
        {
            try
            {
                dataGrid.SelectAll();
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }
        }
    }
}
