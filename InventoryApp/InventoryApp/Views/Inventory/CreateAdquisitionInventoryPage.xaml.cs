﻿using Xamarin.Forms;

namespace InventoryApp.Views
{
    public partial class CreateAdquisitionInventoryPage : ContentPage
    {
        public CreateAdquisitionInventoryPage()
        {
            InitializeComponent();
        }

        private void btnShowAll_Clicked(object sender, System.EventArgs e)
        {
            Others.IsVisible = true;
            btnShowAll.IsVisible = false;
        }
    }
}
