﻿using Xamarin.Forms;

namespace InventoryApp.Views
{
    public partial class SearchInventoriesPage : ContentPage
    {
        public SearchInventoriesPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            MessagingCenter.Subscribe<ViewModels.SearchInventoriesPageViewModel, bool>(this, "SearchEntry.Focus",
            (vm, isMultiline) =>
            {
                try
                {
                    if (isMultiline)
                    {
                        mSearchEditor.Focus();
                    }
                    else
                    {
                        mSearchBar.Focus();
                    }
                }
                catch (System.Exception ex)
                {
                    System.Diagnostics.Debug.Fail(ex.Message + System.Environment.NewLine + ex.InnerException?.Message);
                }
            });
        }

        protected override void OnDisappearing()
        {
            MessagingCenter.Unsubscribe<ViewModels.SearchInventoriesPageViewModel, bool>(this, "SearchEntry.Focus");
            
            base.OnDisappearing();
        }
    }
}
