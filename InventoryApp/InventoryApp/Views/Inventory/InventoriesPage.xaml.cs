﻿using Syncfusion.SfDataGrid.XForms;
using Syncfusion.SfDataGrid.XForms.Exporting;
using Syncfusion.Drawing;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xamarin.Forms;
using InventoryApp.Interfaces;
using System;

namespace InventoryApp.Views
{
    public partial class InventoriesPage : ContentPage, Prism.Navigation.IDestructible
    {
        private int _defaultColumnsCount;
        private int _selectionRangeStart = -1, _selectionRangeEnd = -1;
        public InventoriesPage()
        {
            InitializeComponent();

            _defaultColumnsCount = dataGrid.Columns.Count;
        }

        public void Destroy()
        {
            dataGrid.Dispose();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            MessagingCenter.Subscribe<ViewModels.InventoriesPageViewModel, IEnumerable<Models.CropTrait>>(this, "EvaluationDataGrid.GenerateColumns",
            (vm, extraColumns) =>
            {
                try
                {
                    //Add new descriptors columns
                    if (extraColumns != null && extraColumns.Any())
                    {
                        //Verify that columns have not been generated
                        if (dataGrid.Columns.Count < _defaultColumnsCount + extraColumns.Count())
                        {
                            //Clear trait columns
                            if (dataGrid.Columns.Count > _defaultColumnsCount)
                            {
                                for (int i = dataGrid.Columns.Count - 1; i >= _defaultColumnsCount; i--)
                                {
                                    dataGrid.Columns.RemoveAt(i);
                                }
                            }

                            for (int iDescriptor = 0; iDescriptor < extraColumns.Count(); iDescriptor++)
                            {
                                //var binding = $"Item.CropTraitObservations[{extraColumns.ElementAt(iDescriptor).CropTraitId}].DisplayText";
                                Syncfusion.SfDataGrid.XForms.GridTextColumn gridTextColumn = new Syncfusion.SfDataGrid.XForms.GridTextColumn
                                {
                                    HeaderText = extraColumns.ElementAt(iDescriptor).CropTraitName,
                                    //Width = System.Math.Min(100, decimal.ToDouble(extraColumns.ElementAt(iDescriptor).Width.GetValueOrDefault())),
                                    MappingName = $"Item.CropTraitObservations[{extraColumns.ElementAt(iDescriptor).CropTraitId}].DisplayText"
                                };
                                dataGrid.Columns.Add(gridTextColumn);
                            }
                        }
                    }

                    dataGrid.Refresh();
                }
                catch (System.Exception ex)
                {
                    System.Diagnostics.Debug.Fail(ex.Message + System.Environment.NewLine + ex.InnerException?.Message);
                }
            });

            MessagingCenter.Subscribe<ViewModels.InventoriesPageViewModel>(this, "EvaluationDataGrid.RefreshGrid",
            (vm) =>
            {
                try
                {
                    dataGrid.Refresh();
                }
                catch (System.Exception ex)
                {
                    System.Diagnostics.Debug.Fail(ex.Message + System.Environment.NewLine + ex.InnerException?.Message);
                }
            });
        }

        protected override void OnDisappearing()
        {
            MessagingCenter.Unsubscribe<ViewModels.InventoriesPageViewModel>(this, "EvaluationDataGrid.RefreshGrid");
            MessagingCenter.Unsubscribe<ViewModels.InventoriesPageViewModel, IEnumerable<Models.CropTrait>>(this, "EvaluationDataGrid.GenerateColumns");

            base.OnDisappearing();
        }

        private void Button_Clicked(object sender, System.EventArgs e)
        {
            dataGrid.Refresh();
        }
        private void CancelRangeSelectionButton_Clicked(object sender, EventArgs e)
        {
            _selectionRangeStart = _selectionRangeEnd = -1;
            RangeSelectionView.IsVisible = false;
        }
        private void dataGrid_GridLongPressed(object sender, GridLongPressedEventArgs e)
        {
            try
            {
                if (e.RowData == null)
                    return;

                if (dataGrid.SelectionMode == Syncfusion.SfDataGrid.XForms.SelectionMode.Multiple)
                {
                    if (_selectionRangeStart == -1)
                    {
                        var startInventoryItem = e.RowData as Models.WrappedSelection<Models.InventoryThumbnail>;
                        if (dataGrid.SelectedItems.Contains(startInventoryItem))
                            return;

                        _selectionRangeStart = dataGrid.ResolveToRowIndex(startInventoryItem);

                        dataGrid.SelectedItems.Add(startInventoryItem);
                        //startInventoryItem.IsSelected = true;

                        RangeSelectionView.IsVisible = true;
                    }
                    else if (_selectionRangeEnd == -1)
                    {
                        var endInventoryItem = e.RowData as Models.WrappedSelection<Models.InventoryThumbnail>;
                        _selectionRangeEnd = dataGrid.ResolveToRowIndex(endInventoryItem);

                        if (_selectionRangeEnd >= _selectionRangeStart)
                        {
                            for (int i = _selectionRangeStart; i <= _selectionRangeEnd; i++)
                            {
                                var inventory = dataGrid.GetRecordAtRowIndex(i);// as Models.WrappedSelection<Models.InventoryThumbnail>;
                                if (!dataGrid.SelectedItems.Contains(inventory))
                                {
                                    dataGrid.SelectedItems.Add(inventory);
                                    //inventory.IsSelected = true;
                                }
                            }
                        }
                        else
                        {
                            for (int i = _selectionRangeStart; i >= _selectionRangeEnd; i--)
                            {
                                var inventory = dataGrid.GetRecordAtRowIndex(i);// as Models.WrappedSelection<Models.InventoryThumbnail>;
                                if (!dataGrid.SelectedItems.Contains(inventory))
                                {
                                    dataGrid.SelectedItems.Add(inventory);
                                    //inventory.IsSelected = true;
                                }
                            }
                        }
                        _selectionRangeStart = _selectionRangeEnd = -1;

                        RangeSelectionView.IsVisible = false;
                    }
                }
            }
            catch (System.Exception ex)
            {
                _selectionRangeStart = _selectionRangeEnd = -1;
                RangeSelectionView.IsVisible = false;
            }
        }

        private void ExportButton_Clicked(object sender, System.EventArgs e)
        {
            try
            {
                DataGridExcelExportingController excelExport = new DataGridExcelExportingController();
                DataGridExcelExportingOption options = new DataGridExcelExportingOption();
                var list = new List<string> { "IsSelected", "" };
                options.ExcludedColumns = list;

                var excelEngine = excelExport.ExportToExcel(dataGrid, options);
                if (excelEngine == null)
                {
                    return;
                }
                var workbook = excelEngine.Excel.Workbooks[0];
                if (workbook == null)
                {
                    return;
                }
                MemoryStream stream = new MemoryStream();
                workbook.SaveAs(stream);
                workbook.Close();
                excelEngine.Dispose();

                string tempFileName = $@"{DateTime.Now:yyyyMMdd_HHmmss}_{DateTime.Now.Ticks.GetHashCode().ToString("x").ToUpper()}.xlsx";
                if (Device.RuntimePlatform == Device.UWP)
                {
                    Xamarin.Forms.DependencyService.Get<ISaveWindowsPhone>().Save(tempFileName, "application/msexcel", stream);
                }
                else
                {
                    Xamarin.Forms.DependencyService.Get<ISave>().Save(tempFileName, "application/msexcel", stream);
                }
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }
        }
    }
}
