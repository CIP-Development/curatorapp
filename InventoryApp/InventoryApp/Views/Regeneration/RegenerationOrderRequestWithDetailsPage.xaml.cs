﻿using InventoryApp.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace InventoryApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegenerationOrderRequestWithDetailsPage : TabbedPage
    {
        public RegenerationOrderRequestWithDetailsPage()
        {
            InitializeComponent();
        }

        private void ExportToExcel(object sender, EventArgs e)
        {
            try
            {
                UserInterfaceUtils.ExportToExcel(dataGrid);
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }
        }
    }
}