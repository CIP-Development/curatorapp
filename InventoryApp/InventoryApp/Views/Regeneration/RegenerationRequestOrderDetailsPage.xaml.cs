﻿using InventoryApp.Helpers;
using Syncfusion.SfDataGrid.XForms;
using System;
using Xamarin.Forms;

namespace InventoryApp.Views
{
    public partial class RegenerationRequestOrderDetailsPage : ContentPage
    {
        private int _selectionRangeStart = -1, _selectionRangeEnd = -1;
        public RegenerationRequestOrderDetailsPage()
        {
            InitializeComponent();
        }
        private void CancelRangeSelectionButton_Clicked(object sender, EventArgs e)
        {
            _selectionRangeStart = _selectionRangeEnd = -1;
            RangeSelectionView.IsVisible = false;
        }
        private void dataGrid_GridLongPressed(object sender, Syncfusion.SfDataGrid.XForms.GridLongPressedEventArgs e)
        {
            try
            {
                if (e.RowData == null)
                    return;

                if (dataGrid.SelectionMode == Syncfusion.SfDataGrid.XForms.SelectionMode.Multiple)
                {
                    if (_selectionRangeStart == -1)
                    {
                        var startInventoryItem = e.RowData as Models.OrderRequestItem;
                        if (dataGrid.SelectedItems.Contains(startInventoryItem))
                            return;

                        _selectionRangeStart = dataGrid.ResolveToRowIndex(startInventoryItem);

                        dataGrid.SelectedItems.Add(startInventoryItem);

                        RangeSelectionView.IsVisible = true;
                    }
                    else if (_selectionRangeEnd == -1)
                    {
                        var endInventoryItem = e.RowData as Models.OrderRequestItem;
                        _selectionRangeEnd = dataGrid.ResolveToRowIndex(endInventoryItem);

                        if (_selectionRangeEnd >= _selectionRangeStart)
                        {
                            for (int i = _selectionRangeStart; i <= _selectionRangeEnd; i++)
                            {
                                var inventory = dataGrid.GetRecordAtRowIndex(i);
                                if (!dataGrid.SelectedItems.Contains(inventory))
                                    dataGrid.SelectedItems.Add(inventory);
                            }
                        }
                        else
                        {
                            for (int i = _selectionRangeStart; i >= _selectionRangeEnd; i--)
                            {
                                var inventory = dataGrid.GetRecordAtRowIndex(i);
                                if (!dataGrid.SelectedItems.Contains(inventory))
                                    dataGrid.SelectedItems.Add(inventory);
                            }
                        }
                        _selectionRangeStart = _selectionRangeEnd = -1;

                        RangeSelectionView.IsVisible = false;
                    }
                }
            }
            catch (System.Exception ex)
            {
                _selectionRangeStart = _selectionRangeEnd = -1;
                RangeSelectionView.IsVisible = false;
            }
        }
        private void ExportToExcel(object sender, System.EventArgs e)
        {
            try
            {
                UserInterfaceUtils.ExportToExcel(dataGrid);
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine(ex.Message);
            }
        }
    }
}
