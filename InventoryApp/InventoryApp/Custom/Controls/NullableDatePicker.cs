﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace InventoryApp.Custom.Controls
{
    public class NullableDatePicker : DatePicker
    {
        private string _format = null;
        
        //public static readonly BindableProperty NullableDateProperty = BindableProperty.Create<NullableDatePicker, DateTime?>(p => p.NullableDate, null);
        public static readonly BindableProperty NullableDateProperty = BindableProperty.Create(nameof(NullableDate), typeof(DateTime?), typeof(NullableDatePicker));
        public DateTime? NullableDate
        {
            get { return (DateTime?)GetValue(NullableDateProperty); }
            set { SetValue(NullableDateProperty, value); UpdateDate(); }
        }
        private void UpdateDate()
        {
            if (NullableDate.HasValue) {
                if (_format != null)
                    Format = _format;
                Date = NullableDate.Value;
            }
            else { _format = Format; Format = "Escoger..."; }
        }
        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();
            UpdateDate();
        }
        protected override void OnPropertyChanged(string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);
            if (propertyName == "Date") NullableDate = Date;
        }
    }
}
