﻿using InventoryApp.Interfaces;
using InventoryApp.Models;
using InventoryApp.Services;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using System;
using System.Collections.ObjectModel;

namespace InventoryApp.Dialogs
{
    public class LookupPickerDialogViewModel : BindableBase, IDialogAware
    {
        private readonly IRestService _restService;

        private bool _isBusy;
        public bool IsBusy
        {
            get { return _isBusy; }
            set { SetProperty(ref _isBusy, value); }
        }
        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }
        private string _message;
        public string Message
        {
            get { return _message; }
            set { SetProperty(ref _message, value); }
        }
        private ObservableCollection<Lookup> _entityList;
        public ObservableCollection<Lookup> EntityList
        {
            get { return _entityList; }
            set { SetProperty(ref _entityList, value); }
        }
        private Lookup _selectedEntity;
        public Lookup SelectedEntity
        {
            get { return _selectedEntity; }
            set
            {
                _ = SetProperty(ref _selectedEntity, value);
                if (value != null)
                {
                    RequestClose(new DialogParameters
                    {
                        { "SelectedEntity", _selectedEntity }
                    });
                }
            }
        }

        public LookupPickerDialogViewModel(IRestService restService)
        {
            _restService = restService;

            SelectCommand = new DelegateCommand(ExecuteSelectCommand);
            CancelCommand = new DelegateCommand(ExecuteCancelCommand);
        }

        public DelegateCommand CancelCommand { get; }
        public DelegateCommand SelectCommand { get; }

        void ExecuteCancelCommand()
        {
            RequestClose(null);
        }
        void ExecuteSelectCommand()
        {
            if(SelectedEntity != null)
            {
                RequestClose(new DialogParameters
                {
                    { "SelectedEntity", SelectedEntity }
                });
            }
        }

        public event Action<IDialogParameters> RequestClose;

        public bool CanCloseDialog()
        {
            return true;
        }

        public void OnDialogClosed()
        {
            
        }

        public async void OnDialogOpened(IDialogParameters parameters)
        {
            try
            {
                IsBusy = true;

                Title = "Select Cooperator";

                var tempEntityList = await _restService.GetCooperatorLookupByWorkGroup(null);
                EntityList = new ObservableCollection<Lookup>(tempEntityList);
            }
            catch (Exception ex)
            {
                Message = ex.Message + Environment.NewLine + ex.InnerException?.Message;
            }
            finally
            {
                IsBusy = false;
            }
        }
    }
}
