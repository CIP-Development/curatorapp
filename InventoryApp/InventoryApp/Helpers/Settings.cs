﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Essentials;

namespace InventoryApp.Helpers
{
    public static class Settings
    {
        private static readonly string StringDefaultValue = string.Empty;

        public static string ServerList
        {
            get { return Preferences.Get("server_list_key", StringDefaultValue); }
            set { Preferences.Set("server_list_key", value); }
        }
        public static string Server
        {
            get { return Preferences.Get("server_key", StringDefaultValue); }
            set { Preferences.Set("server_key", value); }
        }
        public static int LangId
        {
            get { return Preferences.Get("lang_id_key", -1); }
            set { Preferences.Set("lang_id_key", value); }
        }
        public static string Username
        {
            get { return Preferences.Get("user_name_key", StringDefaultValue); }
            set { Preferences.Set("user_name_key", value); }
        }
        public static int UserCooperatorId
        {
            get { return Preferences.Get("user_cooperator_id_key", -1); }
            set { Preferences.Set("user_cooperator_id_key", value); }
        }
        public static string UserToken
        {
            get { return Preferences.Get("token_key", StringDefaultValue); }
            set { Preferences.Set("token_key", value); }
        }

        public static int WorkgroupId
        {
            get { return Preferences.Get("workgroup_id_key", -1); }
            set { Preferences.Set("workgroup_id_key", value); }
        }
        public static string WorkgroupName
        {
            get { return Preferences.Get("workgroup_name_key", StringDefaultValue); }
            set { Preferences.Set("workgroup_name_key", value); }
        }
        public static string WorkgroupInvMaintPolicies
        {
            get { return Preferences.Get("workgroup_inv_maint_policies_key", StringDefaultValue); }
            set { Preferences.Set("workgroup_inv_maint_policies_key", value); }
        }
        public static string WorkgroupInventoryDataview
        {
            get {
                string value = Preferences.Get("workgroup_inventory_dataview_key", "");
                return !string.IsNullOrEmpty(value) ? value : "table_mob_inventory"; }
            set { Preferences.Set("workgroup_inventory_dataview_key", value); }
        }
        public static string WorkgroupInventoryThumbnailDataview
        {
            get { string value = Preferences.Get("workgroup_inventory_thumbnail_dataview_key", "");
                return !string.IsNullOrEmpty(value) ? value : "get_mob_inventory_thumbnail"; }
            set { Preferences.Set("workgroup_inventory_thumbnail_dataview_key", value); }
        }
        public static int WorkgroupCooperatorId
        {
            get { return Preferences.Get("workgroup_cooperator_id_key", -1); }
            set { Preferences.Set("workgroup_cooperator_id_key", value); }
        }

        public static string Location1
        {
            get { return Preferences.Get("Location1", StringDefaultValue); }
            set { Preferences.Set("Location1", value); }
        }

        public static string Filter
        {
            get { return Preferences.Get("Filter", StringDefaultValue); }
            set { Preferences.Set("Filter", value); }
        }
        public static string SearchText
        {
            get { return Preferences.Get("SearchText", StringDefaultValue); }
            set { Preferences.Set("SearchText", value); }
        }

        public static string SearchAccessionSearchText
        {
            get { return Preferences.Get("SearchAccession_SearchText", StringDefaultValue); }
            set { Preferences.Set("SearchAccession_SearchText", value); }
        }
        public static string SearchAccessionFilter
        {
            get { return Preferences.Get("SearchAccession_Filter", StringDefaultValue); }
            set { Preferences.Set("SearchAccession_Filter", value); }
        }

        public static string Printer
        {
            get { return Preferences.Get("printer_key", StringDefaultValue); }
            set { Preferences.Set("printer_key", value); }
        }

        public static bool UseSSL
        {
            get { return Preferences.Get("use_ssl_key", false); }
            set { Preferences.Set("use_ssl_key", value); }
        }

        public static string BarcodeFormatCode
        {
            get { return Preferences.Get("barcode_format_key", StringDefaultValue); }
            set { Preferences.Set("barcode_format_key", value); }
        }

        public static bool ScanByCamera
        {
            get { return Preferences.Get("scan_by_camera", false); }
            set { Preferences.Set("scan_by_camera", value); }
        }

        public static int SelectedRegenerationOrderRequestId
        {
            get { return Preferences.Get("selected_regeneration_order_request_id", -1); }
            set { Preferences.Set("selected_regeneration_order_request_id", value); }
        }
        public static string SelectedRegenerationLocation
        {
            get { return Preferences.Get("selected_regeneration_location_key", StringDefaultValue); }
            set { Preferences.Set("selected_regeneration_location_key", value); }
        }
        public static bool IsHarvestGeneral
        {
            get { return Preferences.Get("is_harvest_general_key", false); }
            set { Preferences.Set("is_harvest_general_key", value); }
        }
        public static string BluetoothPrinterMac
        {
            get { return Preferences.Get("bluetooth_printer_mac", StringDefaultValue); }
            set { Preferences.Set("bluetooth_printer_mac", value); }
        }
        public static bool ShowDebugInfo =>
#if DEBUG
                true;
#else
                false;
#endif

    }

}
