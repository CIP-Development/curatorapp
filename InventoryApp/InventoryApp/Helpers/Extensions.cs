﻿using Prism.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InventoryApp.Extensions
{
    public static class Extensions
    {
        public static async Task DisplayErrorAlertAsync(this IPageDialogService pageDialogService, Exception ex)
        {
            await pageDialogService.DisplayAlertAsync("Error", ex.Message + Environment.NewLine + ex.InnerException?.Message, "OK");
        }
    }
}
