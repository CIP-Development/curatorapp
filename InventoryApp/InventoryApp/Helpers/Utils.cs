﻿using InventoryApp.Interfaces;
using InventoryApp.Models.Database;
using InventoryApp.Models.LocalStorage;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace InventoryApp.Helpers
{
    public static class Utils
    {
        public static void RefreshLang(Object obj, IDataStoreService dataStoreService, string formName)
        {
            if (dataStoreService.AppLangResourceList != null)
            {
                PropertyInfo[] props = obj.GetType().GetProperties();
                foreach (PropertyInfo prop in props)
                {
                    if (prop.Name.StartsWith("Ux", StringComparison.Ordinal))
                    {
                        prop.SetValue(obj, dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, formName, prop.Name));
                    }
                }
            }

            /*if (_dataStoreService.AppLangResourceList != null)
            {
                PropertyInfo[] props = this.GetType().GetProperties();
                foreach (PropertyInfo prop in props)
                {
                    if(prop.Name.StartsWith("Ux", StringComparison.Ordinal))
                    {
                        prop.SetValue(this, _dataStoreService.GetAppLangResourceDisplayMember(Settings.LangId, "WelcomePage", prop.Name));
                    }
                }
            }*/
        }

        public static Inventory ConvertInventoryDbToInventory(InventoryDb dbInventory)
        {
            var inventory = new Inventory()
            {
                accession_id = dbInventory.AccessionId,
                //accession_number = dbInventory.AccessionNumber,
                //acc_name_col = string.Empty,
                //acc_name_cul = string.Empty,
                availability_status_code = dbInventory.AvailabilityStatusCode,
                container_code = dbInventory.ContainerCode,
                created_by = dbInventory.CreatedBy,
                created_date = dbInventory.CreatedDate,
                form_type_code = dbInventory.FormTypeCode,
                inventory_id = dbInventory.InventoryId,
                inventory_maint_policy_id = dbInventory.InventoryMaintPolicyId,
                //inventory_number = dbInventory.InventoryNumber,
                inventory_number_part1 = dbInventory.InventoryNumberPart1,
                inventory_number_part2 = dbInventory.InventoryNumberPart2,
                inventory_number_part3 = dbInventory.InventoryNumberPart3,
                is_auto_deducted = dbInventory.IsAutoDeducted,
                is_available = dbInventory.IsAvailable,
                is_distributable = dbInventory.IsDistributable,
                modified_by = dbInventory.ModifiedBy,
                modified_date = dbInventory.ModifiedDate,
                note = dbInventory.Note,
                quantity_on_hand = dbInventory.QuantityOnHand,
                quantity_on_hand_unit_code = dbInventory.QuantityOnHandUnitCode,
                storage_location_part1 = dbInventory.StorageLocationPart1,
                storage_location_part2 = dbInventory.StorageLocationPart2,
                storage_location_part3 = dbInventory.StorageLocationPart3,
                storage_location_part4 = dbInventory.StorageLocationPart4,
                //taxonomy_species_code = string.Empty,
                workgroup_cooperator_id = Settings.WorkgroupCooperatorId,
                //order_request_id = dbInventory.OrderRequestId,
            };
            return inventory;
        }
    }
}
