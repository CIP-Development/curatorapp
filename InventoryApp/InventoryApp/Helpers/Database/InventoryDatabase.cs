﻿using InventoryApp.Interfaces;
using InventoryApp.Models.LocalStorage;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryApp.Helpers
{
    public class InventoryDatabase : IInventoryLocalRepository
    {
        private SQLiteAsyncConnection Database;
        private async Task Init()
        {
            if (Database != null)
                return;

            Database = new SQLiteAsyncConnection(Constants.DatabasePath, Constants.Flags);
            CreateTableResult result = await Database.CreateTableAsync<InventoryDb>();
        }
        public async Task<List<InventoryDb>> GetInventoriesAsync()
        {
            await Init();
            var result = await Database.Table<InventoryDb>().ToListAsync();
            return result;
        }
        public async Task<List<InventoryDb>> GetInventoriesByOrderRequestIdAsync(int orderRequestId)
        {
            await Init();
            var result = await Database.Table<InventoryDb>()
                .Where(x => x.OrderRequestId == orderRequestId)
                .ToListAsync();
            return result;
        }
        public async Task<IEnumerable<InventoryDb>> GetMotherInventoriesByOrderRequestIdAsync(int orderRequestId)
        {
            await Init();
            var result = await Database.Table<InventoryDb>()
                .Where(x => x.OrderRequestId == orderRequestId && x.IsMother && !x.InventoryNumberPart3.Contains("."))
                .ToListAsync();
            return result;
        }
        public async Task<InventoryDb> GetInventoryByInventoryNumberAsync(string inventoryNumber)
        {
            await Init();
            var query = $@"SELECT * FROM inventory 
WHERE {nameof(InventoryDb.InventoryNumberPart1)} || ' ' || {nameof(InventoryDb.InventoryNumberPart2)} || ' ' || {nameof(InventoryDb.InventoryNumberPart3)} || ' ' || {nameof(InventoryDb.FormTypeCode)} = '{inventoryNumber}'";
            var result = (await Database.QueryAsync<InventoryDb>(query))
                .FirstOrDefault();
            return result;
        }
        public async Task<InventoryDb> GetInventoryByGuidAsync(string guid)
        {
            await Init();
            var result = await Database.Table<InventoryDb>()
                .FirstOrDefaultAsync(x => x.Guid.Equals(guid));
            return result;
        }
        public async Task<int> InsertInventoryAsync(InventoryDb InventoryDb)
        {
            await Init();
            return await Database.InsertAsync(InventoryDb);
        }
        public async Task<int> InsertInventoryManyAsync(IEnumerable<InventoryDb> InventoryDbs)
        {
            await Init();
            return await Database.InsertAllAsync(InventoryDbs);
        }
        public async Task<int> DeleteInventoryByIdAsync(int inventoryId)
        {
            await Init();
            return await Database.ExecuteAsync($"DELETE FROM inventory WHERE InventoryId = {inventoryId}");
        }
        public async Task<int> DeleteInventoryByOrderRequestIdAsync(int orderRequestId)
        {
            await Init();
            return await Database.ExecuteAsync($"DELETE FROM inventory WHERE OrderRequestId = {orderRequestId}");
        }
        public async Task<int> GetMinorInventoryIdAsync()
        {
            await Init();
            return await Database.ExecuteScalarAsync<int>($"SELECT MIN(InventoryId) FROM inventory");
        }
        public async Task<DateTime> GetLastModifiedDate()
        {
            await Init();
            return await Database.ExecuteScalarAsync<DateTime>($"SELECT MAX(coalesce(ModifiedDate, CreatedDate)) FROM inventory");
        }
        public async Task<List<InventoryDb>> GetPendingInventoriesByOrderRequestIdAsync(int orderRequestId, DateTime lastSynced)
        {
            await Init();
            var result = await Database.Table<InventoryDb>()
                .Where(x => x.OrderRequestId == orderRequestId &&
                (x.CreatedDate > lastSynced || x.ModifiedDate > lastSynced))
                .ToListAsync();
            return result;
        }
        public async Task<int> PatchInventoryIdAsync(string inventoryGuid, int newInventoryId)
        {
            await Init();
            return await Database.ExecuteAsync($"UPDATE inventory SET InventoryId = {newInventoryId} WHERE Guid = '{inventoryGuid}'");
        }
        public async Task<int> PatchParentInventoryIdAsync(string parentInventoryGuid, int parentInventoryId)
        {
            await Init();
            return await Database.ExecuteAsync($"UPDATE inventory SET {nameof(InventoryDb.ParentInventoryId)} = {parentInventoryId} WHERE {nameof(InventoryDb.ParentInventoryGuid)} = '{parentInventoryGuid}'");
        }

        public async Task<int> UpdateInventoryAsync(InventoryDb InventoryDb)
        {
            await Init();
            return await Database.UpdateAsync(InventoryDb);
        }

        public async Task<int> PatchQuantityAsync(string guid, decimal quantity, int modifiedBy, DateTime modifiedDate)
        {
            await Init();
            var inventory = await Database.Table<InventoryDb>().FirstOrDefaultAsync(x => x.Guid.Equals(guid));
            inventory.QuantityOnHand = quantity;
            inventory.ModifiedBy = modifiedBy;
            inventory.ModifiedDate = modifiedDate;

            return await Database.UpdateAsync(inventory);
        }

        public async Task<InventoryDb> GetInventoryByInventoryNumberPartsAsync(string inventoryNumberPart1, int inventoryNumberPart2, string inventoryNumberPart3)
        {
            await Init();
            var result = await Database.Table<InventoryDb>()
                .FirstOrDefaultAsync(x => x.InventoryNumberPart1.Equals(inventoryNumberPart1) &&
                    x.InventoryNumberPart2 == inventoryNumberPart2 &&
                    x.InventoryNumberPart3.Equals(inventoryNumberPart3));
            return result;
        }

        public async Task<IEnumerable<InventoryDb>> GetByLotAsync(int lot)
        {
            await Init();
            var result = await Database.Table<InventoryDb>()
                .Where(x => x.InventoryNumberPart2 == lot)
                .ToArrayAsync();
            return result;
        }

        #region crossing
        public async Task<List<InventoryDb>> GetCrossingInventoriesByOrderRequestIdAsync(int orderRequestId)
        {
            await Init();
            var result = await Database.Table<InventoryDb>()
                .Where(x => x.OrderRequestId == orderRequestId && x.FormTypeCode.Equals("IF"))
                .ToListAsync();
            return result;
        }

        #endregion
    }
}
