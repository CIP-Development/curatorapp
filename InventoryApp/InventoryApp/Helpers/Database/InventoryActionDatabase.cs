﻿using InventoryApp.Interfaces;
using InventoryApp.Models.LocalStorage;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryApp.Helpers
{
    public class InventoryActionDatabase : IInventoryActionLocalRepository
    {
        private SQLiteAsyncConnection Database;
        private async Task Init()
        {
            if (Database != null)
                return;

            Database = new SQLiteAsyncConnection(Constants.DatabasePath, Constants.Flags);
            CreateTableResult result = await Database.CreateTableAsync<InventoryActionDb>();
        }
        public async Task<int> DeleteByInventoryGuidAsync(string inventoryGuid)
        {
            await Init();
            return await Database.ExecuteAsync($"DELETE FROM inventory_action WHERE {nameof(InventoryActionDb.InventoryGuid)} = '{inventoryGuid}'");
        }

        public async Task<int> DeleteByOrderRequestIdAsync(int orderRequestId)
        {
            await Init();
            var query = $@"DELETE FROM inventory_action
WHERE {nameof(InventoryActionDb.InventoryGuid)} IN (
    SELECT {nameof(InventoryDb.Guid)} FROM inventory WHERE {nameof(InventoryDb.OrderRequestId)} = {orderRequestId}
)";
            return await Database.ExecuteAsync(query);
        }

        public async Task<IEnumerable<InventoryActionDb>> GetAllAsync()
        {
            await Init();
            var result = await Database.Table<InventoryActionDb>().ToListAsync();
            return result;
        }

        public async Task<IEnumerable<InventoryActionDb>> GetByInventoryGuidAsync(string inventoryGuid)
        {
            await Init();
            var result = await Database.Table<InventoryActionDb>()
                .Where(x => x.InventoryGuid.Equals(inventoryGuid))
                .ToListAsync();
            return result;
        }

        public async Task<int> InsertAsync(InventoryActionDb inventoryActionDb)
        {
            await Init();
            return await Database.InsertAsync(inventoryActionDb);
        }

        public async Task<int> InsertManyAsync(IEnumerable<InventoryActionDb> inventoryActionDbs)
        {
            await Init();
            return await Database.InsertAllAsync(inventoryActionDbs);
        }

        public async Task<int> UpdateAsync(InventoryActionDb inventoryActionDb)
        {
            await Init();
            return await Database.UpdateAsync(inventoryActionDb);
        }

        public async Task<IEnumerable<InventoryGuidCount>> GetCountsByManyInventoryGuidAsync(string[] inventoryGuids)
        {
            await Init();
            var query = $@"SELECT {nameof(InventoryActionDb.InventoryGuid)}, COUNT(1) as Count
FROM inventory_action 
WHERE {nameof(InventoryActionDb.InventoryGuid)} IN ('{string.Join("','", inventoryGuids)}')
GROUP BY {nameof(InventoryActionDb.InventoryGuid)}";
            return await Database.QueryAsync<InventoryGuidCount>(query);
        }

        public async Task<DateTime> GetLastModifiedDate()
        {
            await Init();
            return await Database.ExecuteScalarAsync<DateTime>($"SELECT MAX(coalesce(ModifiedDate, CreatedDate)) FROM inventory_action");
        }

        public async Task<int> PatchInventoryIdAsync(string inventoryGuid, int newInventoryId)
        {
            await Init();
            return await Database.ExecuteAsync($"UPDATE inventory_action SET InventoryId = {newInventoryId} WHERE {nameof(InventoryActionDb.InventoryGuid)} = '{inventoryGuid}'");
        }

        public async Task<IEnumerable<InventoryActionDb>> GetPendingByOrderRequestIdAsync(int orderRequestId, DateTime lastSynced)
        {
            await Init();

            var sql = $@"
SELECT * from inventory_action ia
WHERE ia.InventoryGuid in (SELECT i.Guid FROM inventory i WHERE OrderRequestId = {orderRequestId})
AND coalesce(ia.ModifiedDate, ia.CreatedDate) > ?
";
            var result = await Database.QueryAsync<InventoryActionDb>(sql, lastSynced);
            return result;
        }

        public async Task<int> PatchInventoryActionIdAsync(string guid, int newInventoryActionId)
        {
            await Init();
            var sql = $"UPDATE inventory_action SET {nameof(InventoryActionDb.InventoryActionId)} = {newInventoryActionId} WHERE Guid = '{guid}'";
            var result = await Database.ExecuteAsync(sql);
            return result;
        }

        public async Task<IEnumerable<InventoryActionDb>> GetCrossingActionsByOrderRequestAsync(int orderRequestId)
        {
            await Init();
            var sql = $@"
SELECT * from inventory_action ia
WHERE ia.InventoryGuid in (SELECT i.Guid FROM inventory i WHERE OrderRequestId = {orderRequestId})
AND {nameof(InventoryActionDb.ActionNameCode)} in ('CROSSFEMALEORDER','CROSSMALEORDER','FRUIT-HARVEST')
";
            var result = await Database.QueryAsync<InventoryActionDb>(sql);
            return result;
        }

        public async Task<IEnumerable<InventoryActionDb>> GetCrossingActionsByInventoryGuidAsync(string inventoryGuid)
        {
            await Init();

            var sql = $@"
SELECT * from inventory_action ia
WHERE ia.InventoryGuid in (SELECT i.Guid FROM inventory i WHERE {nameof(InventoryActionDb.InventoryGuid)} = ?)
AND {nameof(InventoryActionDb.ActionNameCode)} in ('CROSSFEMALEORDER','CROSSMALEORDER','FRUIT-HARVEST')
";
            var result = await Database.QueryAsync<InventoryActionDb>(sql, inventoryGuid);
            return result;
        }
    }
}
