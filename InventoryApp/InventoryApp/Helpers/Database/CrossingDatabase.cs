﻿using InventoryApp.Interfaces;
using InventoryApp.Models.LocalStorage;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InventoryApp.Helpers
{
    public class CrossingDatabase : ICrossingLocalRepository
    {
        private SQLiteAsyncConnection Database;
        private readonly IInventoryLocalRepository _inventoryLocalRepository;
        private readonly IInventoryActionLocalRepository _inventoryActionLocalRepository;
        public CrossingDatabase(IInventoryLocalRepository inventoryLocalRepository, IInventoryActionLocalRepository inventoryActionLocalRepository)
        {
            _inventoryLocalRepository = inventoryLocalRepository;
            _inventoryActionLocalRepository = inventoryActionLocalRepository;
        }
        private async Task Init()
        {
            if (Database != null)
                return;

            Database = new SQLiteAsyncConnection(Constants.DatabasePath, Constants.Flags);
            CreateTableResult result = await Database.CreateTableAsync<CrossingDb>();
        }
        public async Task<int> InsertInventoryAsync(CrossingDb InventoryDb)
        {
            await Init();
            return await Database.InsertAsync(InventoryDb);
        }

        public async Task<int> UpdateInventoryAsync(CrossingDb InventoryDb)
        {
            //await Init();
            //return await Database.UpdateAsync(InventoryDb);
            var crossingInventory = await _inventoryLocalRepository.GetInventoryByGuidAsync(InventoryDb.Guid);
            crossingInventory.PollinationMethodCode = InventoryDb.CrossingTypeCode;
            crossingInventory.QuantityOnHand = InventoryDb.PollinatedFlowers;
            crossingInventory.ModifiedBy = InventoryDb.ModifiedBy;
            crossingInventory.ModifiedDate = InventoryDb.ModifiedDate;
            var affectedRows1 = await _inventoryLocalRepository.UpdateInventoryAsync(crossingInventory);

            var crossingInventoryActions = await _inventoryActionLocalRepository.GetCrossingActionsByInventoryGuidAsync(InventoryDb.Guid);
            var femaleOrderAction = crossingInventoryActions.FirstOrDefault(ia => ia.ActionNameCode.Equals("CROSSFEMALEORDER"));
            if(femaleOrderAction != null)
            {
                femaleOrderAction.Note = InventoryDb.FemaleOrder.ToString();
                femaleOrderAction.ModifiedBy = InventoryDb.ModifiedBy;
                femaleOrderAction.ModifiedDate = InventoryDb.ModifiedDate;
            }
            var maleOrderAction = crossingInventoryActions.FirstOrDefault(ia => ia.ActionNameCode.Equals("CROSSMALEORDER"));
            if (maleOrderAction != null)
            {
                maleOrderAction.Note = InventoryDb.MaleOrder.ToString();
                maleOrderAction.ModifiedBy = InventoryDb.ModifiedBy;
                maleOrderAction.ModifiedDate = InventoryDb.ModifiedDate;
            }
            await _inventoryActionLocalRepository.UpdateAsync(femaleOrderAction);
            await _inventoryActionLocalRepository.UpdateAsync(maleOrderAction);

            return affectedRows1;
        }

        public async Task<CrossingDb> GetInventoryByGuidAsync(string guid)
        {
            await Init();
            var result = await Database.Table<CrossingDb>()
                .FirstOrDefaultAsync(x => x.Guid.Equals(guid));
            return result;
        }

        public async Task<IEnumerable<CrossingDb>> GetCrossingByOrderRequestIdAsync(int orderRequestId)
        {
            //await Init();
            //var result = await Database.Table<CrossingDb>()
            //    .Where(x => x.OrderRequestId == orderRequestId)
            //    .ToListAsync();
            var crossingInventories = await _inventoryLocalRepository.GetCrossingInventoriesByOrderRequestIdAsync(orderRequestId);
            var crossingInventoryActions = await _inventoryActionLocalRepository.GetCrossingActionsByOrderRequestAsync(orderRequestId);
            var result = crossingInventories.Select(x => new CrossingDb
            {
                InventoryId = x.InventoryId,
                Guid = x.Guid,
                CooperatorId = x.CreatedBy,
                CooperatorName = crossingInventoryActions.FirstOrDefault(ia => ia.InventoryGuid.Equals(x.Guid))?.CooperatorName,
                CrossingDate = x.PropagationDate,
                CrossingName = x.InventoryNumberPart3,
                CrossingTypeCode = x.PollinationMethodCode,
                CrossingType = Models.CodeValueFactory.CrossingTypeList.First(cv => cv.Code.Equals(x.PollinationMethodCode)).Value,
                Locality = x.StorageLocationPart1,
                OrderRequestId = (int)x.OrderRequestId,
                PollinatedFlowers = (int)x.QuantityOnHand,
                FemaleParentGuid = x.ParentInventoryGuid,
                FemaleInventoryId = x.ParentInventoryId,
                FemaleAccessionNumber = x.ParentAccessionNumber,
                ModifiedDate = x.ModifiedDate,
            }).ToList();

            foreach (var crossing in result)
            {
                var femaleOrderAction = crossingInventoryActions.FirstOrDefault(ia => ia.InventoryGuid.Equals(crossing.Guid) && ia.ActionNameCode.Equals("CROSSFEMALEORDER"));
                if (femaleOrderAction != null)
                {
                    crossing.FemaleOrder = int.Parse(femaleOrderAction.Note);
                }
                var maleOrderAction = crossingInventoryActions.FirstOrDefault(ia => ia.InventoryGuid.Equals(crossing.Guid) && ia.ActionNameCode.Equals("CROSSMALEORDER"));
                if (maleOrderAction != null)
                {
                    crossing.MaleOrder = int.Parse(maleOrderAction.Note);
                }
                var fruitHarvestAction = crossingInventoryActions.FirstOrDefault(ia => ia.InventoryGuid.Equals(crossing.Guid) && ia.ActionNameCode.Equals("FRUIT-HARVEST"));
                if (fruitHarvestAction != null)
                {
                    crossing.HarvestedFruits = (int?)fruitHarvestAction.Quantity;
                    crossing.HarvestDate = fruitHarvestAction.ActionDate;
                    crossing.CooperatorId = fruitHarvestAction.CooperatorId;
                    crossing.CooperatorName = fruitHarvestAction.CooperatorName;
                }
            }

            return result;
        }

        public async Task<int> DeleteAllAsync()
        {
            await Init();
            return await Database.DeleteAllAsync<CrossingDb>();
        }

        public async Task<IEnumerable<CrossingDb>> GetAllAsync()
        {
            await Init();
            return await Database.Table<CrossingDb>().ToArrayAsync();
        }
    }
}
