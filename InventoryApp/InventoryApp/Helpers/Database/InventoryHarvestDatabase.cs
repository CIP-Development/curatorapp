﻿using InventoryApp.Interfaces;
using InventoryApp.Models.LocalStorage;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InventoryApp.Helpers
{
    public class InventoryHarvestDatabase : IInventoryHarvestLocalRepository
    {
        private SQLiteAsyncConnection Database;
        private async Task Init()
        {
            if (Database != null)
                return;

            Database = new SQLiteAsyncConnection(Constants.DatabasePath, Constants.Flags);
            CreateTableResult result = await Database.CreateTableAsync<InventoryHarvestDb>();
        }
        public async Task<int> DeleteAllAsync()
        {
            await Init();
            return await Database.DeleteAllAsync<InventoryHarvestDb>();
        }

        public async Task<IEnumerable<InventoryHarvestDb>> GetAllAsync()
        {
            await Init();
            return await Database.Table<InventoryHarvestDb>().ToArrayAsync();
        }

        public async Task<InventoryHarvestDb> GetInventoryHarvestByGuidAsync(string guid)
        {
            await Init();
            var result = await Database.Table<InventoryHarvestDb>()
                .FirstOrDefaultAsync(x => x.Guid.Equals(guid));
            return result;
        }

        public async Task<IEnumerable<InventoryHarvestDb>> GetInventoryHarvestByOrderRequestIdAsync(int orderRequestId)
        {
            await Init();
            var result = await Database.Table<InventoryHarvestDb>()
                .Where(x => x.OrderRequestId == orderRequestId)
                .ToListAsync();
            return result;
        }

        public async Task<int> InsertInventoryHarvestAsync(InventoryHarvestDb InventoryHarverstDb)
        {
            await Init();
            return await Database.InsertAsync(InventoryHarverstDb);
        }

        public async Task<int> UpdateInventoryHarvestAsync(InventoryHarvestDb InventoryHarverstDb)
        {
            await Init();
            return await Database.UpdateAsync(InventoryHarverstDb);
        }
    }
}
