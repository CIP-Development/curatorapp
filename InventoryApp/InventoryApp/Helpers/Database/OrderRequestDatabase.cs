﻿using SQLite;
using InventoryApp.Models.LocalStorage;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using InventoryApp.Interfaces;

namespace InventoryApp.Helpers
{
    public class OrderRequestDatabase : IOrderRequestRepository
    {
        private SQLiteAsyncConnection Database;
        //private static SQLiteAsyncConnection Database;

        //public static readonly AsyncLazy<OrderRequestDatabase> Instance = new AsyncLazy<OrderRequestDatabase>(async () =>
        //{
        //    var instance = new OrderRequestDatabase();
        //    CreateTableResult result = await Database.CreateTableAsync<OrderRequestDb>();
        //    return instance;
        //});

        //public OrderRequestDatabase()
        //{
        //    Database = new SQLiteAsyncConnection(Constants.DatabasePath, Constants.Flags);
        //}

        private async Task Init()
        {
            if (Database != null)
                return;

            Database = new SQLiteAsyncConnection(Constants.DatabasePath, Constants.Flags);
            CreateTableResult result = await Database.CreateTableAsync<OrderRequestDb>();
        }
        public async Task<List<OrderRequestDb>> GetOrderRequestsAsync()
        {
            await Init();

            var result = await Database.Table<OrderRequestDb>().ToListAsync();
            return result;
        }
        public async Task<OrderRequestDb> GetOrderRequestById(int orderRequestId)
        {
            await Init();
            try
            {
                var orderRequestDb = await Database.GetAsync<OrderRequestDb>(orderRequestId);
                return orderRequestDb;
            }
            catch (InvalidOperationException ex)
            {
                return null;
            }
        }
        public async Task<int> InsertItemAsync(OrderRequestDb orderRequestDb)
        {
            await Init();
            return await Database.InsertAsync(orderRequestDb);
        }
        public async Task<int> DeleteOrderRequestByIdAsync(int orderRequestId)
        {
            await Init();
            return await Database.ExecuteAsync($"DELETE FROM order_request WHERE OrderRequestId = {orderRequestId}");
        }
        public async Task<int> PatchLastSyncedDateAsync(int orderRequestId, DateTime lastSyncedDate)
        {
            await Init();
            return await Database.ExecuteAsync($"UPDATE order_request SET LastSyncedDate = ? WHERE OrderRequestId = {orderRequestId}", lastSyncedDate);
        }
        public Task<int> SaveItemAsync(OrderRequestDb orderRequestDb)
        {
            if (orderRequestDb.OrderRequestId != 0)
            {
                return Database.UpdateAsync(orderRequestDb);
            }
            else
            {
                return Database.InsertAsync(orderRequestDb);
            }
        }
    }
}
