﻿using InventoryApp.Interfaces;
using InventoryApp.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InventoryApp.Helpers
{
    public class CodeValueLookupDatabase : ICodeValueLookupLocalRepository
    {
        private SQLiteAsyncConnection Database;
        private async Task Init()
        {
            if (Database != null)
                return;

            Database = new SQLiteAsyncConnection(Constants.DatabasePath, Constants.Flags);
            CreateTableResult result = await Database.CreateTableAsync<CodeValueLookup>();
        }
        public async Task<int> DeleteByGroupNameAsync(string groupName)
        {
            await Init();
            return await Database.ExecuteAsync($"DELETE FROM code_value_lookup WHERE {nameof(CodeValueLookup.group_name)} = '{groupName}'");
        }

        public async Task<IEnumerable<CodeValueLookup>> GetAllAsync()
        {
            await Init();
            return await Database.Table<CodeValueLookup>().ToListAsync();
        }

        public async Task<IEnumerable<CodeValueLookup>> GetByGroupNameAsync(string groupName)
        {
            await Init();
            var result = await Database.Table<CodeValueLookup>()
                    .Where(x => x.group_name.Equals(groupName))
                    .OrderBy(x => x.display_member)
                    .ToArrayAsync();
            return result;
        }

        public async Task<int> InsertManyAsync(IEnumerable<CodeValueLookup> codeValueLookups)
        {
            await Init();
            return await Database.InsertAllAsync(codeValueLookups);
        }
    }
}
