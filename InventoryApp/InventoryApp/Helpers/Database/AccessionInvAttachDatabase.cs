﻿using InventoryApp.Interfaces;
using InventoryApp.Models.LocalStorage;
using SQLite;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace InventoryApp.Helpers
{
    public class AccessionInvAttachDatabase : IAccessionInvAttachLocalRepository
    {
        private SQLiteAsyncConnection Database;
        private async Task Init()
        {
            if (Database != null)
                return;

            Database = new SQLiteAsyncConnection(Constants.DatabasePath, Constants.Flags);
            CreateTableResult result = await Database.CreateTableAsync<AccessionInvAttachDb>();
        }
        public Task<IEnumerable<AccessionInvAttachDb>> GetByGuidAsync(string guid)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<AccessionInvAttachDb>> GetByInventoryGuidAsync(string inventoryGuid)
        {
            await Init();
            var result = await Database.Table<AccessionInvAttachDb>()
                .Where(x => x.InventoryGuid.Equals(inventoryGuid))
                .ToListAsync();
            return result;
        }

        public async Task<int> InsertAsync(AccessionInvAttachDb accessionInvAttachDb)
        {
            await Init();
            return await Database.InsertAsync(accessionInvAttachDb);
        }

        public async Task<IEnumerable<InventoryGuidCount>> GetCountsByManyInventoryGuidAsync(string[] inventoryGuids)
        {
            await Init();
            var query = $@"SELECT {nameof(AccessionInvAttachDb.InventoryGuid)}, COUNT(1) as Count
FROM accession_inv_attach 
WHERE {nameof(AccessionInvAttachDb.InventoryGuid)} IN ('{string.Join("','", inventoryGuids)}')
GROUP BY {nameof(AccessionInvAttachDb.InventoryGuid)}";
            return await Database.QueryAsync<InventoryGuidCount>(query);
        }

        public async Task<IEnumerable<AccessionInvAttachDb>> GetAllAsync()
        {
            await Init();
            return await Database.Table<AccessionInvAttachDb>().ToArrayAsync();
        }

        public async Task<DateTime> GetLastModifiedDate()
        {
            await Init();
            return await Database.ExecuteScalarAsync<DateTime>($"SELECT MAX(coalesce(ModifiedDate, CreatedDate)) FROM accession_inv_attach");
        }

        public async Task<int> PatchInventoryIdAsync(string inventoryGuid, int newInventoryId)
        {
            await Init();
            return await Database.ExecuteAsync($"UPDATE accession_inv_attach SET InventoryId = {newInventoryId} WHERE Guid = '{inventoryGuid}'");
        }

        public async Task<IEnumerable<AccessionInvAttachDb>> GetPendingByOrderRequestIdAsync(int orderRequestId, DateTime lastSynced)
        {
            await Init();

            var sql = $@"SELECT * from accession_inv_attach aia
WHERE aia.InventoryGuid in (SELECT i.Guid FROM inventory i WHERE OrderRequestId = {orderRequestId})
AND coalesce(aia.ModifiedDate, aia.CreatedDate) > ?
";
            var result = await Database.QueryAsync<AccessionInvAttachDb>(sql, lastSynced);
            return result;
        }

        public async Task<int> PatchAccessionInvAttachIdAsync(string guid, int newAccessionInvAttatchId)
        {
            await Init();
            var sql = $"UPDATE accession_inv_attach SET {nameof(AccessionInvAttachDb.AccessionInvAttachId)} = {newAccessionInvAttatchId} WHERE Guid = '{guid}'";
            var result = await Database.ExecuteAsync(sql);
            return result;
        }
    }
}
