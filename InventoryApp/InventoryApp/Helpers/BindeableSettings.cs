﻿using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Text;

namespace InventoryApp.Helpers
{
    public class BindeableSettings : BindableBase
    {
        private static BindeableSettings m_Instance;
        public static BindeableSettings Instance
        {
            get
            {
                if (m_Instance == null)
                {
                    m_Instance = new BindeableSettings();
                }

                return m_Instance;
            }
        }
    }
}
