﻿using InventoryApp.Interfaces;
using InventoryApp.Models;
using InventoryApp.Models.Database;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace InventoryApp.Helpers
{
    public class RestService : IRestService
    {
        readonly HttpClient _httpClient;
        public List<InventoryViability> InventoryViabilityItems { get; private set; }

        private const string SearchEndPoint = "://{0}/GringlobalService/WCFService.svc/search/{1}?dataview={2}";
        private const string RestUrlC = "://{0}/GringlobalService/WCFService.svc/rest/{1}";
        private const string RestUrlRUD = "://{0}/GringlobalService/WCFService.svc/rest/{1}/{2}";
        private const string InventoryViabilityListUrl = "://{0}/GringlobalService/WCFService.svc/getdata/get_mob_inventory_viability_list";
        private const string GetDataURL = "://{0}/GringlobalService/WCFService.svc/getdata/{1}";
        private const string GetDataEndPoint = "://{0}/GringlobalService/WCFService.svc/getdata/{1}?parameters={2}";

        public RestService()
        {
            //workaround for android ssl validation bug
            var httpClientHandler = new HttpClientHandler
            {
                ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => { return true; }
            };
            _httpClient = new HttpClient(httpClientHandler);
            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.MaxResponseContentBufferSize = 256000;
            _httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", Settings.UserToken);

            //_client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //_client.DefaultRequestHeaders.AcceptEncoding.Add(new StringWithQualityHeaderValue("utf-8"));
            //_client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", string.Format("Bearer {0}", Settings.Token));

            //specify to use TLS 1.2 as default connection
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
        }

        #region InventoryViability
        public async Task<string> CreateInventoryViabilityAsync(InventoryViability item)
        {
            string result = string.Empty;

            var data = JsonConvert.SerializeObject(JsonConvert.SerializeObject(item));
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            string URL = (Settings.UseSSL ? "https" : "http") + string.Format(RestUrlC, Settings.Server, "inventory_viability");
            var response = await _httpClient.PostAsync(URL, content);

            string resultContent = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                //result = JsonConvert.DeserializeObject<List<InventoryViability>>(resultContent);
                result = resultContent;
            }
            else
            {
                throw new Exception(resultContent);
            }

            return result;
        }
        public async Task<string> UpdateInventoryViabilityAsync(InventoryViability item)
        {
            string result = string.Empty;

            var data = JsonConvert.SerializeObject(JsonConvert.SerializeObject(item));
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            string URL = (Settings.UseSSL ? "https" : "http") + string.Format(RestUrlRUD, Settings.Server, "inventory_viability", item.InventoryViabilityId);
            var response = await _httpClient.PutAsync(URL, content);

            string resultContent = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                //result = JsonConvert.DeserializeObject<List<InventoryViability>>(resultContent);
                result = resultContent;
            }
            else
            {
                throw new Exception(resultContent);
            }

            return result;
        }

        public Task DeleteInventoryViabilityAsync(string id)
        {
            throw new NotImplementedException();
        }
        public Task SaveInventoryViability(InventoryViability item, bool isNewItem)
        {
            throw new NotImplementedException();
        }
        public async Task<List<InventoryViability>> RefreshInventoryViabilityAsync()
        {
            InventoryViabilityItems = new List<InventoryViability>();

            string URL = (Settings.UseSSL ? "https" : "http") + string.Format(InventoryViabilityListUrl, Settings.Server);

            try
            {
                var response = await _httpClient.GetAsync(URL);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    InventoryViabilityItems = JsonConvert.DeserializeObject<List<InventoryViability>>(content);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(@"				ERROR {0}", ex.Message);
            }

            return InventoryViabilityItems;
        }
        #endregion

        public async Task<List<InventoryViability>> SearchInventoryViabilityAsync(string query, string dataview, string resolver)
        {

            List<InventoryViability> result = null;
            var data = JsonConvert.SerializeObject(query);

            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
            string URL = (Settings.UseSSL ? "https" : "http") + string.Format(SearchEndPoint, Settings.Server, resolver, dataview);
            var response = await _httpClient.PostAsync(URL, content);

            string resultContent = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                result = JsonConvert.DeserializeObject<List<InventoryViability>>(resultContent);
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.NoContent)
            {
                result = null;
            }
            else
            {
                throw new Exception(resultContent);
            }

            return result;
        }

        public async Task<List<InventoryThumbnail>> SearchInventoryThumbnailAsync(string query, string dataview, string resolver)
        {
            List<InventoryThumbnail> result = null;
            var data = JsonConvert.SerializeObject(query);

            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            string URL = (Settings.UseSSL ? "https" : "http") + string.Format(SearchEndPoint, Settings.Server, resolver, dataview);
            var response = await _httpClient.PostAsync(URL, content);

            string resultContent = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                result = JsonConvert.DeserializeObject<List<InventoryThumbnail>>(resultContent);
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.NoContent)
            {
                result = null;
            }
            else
            {
                throw new Exception(resultContent);
            }

            return result;
        }

        #region InventoryViabilityData
        public async Task<List<InventoryViabilityData>> SearchInventoryViabilityDataAsync(string query, string dataview, string resolver)
        {
            List<InventoryViabilityData> result = null;
            var data = JsonConvert.SerializeObject(query);

            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            string URL = (Settings.UseSSL ? "https" : "http") + string.Format(SearchEndPoint, Settings.Server, resolver, dataview);
            var response = await _httpClient.PostAsync(URL, content);

            string resultContent = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                result = JsonConvert.DeserializeObject<List<InventoryViabilityData>>(resultContent);
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.NoContent)
            {
                result = new List<InventoryViabilityData>();
            }
            else
            {
                throw new Exception(JsonConvert.DeserializeObject<string>(resultContent));
            }
            return result;
        }

        public async Task<string> CreateInventoryViabilityDataAsync(InventoryViabilityData item)
        {
            string result = string.Empty;

            var data = JsonConvert.SerializeObject(JsonConvert.SerializeObject(item));
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            string URL = (Settings.UseSSL ? "https" : "http") + string.Format(RestUrlC, Settings.Server, "inventory_viability_data");
            var response = await _httpClient.PostAsync(URL, content);

            string resultContent = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                result = resultContent;
            }
            else
            {
                throw new Exception(JsonConvert.DeserializeObject<string>(resultContent));
            }

            return result;
        }
        public async Task<string> UpdateInventoryViabilityDataAsync(InventoryViabilityData item)
        {
            string result = string.Empty;

            var data = JsonConvert.SerializeObject(JsonConvert.SerializeObject(item));
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            string URL = (Settings.UseSSL ? "https" : "http") + string.Format(RestUrlRUD, Settings.Server, "inventory_viability_data", item.InventoryViabilityDataId);
            var response = await _httpClient.PutAsync(URL, content);

            string resultContent = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                result = resultContent;
            }
            else
            {
                throw new Exception(JsonConvert.DeserializeObject<string>(resultContent));
            }

            return result;
        }

        public async Task<List<ViabilityOrderItem>> GetViabilityRequestItemList(string orderRequestId)
        {
            List<ViabilityOrderItem> result = null;

            string URL = (Settings.UseSSL ? "https" : "http") + string.Format(GetDataEndPoint, Settings.Server, "get_mob_viability_request_item_by_order_request_id", 
                System.Net.WebUtility.UrlEncode(":orderrequestid=" + orderRequestId + ";:syslangid=" + Settings.LangId));
            var response = await _httpClient.GetAsync(URL);

            string resultContent = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                result = JsonConvert.DeserializeObject<List<ViabilityOrderItem>>(resultContent);
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.NoContent)
            {
                result = new List<ViabilityOrderItem>();
            }
            else
            {
                throw new Exception(JsonConvert.DeserializeObject<string>(resultContent));
            }

            return result;
        }
        public async Task<ViabilityOrderItem> ReadViabilityRequestItem(int orderRequestItemId)
        {
            ViabilityOrderItem result = null;

            string URL = (Settings.UseSSL ? "https" : "http") + string.Format(GetDataEndPoint, Settings.Server, "get_mob_viability_request_item", 
                System.Net.WebUtility.UrlEncode(":orderrequestitemid=" + orderRequestItemId + ";:syslangid=" + Settings.LangId));
            var response = await _httpClient.GetAsync(URL);

            string resultContent = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var list = JsonConvert.DeserializeObject<List<ViabilityOrderItem>>(resultContent);
                if (list != null && list.Count > 0)
                {
                    result = list.FirstOrDefault();
                }
                else
                    throw new Exception("Order request not found");
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.NoContent)
            {
                throw new Exception("Order request not found");
            }
            else
            {
                throw new Exception(JsonConvert.DeserializeObject<string>(resultContent));
            }

            return result;
        }
        #endregion

        #region OrderRequestItem
        public async Task<string> UpdateOrderItemRequest(ViabilityOrderItem orderItem)
        {
            string result = string.Empty;

            var data = JsonConvert.SerializeObject(JsonConvert.SerializeObject(orderItem));
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            string URL = (Settings.UseSSL ? "https" : "http") + string.Format(RestUrlRUD, Settings.Server, "order_request_item", orderItem.order_request_item_id);
            var response = await _httpClient.PutAsync(URL, content) ;

            string resultContent = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                result = resultContent;
            }
            else
            {
                throw new Exception(JsonConvert.DeserializeObject<string>(resultContent));
            }

            return result;
        }
        #endregion

        #region OrderRequestItemAction
        public async Task<string> CreateOrderRequestItemActionAsync(OrderRequestItemAction orderRequestItemAction)
        {
            string result = string.Empty;

            var data = JsonConvert.SerializeObject(JsonConvert.SerializeObject(orderRequestItemAction));
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            string URL = (Settings.UseSSL ? "https" : "http") + string.Format(RestUrlC, Settings.Server, "order_request_item_action");
            var response = await _httpClient.PostAsync(URL, content);

            string resultContent = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                //result = JsonConvert.DeserializeObject<List<InventoryViability>>(resultContent);
                result = resultContent;
            }
            else
            {
                throw new Exception(resultContent);
            }

            return result;
        }
        public async Task<IEnumerable<OrderRequestItemAction>> GetOrderRequestItemActionAsync(int[] orderRequestItemIds)
        {
            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", string.Format("Bearer {0}", Settings.UserToken));

            string URL = string.Format("{0}://{1}/GringlobalService/WCFService.svc/search/{2}",
                Settings.UseSSL ? "https" : "http",
                Settings.Server,
                "order_request_item_action");
            
            string data = JsonConvert.SerializeObject($"@order_request_item.order_request_item_id in ({string.Join(",", orderRequestItemIds)})");
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync(URL, content);
            string resultContent = response.Content.ReadAsStringAsync().Result;
            
            IEnumerable<OrderRequestItemAction> result;
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                result = JsonConvert.DeserializeObject<IEnumerable<OrderRequestItemAction>>(resultContent);
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.NoContent)
            {
                result = Enumerable.Empty<OrderRequestItemAction>();
            }
            else
            {
                throw new Exception(resultContent);
            }

            return result;
        }
        public async Task<OrderRequestItemAction> GetOrderRequestItemActionByInventoryViabilityIdAsync(int inventoryViabilityId)
        {
            _httpClient.DefaultRequestHeaders.Clear();
            _ = _httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", string.Format("Bearer {0}", Settings.UserToken));

            string URL = string.Format("{0}://{1}/GringlobalService/WCFService.svc/search/{2}",
                Settings.UseSSL ? "https" : "http",
                Settings.Server,
                "order_request_item_action");

            string data = JsonConvert.SerializeObject($"@order_request_item_action.action_name_code = 'VIABTEST' and @order_request_item_action.action_information = {inventoryViabilityId}");
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync(URL, content);
            string resultContent = response.Content.ReadAsStringAsync().Result;

            OrderRequestItemAction result;
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var results = JsonConvert.DeserializeObject<IEnumerable<OrderRequestItemAction>>(resultContent);
                result = results.FirstOrDefault();
            }
            else if (response.StatusCode == HttpStatusCode.NoContent)
            {
                result = null;
            }
            else
            {
                throw new Exception(resultContent);
            }

            return result;
        }
        #endregion
        public async Task<List<InventoryViabilityRule>> SearchInventoryViabilityRuleAsync(string query, string dataview, string resolver)
        {
            List<InventoryViabilityRule> result = null;
            var data = JsonConvert.SerializeObject(query);

            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            string URL = (Settings.UseSSL ? "https" : "http") + string.Format(SearchEndPoint, Settings.Server, resolver, dataview);
            var response = await _httpClient.PostAsync(URL, content);

            string resultContent = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                result = JsonConvert.DeserializeObject<List<InventoryViabilityRule>>(resultContent);
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.NoContent)
            {
                result = null;
            }
            else
            {
                throw new Exception(resultContent);
            }
            return result;
        }

        #region OrderRequest
        public async Task<IEnumerable<OrderRequest>> GetOrderRequests(string[] orderTypeCodes)
        {
            var result = Enumerable.Empty<OrderRequest>();

            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", string.Format("Bearer {0}", Settings.UserToken));

            string URL = string.Format("{0}://{1}/GringlobalService/WCFService.svc/search/{2}",
                Settings.UseSSL ? "https" : "http",
                Settings.Server,
                "order_request");

            string data = JsonConvert.SerializeObject("@order_request.order_type_code in ('RE','GR','GR_IO','RE_VIAB','RE_MULT')");
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync(URL, content);
            string resultContent = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                result = JsonConvert.DeserializeObject<List<OrderRequest>>(resultContent);
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.NoContent)
            {
                result = Enumerable.Empty<OrderRequest>();
            }
            else
            {
                throw new Exception(resultContent);
            }

            return result;
        }
        public async Task<IEnumerable<OrderRequestItem>> GetOrderRequestItems(int orderRequestId)
        {
            var result = Enumerable.Empty<OrderRequestItem>();

            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", string.Format("Bearer {0}", Settings.UserToken));

            string URL = string.Format("{0}://{1}/GringlobalService/WCFService.svc/search/{2}",
                Settings.UseSSL ? "https" : "http",
                Settings.Server,
                "order_request_item");

            string data = JsonConvert.SerializeObject($"@order_request_item.order_request_id = {orderRequestId}");
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync(URL, content);
            string resultContent = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                result = JsonConvert.DeserializeObject<List<OrderRequestItem>>(resultContent);
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.NoContent)
            {
                result = Enumerable.Empty<OrderRequestItem>();
            }
            else
            {
                throw new Exception(resultContent);
            }

            return result;
        }

        public async Task<OrderRequest> GetOrderRequestAsync(int orderRequestId)
        {
            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", string.Format("Bearer {0}", Settings.UserToken));

            string URL = string.Format("{0}://{1}/GringlobalService/WCFService.svc/rest/{2}/{3}",
                Settings.UseSSL ? "https" : "http",
                Settings.Server,
                "order_request",
                orderRequestId);

            var response = await _httpClient.GetAsync(URL);
            string resultContent = response.Content.ReadAsStringAsync().Result;

            OrderRequest result;
            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                result = JsonConvert.DeserializeObject<OrderRequest>(resultContent);
            }
            else
            {
                throw new Exception(resultContent);
            }

            return result;
        }
        public async Task<string> CreateOrderRequestAsync(OrderRequest orderRequest)
        {
            string result = string.Empty;

            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", string.Format("Bearer {0}", Settings.UserToken));
            string URL = string.Format("{0}://{1}/GringlobalService/WCFService.svc/rest/{2}",
                Settings.UseSSL ? "https" : "http",
                Settings.Server,
                "order_request");

            string data = JsonConvert.SerializeObject(JsonConvert.SerializeObject(orderRequest));
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync(URL, content);
            string resultContent = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode == HttpStatusCode.OK)
            {
                result = JsonConvert.DeserializeObject<string>(resultContent);
            }
            else
            {
                throw new Exception(resultContent);
            }

            return result;
        }
        public async Task<string> UpdateOrderRequestAsync(OrderRequest orderRequest)
        {
            string result = string.Empty;

            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", string.Format("Bearer {0}", Settings.UserToken));
            string URL = string.Format("{0}://{1}/GringlobalService/WCFService.svc/rest/{2}/{3}",
                Settings.UseSSL ? "https" : "http",
                Settings.Server,
                "order_request",
                orderRequest.order_request_id);

            string data = JsonConvert.SerializeObject(JsonConvert.SerializeObject(orderRequest));
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            var response = await _httpClient.PutAsync(URL, content);
            string resultContent = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode == HttpStatusCode.OK)
            {
                //result = JsonConvert.DeserializeObject<string>(resultContent);
                result = resultContent;
            }
            else
            {
                throw new Exception(resultContent);
            }

            return result;
        }

        public async Task<IEnumerable<Inventory>> GetRegenerationInventoriesAsync(int orderRequestId)
        {
            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", string.Format("Bearer {0}", Settings.UserToken));

            string URL = string.Format("{0}://{1}/GringlobalService/WCFService.svc/search/{2}",
                Settings.UseSSL ? "https" : "http",
                Settings.Server,
                "inventory");

            string data = JsonConvert.SerializeObject($"@inventory.inventory_number_part1 = '{orderRequestId}'");
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync(URL, content);
            //string URL = string.Format("{0}://{1}/GringlobalService/WCFService.svc/getdata/{2}?parameters={3}",
            //    Settings.UseSSL ? "https" : "http",
            //    Settings.Server,
            //    "get_mob_regeneration_inventories_by_order_request_id",
            //    $":orderrequestid={orderRequestId}");

            //var response = await _httpClient.GetAsync(URL);
            string resultContent = response.Content.ReadAsStringAsync().Result;
            
            IEnumerable<Inventory> result;
            if (response.StatusCode == HttpStatusCode.OK)
            {
                result = JsonConvert.DeserializeObject<List<Inventory>>(resultContent);
            }
            else if (response.StatusCode == HttpStatusCode.NoContent)
            {
                result = Enumerable.Empty<Inventory>();
            }
            else
            {
                throw new Exception(resultContent);
            }

            return result;
        }
        #endregion

        #region OrderRequestAction
        public async Task<IEnumerable<OrderRequestAction>> GetOrderRequestActionAsync(int orderRequestId)
        {
            _httpClient.DefaultRequestHeaders.Clear();
            _ = _httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", string.Format("Bearer {0}", Settings.UserToken));

            string URL = string.Format("{0}://{1}/GringlobalService/WCFService.svc/search/{2}",
                Settings.UseSSL ? "https" : "http",
                Settings.Server,
                "order_request_action");

            string data = JsonConvert.SerializeObject($"@order_request_action.order_request_id = {orderRequestId}");
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync(URL, content);
            string resultContent = response.Content.ReadAsStringAsync().Result;
            
            IEnumerable<OrderRequestAction> result;
            if (response.StatusCode == HttpStatusCode.OK)
            {
                result = JsonConvert.DeserializeObject<IEnumerable<OrderRequestAction>>(resultContent);
            }
            else if (response.StatusCode == HttpStatusCode.NoContent)
            {
                result = Enumerable.Empty<OrderRequestAction>();
            }
            else
            {
                throw new Exception(resultContent);
            }

            return result;
        }

        public async Task<string> CreateOrderRequestActionAsync(OrderRequestAction orderRequestAction)
        {
            _httpClient.DefaultRequestHeaders.Clear();
            _ = _httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", string.Format("Bearer {0}", Settings.UserToken));
            string URL = string.Format("{0}://{1}/GringlobalService/WCFService.svc/rest/{2}",
                Settings.UseSSL ? "https" : "http",
                Settings.Server,
                "order_request_action");

            string data = JsonConvert.SerializeObject(JsonConvert.SerializeObject(orderRequestAction));
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync(URL, content);
            string resultContent = response.Content.ReadAsStringAsync().Result;

            string result;
            if (response.StatusCode == HttpStatusCode.OK)
            {
                result = JsonConvert.DeserializeObject<string>(resultContent);
            }
            else
            {
                throw new Exception(resultContent);
            }

            return result;
        }
        #endregion

        #region OrderRequestItem
        public async Task<string> CreateOrderRequestItemAsync(OrderRequestItem orderRequestItem)
        {
            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", string.Format("Bearer {0}", Settings.UserToken));
            string URL = string.Format("{0}://{1}/GringlobalService/WCFService.svc/rest/{2}",
                Settings.UseSSL ? "https" : "http",
                Settings.Server,
                "order_request_item");

            string data = JsonConvert.SerializeObject(JsonConvert.SerializeObject(orderRequestItem));
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync(URL, content);
            string resultContent = response.Content.ReadAsStringAsync().Result;

            string result;
            if (response.StatusCode == HttpStatusCode.OK)
            {
                result = JsonConvert.DeserializeObject<string>(resultContent);
            }
            else
            {
                throw new Exception(resultContent);
            }

            return result;
        }
        public async Task<string> UpdateOrderRequestItemAsync(OrderRequestItem orderRequestItem)
        {
            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", string.Format("Bearer {0}", Settings.UserToken));
            string URL = string.Format("{0}://{1}/GringlobalService/WCFService.svc/rest/{2}/{3}",
                Settings.UseSSL ? "https" : "http",
                Settings.Server,
                "order_request_item",
                orderRequestItem.order_request_item_id);

            string data = JsonConvert.SerializeObject(JsonConvert.SerializeObject(orderRequestItem));
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            var response = await _httpClient.PutAsync(URL, content);
            string resultContent = response.Content.ReadAsStringAsync().Result;

            string result;
            if (response.StatusCode == HttpStatusCode.OK)
            {
                //result = JsonConvert.DeserializeObject<string>(resultContent);
                result = resultContent;
            }
            else
            {
                throw new Exception(resultContent);
            }

            return result;
        }

        public async Task DeleteOrderRequestItemAsync(int orderRequestItemId)
        {
            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", string.Format("Bearer {0}", Settings.UserToken));

            string URL = string.Format("{0}://{1}/GringlobalService/WCFService.svc/rest/{2}/{3}",
                Settings.UseSSL ? "https" : "http",
                Settings.Server,
                "order_request_item",
                orderRequestItemId);

            var response = await _httpClient.DeleteAsync(URL);
            string resultContent = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new Exception(resultContent);
            }
        }
        #endregion

        #region Cooperator

        public async Task<IEnumerable<Lookup>> GetCooperatorLookupByWorkGroup(int? workgroupId)
        {
            var result = Enumerable.Empty<Lookup>();

            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", string.Format("Bearer {0}", Settings.UserToken));
            string URL = string.Format("{0}://{1}/GringlobalService/WCFService.svc/getdata/{2}?{3}",
                Settings.UseSSL ? "https" : "http",
                Settings.Server,
                "get_mob_cooperator_lookup_by_workgroup",
                "parameters=:sysgroupid=" + (workgroupId == null ? string.Empty : workgroupId.ToString()));

            var response = await _httpClient.GetAsync(URL);
            string resultContent = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode == HttpStatusCode.OK)
            {
                result = JsonConvert.DeserializeObject<IEnumerable<Lookup>>(resultContent);
            }
            else
            {
                throw new Exception(resultContent);
            }

            return result;
        }

        #endregion

        #region Inventory
        public async Task<IEnumerable<Inventory>> GetInventoriesByAccessionsAsync(int[] accessionIds)
        {
            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", string.Format("Bearer {0}", Settings.UserToken));

            string URL = string.Format("{0}://{1}/GringlobalService/WCFService.svc/search/{2}",
                Settings.UseSSL ? "https" : "http",
                Settings.Server,
                "inventory");

            string data = JsonConvert.SerializeObject($"@inventory.accession_id in ({string.Join(",", accessionIds)})");
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync(URL, content);
            string resultContent = response.Content.ReadAsStringAsync().Result;

            IEnumerable<Inventory> result;
            if (response.StatusCode == HttpStatusCode.OK)
            {
                result = JsonConvert.DeserializeObject<IEnumerable<Inventory>>(resultContent);
            }
            else if (response.StatusCode == HttpStatusCode.NoContent)
            {
                result = Enumerable.Empty<Inventory>();
            }
            else
            {
                throw new Exception(resultContent);
            }

            return result;
        }
        public async Task<Inventory> GetInventoryAsync(int inventoryId)
        {
            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", string.Format("Bearer {0}", Settings.UserToken));

            string URL = (Settings.UseSSL ? "https" : "http") + string.Format(RestUrlRUD, Settings.Server, "inventory", inventoryId);
            var response = await _httpClient.GetAsync(URL);

            string resultContent = response.Content.ReadAsStringAsync().Result;

            Inventory result;
            if (response.StatusCode == HttpStatusCode.OK)
            {
                result = JsonConvert.DeserializeObject<Inventory>(resultContent);
            }
            else
            {
                throw new Exception(JsonConvert.DeserializeObject<string>(resultContent));
            }

            return result;
        }
        public async Task<string> CreateInventoryAsync(Inventory inventory)
        {
            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", string.Format("Bearer {0}", Settings.UserToken));
            string URL = string.Format("{0}://{1}/GringlobalService/WCFService.svc/rest/{2}",
                Settings.UseSSL ? "https" : "http",
                Settings.Server,
                "inventory");

            string data = JsonConvert.SerializeObject(JsonConvert.SerializeObject(inventory));
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync(URL, content);
            string resultContent = response.Content.ReadAsStringAsync().Result;

            string result;
            if (response.StatusCode == HttpStatusCode.OK)
            {
                result = JsonConvert.DeserializeObject<string>(resultContent);
            }
            else
            {
                throw new Exception(resultContent);
            }
            return result;
        }
        public async Task<string> UpdateInventoryAsync(Inventory item)
        {
            string result = string.Empty;

            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", string.Format("Bearer {0}", Settings.UserToken));

            var data = JsonConvert.SerializeObject(JsonConvert.SerializeObject(item));
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            string URL = (Settings.UseSSL ? "https" : "http") + string.Format(RestUrlRUD, Settings.Server, "inventory", item.inventory_id);
            var response = await _httpClient.PutAsync(URL, content);

            string resultContent = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                result = resultContent;
            }
            else
            {
                throw new Exception(JsonConvert.DeserializeObject<string>(resultContent));
            }

            return result;
        }
        #endregion

        #region Method
        public async Task<IEnumerable<Lookup>> GetMethodLookupByStudyReasonCodeAsync(string studyReasonCode)
        {
            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", string.Format("Bearer {0}", Settings.UserToken));

            string URL = (Settings.UseSSL ? "https" : "http") +
                string.Format(GetDataEndPoint, Settings.Server, "get_mob_method_lookup_by_study_reason_code", ":studyreasoncode=" + studyReasonCode);
            var response = await _httpClient.GetAsync(URL);
            string resultContent = response.Content.ReadAsStringAsync().Result;
            
            List<Lookup> result;
            if (response.StatusCode == HttpStatusCode.OK)
            {
                result = JsonConvert.DeserializeObject<List<Lookup>>(resultContent);
            }
            else
            {
                throw new Exception(resultContent);
            }
            return result;
        }
        #endregion

        #region CropTraitObservation
        public async Task<string> CreateCropTraitObservationAsync(CropTraitObservationInsertDto cropTraitObservation)
        {
            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", string.Format("Bearer {0}", Settings.UserToken));
            string URL = string.Format("{0}://{1}/GringlobalService/WCFService.svc/rest/{2}",
                Settings.UseSSL ? "https" : "http",
                Settings.Server,
                "crop_trait_observation");

            string data = JsonConvert.SerializeObject(JsonConvert.SerializeObject(cropTraitObservation));
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync(URL, content);
            string resultContent = response.Content.ReadAsStringAsync().Result;

            string result;
            if (response.StatusCode == HttpStatusCode.OK)
            {
                result = JsonConvert.DeserializeObject<string>(resultContent);
            }
            else
            {
                throw new Exception(resultContent);
            }
            return result;
        }
        public async Task<string> UpdateCropTraitObservationAsync(CropTraitObservationInsertDto cropTraitObservation)
        {
            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", string.Format("Bearer {0}", Settings.UserToken));
            string URL = string.Format("{0}://{1}/GringlobalService/WCFService.svc/rest/{2}/{3}",
                Settings.UseSSL ? "https" : "http",
                Settings.Server,
                "crop_trait_observation",
                cropTraitObservation.crop_trait_observation_id);

            string data = JsonConvert.SerializeObject(JsonConvert.SerializeObject(cropTraitObservation));
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            var response = await _httpClient.PutAsync(URL, content);
            string resultContent = response.Content.ReadAsStringAsync().Result;

            string result;
            if (response.StatusCode == HttpStatusCode.OK)
            {
                result = JsonConvert.DeserializeObject<string>(resultContent);
            }
            else
            {
                throw new Exception(resultContent);
            }
            return result;
        }
        public async Task<IEnumerable<CropTraitObservation>> GetCropTraitObservationByInventoriesAndCropTraitsAsync(string inventoryIds, string cropTraitIds, int methodId = 1)
        {
            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", string.Format("Bearer {0}", Settings.UserToken));

            string URL = (Settings.UseSSL ? "https" : "http") + string.Format(GetDataEndPoint, Settings.Server,
                "get_mob_read_crop_trait_observation",
                WebUtility.UrlEncode($":inventoryid={inventoryIds};:croptraitid={cropTraitIds};:methodid={methodId};:syslangid={Settings.LangId}")
                );
            var response = await _httpClient.GetAsync(URL);

            string resultContent = response.Content.ReadAsStringAsync().Result;
            IEnumerable<CropTraitObservation> result;
            if (response.StatusCode == HttpStatusCode.OK)
            {
                result = JsonConvert.DeserializeObject<IEnumerable<CropTraitObservation>>(resultContent);
            }
            else
            {
                throw new Exception(resultContent);
            }

            return result;
        }
        public async Task<IEnumerable<CropTraitObservation>> GetCropTraitObservationByOrderRequestIdAsync(int regenerationOrderRequestId)
        {
            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", string.Format("Bearer {0}", Settings.UserToken));

            string URL = string.Format("{0}://{1}/GringlobalService/WCFService.svc/getdata/{2}?parameters={3}",
                Settings.UseSSL ? "https" : "http",
                Settings.Server,
                "get_mob_crop_trait_observation_by_order_request",
                $":orderrequestid={regenerationOrderRequestId}");

            var response = await _httpClient.GetAsync(URL);
            string resultContent = response.Content.ReadAsStringAsync().Result;

            IEnumerable<CropTraitObservation> result;
            if (response.StatusCode == HttpStatusCode.OK)
            {
                result = JsonConvert.DeserializeObject<List<CropTraitObservation>>(resultContent);
            }
            else if (response.StatusCode == HttpStatusCode.NoContent)
            {
                result = Enumerable.Empty<CropTraitObservation>();
            }
            else
            {
                throw new Exception(resultContent);
            }

            return result;
        }
        #endregion

        #region InventoryAction
        public async Task<IEnumerable<InventoryAction>> GetInventoryActionsByInventoryIdAsync(int inventoryId)
        {
            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", string.Format("Bearer {0}", Settings.UserToken));

            string URL = string.Format("{0}://{1}/GringlobalService/WCFService.svc/search/{2}",
                Settings.UseSSL ? "https" : "http",
                Settings.Server,
                "inventory_action");

            string data = JsonConvert.SerializeObject($"@inventory_action.inventory_id = {inventoryId}");
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync(URL, content);
            string resultContent = response.Content.ReadAsStringAsync().Result;

            IEnumerable<InventoryAction> result;
            if (response.StatusCode == HttpStatusCode.OK)
            {
                result = JsonConvert.DeserializeObject<IEnumerable<InventoryAction>>(resultContent);
            }
            else if (response.StatusCode == HttpStatusCode.NoContent)
            {
                result = Enumerable.Empty<InventoryAction>();
            }
            else
            {
                throw new Exception(resultContent);
            }

            return result;
        }
        public async Task<string> CreateInventoryActionAsync(InventoryAction inventoryAction)
        {
            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", string.Format("Bearer {0}", Settings.UserToken));
            string URL = string.Format("{0}://{1}/GringlobalService/WCFService.svc/rest/{2}",
                Settings.UseSSL ? "https" : "http",
                Settings.Server,
                "inventory_action");

            string data = JsonConvert.SerializeObject(JsonConvert.SerializeObject(inventoryAction));
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync(URL, content);
            string resultContent = response.Content.ReadAsStringAsync().Result;

            string result;
            if (response.StatusCode == HttpStatusCode.OK)
            {
                result = JsonConvert.DeserializeObject<string>(resultContent);
            }
            else
            {
                throw new Exception(resultContent);
            }
            return result;
        }
        public async Task<string> UpdateInventoryActionAsync(InventoryAction inventoryAction)
        {
            string result = string.Empty;

            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", string.Format("Bearer {0}", Settings.UserToken));
            string URL = string.Format("{0}://{1}/GringlobalService/WCFService.svc/rest/{2}/{3}",
                Settings.UseSSL ? "https" : "http",
                Settings.Server,
                "inventory_action",
                inventoryAction.inventory_action_id);

            string data = JsonConvert.SerializeObject(JsonConvert.SerializeObject(inventoryAction));
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            var response = await _httpClient.PutAsync(URL, content);
            string resultContent = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode == HttpStatusCode.OK)
            {
                //result = JsonConvert.DeserializeObject<string>(resultContent);
            }
            else
            {
                throw new Exception(resultContent);
            }

            return result;
        }
        public async Task<IEnumerable<InventoryAction>> GetInventoryActionsByInventoryIdAndActionCodesAsync(int inventoryId, string[] actionCodes)
        {
            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", string.Format("Bearer {0}", Settings.UserToken));

            string URL = string.Format("{0}://{1}/GringlobalService/WCFService.svc/search/{2}",
                Settings.UseSSL ? "https" : "http",
                Settings.Server,
                "inventory_action");

            string data = JsonConvert.SerializeObject($"@inventory_action.inventory_id = {inventoryId} and @inventory_action.action_name_code IN ('{string.Join("','", actionCodes)}')");
            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");

            var response = await _httpClient.PostAsync(URL, content);
            string resultContent = response.Content.ReadAsStringAsync().Result;

            IEnumerable<InventoryAction> result;
            if (response.StatusCode == HttpStatusCode.OK)
            {
                result = JsonConvert.DeserializeObject<IEnumerable<InventoryAction>>(resultContent);
            }
            else if (response.StatusCode == HttpStatusCode.NoContent)
            {
                result = Enumerable.Empty<InventoryAction>();
            }
            else
            {
                throw new Exception(resultContent);
            }

            return result;
        }
        #endregion

        #region CodeValue
        public async Task<List<CodeValueLookup>> GetCodeValueLookupList(DateTime modifiedDate)
        {
            List<CodeValueLookup> result = null;

            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", string.Format("Bearer {0}", Settings.UserToken));

            string URL = @"http://{0}/GringlobalService/WCFService.svc/getdata/{1}?parameters={2}";
            URL = string.Format(URL, Settings.Server, "mob_code_value_lookup", ":createddate;:modifieddate");
            //URL = @"http://192.168.137.1/GringlobalService/WCFService.svc/getdata/mob_code_value_lookup?parameters=:createddate;:modifieddate";
            //URL = @"http://192.168.137.1/GringlobalService/WCFService.svc/getdata/mob_code_value_by_groupname?parameters=:groupname=ACCESSION_NAME_TYPE";
            var response = await _httpClient.GetAsync(URL);

            string resultContent = response.Content.ReadAsStringAsync().Result;

            if (response.StatusCode == System.Net.HttpStatusCode.OK)
            {
                result = JsonConvert.DeserializeObject<List<CodeValueLookup>>(resultContent);
            }
            else if (response.StatusCode == System.Net.HttpStatusCode.NoContent)
            {
                result = null;
            }
            else
            {
                throw new Exception(resultContent);
            }
            return result;
        }

        public async Task<IEnumerable<CodeValueMap>> GetCodeValueLookupMapList(int langId)
        {
            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", string.Format("Bearer {0}", Settings.UserToken));

            string URL = string.Format("{0}://{1}/GringlobalService/WCFService.svc/getdata/{2}?parameters={3}",
                Settings.UseSSL ? "https" : "http",
                Settings.Server,
                "get_mob_code_value_map",
                ":syslangid=" + Settings.LangId);

            var response = await _httpClient.GetAsync(URL);
            string resultContent = response.Content.ReadAsStringAsync().Result;

            IEnumerable<CodeValueMap> result;
            if (response.StatusCode == HttpStatusCode.OK)
            {
                result = JsonConvert.DeserializeObject<IEnumerable<CodeValueMap>>(resultContent);
            }
            else
            {
                throw new Exception(resultContent);
            }

            return result;
        }
        public async Task<IEnumerable<CodeValueLookup>> GetCodeValueLookupList(string groupName)
        {
            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", string.Format("Bearer {0}", Settings.UserToken));

            string URL = string.Format("{0}://{1}/GringlobalService/WCFService.svc/getdata/{2}?parameters={3}",
                Settings.UseSSL ? "https" : "http",
                Settings.Server,
                "get_mob_code_value_by_groupname",
                $":groupname={groupName};:syslangid={Settings.LangId}");

            var response = await _httpClient.GetAsync(URL);
            string resultContent = response.Content.ReadAsStringAsync().Result;

            IEnumerable<CodeValueLookup> result;
            if (response.StatusCode == HttpStatusCode.OK)
            {
                result = JsonConvert.DeserializeObject<IEnumerable<CodeValueLookup>>(resultContent);
            }
            else
            {
                throw new Exception(resultContent);
            }

            return result;
        }

        #endregion
        public async Task<IEnumerable<string>> GetInventoryStorageLocationPart4Distinct()
        {
            //get_mob_inventory_storage_location_part4_distinct
            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", string.Format("Bearer {0}", Settings.UserToken));

            string URL = string.Format("{0}://{1}/GringlobalService/WCFService.svc/getdata/{2}?parameters={3}",
                Settings.UseSSL ? "https" : "http",
                Settings.Server,
                "get_mob_inventory_storage_location_part4_distinct",
                string.Empty);

            var response = await _httpClient.GetAsync(URL);
            string resultContent = response.Content.ReadAsStringAsync().Result;

            var result = new List<string>();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                foreach (var item in Newtonsoft.Json.Linq.JArray.Parse(resultContent))
                {
                    result.Add(item["storage_location_part4"].ToString());
                }
            }
            else
            {
                throw new Exception(resultContent);
            }

            return result;
        }

        
    }


    public interface RestApi<T, in Tkey> where T : class
    {

    }

    public class RestClass<T, Tkey> where T : class
    {
        readonly HttpClient _client;
        private const string InventoryViabilityListUrl = "http://{0}/GrinGlobalService/WCFService.svc/getdata/get_mob_inventory_viability_list";

        public RestClass(HttpClient client)
        {
            _client = client;
        }

        public T Entity { get; set; }
        /*
        public async Task<T> CreateAsync(Tkey id)
        {
            return Entity;
        }

        public async Task<T> ReadAsync(Tkey id)
        {
            return Entity;
        }

        public async Task<T> UpdateAsync(Tkey id)
        {
            return Entity;
        }

        public async Task<T> DeleteAsync(Tkey id)
        {
            return Entity;
        }
        */
    }
}
