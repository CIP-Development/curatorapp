﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using InventoryApp.Droid;
using InventoryApp.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Zebra.Sdk.Comm;
using Zebra.Sdk.Printer.Discovery;

[assembly: Dependency(typeof(ConnectionManagerImplementation))]
namespace InventoryApp.Droid
{
    public class ConnectionManagerImplementation : IConnectionManager
    {
        public string BuildBluetoothConnectionChannelsString(string macAddress)
        {
            BluetoothConnection connection = new BluetoothConnection(macAddress);
            connection.Open(); // Check connection

            try
            {
                ServiceDiscoveryHandlerImplementation serviceDiscoveryHandler = new ServiceDiscoveryHandlerImplementation();
                BluetoothDiscoverer.FindServices(Android.App.Application.Context, macAddress, serviceDiscoveryHandler);

                while (!serviceDiscoveryHandler.Finished)
                {
                    Task.Delay(100);
                }

                StringBuilder sb = new StringBuilder();
                foreach (ConnectionChannel connectionChannel in serviceDiscoveryHandler.ConnectionChannels)
                {
                    sb.AppendLine(connectionChannel.ToString());
                }
                return sb.ToString();
            }
            finally
            {
                try
                {
                    connection?.Close();
                }
                catch (ConnectionException) { }
            }
        }

        public void FindBluetoothPrinters(DiscoveryHandler discoveryHandler)
        {
            BluetoothDiscoverer.FindPrinters(Android.App.Application.Context, discoveryHandler);
        }

        public Connection GetBluetoothConnection(string macAddress)
        {
            return new BluetoothConnection(macAddress);
        }

        public StatusConnection GetBluetoothStatusConnection(string macAddress)
        {
            return new BluetoothStatusConnection(macAddress);
        }

        public MultichannelConnection GetMultichannelBluetoothConnection(string macAddress)
        {
            return new MultichannelBluetoothConnection(macAddress);
        }

        public Task<IEnumerable<DiscoveredPrinter>> GetPairedBluetoothDevices()
        {
            throw new NotImplementedException();
        }

        private class ServiceDiscoveryHandlerImplementation : ServiceDiscoveryHandler
        {

            public List<ConnectionChannel> ConnectionChannels { get; private set; }

            public bool Finished { get; private set; }

            public ServiceDiscoveryHandlerImplementation()
            {
                ConnectionChannels = new List<ConnectionChannel>();
            }

            public void DiscoveryFinished()
            {
                Finished = true;
            }

            public void FoundService(ConnectionChannel channel)
            {
                ConnectionChannels.Add(channel);
            }
        }
    }
}